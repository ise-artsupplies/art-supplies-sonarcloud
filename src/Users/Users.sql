use ArtSupplies

/* ======================
        Logins
========================*/
IF EXISTS(SELECT 1
          FROM Sys.Syslogins
          WHERE Loginname = 'Employeelogin')
    DROP LOGIN Employeelogin


IF EXISTS(SELECT 1
          FROM Sys.Syslogins
          WHERE Loginname = 'Framerlogin')
    DROP LOGIN Framerlogin

IF EXISTS(SELECT 1
          FROM Sys.Syslogins
          WHERE Loginname = 'Adminlogin')
    DROP LOGIN Adminlogin

CREATE LOGIN Employeelogin WITH PASSWORD = 'EmployeePassword', DEFAULT_DATABASE = ArtSupplies;
CREATE LOGIN Adminlogin WITH PASSWORD = 'AdminPassword', DEFAULT_DATABASE = ArtSupplies;
CREATE LOGIN Framerlogin WITH PASSWORD = 'FramerPassword', DEFAULT_DATABASE = ArtSupplies;
/* ======================
        Users
========================*/
DROP USER IF EXISTS Employee
CREATE USER Employee FOR LOGIN Employeelogin;

DROP USER IF EXISTS Framer
CREATE USER Framer FOR LOGIN Framerlogin;

DROP USER IF EXISTS Admin
CREATE USER Admin FOR LOGIN Adminlogin;


/* ======================
        Roles
========================*/
drop role if exists Employeerole
drop role if exists Adminrole
drop role if exists Framerrole

CREATE ROLE Employeerole;
CREATE ROLE Framerrole;
CREATE ROLE Adminrole;

ALTER ROLE Employeerole
    ADD MEMBER Employee;
ALTER ROLE Framerrole
    ADD MEMBER Framer;
ALTER ROLE Adminrole
    ADD MEMBER Admin;

/* ======================
        Authorization
========================*/

grant select on schema::Dbo to Employeerole;
grant select on schema::Dbo to Framerrole;
grant select, insert, update, delete, execute on schema::Dbo to Adminrole;
grant select on schema::Hist to Adminrole;

/* ---------------------
        Add product
 -----------------------*/
grant execute on object::Dbo.Sp_Createproduct to Employeerole
grant execute on object::Dbo.Sp_Createkwastproduct to Employeerole
grant execute on object::Dbo.Sp_Createsetproduct to Employeerole
grant execute on object::Dbo.Sp_Createverfproduct to Employeerole

/* ---------------------
        Delete product
 -----------------------*/
grant execute on object::Dbo.Sp_Deleteproduct to Employeerole

/* ---------------------
        Update product
 -----------------------*/
grant execute on object::Dbo.Sp_Updateproduct to Employeerole
grant execute on object::Dbo.Sp_Updatekwastproduct to Employeerole
grant execute on object::Dbo.Sp_Updatesetproduct to Employeerole
grant execute on object::Dbo.Sp_Updateverfproduct to Employeerole
grant execute on object::Dbo.Sp_Updateproducttokwastproduct to Employeerole
grant execute on object::Dbo.Sp_Updateproducttoverfproduct to Employeerole
grant execute on object::Dbo.Sp_Updateproducttosetproduct to Employeerole

/* ---------------------
   Create framingorder
 -----------------------*/
grant execute on object::Dbo.Sp_Create_FramingOrderMetPassepartout to Framerrole
grant execute on object::Dbo.Sp_Create_FramingOrder to Framerrole

 /* ---------------------
   Update framingorder
 -----------------------*/
grant execute on object::Dbo.sp_update_framingOrder to Framerrole
grant execute on object::Dbo.sp_update_framingOrderMetPassePartout to Framerrole
grant execute on object::Dbo.sp_setFramingOrderMetPassepartoutToFramingOrder to Framerrole
grant execute on object::Dbo.sp_setFramingOrderToFramingOrderMetPassepartout to Framerrole

/* ---------------------
   Delete framingorder
 -----------------------*/
grant execute on object::Dbo.Sp_Deleteframingorder to Framerrole

go