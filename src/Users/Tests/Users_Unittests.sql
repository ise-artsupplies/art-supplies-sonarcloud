USE Artsupplies
GO

--EMPLOYEE
EXEC Tsqlt.Newtestclass 'Test_Security_Employee'
GO

CREATE OR
ALTER PROCEDURE Test_Security_Employee.[Test no reading rights on table FRAMER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMER'

    EXEC Tsqlt.Expectnoexception
    EXECUTE AS USER = 'Employee'
    EXEC ('SELECT * FROM FRAMER')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Employee.[Test no insert rights on table FRAMER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMER'

    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Employee'
    EXEC ('INSERT INTO FRAMER VALUES (2, ''PIET'')')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Employee.[Test no delete rights on table FRAMER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMER'
    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Employee'
    EXEC ('DELETE FROM FRAMER WHERE FRAMER_FRAMERID = 1')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Employee.[Test no update rights on table FRAMER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMER'
    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Employee'
    EXEC ('UPDATE FRAMER SET FRAMER_NAAM = ''JAN'' WHERE FRAMER_FRAMERID = 1')
    REVERT
END
GO

create or alter procedure Test_Security_Employee.[Test if employee has rights on sp_createProduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Createproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Createkwastproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Createkwastproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Createsetproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Createsetproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Createverfproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Createverfproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Deleteproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Deleteproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Updateproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Updateproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Updatekwastproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Updatekwastproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Updatesetproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Updatesetproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Updateverfproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Updateverfproduct')
    revert

end
go
create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Updateproducttokwastproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Updateproducttokwastproduct')
    revert

end
go
create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Updateproducttoverfproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Updateproducttoverfproduct')
    revert

end
go

create or alter procedure Test_Security_Employee.[Test if employee has rights on Sp_Updateproducttosetproduct]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Employee'
    exec ('exec Sp_Updateproducttosetproduct')
    revert

end
go

--FRAMER
EXEC Tsqlt.Newtestclass 'Test_Security_Framer'
GO

CREATE OR
ALTER PROCEDURE Test_Security_Framer.[Test reading rights on table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Expectnoexception
    EXECUTE AS USER = 'Framer'
    EXEC ('SELECT * FROM Framingorder')

    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Framer.[Test no insert rights on table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMINGORDER'

    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Framer'
    EXEC ('INSERT INTO FRAMINGORDER VALUES (1, ''TEST'', 1, 1, 22.3, 15.5, 3.50, ''01-02-2015'')')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Framer.[Test no delete rights on table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMINGORDER'

    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Framer'
    EXEC ('DELETE FROM FRAMINGORDER WHERE FRAMINGORDER_ORDERNUMMER = 2')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Framer.[Test no update rights on table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMINGORDER'

    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Framer'
    EXEC ('UPDATE FRAMINGORDER SET GLAS_GLASSOORT = ''TEST2'' WHERE GLAS_GLASSOORT = ''TEST''')
    REVERT
END
GO

create or alter procedure  Test_Security_Framer.[Test if framer has rights on sp_update_framingOrder]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Framer'
    exec('exec sp_update_framingOrder')
    revert
end
go
create or alter procedure  Test_Security_Framer.[Test if framer has rights on sp_update_framingOrderMetPassePartout]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Framer'
    exec('exec sp_update_framingOrderMetPassePartout')
    revert
end
go
create or alter procedure  Test_Security_Framer.[Test if framer has rights on sp_setFramingOrderMetPassepartoutToFramingOrder]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Framer'
    exec('exec sp_setFramingOrderMetPassepartoutToFramingOrder')
    revert
end
go
create or alter procedure  Test_Security_Framer.[Test if framer has rights on sp_setFramingOrderToFramingOrderMetPassepartout]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Framer'
    exec('exec sp_setFramingOrderToFramingOrderMetPassepartout')
    revert
end
go
create or alter procedure  Test_Security_Framer.[Test if framer has rights on Sp_Deleteframingorder]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Framer'
    exec('exec Sp_Deleteframingorder')
    revert
end
go

create or alter procedure  Test_Security_Framer.[Test if framer has rights on Sp_Create_FramingOrder]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Framer'
    exec('exec Sp_Create_FramingOrder')
    revert
end
go

create or alter procedure  Test_Security_Framer.[Test if framer has rights on Sp_Create_FramingOrderMetPassepartout]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Framer'
    exec('exec Sp_Create_FramingOrderMetPassepartout')
    revert
end
go

--ADMIN
EXEC Tsqlt.Newtestclass 'Test_Security_ADMIN'
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test reading rights on hist table PRODUCT]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.PRODUCT'
    EXEC Tsqlt.Faketable 'hist.PRODUCT'

    EXEC Tsqlt.Expectnoexception
    EXECUTE AS USER = 'Admin'
    EXEC ('SELECT * FROM hist.PRODUCT')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test reading rights on table PRODUCT]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.PRODUCT'

    EXEC Tsqlt.Expectnoexception
    EXECUTE AS USER = 'Admin'
    EXEC ('SELECT * FROM PRODUCT')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test insert rights on table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMINGORDER'

    EXEC Tsqlt.Expectnoexception
    EXECUTE AS USER = 'Admin'
    EXEC ('INSERT INTO FRAMINGORDER (FRAMINGORDER_ORDERNUMMER, KUNSTWERKSOORT_SOORTNAAM, GLAS_GLASSOORT, LIJST_LIJSTNUMMER,
                          FRAMER_FRAMERID, FRAMINGORDER_BREEDTE_KUNSTWERK, FRAMINGORDER_HOOGTE_KUNSTWERK,
                          FRAMINGORDER_PRIJS, FRAMINGORDER_OPLEVERINGSDATUM)
            VALUES (1, ''TEST'', 1, 1, 1, 22.3, 15.5, 3.50, ''01-02-2015'')')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test no insert rights on hist table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'hist.FRAMINGORDER'

    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Admin'
    EXEC ('INSERT INTO hist.FRAMINGORDER VALUES (1, ''TEST'', 1, 1, 22.3, 15.5, 3.50, ''01-02-2015'')')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test update rights on table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Expectnoexception
    EXECUTE AS USER = 'Admin'
    EXEC ('UPDATE FRAMINGORDER SET GLAS_GLASSOORT = ''TEST2'' WHERE GLAS_GLASSOORT = ''TEST''')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test no update rights on hist table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMINGORDER'

    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Admin'
    EXEC ('UPDATE hist.FRAMINGORDER SET GLAS_GLASSOORT = ''TEST2'' WHERE GLAS_GLASSOORT = ''TEST''')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test delete rights on table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMINGORDER'

    EXEC Tsqlt.Expectnoexception
    EXECUTE AS USER = 'Admin'
    EXEC ('DELETE FROM FRAMINGORDER WHERE FRAMINGORDER_ORDERNUMMER = 2')
    REVERT
END
GO

CREATE OR
ALTER PROCEDURE Test_Security_Admin.[Test no delete rights on hist table FRAMINGORDER]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.FRAMINGORDER'
    EXEC Tsqlt.Faketable 'hist.FRAMINGORDER'

    EXEC Tsqlt.Expectexception
    EXECUTE AS USER = 'Admin'
    EXEC ('DELETE FROM hist.FRAMINGORDER WHERE FRAMINGORDER_ORDERNUMMER = 2')
    REVERT
END
GO

create or alter procedure  Test_Security_ADMIN.[Test if admin has rights on SpChanges_By_User]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Admin'
    exec('exec SpChanges_By_User')
    revert
end
go

create or alter procedure  Test_Security_ADMIN.[Test if admin has rights on SpChanges_In_Pigment]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Admin'
    exec('exec SpChanges_In_Pigment')
    revert
end
go


create or alter procedure  Test_Security_ADMIN.[Test if admin has rights on SpChanges_In_Price]
as
begin
    -- Assertion
    exec tSQLt.ExpectException
    @ExpectedErrorNumber = 201

    -- Execution
    execute as user = 'Admin'
    exec('exec SpChanges_In_Price')
    revert
end
go

EXEC Tsqlt.Runtestclass 'Test_Security_ADMIN'
GO
EXEC Tsqlt.Runtestclass 'Test_Security_Employee'
GO
EXEC Tsqlt.Runtestclass 'Test_Security_Framer'
GO
