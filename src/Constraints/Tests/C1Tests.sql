use ArtSupplies


EXEC Tsqlt.Newtestclass 'Test_Tr_Products_In_Set'
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if error is thrown when set is inserted into set single record]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'set', '', 3, 2.50),
           (2, 'test2', 'set', '', 3, 5.50)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectexception @Expectederrornumber = 50001,
         @Expectedmessage = 'A set cannot be part of another set'

    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 2, 3)
END;
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if error is thrown when set is inserted into set multiple records]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'set', '', 3, 2.50),
           (2, 'test2', 'set', '', 3, 5.50)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectexception @Expectederrornumber = 50001,
         @Expectedmessage = 'A set cannot be part of another set'

    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 2, 3), (2, 1, 20)
END;
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if error is thrown when set is inserted into set 1 incorrect record, 1 correct record]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'set', '', 3, 2.50),
           (2, 'test2', 'set', '', 3, 5.50),
           (3, 'test3', 'kwast', '', 2, 7.00)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectexception @Expectederrornumber = 50001,
         @Expectedmessage = 'A set cannot be part of another set'

    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (3, 2, 3), (2, 1, 20)
END;
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if error is thrown when set_product is not of type "set" single record]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'kwast', '', 3, 2.50),
           (2, 'test2', 'potlood', '', 3, 5.50)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectexception @Expectederrornumber = 50002,
         @Expectedmessage = 'A set has to be of product type set'

    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 2, 3)
END;
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if error is thrown when set_product is not of type "set" multiple records]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'kwast', '', 3, 2.50),
           (2, 'test2', 'potlood', '', 3, 5.50)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectexception @Expectederrornumber = 50002,
         @Expectedmessage = 'A set has to be of product type set'

    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 2, 3), (2, 1, 1)
END;
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if error is thrown when set_product is not of type "set" 1 correct record 1 incorrect record]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'kwast', '', 3, 2.50),
           (2, 'test2', 'potlood', '', 3, 5.50),
           (3, 'test3', 'set', '', 7, 5.00)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectexception @Expectederrornumber = 50002,
         @Expectedmessage = 'A set has to be of product type set'

    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 3, 3), (1, 2, 1)
END;
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if a correct product can be inserted into set]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'kwast', '', 3, 2.50),
           (2, 'test2', 'set', '', 3, 5.50)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectnoexception
    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 2, 3)
END;
GO

CREATE OR
ALTER PROCEDURE Test_Tr_Products_In_Set.[Test if a correct product can be inserted into set, multiple records]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Product';
    EXEC Tsqlt.Faketable 'dbo', 'Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'kwast', '', 3, 2.50),
           (2, 'test2', 'set', '', 3, 5.50),
           (3, 'test3', 'potlood', '', 6, 2.00)

    EXEC Tsqlt.Applytrigger @Tablename = 'dbo.Product_In_Set', @Triggername = 'Tr_Products_In_Set'
    EXEC Tsqlt.Expectnoexception
    INSERT
    INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 2, 3), (3, 2, 6)
END;
GO

EXEC Tsqlt.Runtestclass 'Test_Tr_Products_In_Set'