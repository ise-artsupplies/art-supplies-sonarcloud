use ArtSupplies

EXEC Tsqlt.Newtestclass 'Test_CHK_PRODUCTTYPE_PRIJSKLASSE'
GO

CREATE OR
ALTER PROCEDURE Test_CHK_PRODUCTTYPE_PRIJSKLASSE.[Test if correct record without a product type is inserted]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product';
    EXEC Tsqlt.ApplyConstraint 'dbo.Product','CHK_PRODUCTTYPE_PRIJSKLASSE'
    EXEC Tsqlt.Expectnoexception
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad, Product_Verkoopprijs)
    VALUES('1001','Superezels','Ezel','Een ezel best wel voor de hand liggend lijkt mij',12,13)

END;
GO

CREATE OR
ALTER PROCEDURE Test_CHK_PRODUCTTYPE_PRIJSKLASSE.[Test if record of product type "kwast" without a price is inserted]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product';
    EXEC Tsqlt.ApplyConstraint 'dbo.Product','CHK_PRODUCTTYPE_PRIJSKLASSE'
    EXEC Tsqlt.Expectnoexception
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad, Product_Verkoopprijs)
    VALUES('1001','Verf en zo','Verf','verf lijkt me wel duidelijk',12,NULL)

END;
GO

CREATE OR
ALTER PROCEDURE Test_CHK_PRODUCTTYPE_PRIJSKLASSE.[Test if error is thrown when record of product type "kwast" with a price is inserted]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product';
    EXEC Tsqlt.ApplyConstraint 'dbo.Product','CHK_PRODUCTTYPE_PRIJSKLASSE'
    EXEC Tsqlt.Expectexception
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad, Product_Verkoopprijs)
    VALUES('1001','Verf en zo','Verf','verf lijkt me wel duidelijk',12,15)
END;
GO
EXEC Tsqlt.Runtestclass 'Test_CHK_PRODUCTTYPE_PRIJSKLASSE'