USE Artsupplies

-- Domain contraints

-- Table KwastProduct_Techniek
ALTER TABLE Kwastproduct_Techniek
    DROP CONSTRAINT IF EXISTS Check_Productnr;

ALTER TABLE Kwastproduct_Techniek
    ADD CONSTRAINT Check_Productnr
        CHECK (Product_Productnr > 0 );

-- Table KwastProduct
ALTER TABLE Kwastproduct
    DROP CONSTRAINT IF EXISTS Check_Productnummer;

ALTER TABLE Kwastproduct
    ADD CONSTRAINT Check_Productnummer
        CHECK (Product_Productnr > 0 );

ALTER TABLE Kwastproduct
    DROP CONSTRAINT IF EXISTS Check_Prijsklasse;

ALTER TABLE Kwastproduct
    ADD CONSTRAINT Check_Prijsklasse
        CHECK (Prijsgroep_Prijsklasse > 0 );

ALTER TABLE Kwastproduct
    DROP CONSTRAINT IF EXISTS Check_Maat;

ALTER TABLE Kwastproduct
    ADD CONSTRAINT Check_Maat
        CHECK (Kwast_Maat > 0 );

-- Table Product
ALTER TABLE Product
    DROP CONSTRAINT IF EXISTS Check_Product_Productnr;

ALTER TABLE Product
    ADD CONSTRAINT Check_Product_Productnr
        CHECK (Product_Productnr > 0 );

ALTER TABLE Product
    DROP CONSTRAINT IF EXISTS Check_Aantal_Voorraad;

ALTER TABLE Product
    ADD CONSTRAINT Check_Aantal_Voorraad
        CHECK (Product_Voorraad >= 0 );

ALTER TABLE Product
    DROP CONSTRAINT IF EXISTS Check_Prijs_Verkoopprijs;

ALTER TABLE Product
    ADD CONSTRAINT Check_Prijs_Verkoopprijs
        CHECK (Product_Verkoopprijs >= 0 );

-- Table SetProduct
ALTER TABLE Setproduct
    DROP CONSTRAINT IF EXISTS Check_Setproduct_Productnr;

ALTER TABLE Setproduct
    ADD CONSTRAINT Check_Setproduct_Productnr
        CHECK (Product_Productnr > 0 );

-- Table Product_In_Set
ALTER TABLE Product_In_Set
    DROP CONSTRAINT IF EXISTS Check_Product_In_Set_Productnr;

ALTER TABLE Product_In_Set
    ADD CONSTRAINT Check_Product_In_Set_Productnr
        CHECK (Product_Productnr > 0 );

ALTER TABLE Product_In_Set
    DROP CONSTRAINT IF EXISTS Check_Set_Product_Productnr;

ALTER TABLE Product_In_Set
    ADD CONSTRAINT Check_Set_Product_Productnr
        CHECK (Product_Productnr > 0 );

-- Table Prijsgroep
ALTER TABLE Prijsgroep
    DROP CONSTRAINT IF EXISTS Check_Prijsgroep_Prijsklasse;

ALTER TABLE Prijsgroep
    ADD CONSTRAINT Check_Prijsgroep_Prijsklasse
        CHECK (Prijsgroep_Prijsklasse > 0 );

ALTER TABLE Prijsgroep
    DROP CONSTRAINT IF EXISTS Check_Prijsgroep_Prijs;

ALTER TABLE Prijsgroep
    ADD CONSTRAINT Check_Prijsgroep_Prijs
        CHECK (Prijsgroep_Prijs >= 0 );

-- Table Verfproduct
ALTER TABLE Verfproduct
    DROP CONSTRAINT IF EXISTS Check_Verfproduct_Productnr;

ALTER TABLE Verfproduct
    ADD CONSTRAINT Check_Verfproduct_Productnr
        CHECK (Product_Productnr > 0 );

ALTER TABLE Verfproduct
    DROP CONSTRAINT IF EXISTS Check_Verfproduct_Prijsgroep_Prijsklasse;

ALTER TABLE Verfproduct
    ADD CONSTRAINT Check_Verfproduct_Prijsgroep_Prijsklasse
        CHECK (Prijsgroep_Prijsklasse >= 0 );

-- Table Kleur
ALTER TABLE Kleur
    DROP CONSTRAINT IF EXISTS Check_Kleur_Pgwaarde;

ALTER TABLE Kleur
    ADD CONSTRAINT Check_Kleur_Pgwaarde
        CHECK (Kleur_Pgwaarde > 0 );

-- Table VerfProduct_Waarschuwing
ALTER TABLE Verfproduct_Waarschuwing
    DROP CONSTRAINT IF EXISTS Check_Verfproduct_Waarschuwing_Productnr;

ALTER TABLE Verfproduct_Waarschuwing
    ADD CONSTRAINT Check_Verfproduct_Waarschuwing_Productnr
        CHECK (Product_Productnr > 0 );

-- Table LijstType
ALTER TABLE Lijsttype
    DROP CONSTRAINT IF EXISTS Check_Lijsttype_Typenummer;

ALTER TABLE Lijsttype
    ADD CONSTRAINT Check_Lijsttype_Typenummer
        CHECK (Lijsttype_Typenummer > 0 );

-- Table Lijst
ALTER TABLE Lijst
    DROP CONSTRAINT IF EXISTS Check_Lijst_Lijstnummer;

ALTER TABLE Lijst
    ADD CONSTRAINT Check_Lijst_Lijstnummer
        CHECK (Lijst_Lijstnummer > 0 );

ALTER TABLE Lijst
    DROP CONSTRAINT IF EXISTS Check_Lijst_Typenummer;

ALTER TABLE Lijst
    ADD CONSTRAINT Check_Lijst_Typenummer
        CHECK (Lijsttype_Typenummer > 0 );

ALTER TABLE Lijst
    DROP CONSTRAINT IF EXISTS Check_Lijst_Prijs;

ALTER TABLE Lijst
    ADD CONSTRAINT Check_Lijst_Prijs
        CHECK (Lijst_Prijs >= 0 );

-- Table FramingOrder
ALTER TABLE Framingorder
    DROP CONSTRAINT IF EXISTS Check_Framingorder_Ordernummer;

ALTER TABLE Framingorder
    ADD CONSTRAINT Check_Framingorder_Ordernummer
        CHECK (Framingorder_Ordernummer > 0 );

ALTER TABLE Framingorder
    DROP CONSTRAINT IF EXISTS Check_Framingorder_Lijstnummer;

ALTER TABLE Framingorder
    ADD CONSTRAINT Check_Framingorder_Lijstnummer
        CHECK (Lijst_Lijstnummer > 0 );

ALTER TABLE Framingorder
    DROP CONSTRAINT IF EXISTS Check_Framingorder_Framerid;

ALTER TABLE Framingorder
    ADD CONSTRAINT Check_Framingorder_Framerid
        CHECK (Framer_Framerid > 0 );

ALTER TABLE Framingorder
    DROP CONSTRAINT IF EXISTS Check_Framingorder_Breedte_Lijstafmeting;

ALTER TABLE Framingorder
    ADD CONSTRAINT Check_Framingorder_Breedte_Lijstafmeting
        CHECK (Framingorder_Breedte_Kunstwerk > 0 );

ALTER TABLE Framingorder
    DROP CONSTRAINT IF EXISTS Check_Framingorder_Hoogte_Lijstafmeting;

ALTER TABLE Framingorder
    ADD CONSTRAINT Check_Framingorder_Hoogte_Lijstafmeting
        CHECK (Framingorder_Hoogte_Kunstwerk > 0 );

ALTER TABLE Framingorder
    DROP CONSTRAINT IF EXISTS Check_Framingorder_Prijs;

ALTER TABLE Framingorder
    ADD CONSTRAINT Check_Framingorder_Prijs
        CHECK (Framingorder_Prijs >= 0 );

ALTER TABLE Framingorder
    ADD CONSTRAINT Check_Aanvraagdatum
        CHECK (Framingorder_Opleveringsdatum > Framingorder_Aanvraagdatum );

-- Table Framer
ALTER TABLE Framer
    DROP CONSTRAINT IF EXISTS Check_Framer_Framerid;

ALTER TABLE Framer
    ADD CONSTRAINT Check_Framer_Framerid
        CHECK (Framer_Framerid > 0 );

--Table PassePartout
ALTER TABLE Passepartout
    DROP CONSTRAINT IF EXISTS Check_Passepartout_Nummer;

ALTER TABLE Passepartout
    ADD CONSTRAINT Check_Passepartout_Nummer
        CHECK (Passepartout_Passepartoutnummer > 0 );

ALTER TABLE Passepartout
    DROP CONSTRAINT IF EXISTS Check_Passepartout_Prijs;

ALTER TABLE Passepartout
    ADD CONSTRAINT Check_Passepartout_Prijs
        CHECK (Passepartout_Prijs >= 0 );

-- Table FramingOrderMetPassePartout
ALTER TABLE Framingordermetpassepartout
    DROP CONSTRAINT IF EXISTS Check_Framingordermetpassepartout_Framingordernummer;

ALTER TABLE Framingordermetpassepartout
    ADD CONSTRAINT Check_Framingordermetpassepartout_Framingordernummer
        CHECK (Framingorder_Ordernummer > 0 );

ALTER TABLE Framingordermetpassepartout
    DROP CONSTRAINT IF EXISTS Check_Framingordermetpassepartout_Passepartoutnummer;

ALTER TABLE Framingordermetpassepartout
    ADD CONSTRAINT Check_Framingordermetpassepartout_Passepartoutnummer
        CHECK (Passepartout_Passepartoutnummer > 0 );
GO

-- Product in set can't be of type set
CREATE
    OR
    ALTER
    TRIGGER Tr_Products_In_Set
    ON Product_In_Set
    AFTER
        INSERT
    AS
BEGIN
    IF (@@ROWCOUNT = 0) RETURN
    SET NOCOUNT ON
    BEGIN TRY
        --CHECK If a SET Is Part OF another
        IF EXISTS(SELECT 1
                  FROM Inserted I
                           INNER JOIN Product P ON I.Product_Productnr = P.Product_Productnr
                  WHERE P.Product_Type = 'set')
            BEGIN
                ;THROW 50001, 'A set cannot be part of another set', 1
            END

        --Check if a set is not of product type SET
        IF EXISTS(SELECT 1
                  FROM Inserted I
                           INNER JOIN Product P ON I.Set_Product_Productnr = P.Product_Productnr
                  WHERE P.Product_Type != 'set')
            BEGIN
                ;THROW 50002, 'A set has to be of product type set', 1
            END
    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
END
GO

-- Product of type "verf" or "kwast" has to have a prijsklasse and a no prijs
ALTER TABLE Product
    ADD CONSTRAINT Chk_Producttype_Prijsklasse
        CHECK
            (Product_Type != 'Verf' AND Product_Type != 'Kwast' OR
             (Product_Type = 'Verf' AND Product_Verkoopprijs IS NULL) OR
             (Product_Type = 'Kwast' AND Product_Verkoopprijs IS NULL))
GO

CREATE
    OR ALTER
    TRIGGER Tr_Product_Must_Be_Of_Type_Kwast
    ON Kwastproduct
    AFTER INSERT, UPDATE
    AS
BEGIN
    IF (@@ROWCOUNT = 0) RETURN
    SET NOCOUNT ON
    BEGIN TRY
        IF EXISTS(SELECT 1
                  FROM Inserted I
                           INNER JOIN Product P ON I.Product_Productnr = P.Product_Productnr
                  WHERE P.Product_Type != 'KWAST')
            BEGIN
                ;THROW 50004, 'Product must be of type Kwast', 1
            END
    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
END
GO

CREATE
    OR ALTER TRIGGER Tr_Product_Must_Be_Of_Type_Verf
    ON Verfproduct
    AFTER INSERT, UPDATE
    AS
BEGIN
    IF (@@ROWCOUNT = 0) RETURN
    SET NOCOUNT ON
    BEGIN TRY
        IF EXISTS(SELECT 1
                  FROM Inserted I
                           INNER JOIN Product P ON I.Product_Productnr = P.Product_Productnr
                  WHERE P.Product_Type != 'VERF')
            BEGIN
                ;THROW 50005, 'Product must be of type Verf', 1
            END
    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
END
GO