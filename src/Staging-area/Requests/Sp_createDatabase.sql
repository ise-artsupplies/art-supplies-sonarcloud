/*
 Author: Rens Harinck
 Date: 29-4-2021
 Description: Creates the database for the staging area.
 */
use ArtSupplies
go

create or
alter proc sp_createDatabase
as
begin
    declare @url nvarchar(255)
    declare @object as int;
    declare @etag as nvarchar(4000);
    declare @authorization varchar(255)

    exec sp_GetDbUrl @url = @url out

    exec sp_getETagIfExists @path = '', @etag = @etag out

    -- Als etag bestaat, bestaat de database. Daarom gaan we hem verwijderen
    if (@etag is not null)
        begin
            exec sp_deleteResourceByETag @path = '', @etag = @etag
        end

    exec sp_getAuthorizationValue @combined = @authorization out

    exec sp_oacreate 'msxml2.xmlhttp', @object out;
    exec sp_oamethod @object, 'open', null, 'put',
         @url,
         'false'

    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', @authorization
    exec sp_oamethod @object, 'send'
end
go