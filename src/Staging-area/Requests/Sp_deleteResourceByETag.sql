/*
 Author: Rens Harinck
 Date: 29-4-2021
 Description: Deletes recourse by ETag.
 */
use ArtSupplies
GO

create or
alter proc sp_deleteResourceByETag @path nvarchar(255),
                                   @etag nvarchar(4000)
as
begin
    declare @object as int;
    declare @url nvarchar(255)
    declare @authorization varchar(255)


    exec sp_GetDbUrl @url = @url out
    set @url = @url + @path

    exec sp_getAuthorizationValue @combined = @authorization out

    exec sp_oacreate 'msxml2.xmlhttp', @object out;
    exec sp_oamethod @object, 'open', null, 'delete',@url,'false'
    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', @authorization
    exec sp_OAMethod @object, 'setrequestheader', null, 'If-Match', @etag
    exec sp_oamethod @object, 'send'
end
go
