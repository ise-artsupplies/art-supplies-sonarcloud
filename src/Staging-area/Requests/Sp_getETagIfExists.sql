/*
 Author: Rens Harinck
 Date: 29-4-2021
 Description: Returns the etag of the specified path, if it exists. The url used is the default url specified in spGetDbUrl.sql + the path specified with the @path variable.
 The @etag variable is overwritten on output.
 */
use ArtSupplies
go

create or
alter proc sp_getETagIfExists @path nvarchar(255),
                             @etag nvarchar(4000) output
as
begin
    declare @object int;
    declare @status int;
    declare @url nvarchar(255)
    declare @authorization varchar(255)

    exec sp_GetDbUrl @url = @url out
    set @url = @url + @path

    exec sp_getAuthorizationValue @combined = @authorization out

    exec sp_oacreate 'msxml2.xmlhttp', @object out;
    exec sp_oamethod @object, 'open', null, 'get',@url,'false'
    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', @authorization
    exec sp_oamethod @object, 'send'
    exec sp_OAMethod @object, 'getResponseHeader', @etag out, 'ETag'
    exec sp_OAMethod @object, 'status', @status out

    if (@status is null)
    begin
        ;throw 50011, 'Connection failed with Restheart' , 1
    end
end
go