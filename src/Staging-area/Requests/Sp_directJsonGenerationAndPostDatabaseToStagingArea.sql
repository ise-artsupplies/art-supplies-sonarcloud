/*
 Author: Rens Harinck
 Date: 30-4-2021
 Description: Generates json of all products in the database, and fills the mongodb database. The given json variable is overwritten, and serves as the output.
 Last date modified: 18-5-2021
 */

use ArtSupplies
go

create or
alter proc Sp_directJsonGenerationAndPostDatabaseToStagingArea
as
begin

    exec sp_createDatabase

    declare @#Products int
    declare @nProduct int = 1
    declare @productNr int
    declare @jsonArray varchar(max) = '['
    declare @jsonObject varchar(max)
    declare @maxLength int = 8000
    declare @url nvarchar(255)
    declare @object as int
    declare @authorization varchar(255)

    exec sp_getDbUrl @url = @url out
    exec sp_getAuthorizationValue @combined = @authorization out

    select @#Products = count(*)
    from PRODUCT

    while (@nProduct <= @#Products)
        begin
            if (@jsonArray != '[')
                set @jsonArray = @jsonArray + ','

            select @productNr = PRODUCT_PRODUCTNR
            from (
                     select PRODUCT_PRODUCTNR, row_number() over (order by PRODUCT_PRODUCTNR asc) as row
                     from PRODUCT
                 ) p
            where row = @nProduct

            if (exists(select 1 from VERFPRODUCT where PRODUCT_PRODUCTNR = @productNr))
                begin
                    -- product is of type VERFPRODUCT
                    exec sp_generateVerfproductJsonByProductnr @productNr, @jsonObject out
                end
            else
                begin
                    if (exists(select 1 from KWASTPRODUCT where PRODUCT_PRODUCTNR = @productNr))
                        begin
                            -- product is of type KWASTPRODUCT
                            exec sp_generateKwastJsonByProductnr @productNr, @jsonObject out
                        end
                    else
                        begin
                            if (exists(select 1 from SETPRODUCT where PRODUCT_PRODUCTNR = @productNr))
                                begin
                                    -- product is of type SETPRODUCT
                                    exec sp_generateSetproductJsonByProductnr @productNr, @jsonObject out
                                end
                            else
                                begin
                                    -- product is of default PRODUCT type
                                    exec sp_generateProductJsonByProductnr @productNr, @jsonObject out
                                end
                        end
                end

            if ((len(@jsonArray) + len(@jsonObject) >= @maxLength - 1))
                begin
                    set @jsonArray = @jsonArray + ']'
                    exec sp_oacreate 'msxml2.xmlhttp', @object out;
                    exec sp_oamethod @object, 'open', null, 'post',
                         @url,
                         'false'

                    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', @authorization
                    exec sp_oamethod @object, 'send', null, @jsonArray
                    set @jsonArray = '['
                end

            set @jsonObject = right(@jsonObject, len(@jsonObject) - 1)
            set @jsonObject = left(@jsonObject, len(@jsonObject) - 1)
            set @jsonArray = @jsonArray + @jsonObject

            set @nProduct = @nProduct + 1
        end
    if (@jsonArray <> '[')
        begin
            set @jsonArray = @jsonArray + ']'
            exec sp_oacreate 'msxml2.xmlhttp', @object out;
            exec sp_oamethod @object, 'open', null, 'post',
                 @url,
                 'false'

            exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', @authorization
            exec sp_oamethod @object, 'send', null, @jsonArray
        end
end
go