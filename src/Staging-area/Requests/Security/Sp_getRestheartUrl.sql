/*
 Author: Rens Harinck
 Date: 29-4-2021
 Description: Returns the url of restheart.
 */
use ArtSupplies
go

create or
alter proc sp_GetRestheartUrl @url nvarchar(255) out
as
begin
    set @url = 'http://localhost:8080'
end