/*
 Author: Rens Harinck
 Date: 29-4-2021
 Description: Returns the base-64 encoding of a Authorization header with the username and password in the proc.  CUSTOM USER DOESN'T EXISTS YET!
 */
use ArtSupplies
go

create or
alter proc sp_getAuthorizationValue @combined varchar(255) out
as
begin
    -- nvarchar can't be used with the xml function
    declare @username varchar(255) = 'admin'
    declare @password varchar(255) = 'secret'
    set @combined = @username + ':' + @password

    exec sp_EncodeBase64 @combined out

    set @combined = 'Basic ' + @combined
end
go

-- Tests
exec tsqlt.DropClass 'sp_getAuthorizationValueTests';

exec tsqlt.newtestclass 'sp_getAuthorizationValueTests';
go

create or
alter procedure sp_getAuthorizationValueTests.[Test if sp_EncodeBase64 gets executed]
as
begin
    -- Setup
    declare @inputVar varchar(255)
    declare @sp_Called bit
    exec tSQLt.SpyProcedure 'dbo.sp_EncodeBase64'

    -- Execution
    exec dbo.sp_getAuthorizationValue @inputVar out

    if (exists(select 1 from dbo.sp_EncodeBase64_SpyProcedureLog))
        begin
            set @sp_Called = 1
        end
    -- Assertion
    exec tSQLt.AssertEquals 1, @sp_Called
end
go

exec tSQLt.RunTestClass @TestClassName = 'sp_getAuthorizationValueTests'