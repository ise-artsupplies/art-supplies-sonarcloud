/*
 Author: Rens Harinck
 Date: 29-4-2021
 Description: Encodes string to base64
 Last date modified: 19-5-2021
 */
use ArtSupplies
go

set ansi_nulls, quoted_identifier on;
go

create or
alter proc sp_EncodeBase64 @input varchar(255) out
as
begin
    -- nvarchar can't be used with the xml function
    select @input = base64encoding
    from (select cast(N'' as xml).value(
                         'xs:base64Binary(sql:column("bin"))'
                     , 'VARCHAR(MAX)'
                     ) Base64Encoding
          from (
                   select cast(@input as varbinary(max)) as bin
               ) as bin_sql_server_temp) a
end
go

-- Tests
exec tsqlt.DropClass 'sp_EncodeBase64Tests'

exec tsqlt.newtestclass 'sp_EncodeBase64Tests';
go

create or alter procedure sp_EncodeBase64Tests.[Test if proc encodes in base64]
as
begin
    -- Setup
    declare @inputVar varchar(255) = 'admin:secret'
    declare @expectedEncodedInputVar varchar(255) = 'YWRtaW46c2VjcmV0'

    -- Execution
    exec sp_EncodeBase64 @input = @inputVar out

    -- Assertion
    exec tSQLt.AssertEquals @expectedEncodedInputVar, @inputVar

end
go

exec tSQLt.RunTestClass @TestClassName = 'sp_EncodeBase64Tests'