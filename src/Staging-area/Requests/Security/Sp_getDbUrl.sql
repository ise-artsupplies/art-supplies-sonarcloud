/*
 Author: Rens Harinck
 Date: 29-4-2021
 Description: Returns the url of the database.
 */
use ArtSupplies
go

create or
alter proc sp_GetDbUrl @url nvarchar(255) out
as
begin
    exec sp_GetRestheartUrl @url out
    set @url = @url + '/products'
end