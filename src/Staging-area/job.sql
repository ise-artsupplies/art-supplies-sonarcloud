/*
 Author: Rens Harinck
 Date: 21-05-2021
 Description: Sets up a job and attaches a schedule.
 Disclaimer: SQL SERVER AGENT must be running before running this script.
 */
use msdb
go

if (exists(
        select 1
        from msdb.dbo.sysjobs
        where (name = N'Send ArtSupplies database to staging area')
    )
    )
    begin
        exec msdb.dbo.sp_delete_job
             @job_name = N'Send ArtSupplies database to staging area'
    end

exec sp_add_job
     @job_name = N'Send ArtSupplies database to staging area'

exec sp_add_jobstep
     @job_name = N'Send ArtSupplies database to staging area',
     @step_name = N'Call procedure that handles it',
     @subsystem = N'TSQL',
     @command = N'exec dbo.Sp_directJsonGenerationAndPostDatabaseToStagingArea',
     @database_name = 'ArtSupplies',
     @retry_attempts = 0

if (exists(
        select 1
        from msdb.dbo.sysschedules
        where (name = N'ArtSuppliesRunDaily')
    )
    )
    begin
        exec sp_delete_schedule
             @schedule_name = N'ArtSuppliesRunDaily'
    end

exec sp_add_schedule
     @schedule_name = N'ArtSuppliesRunDaily',
     @freq_type = 4,
     @freq_interval = 1,
     @active_start_time = 060000

exec sp_attach_schedule
     @job_name = N'Send ArtSupplies database to staging area',
     @schedule_name = N'ArtSuppliesRunDaily'

declare @job_id uniqueidentifier
select @job_id = a.job_id
from msdb.dbo.sysjobservers a
         inner join msdb.dbo.sysjobs b on a.job_id = b.job_id
where (b.name = N'Send ArtSupplies database to staging area')
if (@job_id is not null)
    begin
        exec sp_delete_jobserver
             @job_id = @job_id,
             @server_name = @@servername
    end

exec sp_add_jobserver
     @job_name = N'Send ArtSupplies database to staging area'