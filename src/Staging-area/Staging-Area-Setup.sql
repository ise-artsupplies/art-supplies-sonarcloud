/*
 Author: Rens Harinck
 Date: 28-4-2021
 Description: Sets up the database for the staging area
 Disclaimer: Test on database objects that use Ole Automation Procedures can't be developed. The tSQLt test framework is unable to access these methods.
 Last date altered: 21-5-2021
 */
use ArtSupplies
go

-- Configure MSSQL for sending http-Requests
exec sp_configure 'show advanced options', 1;
reconfigure
go
sp_configure 'Ole Automation Procedures', 1;
reconfigure
go

exec sp_createDatabase
exec Sp_directJsonGenerationAndPostDatabaseToStagingArea


