/*
 Author: Rens Harinck
 Date: 18-5-2021
 Description: Generates json for a setproduct by productnr.
 */
use ArtSupplies
go

create or
alter proc Sp_generateSetproductJsonByProductnr @productnr int, @jsonObject varchar(max) out
as
begin
    declare @productsInSetReplaceble varchar(20) = 'REPLACEBLE_PRODUCTSINSET'
    declare @jsonProductsInSetArray varchar(max)
    declare @nRecord int = 1
    declare @#Records int
    declare @jsonSingularProduct varchar(max)
    declare @currProductnr int

    select @#Records = count(*)
    from PRODUCT_IN_SET pis
    where pis.SET_PRODUCT_PRODUCTNR = @productnr

    set @jsonProductsInSetArray = '['

    while (@nRecord <= @#Records)
        begin
            if (@nRecord <> 1)
                set @jsonProductsInSetArray = @jsonProductsInSetArray + ','

            select @currProductnr = t.PRODUCT_PRODUCTNR
            from (select pis.PRODUCT_PRODUCTNR,
                         row_number() over (order by pis.PRODUCT_PRODUCTNR) as row
                  from PRODUCT_IN_SET pis
                  where pis.SET_PRODUCT_PRODUCTNR = @productnr) t
            where row = @nRecord
            select @jsonProductsInSetArray = @jsonProductsInSetArray +
                                             (select t.PRODUCT_PRODUCTNR,
                                                     t.PRODUCT_IN_SET_AANTAL
                                              from (select pis.PRODUCT_PRODUCTNR,
                                                           pis.PRODUCT_IN_SET_AANTAL,
                                                           row_number() over (order by pis.PRODUCT_PRODUCTNR) as row
                                                    from PRODUCT_IN_SET pis
                                                    where pis.SET_PRODUCT_PRODUCTNR = @productnr) t
                                              where row = @nRecord
                                              for json path)

            if (exists(select 1 from VERFPRODUCT where PRODUCT_PRODUCTNR = @currProductnr))
                begin
                    -- product is of type VERFPRODUCT
                    exec sp_generateVerfproductJsonByProductnr @currProductnr, @jsonSingularProduct out
                end
            else
                begin
                    if (exists(select 1 from KWASTPRODUCT where PRODUCT_PRODUCTNR = @currProductnr))
                        begin
                            -- product is of type KWASTPRODUCT
                            exec sp_generateKwastJsonByProductnr @currProductnr, @jsonSingularProduct out
                        end
                    else
                        begin
                            -- product is of default PRODUCT type
                            exec sp_generateProductJsonByProductnr @currProductnr, @jsonSingularProduct out
                        end
                end
            set @jsonSingularProduct =
                    replace(@jsonSingularProduct, '"_id":' + cast(@currProductnr as varchar) + ',', '')
            set @jsonProductsInSetArray =
                    replace(@jsonProductsInSetArray, '"PRODUCT_PRODUCTNR":' + cast(@currProductnr as varchar),
                            '"PRODUCT_' + cast(@nRecord as varchar) + '":' + @jsonSingularProduct)
            set @nRecord = @nRecord + 1
        end
    set @jsonProductsInSetArray = @jsonProductsInSetArray + ']'

    select @jsonObject = (select p.PRODUCT_PRODUCTNR                                                   as '_id',
                                 p.PRODUCT_PRODUCTNR                                                   as 'PRODUCT_PRODUCTNR',
                                 p.MERKNAAM,
                                 p.PRODUCT_TYPE,
                                 p.PRODUCT_PRODUCTOMSCHRIJVING,
                                 p.PRODUCT_VOORRAAD,
                                 p.PRODUCT_VERKOOPPRIJS,
                                 s.SETPRODUCT_VERPAKKING,
                                 iif(@jsonProductsInSetArray = ('[]'), null, @productsInSetReplaceble) as 'PRODUCTS_IN_SET'
                          from PRODUCT p
                                   inner join SETPRODUCT s on p.PRODUCT_PRODUCTNR = s.PRODUCT_PRODUCTNR
                          where p.PRODUCT_PRODUCTNR = @productnr
                          for json path)

    set @jsonObject = replace(@jsonObject, '"' + @productsInSetReplaceble + '"', @jsonProductsInSetArray)
end
go

exec tsqlt.DropClass 'Sp_generateSetProductJsonByProductnrTests';
go

exec tsqlt.newtestclass 'Sp_generateSetProductJsonByProductnrTests';
go

create or
alter procedure Sp_generateSetProductJsonByProductnrTests.[Test if generated json no products in set is valid]
as
begin
    -- Setup
    declare @actual varchar(4000)
    declare @expected varchar(4000) = '[{"_id":1,"PRODUCT_PRODUCTNR":1,"MERKNAAM":"Avaveo","PRODUCT_TYPE":"SET","PRODUCT_PRODUCTOMSCHRIJVING":"Curabitur at ipsum ac tellus semper interdum.","PRODUCT_VOORRAAD":2,"PRODUCT_VERKOOPPRIJS":130.0000,"SETPRODUCT_VERPAKKING":"Metalen doos voorzien van scharnierende deksels om als mengpaletten te gebruiken"}]'

    exec tSQLt.FakeTable 'dbo.PRODUCT_IN_SET'
    exec tSQLt.FakeTable 'dbo.SETPRODUCT'
    exec tSQLt.FakeTable 'dbo.PRODUCT'

    insert into dbo.PRODUCT values (1, 'Avaveo', 'SET', 'Curabitur at ipsum ac tellus semper interdum.', 2, 130.0000)
    insert into dbo.SETPRODUCT
    values (1, 'Metalen doos voorzien van scharnierende deksels om als mengpaletten te gebruiken')

    -- Execution
    exec Sp_generateSetproductJsonByProductnr 1, @actual out

    -- Assertion
    exec tSQLt.AssertEquals @expected, @actual

end
go

exec tSQLt.RunTestClass @TestClassName = 'Sp_generateSetProductJsonByProductnrTests'
go
