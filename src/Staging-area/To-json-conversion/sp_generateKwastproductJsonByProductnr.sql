/*
 Author: Rens Harinck
 Date: 17-5-2021
 Description: Generates json for kwast by productnr.
 */
use ArtSupplies
go

create or
alter proc Sp_generateKwastJsonByProductnr @productnr int, @jsonObject varchar(max) out
as
begin
    declare @nRecord int = 1
    declare @#Records int
    declare @replaceableTechnieken varchar(20) = 'REPLACEBLE_TECHNIEKEN'
    declare @replaceblePrijsgroep varchar(20) = 'REPLACEBLE_PRIJSGROEP'

    select @#Records = count(*)
    from KWASTPRODUCT_TECHNIEK
    where PRODUCT_PRODUCTNR = @productnr

    declare @techniekenJsonObject varchar(255) = '['

    while (@nRecord <= @#Records)
        begin
            select @techniekenJsonObject = @techniekenJsonObject + '"' + t.TECHNIEK_TECHNIEKNAAM + '"'
            from (select TECHNIEK_TECHNIEKNAAM,
                         row_number() over (order by TECHNIEK_TECHNIEKNAAM) as row
                  from KWASTPRODUCT_TECHNIEK
                  where PRODUCT_PRODUCTNR = @productnr) t
            where row = @nRecord

            if (@nRecord <> @#Records)
                set @techniekenJsonObject = @techniekenJsonObject + ','

            set @nRecord = @nRecord + 1
        end

    set @techniekenJsonObject = @techniekenJsonObject + ']'

    declare @prijsgroepJsonObject varchar(max)

    select @prijsgroepJsonObject = '"PRIJSGROEP_PRIJSKLASSE":' + cast(PRIJSGROEP_PRIJSKLASSE as varchar) +
    ',"PRIJSGROEP_PRIJS":' + cast(PRIJSGROEP_PRIJS as varchar)
    from PRIJSGROEP
    where PRIJSGROEP_PRIJSKLASSE =
          (select PRIJSGROEP_PRIJSKLASSE
          from dbo.KWASTPRODUCT
          where PRODUCT_PRODUCTNR = @productnr)

    select @jsonObject = (select p.PRODUCT_PRODUCTNR as '_id',
                                 p.PRODUCT_PRODUCTNR as 'PRODUCT_PRODUCTNR',
                                 p.MERKNAAM,
                                 p.PRODUCT_TYPE,
                                 p.PRODUCT_PRODUCTOMSCHRIJVING,
                                 p.PRODUCT_VOORRAAD,
                                 k.MATERIAAL_MATERIAALNAAM,
                                 k.KWAST_MAAT,
                                 k.KWAST_VORM,
                                 p2.PRIJSGROEP_PRIJS as 'PRODUCT_VERKOOPPRIJS',
                                 @replaceblePrijsgroep as 'PRIJSGROEP',
                                 (iif(@techniekenJsonObject = ('[]'), null, @replaceableTechnieken)
                                     )
                                                     as 'TECHNIEKEN'
                          from PRODUCT p
                                   inner join
                               KWASTPRODUCT k on p.PRODUCT_PRODUCTNR = k.PRODUCT_PRODUCTNR
                                   inner join
                               PRIJSGROEP p2 on k.PRIJSGROEP_PRIJSKLASSE = p2.PRIJSGROEP_PRIJSKLASSE
                          where p.PRODUCT_PRODUCTNR = @productnr
                          for json path)

    set @jsonObject = replace(@jsonObject, '"' + @replaceableTechnieken + '"', @techniekenJsonObject)
    set @jsonObject = replace(@jsonObject, '"' + @replaceblePrijsgroep + '"', '{' + @prijsgroepJsonObject + '}')
end
go

exec tsqlt.DropClass 'Sp_generateKwastJsonByProductnrTests';
go

exec tsqlt.newtestclass 'Sp_generateKwastJsonByProductnrTests';
go

create or
alter procedure Sp_generateKwastJsonByProductnrTests.[Test if generated json is valid]
as
begin
    -- Setup
    declare @actual varchar(4000)
    declare @expected varchar(4000) = '[{"_id":2,"PRODUCT_PRODUCTNR":2,"MERKNAAM":"Avaveo","PRODUCT_TYPE":"KWAST","PRODUCT_PRODUCTOMSCHRIJVING":"Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.","PRODUCT_VOORRAAD":22,"MATERIAAL_MATERIAALNAAM":"African pied wagtail","KWAST_MAAT":2,"KWAST_VORM":"Rond","PRODUCT_VERKOOPPRIJS":20.0000,"PRIJSGROEP":{"PRIJSGROEP_PRIJSKLASSE":3,"PRIJSGROEP_PRIJS":20.00},"TECHNIEKEN":["benchmark"]}]'

    exec tSQLt.FakeTable 'dbo.KWASTPRODUCT'
    exec tSQLt.FakeTable 'dbo.PRIJSGROEP'
    exec tSQLt.FakeTable 'dbo.PRODUCT'
    exec tSQLt.FakeTable 'dbo.KWASTPRODUCT_TECHNIEK'

    insert into dbo.KWASTPRODUCT values (2, 'African pied wagtail', 3, 2, 'Rond')
    insert into dbo.PRIJSGROEP values (3, 20.00)
    insert into dbo.PRODUCT
    values (2, 'Avaveo', 'KWAST', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', 22, null)
    insert into dbo.KWASTPRODUCT_TECHNIEK values ('benchmark', 2)

    -- Execution
    exec Sp_generateKwastJsonByProductnr 2, @actual out

    -- Assertion
    exec tSQLt.AssertEquals @expected, @actual

end
go

create or
alter procedure Sp_generateKwastJsonByProductnrTests.[Test if generated json is valid empty technieken]
as
begin
    -- Setup
    declare @actual varchar(4000)
    declare @expected varchar(4000) = '[{"_id":2,"PRODUCT_PRODUCTNR":2,"MERKNAAM":"Avaveo","PRODUCT_TYPE":"KWAST","PRODUCT_PRODUCTOMSCHRIJVING":"Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.","PRODUCT_VOORRAAD":22,"MATERIAAL_MATERIAALNAAM":"African pied wagtail","KWAST_MAAT":2,"KWAST_VORM":"Rond","PRODUCT_VERKOOPPRIJS":20.0000,"PRIJSGROEP":{"PRIJSGROEP_PRIJSKLASSE":3,"PRIJSGROEP_PRIJS":20.00}}]'
    exec tSQLt.FakeTable 'dbo.KWASTPRODUCT'
    exec tSQLt.FakeTable 'dbo.PRIJSGROEP'
    exec tSQLt.FakeTable 'dbo.PRODUCT'
    exec tSQLt.FakeTable 'dbo.KWASTPRODUCT_TECHNIEK'

    insert into dbo.KWASTPRODUCT values (2, 'African pied wagtail', 3, 2, 'Rond')
    insert into dbo.PRIJSGROEP values (3, 20.00)
    insert into dbo.PRODUCT
    values (2, 'Avaveo', 'KWAST', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', 22, null)

    -- Execution
    exec Sp_generateKwastJsonByProductnr 2, @actual out

    -- Assertion
    exec tSQLt.AssertEquals @expected, @actual

end
go

exec tSQLt.RunTestClass @TestClassName = 'Sp_generateKwastJsonByProductnrTests'
go
