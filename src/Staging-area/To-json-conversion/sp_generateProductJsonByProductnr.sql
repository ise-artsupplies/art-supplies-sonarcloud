/*
 Author: Rens Harinck
 Date: 17-5-2021
 Description: Generates json for product by productnr.
 Last date modified: 19-5-2021
 */
use ArtSupplies
go

create or
alter proc Sp_generateProductJsonByProductnr @productnr int, @jsonObject varchar(max) out
as
begin
    select @jsonObject = (select p.PRODUCT_PRODUCTNR as '_id',
                                 p.PRODUCT_PRODUCTNR as 'PRODUCT_PRODUCTNR',
                                 p.MERKNAAM,
                                 p.PRODUCT_TYPE,
                                 p.PRODUCT_PRODUCTOMSCHRIJVING,
                                 p.PRODUCT_VOORRAAD,
                                 p.PRODUCT_VERKOOPPRIJS
                          from PRODUCT p
                          where p.PRODUCT_PRODUCTNR = @productnr
                          for json path)
end
go

exec tsqlt.DropClass 'Sp_generateProductJsonByProductnrTests';
go

exec tsqlt.newtestclass 'Sp_generateProductJsonByProductnrTests';
go

create or
alter procedure Sp_generateProductJsonByProductnrTests.[Test if generated json is valid]
as
begin
    -- Setup
    declare @actual varchar(4000)
    declare @expected varchar(4000) = '[{"_id":2,"PRODUCT_PRODUCTNR":2,"MERKNAAM":"Avaveo","PRODUCT_TYPE":"KWAST","PRODUCT_PRODUCTOMSCHRIJVING":"Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.","PRODUCT_VOORRAAD":22}]'

    exec tSQLt.FakeTable 'dbo.PRODUCT'

    insert into dbo.PRODUCT
    values (2, 'Avaveo', 'KWAST', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', 22, null)

    -- Execution
    exec Sp_generateProductJsonByProductnr 2, @actual out

    -- Assertion
    exec tSQLt.AssertEquals @expected, @actual

end
go

exec tSQLt.RunTestClass @TestClassName = 'Sp_generateProductJsonByProductnrTests'
go
