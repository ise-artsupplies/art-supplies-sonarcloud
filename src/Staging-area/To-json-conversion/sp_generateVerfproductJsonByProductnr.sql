/*
 Author: Rens Harinck
 Date: 17-5-2021
 Description: Generates json for verf by productnr.
 Last date modified: 18-5-2021
 */
use ArtSupplies
go

create or
alter proc Sp_generateVerfProductJsonByProductnr @productnr int, @jsonObject varchar(max) out
as
begin
    declare @nRecord int = 1
    declare @#Records int
    declare @replaceableKleur varchar(20) = 'REPLACEBLE_KLEUR'
    declare @replaceableWaarschuwing varchar(20) = 'REPLACEBLE_WAARSCHUWING'
    declare @replaceableEigenschap varchar(20) = 'REPLACEBLE_EIGENSCHAP'
    declare @replaceablePigment varchar(20) = 'REPLACEBLE_PIGMENT'
    declare @replaceblePrijsgroep varchar(20) = 'REPLACEBLE_PRIJSGROEP'

    select @#Records = count(*)
    from VERFPRODUCT_WAARSCHUWING v
             inner join WAARSCHUWINGSCODE w
                        on v.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE = w.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE
    where PRODUCT_PRODUCTNR = @productnr

    declare @jsonArrayWaarschuwingen varchar(max) = '['

    while (@nRecord <= @#Records)
        begin
            select @jsonArrayWaarschuwingen = @jsonArrayWaarschuwingen + '{"WAARSCHUWINGSCODE_WAARSCHUWINGSCODE":"' +
                                              t.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE +
                                              '","WAARSCHUWINGSCODE_BESCHRIJVING":' + '"' +
                                              t.WAARSCHUWINGSCODE_BESCHRIJVING + '"}'
            from (select w.WAARSCHUWINGSCODE_BESCHRIJVING,
                         w.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE,
                         row_number() over (order by v.PRODUCT_PRODUCTNR) as row
                  from VERFPRODUCT_WAARSCHUWING v
                           inner join WAARSCHUWINGSCODE w
                                      on v.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE = w.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE
                  where PRODUCT_PRODUCTNR = @productnr) t
            where row = @nRecord

            if (@nRecord <> @#Records)
                set @jsonArrayWaarschuwingen = @jsonArrayWaarschuwingen + ','

            set @nRecord = @nRecord + 1
        end
    set @nRecord = 1
    set @jsonArrayWaarschuwingen = @jsonArrayWaarschuwingen + ']'

    select @#Records = count(*)
    from VERFPRODUCT_EIGENSCHAP ve
             inner join EIGENSCHAP e
                        on ve.EIGENSCHAP_EIGENSCHAPCODE = e.EIGENSCHAP_EIGENSCHAPCODE
    where PRODUCT_PRODUCTNR = @productnr

    declare @jsonArrayEigenschappen varchar(max) = '['

    while (@nRecord <= @#Records)
        begin
            select @jsonArrayEigenschappen = @jsonArrayEigenschappen + '"' + t.EIGENSCHAP_BESCHRIJVING + '"'
            from (select e.EIGENSCHAP_BESCHRIJVING,
                         row_number() over (order by ve.PRODUCT_PRODUCTNR) as row
                  from VERFPRODUCT_EIGENSCHAP ve
                           inner join EIGENSCHAP e
                                      on ve.EIGENSCHAP_EIGENSCHAPCODE = e.EIGENSCHAP_EIGENSCHAPCODE
                  where ve.PRODUCT_PRODUCTNR = @productnr) t
            where row = @nRecord

            if (@nRecord <> @#Records)
                set @jsonArrayEigenschappen = @jsonArrayEigenschappen + ','

            set @nRecord = @nRecord + 1
        end
    set @nRecord = 1
    set @jsonArrayEigenschappen = @jsonArrayEigenschappen + ']'

    declare @kleurJsonObject varchar(max)
    select @kleurJsonObject = (
        select k.KLEUR_KLEURCODE,
               k.KLEUR_KLEUROMSCHRIJVING,
               k.KLEUR_PGWAARDE,
               @replaceablePigment as 'PIGMENT'
        from VERFPRODUCT v
                 inner join
             KLEUR k on v.KLEUR_KLEURCODE = k.KLEUR_KLEURCODE
        where v.PRODUCT_PRODUCTNR = @productnr
        for json path
    )
    set @kleurJsonObject = replace(@kleurJsonObject, '[{', '')
    set @kleurJsonObject = replace(@kleurJsonObject, '}]', '')

    select @#Records = count(w2.WAARSCHUWINGSCODE_BESCHRIJVING)
    from PIGMENT p
             inner join KLEUR_GEMAAKT_VAN_PIGMENT kgvp on p.PIGMENTNAAM = kgvp.PIGMENTNAAM
             inner join KLEUR k2 on k2.KLEUR_KLEURCODE = kgvp.KLEUR_KLEURCODE
             inner join VERFPRODUCT v2 on k2.KLEUR_KLEURCODE = v2.KLEUR_KLEURCODE
             inner join WAARSCHUWING_VAN_PIGMENT w on w.PIGMENTNAAM = p.PIGMENTNAAM
             inner join WAARSCHUWINGSCODE w2
                        on w.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE = w2.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE
    where v2.PRODUCT_PRODUCTNR = @productnr

    declare @jsonArrayPigment varchar(max)
    declare @jsonWaarschuwingenPigment varchar(max) = ''

    select @jsonArrayPigment = '{"PIGMENTNAAM": "' + p.PIGMENTNAAM + '"'
    from PIGMENT p
             inner join KLEUR_GEMAAKT_VAN_PIGMENT kgvp on p.PIGMENTNAAM = kgvp.PIGMENTNAAM
             inner join KLEUR k2 on k2.KLEUR_KLEURCODE = kgvp.KLEUR_KLEURCODE
             inner join VERFPRODUCT v2 on k2.KLEUR_KLEURCODE = v2.KLEUR_KLEURCODE
    where v2.PRODUCT_PRODUCTNR = @productnr

    while (@nRecord <= @#Records)
        begin
            if (@nRecord <> 1)
                set @jsonWaarschuwingenPigment = @jsonWaarschuwingenPigment + ','

            select @jsonWaarschuwingenPigment =
                   @jsonWaarschuwingenPigment + '{"WAARSCHUWINGSCODE_WAARSCHUWINGSCODE":"'
                       + t.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE +
                   '","WAARSCHUWINGSCODE_BESCHRIJVING":"' + t.WAARSCHUWINGSCODE_BESCHRIJVING + '"}'
            from (select w2.WAARSCHUWINGSCODE_BESCHRIJVING,
                         w2.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE,
                         row_number() over (order by v2.PRODUCT_PRODUCTNR) as row
                  from PIGMENT p
                           inner join KLEUR_GEMAAKT_VAN_PIGMENT kgvp on p.PIGMENTNAAM = kgvp.PIGMENTNAAM
                           inner join KLEUR k2 on k2.KLEUR_KLEURCODE = kgvp.KLEUR_KLEURCODE
                           inner join VERFPRODUCT v2 on k2.KLEUR_KLEURCODE = v2.KLEUR_KLEURCODE
                           inner join WAARSCHUWING_VAN_PIGMENT w on w.PIGMENTNAAM = p.PIGMENTNAAM
                           inner join WAARSCHUWINGSCODE w2
                                      on w.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE = w2.WAARSCHUWINGSCODE_WAARSCHUWINGSCODE
                  where v2.PRODUCT_PRODUCTNR = @productnr) t
            where row = @nRecord

            set @nRecord = @nRecord + 1
        end
    set @nRecord = 1
    if (@jsonWaarschuwingenPigment <> '')
        begin
            set @jsonWaarschuwingenPigment = ',"WAARSCHUWINGEN": [' + @jsonWaarschuwingenPigment + ']}'
            set @jsonArrayPigment = @jsonArrayPigment + @jsonWaarschuwingenPigment
        end
    else
        begin
            set @jsonArrayPigment = @jsonArrayPigment + '}'
        end

    set @kleurJsonObject = replace(@kleurJsonObject, '"' + @replaceablePigment + '"', @jsonArrayPigment)

    declare @prijsgroepJsonObject varchar(max)

    select @prijsgroepJsonObject = '"PRIJSGROEP_PRIJSKLASSE":' + cast(PRIJSGROEP_PRIJSKLASSE as varchar) +
    ',"PRIJSGROEP_PRIJS":' + cast(PRIJSGROEP_PRIJS as varchar)
    from PRIJSGROEP
    where PRIJSGROEP_PRIJSKLASSE =
          (select PRIJSGROEP_PRIJSKLASSE
          from dbo.VERFPRODUCT
          where PRODUCT_PRODUCTNR = @productnr)


    select @jsonObject = (
        select p.PRODUCT_PRODUCTNR                                                    as '_id',
               p.PRODUCT_PRODUCTNR                                                    as 'PRODUCT_PRODUCTNR',
               p.MERKNAAM,
               p.PRODUCT_TYPE,
               p.PRODUCT_PRODUCTOMSCHRIJVING,
               p.PRODUCT_VOORRAAD,
               p2.PRIJSGROEP_PRIJS                                                    as 'PRODUCT_VERKOOPPRIJS',
               @replaceblePrijsgroep as 'PRIJSGROEP',
               iif(@jsonArrayWaarschuwingen = ('[]'), null, @replaceableWaarschuwing) as 'VERFPRODUCT_WAARSCHUWINGEN',
               iif(@jsonArrayEigenschappen = ('[]'), null, @replaceableEigenschap)    as 'EIGENSCHAPPEN',
               @replaceableKleur                                                      as 'KLEUR'
        from PRODUCT p
                 inner join
             VERFPRODUCT v on p.PRODUCT_PRODUCTNR = v.PRODUCT_PRODUCTNR
                 inner join
             PRIJSGROEP p2 on v.PRIJSGROEP_PRIJSKLASSE = p2.PRIJSGROEP_PRIJSKLASSE
        where p.PRODUCT_PRODUCTNR = @productnr
        for json path
    )

    set @jsonObject = replace(@jsonObject, '"' + @replaceableWaarschuwing + '"', @jsonArrayWaarschuwingen)
    set @jsonObject = replace(@jsonObject, '"' + @replaceableEigenschap + '"', @jsonArrayEigenschappen)
    set @jsonObject = replace(@jsonObject, '"' + @replaceableKleur + '"', '{' + @kleurJsonObject + '}')
    set @jsonObject = replace(@jsonObject, '"' + @replaceblePrijsgroep + '"', '{' + @prijsgroepJsonObject + '}')

end
go

exec tsqlt.DropClass 'Sp_generateVerfproductJsonByProductnrTests';
go

exec tsqlt.newtestclass 'Sp_generateVerfproductJsonByProductnrTests';
go

create or
alter procedure Sp_generateVerfproductJsonByProductnrTests.[Test if generated json is valid]
as
begin
    -- Setup
    declare @actual varchar(4000)
    declare @expected varchar(4000) = '[{"_id":3,"PRODUCT_PRODUCTNR":3,"MERKNAAM":"Avaveo","PRODUCT_TYPE":"VERF","PRODUCT_PRODUCTOMSCHRIJVING":"Aliquam sit amet diam in magna bibendum imperdiet.","PRODUCT_VOORRAAD":26,"PRODUCT_VERKOOPPRIJS":1.0000,"PRIJSGROEP":{"PRIJSGROEP_PRIJSKLASSE":26,"PRIJSGROEP_PRIJS":1.00},"VERFPRODUCT_WAARSCHUWINGEN":[{"WAARSCHUWINGSCODE_WAARSCHUWINGSCODE":"EUH006","WAARSCHUWINGSCODE_BESCHRIJVING":"Dangerous material"}],"EIGENSCHAPPEN":["eigenschap 1"],"KLEUR":{"KLEUR_KLEURCODE":"#","KLEUR_KLEUROMSCHRIJVING":"White-winged tern","KLEUR_PGWAARDE":2,"PIGMENT":{"PIGMENTNAAM": "ern, white-winged"}}}]'
    exec tSQLt.FakeTable 'dbo.PRODUCT'
    exec tSQLt.FakeTable 'dbo.VERFPRODUCT'
    exec tSQLt.FakeTable 'dbo.EIGENSCHAP'
    exec tSQLt.FakeTable 'dbo.VERFPRODUCT_EIGENSCHAP'
    exec tSQLt.FakeTable 'dbo.KLEUR'
    exec tSQLt.FakeTable 'dbo.KLEUR_GEMAAKT_VAN_PIGMENT'
    exec tSQLt.FakeTable 'dbo.PIGMENT'
    exec tSQLt.FakeTable 'dbo.PRIJSGROEP'
    exec tSQLt.FakeTable 'dbo.VERFPRODUCT_WAARSCHUWING'
    exec tSQLt.FakeTable 'dbo.WAARSCHUWING_VAN_PIGMENT'
    exec tSQLt.FakeTable 'dbo.WAARSCHUWINGSCODE'

    insert into dbo.PRODUCT values (3, 'Avaveo', 'VERF', 'Aliquam sit amet diam in magna bibendum imperdiet.', 26, null)
    declare @mockColorCode varchar = '#2259e9';
    insert into dbo.VERFPRODUCT values (3, @mockColorCode, 'HALF', 26, '18,12 x 5ml tubes', 'WASBENZINE')
    insert into dbo.EIGENSCHAP values (1, 'eigenschap 1')
    insert into dbo.VERFPRODUCT_EIGENSCHAP values (1, 3)
    insert into dbo.KLEUR values (@mockColorCode, 'White-winged tern', 2)
    insert into dbo.KLEUR_GEMAAKT_VAN_PIGMENT
    values ('ern, white-winged', @mockColorCode)
    insert into dbo.PIGMENT values ('ern, white-winged')
    insert into dbo.PRIJSGROEP values (26, 1)
    insert into dbo.VERFPRODUCT_WAARSCHUWING values ('EUH006', 3)
    insert into dbo.WAARSCHUWING_VAN_PIGMENT values ('White-winged tern', 'H411')
    insert into dbo.WAARSCHUWINGSCODE
    values ('H411', 'Toxic to aquatic life with long-lasting effects '),
           ('EUH006', 'Dangerous material')

    -- Execution
    exec Sp_generateVerfproductJsonByProductnr 3, @actual out

    -- Assertion
    exec tSQLt.AssertEquals @expected, @actual

end
go

exec tSQLt.RunTestClass @TestClassName = 'Sp_generateVerfproductJsonByProductnrTests'
go
