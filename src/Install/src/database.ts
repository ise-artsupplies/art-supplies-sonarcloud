import {exec} from 'child_process';


export class DatabaseConnection {
    amountExecuted = 0;
    async query(input: string): Promise<void> {
        // -b kill script when errors are found
        // -r output stderr when errors occur. Makes debugging easier

        let defaultDatabase = this.amountExecuted == 0 ? 'master' : 'ArtSupplies'
        const result = await this.execCommand(`sqlcmd -d ${defaultDatabase} -b -r 1 -i "${input}"`)
        this.amountExecuted++;
        return result
    }

    execCommand(cmd: string): Promise<void> {
        return new Promise((resolve: any, reject: any) => {
            exec(cmd, (error, stdout, stderr) => {
                if (error) {
                    reject(error)
                }
                const output = {stdout: stdout, stderr: stderr}
                console.log("Output:", output)
                resolve()
            })
        })
    }
}