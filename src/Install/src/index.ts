import {DatabaseConnection} from './database';
import path from 'path';

class Main {
    databaseConn = new DatabaseConnection();
    arguments!: string[]
    isProd: boolean = false;

    run(): void {
        this.handleArguments();
        this.printWarning()
        setTimeout(async () => {
            await this.installDDL()
            await this.installTestFramework()
            await this.installHistoryTables()
            await this.installConstraints();
            await this.createStoredProcedures();
            await this.insertData();
            await this.createStagingArea();
            await this.createUsers();
            await this.createStoredProcedureTests();
            console.log('\n\n\n\n\nDone!');
        }, 3000)
    }

    private printWarning(): void {
        console.log('\n'.repeat(10))
        console.warn('WARNING!');
        console.warn('WARNING! Stored procedures with the prefix Sp_ (case sensitive) are not allowed in the \'Master\' database.')
        console.warn('WARNING! The script will not fail, but you will get strange errors when creating stored procedures that start with Sp_')
        console.warn('WARNING!');
        console.log('\n'.repeat(5))
    }

    private async runQuery(bestand: string): Promise<void> {
        console.log(`Running: ${bestand}`)
        const bestandPad = this.findPathToFile(bestand)
        try {
            await this.databaseConn.query(bestandPad)
        } catch (e) {
            console.error(e)
            process.exit(1)
        }
    }

    private findPathToFile(file: string): string {
        return path.join(__dirname, '..', '..', file)
    }

    private async installTestFramework(): Promise<void> {
        await this.runQuery('tSQLt/PrepareServer-no-analysis.sql')
        await this.runQuery('tSQLt/tSQLt.class-no-analysis.sql')
    }

    private async installDDL(): Promise<void> {
        await this.runQuery('Database_generation/DDL-no-analysis.sql')
    }

    private async installHistoryTables(): Promise<void> {
        await this.runQuery('Database_generation/History-tables.sql')
    }

    private async installConstraints(): Promise<void> {
        await this.runQuery('Constraints/Constraints.sql')
        await this.runQuery('Constraints/Tests/C1Tests.sql')
        await this.runQuery('Constraints/Tests/C2Tests.sql')
    }

    private async createUsers(): Promise<void> {
        await this.runQuery('Users/Users.sql')
        await this.runQuery('Users/Tests/Users_Unittests.sql')
    }

    private async createStagingArea(): Promise<void> {
        const root = 'Staging-area'

        await this.runQuery(`${root}/To-json-conversion/Sp_generateKwastproductJsonByProductnr.sql`);
        await this.runQuery(`${root}/To-json-conversion/Sp_generateProductJsonByProductnr.sql`);
        await this.runQuery(`${root}/To-json-conversion/Sp_generateVerfproductJsonByProductnr.sql`);
        await this.runQuery(`${root}/To-json-conversion/Sp_generateSetproductJsonByProductnr.sql`);
        await this.runQuery(`${root}/Requests/Sp_directJsonGenerationAndPostDatabaseToStagingArea.sql`);

        await this.runQuery(`${root}/Requests/Security/Sp_encodeBase64.sql`)
        await this.runQuery(`${root}/Requests/Security/Sp_getAuthorizationValue.sql`)
        await this.runQuery(`${root}/Requests/Security/Sp_getRestheartUrl.sql`)
        await this.runQuery(`${root}/Requests/Security/Sp_getDbUrl.sql`)

        await this.runQuery(`${root}/Requests/Sp_createDatabase.sql`)
        await this.runQuery(`${root}/Requests/Sp_getETagIfExists.sql`)
        await this.runQuery(`${root}/Requests/Sp_deleteResourceByETag.sql`)

        await this.runQuery(`${root}/Staging-Area-Setup.sql`)
        await this.runQuery(`${root}/job.sql`)
    }

    private async createStoredProcedures(): Promise<void> {
        await this.runQuery(`StoredProcedures/Search-in-history/Stored-Procedures.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Create_Product.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Delete_Product.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Update_Product.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Delete_FramingOrder.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Update_framingorder.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Create_FramingOrder.sql`);
    }

    private async createStoredProcedureTests(): Promise<void> {
        await this.runQuery(`StoredProcedures/Search-in-history/Tests/Sp_Changes_In_PigmentTest.sql`);
        await this.runQuery(`StoredProcedures/Search-in-history/Tests/Sp_Changes_by_userTests.sql`);
        await this.runQuery(`StoredProcedures/Search-in-history/Tests/Sp_Changes_In_PriceTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Tests/Create_ProductTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Tests/Delete_ProductTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Tests/Update_ProductTests.sql`)
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Tests/Delete_FramingOrderTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Tests/Update_framingorderTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Tests/Create_FramingOrderTests.sql`);
    }

    private async insertData() {
        if (!this.isProd) await this.runQuery(`Database_generation/Insert-no-analysis.sql`)
    }

    private handleArguments(): void {
        this.arguments = process.argv.splice(2);
        const isProdArgument = this.arguments.find((value) => {
            return value === '--prod'
        })
        this.isProd = isProdArgument !== undefined
    }
}

const main = new Main()
main.run()
