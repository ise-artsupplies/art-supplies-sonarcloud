"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseConnection = void 0;
const child_process_1 = require("child_process");
class DatabaseConnection {
    constructor() {
        this.amountExecuted = 0;
    }
    async query(input) {
        // -b kill script when errors are found
        // -r output stderr when errors occur. Makes debugging easier
        let defaultDatabase = this.amountExecuted == 0 ? 'master' : 'ArtSupplies';
        const result = await this.execCommand(`sqlcmd -d ${defaultDatabase} -b -r 1 -i "${input}"`);
        this.amountExecuted++;
        return result;
    }
    execCommand(cmd) {
        return new Promise((resolve, reject) => {
            child_process_1.exec(cmd, (error, stdout, stderr) => {
                if (error) {
                    reject(error);
                }
                const output = { stdout: stdout, stderr: stderr };
                console.log("Output:", output);
                resolve();
            });
        });
    }
}
exports.DatabaseConnection = DatabaseConnection;
