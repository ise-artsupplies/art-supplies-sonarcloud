"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("./database");
const path_1 = __importDefault(require("path"));
class Main {
    constructor() {
        this.databaseConn = new database_1.DatabaseConnection();
        this.isProd = false;
    }
    run() {
        this.handleArguments();
        this.printWarning();
        setTimeout(async () => {
            await this.installDDL();
            await this.installTestFramework();
            await this.installHistoryTables();
            await this.installConstraints();
            await this.createStoredProcedures();
            await this.insertData();
            await this.createStagingArea();
            await this.createUsers();
            await this.createStoredProcedureTests();
            console.log('\n\n\n\n\nDone!');
        }, 3000);
    }
    printWarning() {
        console.log('\n'.repeat(10));
        console.warn('WARNING!');
        console.warn('WARNING! Stored procedures with the prefix Sp_ (case sensitive) are not allowed in the \'Master\' database.');
        console.warn('WARNING! The script will not fail, but you will get strange errors when creating stored procedures that start with Sp_');
        console.warn('WARNING!');
        console.log('\n'.repeat(5));
    }
    async runQuery(bestand) {
        console.log(`Running: ${bestand}`);
        const bestandPad = this.findPathToFile(bestand);
        try {
            await this.databaseConn.query(bestandPad);
        }
        catch (e) {
            console.error(e);
            process.exit(1);
        }
    }
    findPathToFile(file) {
        return path_1.default.join(__dirname, '..', '..', file);
    }
    async installTestFramework() {
        await this.runQuery('tSQLt/PrepareServer-no-analysis.sql');
        await this.runQuery('tSQLt/tSQLt.class-no-analysis.sql');
    }
    async installDDL() {
        await this.runQuery('Database_generation/DDL-no-analysis.sql');
    }
    async installHistoryTables() {
        await this.runQuery('Database_generation/History-tables.sql');
    }
    async installConstraints() {
        await this.runQuery('Constraints/Constraints.sql');
        await this.runQuery('Constraints/Tests/C1Tests.sql');
        await this.runQuery('Constraints/Tests/C2Tests.sql');
    }
    async createUsers() {
        await this.runQuery('Users/Users.sql');
        await this.runQuery('Users/Tests/Users_Unittests.sql');
    }
    async createStagingArea() {
        const root = 'Staging-area';
        await this.runQuery(`${root}/To-json-conversion/Sp_generateKwastproductJsonByProductnr.sql`);
        await this.runQuery(`${root}/To-json-conversion/Sp_generateProductJsonByProductnr.sql`);
        await this.runQuery(`${root}/To-json-conversion/Sp_generateVerfproductJsonByProductnr.sql`);
        await this.runQuery(`${root}/To-json-conversion/Sp_generateSetproductJsonByProductnr.sql`);
        await this.runQuery(`${root}/Requests/Sp_directJsonGenerationAndPostDatabaseToStagingArea.sql`);
        await this.runQuery(`${root}/Requests/Security/Sp_encodeBase64.sql`);
        await this.runQuery(`${root}/Requests/Security/Sp_getAuthorizationValue.sql`);
        await this.runQuery(`${root}/Requests/Security/Sp_getRestheartUrl.sql`);
        await this.runQuery(`${root}/Requests/Security/Sp_getDbUrl.sql`);
        await this.runQuery(`${root}/Requests/Sp_createDatabase.sql`);
        await this.runQuery(`${root}/Requests/Sp_getETagIfExists.sql`);
        await this.runQuery(`${root}/Requests/Sp_deleteResourceByETag.sql`);
        await this.runQuery(`${root}/Staging-Area-Setup.sql`);
        await this.runQuery(`${root}/job.sql`);
    }
    async createStoredProcedures() {
        await this.runQuery(`StoredProcedures/Search-in-history/Stored-Procedures.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Create_Product.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Delete_Product.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Update_Product.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Delete_FramingOrder.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Update_framingorder.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Create_FramingOrder.sql`);
    }
    async createStoredProcedureTests() {
        await this.runQuery(`StoredProcedures/Search-in-history/Tests/Sp_Changes_In_PigmentTest.sql`);
        await this.runQuery(`StoredProcedures/Search-in-history/Tests/Sp_Changes_by_userTests.sql`);
        await this.runQuery(`StoredProcedures/Search-in-history/Tests/Sp_Changes_In_PriceTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Tests/Create_ProductTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Tests/Delete_ProductTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_Products/Tests/Update_ProductTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Tests/Delete_FramingOrderTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Tests/Update_framingorderTests.sql`);
        await this.runQuery(`StoredProcedures/CRUD_FramingOrder/Tests/Create_FramingOrderTests.sql`);
    }
    async insertData() {
        if (!this.isProd)
            await this.runQuery(`Database_generation/Insert-no-analysis.sql`);
    }
    handleArguments() {
        this.arguments = process.argv.splice(2);
        const isProdArgument = this.arguments.find((value) => {
            return value === '--prod';
        });
        this.isProd = isProdArgument !== undefined;
    }
}
const main = new Main();
main.run();
