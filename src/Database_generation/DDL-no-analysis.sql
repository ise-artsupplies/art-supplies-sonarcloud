/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2014                    */
/* Created on:     02/06/2021 10:25:45                          */
/*==============================================================*/
use master
drop database if exists ArtSupplies
GO

create database ArtSupplies
go

use ArtSupplies

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FRAMINGORDER') and o.name = 'FK_FRAMINGO_FRAMINGOR_GLAS')
alter table FRAMINGORDER
   drop constraint FK_FRAMINGO_FRAMINGOR_GLAS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FRAMINGORDER') and o.name = 'FK_FRAMINGO_FRAMINGOR_KUNSTWER')
alter table FRAMINGORDER
   drop constraint FK_FRAMINGO_FRAMINGOR_KUNSTWER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FRAMINGORDER') and o.name = 'FK_FRAMINGO_FRAMINGOR_FRAMER')
alter table FRAMINGORDER
   drop constraint FK_FRAMINGO_FRAMINGOR_FRAMER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FRAMINGORDER') and o.name = 'FK_FRAMINGO_LIJST_IN__LIJST')
alter table FRAMINGORDER
   drop constraint FK_FRAMINGO_LIJST_IN__LIJST
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FRAMINGORDERMETPASSEPARTOUT') and o.name = 'FK_FRAMINGO_IS_EEN_FR_FRAMINGO')
alter table FRAMINGORDERMETPASSEPARTOUT
   drop constraint FK_FRAMINGO_IS_EEN_FR_FRAMINGO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FRAMINGORDERMETPASSEPARTOUT') and o.name = 'FK_FRAMINGO_PASSEPART_PASSEPAR')
alter table FRAMINGORDERMETPASSEPARTOUT
   drop constraint FK_FRAMINGO_PASSEPART_PASSEPAR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KLEUR_GEMAAKT_VAN_PIGMENT') and o.name = 'FK_KLEUR_GE_KLEUR_GEM_PIGMENT')
alter table KLEUR_GEMAAKT_VAN_PIGMENT
   drop constraint FK_KLEUR_GE_KLEUR_GEM_PIGMENT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KLEUR_GEMAAKT_VAN_PIGMENT') and o.name = 'FK_KLEUR_GE_KLEUR_GEM_KLEUR')
alter table KLEUR_GEMAAKT_VAN_PIGMENT
   drop constraint FK_KLEUR_GE_KLEUR_GEM_KLEUR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KWASTPRODUCT') and o.name = 'FK_KWASTPRO_IS_EEN_PR_PRODUCT')
alter table KWASTPRODUCT
   drop constraint FK_KWASTPRO_IS_EEN_PR_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KWASTPRODUCT') and o.name = 'FK_KWASTPRO_KWASTPROD_MATERIAA')
alter table KWASTPRODUCT
   drop constraint FK_KWASTPRO_KWASTPROD_MATERIAA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KWASTPRODUCT') and o.name = 'FK_KWASTPRO_PRIJSGROE_PRIJSGRO')
alter table KWASTPRODUCT
   drop constraint FK_KWASTPRO_PRIJSGROE_PRIJSGRO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KWASTPRODUCT_TECHNIEK') and o.name = 'FK_KWASTPRO_KWASTPROD_TECHNIEK')
alter table KWASTPRODUCT_TECHNIEK
   drop constraint FK_KWASTPRO_KWASTPROD_TECHNIEK
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KWASTPRODUCT_TECHNIEK') and o.name = 'FK_KWASTPRO_KWASTPROD_KWASTPRO')
alter table KWASTPRODUCT_TECHNIEK
   drop constraint FK_KWASTPRO_KWASTPROD_KWASTPRO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LIJST') and o.name = 'FK_LIJST_LIJSTDOOR_MERK')
alter table LIJST
   drop constraint FK_LIJST_LIJSTDOOR_MERK
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LIJST') and o.name = 'FK_LIJST_TYPE_VAN__LIJSTTYP')
alter table LIJST
   drop constraint FK_LIJST_TYPE_VAN__LIJSTTYP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PASSEPARTOUT') and o.name = 'FK_PASSEPAR_PASSEPART_PAPIERSO')
alter table PASSEPARTOUT
   drop constraint FK_PASSEPAR_PASSEPART_PAPIERSO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PASSEPARTOUT') and o.name = 'FK_PASSEPAR_PASSEPART_PASSEPAR')
alter table PASSEPARTOUT
   drop constraint FK_PASSEPAR_PASSEPART_PASSEPAR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCT') and o.name = 'FK_PRODUCT_PRODUCT_V_MERK')
alter table PRODUCT
   drop constraint FK_PRODUCT_PRODUCT_V_MERK
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCT') and o.name = 'FK_PRODUCT_PRODUCT_V_PRODUCTT')
alter table PRODUCT
   drop constraint FK_PRODUCT_PRODUCT_V_PRODUCTT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCT_IN_SET') and o.name = 'FK_PRODUCT__ONDERDEEL_SETPRODU')
alter table PRODUCT_IN_SET
   drop constraint FK_PRODUCT__ONDERDEEL_SETPRODU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCT_IN_SET') and o.name = 'FK_PRODUCT__PRODUCT_I_PRODUCT')
alter table PRODUCT_IN_SET
   drop constraint FK_PRODUCT__PRODUCT_I_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SETPRODUCT') and o.name = 'FK_SETPRODU_IS_EEN_PR_PRODUCT')
alter table SETPRODUCT
   drop constraint FK_SETPRODU_IS_EEN_PR_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT') and o.name = 'FK_VERFPROD_IS_EEN_PR_PRODUCT')
alter table VERFPRODUCT
   drop constraint FK_VERFPROD_IS_EEN_PR_PRODUCT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT') and o.name = 'FK_VERFPROD_KLEUR_VAN_KLEUR')
alter table VERFPRODUCT
   drop constraint FK_VERFPROD_KLEUR_VAN_KLEUR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT') and o.name = 'FK_VERFPROD_PRIJSGROE_PRIJSGRO')
alter table VERFPRODUCT
   drop constraint FK_VERFPROD_PRIJSGROE_PRIJSGRO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT') and o.name = 'FK_VERFPROD_VERFPRODU_TRANSPAR')
alter table VERFPRODUCT
   drop constraint FK_VERFPROD_VERFPRODU_TRANSPAR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT') and o.name = 'FK_VERFPROD_VERFPRODU_VERDUNNE')
alter table VERFPRODUCT
   drop constraint FK_VERFPROD_VERFPRODU_VERDUNNE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT_EIGENSCHAP') and o.name = 'FK_VERFPROD_VERFPRODU_EIGENSCH')
alter table VERFPRODUCT_EIGENSCHAP
   drop constraint FK_VERFPROD_VERFPRODU_EIGENSCH
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT_EIGENSCHAP') and o.name = 'FK_VERFPROD_VERFPRODU_VERFPROD')
alter table VERFPRODUCT_EIGENSCHAP
   drop constraint FK_VERFPROD_VERFPRODU_VERFPROD
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT_WAARSCHUWING') and o.name = 'FK_VERFPROD_VERFPRODU_WAARSCHU')
alter table VERFPRODUCT_WAARSCHUWING
   drop constraint FK_VERFPROD_VERFPRODU_WAARSCHU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VERFPRODUCT_WAARSCHUWING') and o.name = 'FK_VERFPRODWAARSHUWING_VERFPRODU_VERFPROD')
alter table VERFPRODUCT_WAARSCHUWING
   drop constraint FK_VERFPRODWAARSHUWING_VERFPRODU_VERFPROD
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WAARSCHUWING_VAN_PIGMENT') and o.name = 'FK_WAARSCHU_WAARSCHUW_PIGMENT')
alter table WAARSCHUWING_VAN_PIGMENT
   drop constraint FK_WAARSCHU_WAARSCHUW_PIGMENT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WAARSCHUWING_VAN_PIGMENT') and o.name = 'FK_WAARSCHU_WAARSCHUW_WAARSCHU')
alter table WAARSCHUWING_VAN_PIGMENT
   drop constraint FK_WAARSCHU_WAARSCHUW_WAARSCHU
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EIGENSCHAP')
            and   type = 'U')
   drop table EIGENSCHAP
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FRAMER')
            and   type = 'U')
   drop table FRAMER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('FRAMINGORDER')
            and   name  = 'LIJST_IN_FRAMINGORDER_FK'
            and   indid > 0
            and   indid < 255)
   drop index FRAMINGORDER.LIJST_IN_FRAMINGORDER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('FRAMINGORDER')
            and   name  = 'FRAMINGORDER_UITGEVOERD_DOOR_FRAMER_FK'
            and   indid > 0
            and   indid < 255)
   drop index FRAMINGORDER.FRAMINGORDER_UITGEVOERD_DOOR_FRAMER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('FRAMINGORDER')
            and   name  = 'FRAMINGORDER_GLASSOORT_FK'
            and   indid > 0
            and   indid < 255)
   drop index FRAMINGORDER.FRAMINGORDER_GLASSOORT_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('FRAMINGORDER')
            and   name  = 'FRAMINGORDER_KUNSTWERKSOORT_FK'
            and   indid > 0
            and   indid < 255)
   drop index FRAMINGORDER.FRAMINGORDER_KUNSTWERKSOORT_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FRAMINGORDER')
            and   type = 'U')
   drop table FRAMINGORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('FRAMINGORDERMETPASSEPARTOUT')
            and   name  = 'PASSEPARTOUTVANFRAMINGORDERMETPASSEPARTOUT_FK'
            and   indid > 0
            and   indid < 255)
   drop index FRAMINGORDERMETPASSEPARTOUT.PASSEPARTOUTVANFRAMINGORDERMETPASSEPARTOUT_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FRAMINGORDERMETPASSEPARTOUT')
            and   type = 'U')
   drop table FRAMINGORDERMETPASSEPARTOUT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('GLAS')
            and   type = 'U')
   drop table GLAS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KLEUR')
            and   type = 'U')
   drop table KLEUR
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('KLEUR_GEMAAKT_VAN_PIGMENT')
            and   name  = 'KLEUR_GEMAAKT_VAN_PIGMENT2_FK'
            and   indid > 0
            and   indid < 255)
   drop index KLEUR_GEMAAKT_VAN_PIGMENT.KLEUR_GEMAAKT_VAN_PIGMENT2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('KLEUR_GEMAAKT_VAN_PIGMENT')
            and   name  = 'KLEUR_GEMAAKT_VAN_PIGMENT_FK'
            and   indid > 0
            and   indid < 255)
   drop index KLEUR_GEMAAKT_VAN_PIGMENT.KLEUR_GEMAAKT_VAN_PIGMENT_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KLEUR_GEMAAKT_VAN_PIGMENT')
            and   type = 'U')
   drop table KLEUR_GEMAAKT_VAN_PIGMENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KUNSTWERKSOORT')
            and   type = 'U')
   drop table KUNSTWERKSOORT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('KWASTPRODUCT')
            and   name  = 'PRIJSGROEP_VAN_KWASTPRODUCT_FK'
            and   indid > 0
            and   indid < 255)
   drop index KWASTPRODUCT.PRIJSGROEP_VAN_KWASTPRODUCT_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('KWASTPRODUCT')
            and   name  = 'KWASTPRODUCT_MATERIAAL_FK'
            and   indid > 0
            and   indid < 255)
   drop index KWASTPRODUCT.KWASTPRODUCT_MATERIAAL_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KWASTPRODUCT')
            and   type = 'U')
   drop table KWASTPRODUCT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('KWASTPRODUCT_TECHNIEK')
            and   name  = 'KWASTPRODUCT_TECHNIEK2_FK'
            and   indid > 0
            and   indid < 255)
   drop index KWASTPRODUCT_TECHNIEK.KWASTPRODUCT_TECHNIEK2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('KWASTPRODUCT_TECHNIEK')
            and   name  = 'KWASTPRODUCT_TECHNIEK_FK'
            and   indid > 0
            and   indid < 255)
   drop index KWASTPRODUCT_TECHNIEK.KWASTPRODUCT_TECHNIEK_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KWASTPRODUCT_TECHNIEK')
            and   type = 'U')
   drop table KWASTPRODUCT_TECHNIEK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LIJST')
            and   name  = 'TYPE_VAN_LIJST_FK'
            and   indid > 0
            and   indid < 255)
   drop index LIJST.TYPE_VAN_LIJST_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LIJST')
            and   name  = 'LIJSTDOORMERK_FK'
            and   indid > 0
            and   indid < 255)
   drop index LIJST.LIJSTDOORMERK_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LIJST')
            and   type = 'U')
   drop table LIJST
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LIJSTTYPE')
            and   type = 'U')
   drop table LIJSTTYPE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MATERIAAL')
            and   type = 'U')
   drop table MATERIAAL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MERK')
            and   type = 'U')
   drop table MERK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PAPIERSOORT')
            and   type = 'U')
   drop table PAPIERSOORT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PASSEPARTOUT')
            and   name  = 'PASSEPARTOUT_PAPIERSOORT_FK'
            and   indid > 0
            and   indid < 255)
   drop index PASSEPARTOUT.PASSEPARTOUT_PAPIERSOORT_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PASSEPARTOUT')
            and   name  = 'PASSEPARTOUT_PASSEPARTOUTSOORT_FK'
            and   indid > 0
            and   indid < 255)
   drop index PASSEPARTOUT.PASSEPARTOUT_PASSEPARTOUTSOORT_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PASSEPARTOUT')
            and   type = 'U')
   drop table PASSEPARTOUT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PASSEPARTOUTSOORT')
            and   type = 'U')
   drop table PASSEPARTOUTSOORT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PIGMENT')
            and   type = 'U')
   drop table PIGMENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRIJSGROEP')
            and   type = 'U')
   drop table PRIJSGROEP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PRODUCT')
            and   name  = 'PRODUCT_VAN_TYPE_FK'
            and   indid > 0
            and   indid < 255)
   drop index PRODUCT.PRODUCT_VAN_TYPE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PRODUCT')
            and   name  = 'PRODUCT_VAN_MERK_FK'
            and   indid > 0
            and   indid < 255)
   drop index PRODUCT.PRODUCT_VAN_MERK_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCT')
            and   type = 'U')
   drop table PRODUCT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCTTYPE')
            and   type = 'U')
   drop table PRODUCTTYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PRODUCT_IN_SET')
            and   name  = 'ONDERDEEL_ZIT_IN_SET_FK'
            and   indid > 0
            and   indid < 255)
   drop index PRODUCT_IN_SET.ONDERDEEL_ZIT_IN_SET_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PRODUCT_IN_SET')
            and   name  = 'PRODUCT_IN_SET_ONDERDEEL_FK'
            and   indid > 0
            and   indid < 255)
   drop index PRODUCT_IN_SET.PRODUCT_IN_SET_ONDERDEEL_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCT_IN_SET')
            and   type = 'U')
   drop table PRODUCT_IN_SET
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SETPRODUCT')
            and   type = 'U')
   drop table SETPRODUCT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TECHNIEK')
            and   type = 'U')
   drop table TECHNIEK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TRANSPARANTIE')
            and   type = 'U')
   drop table TRANSPARANTIE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VERDUNNER')
            and   type = 'U')
   drop table VERDUNNER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT')
            and   name  = 'VERFPRODUCT_VERDUNNER_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT.VERFPRODUCT_VERDUNNER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT')
            and   name  = 'PRIJSGROEP_VAN_VERFPRODUCT_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT.PRIJSGROEP_VAN_VERFPRODUCT_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT')
            and   name  = 'VERFPRODUCT_TRANSPARANTIE_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT.VERFPRODUCT_TRANSPARANTIE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT')
            and   name  = 'KLEUR_VAN_VERFPRODUCT_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT.KLEUR_VAN_VERFPRODUCT_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VERFPRODUCT')
            and   type = 'U')
   drop table VERFPRODUCT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT_EIGENSCHAP')
            and   name  = 'VERFPRODUCT_EIGENSCHAP2_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT_EIGENSCHAP.VERFPRODUCT_EIGENSCHAP2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT_EIGENSCHAP')
            and   name  = 'VERFPRODUCT_EIGENSCHAP_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT_EIGENSCHAP.VERFPRODUCT_EIGENSCHAP_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VERFPRODUCT_EIGENSCHAP')
            and   type = 'U')
   drop table VERFPRODUCT_EIGENSCHAP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT_WAARSCHUWING')
            and   name  = 'VERFPRODUCT_WAARSCHUWING2_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT_WAARSCHUWING.VERFPRODUCT_WAARSCHUWING2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VERFPRODUCT_WAARSCHUWING')
            and   name  = 'VERFPRODUCT_WAARSCHUWING_FK'
            and   indid > 0
            and   indid < 255)
   drop index VERFPRODUCT_WAARSCHUWING.VERFPRODUCT_WAARSCHUWING_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VERFPRODUCT_WAARSCHUWING')
            and   type = 'U')
   drop table VERFPRODUCT_WAARSCHUWING
go

if exists (select 1
            from  sysobjects
           where  id = object_id('WAARSCHUWINGSCODE')
            and   type = 'U')
   drop table WAARSCHUWINGSCODE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('WAARSCHUWING_VAN_PIGMENT')
            and   name  = 'WAARSCHUWING_VAN_PIGMENT2_FK'
            and   indid > 0
            and   indid < 255)
   drop index WAARSCHUWING_VAN_PIGMENT.WAARSCHUWING_VAN_PIGMENT2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('WAARSCHUWING_VAN_PIGMENT')
            and   name  = 'WAARSCHUWING_VAN_PIGMENT_FK'
            and   indid > 0
            and   indid < 255)
   drop index WAARSCHUWING_VAN_PIGMENT.WAARSCHUWING_VAN_PIGMENT_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('WAARSCHUWING_VAN_PIGMENT')
            and   type = 'U')
   drop table WAARSCHUWING_VAN_PIGMENT
go

if exists(select 1 from systypes where name='AANTAL')
   drop type AANTAL
go

if exists(select 1 from systypes where name='BESCHRIJVING')
   drop type BESCHRIJVING
go

if exists(select 1 from systypes where name='DATUM')
   drop type DATUM
go

if exists(select 1 from systypes where name='EIGENSCHAPCODE')
   drop type EIGENSCHAPCODE
go

if exists(select 1 from systypes where name='FRAMINGORDERNUMMER')
   drop type FRAMINGORDERNUMMER
go

if exists(select 1 from systypes where name='KLEURCODE')
   drop type KLEURCODE
go

if exists(select 1 from systypes where name='LIJSTAFMETING')
   drop type LIJSTAFMETING
go

if exists(select 1 from systypes where name='LIJSTNUMMER')
   drop type LIJSTNUMMER
go

if exists(select 1 from systypes where name='NAAM')
   drop type NAAM
go

if exists(select 1 from systypes where name='NUMMER')
   drop type NUMMER
go

if exists(select 1 from systypes where name='PASSEPARTOUTNUMMER')
   drop type PASSEPARTOUTNUMMER
go

if exists(select 1 from systypes where name='PRIJS')
   drop type PRIJS
go

if exists(select 1 from systypes where name='PRODUCTNUMMER')
   drop type PRODUCTNUMMER
go

if exists(select 1 from systypes where name='TYPENUMMER')
   drop type TYPENUMMER
go

if exists(select 1 from systypes where name='WAARSCHUWINGSCODE')
   drop type WAARSCHUWINGSCODE
go

/*==============================================================*/
/* Domain: AANTAL                                               */
/*==============================================================*/
create type AANTAL
   from int
go

/*==============================================================*/
/* Domain: BESCHRIJVING                                         */
/*==============================================================*/
create type BESCHRIJVING
   from varchar(256)
go

/*==============================================================*/
/* Domain: DATUM                                                */
/*==============================================================*/
create type DATUM
   from datetime
go

/*==============================================================*/
/* Domain: EIGENSCHAPCODE                                       */
/*==============================================================*/
create type EIGENSCHAPCODE
   from varchar(256)
go

/*==============================================================*/
/* Domain: FRAMINGORDERNUMMER                                   */
/*==============================================================*/
create type FRAMINGORDERNUMMER
   from int
go

/*==============================================================*/
/* Domain: KLEURCODE                                            */
/*==============================================================*/
create type KLEURCODE
   from varchar(256)
go

/*==============================================================*/
/* Domain: LIJSTAFMETING                                        */
/*==============================================================*/
create type LIJSTAFMETING
   from numeric(3,1)
go

/*==============================================================*/
/* Domain: LIJSTNUMMER                                          */
/*==============================================================*/
create type LIJSTNUMMER
   from int
go

/*==============================================================*/
/* Domain: NAAM                                                 */
/*==============================================================*/
create type NAAM
   from varchar(256)
go

/*==============================================================*/
/* Domain: NUMMER                                               */
/*==============================================================*/
create type NUMMER
   from int
go

/*==============================================================*/
/* Domain: PASSEPARTOUTNUMMER                                   */
/*==============================================================*/
create type PASSEPARTOUTNUMMER
   from int
go

/*==============================================================*/
/* Domain: PRIJS                                                */
/*==============================================================*/
create type PRIJS
   from money
go

/*==============================================================*/
/* Domain: PRODUCTNUMMER                                        */
/*==============================================================*/
create type PRODUCTNUMMER
   from int
go

/*==============================================================*/
/* Domain: TYPENUMMER                                           */
/*==============================================================*/
create type TYPENUMMER
   from int
go

/*==============================================================*/
/* Domain: WAARSCHUWINGSCODE                                    */
/*==============================================================*/
create type WAARSCHUWINGSCODE
   from varchar(256)
go

/*==============================================================*/
/* Table: EIGENSCHAP                                            */
/*==============================================================*/
create table EIGENSCHAP (
   EIGENSCHAP_EIGENSCHAPCODE EIGENSCHAPCODE       not null,
   EIGENSCHAP_BESCHRIJVING BESCHRIJVING         null,
   constraint PK_EIGENSCHAP primary key (EIGENSCHAP_EIGENSCHAPCODE)
)
go

/*==============================================================*/
/* Table: FRAMER                                                */
/*==============================================================*/
create table FRAMER (
   FRAMER_FRAMERID      NUMMER               not null,
   FRAMER_NAAM          NAAM                 not null,
   constraint PK_FRAMER primary key (FRAMER_FRAMERID)
)
go

/*==============================================================*/
/* Table: FRAMINGORDER                                          */
/*==============================================================*/
create table FRAMINGORDER (
   FRAMINGORDER_ORDERNUMMER FRAMINGORDERNUMMER   not null,
   KUNSTWERKSOORT_SOORTNAAM NAAM                 not null,
   GLAS_GLASSOORT       NAAM                 null,
   LIJST_LIJSTNUMMER    LIJSTNUMMER          not null,
   FRAMER_FRAMERID      NUMMER               not null,
   FRAMINGORDER_BREEDTE_KUNSTWERK LIJSTAFMETING        not null,
   FRAMINGORDER_HOOGTE_KUNSTWERK LIJSTAFMETING        not null,
   FRAMINGORDER_PRIJS   PRIJS                not null,
   FRAMINGORDER_OPLEVERINGSDATUM DATUM                not null,
   FRAMINGORDER_AANVRAAGDATUM DATUM                not null,
   constraint PK_FRAMINGORDER primary key (FRAMINGORDER_ORDERNUMMER)
)
go

/*==============================================================*/
/* Index: FRAMINGORDER_KUNSTWERKSOORT_FK                        */
/*==============================================================*/




create nonclustered index FRAMINGORDER_KUNSTWERKSOORT_FK on FRAMINGORDER (KUNSTWERKSOORT_SOORTNAAM ASC)
go

/*==============================================================*/
/* Index: FRAMINGORDER_GLASSOORT_FK                             */
/*==============================================================*/




create nonclustered index FRAMINGORDER_GLASSOORT_FK on FRAMINGORDER (GLAS_GLASSOORT ASC)
go

/*==============================================================*/
/* Index: FRAMINGORDER_UITGEVOERD_DOOR_FRAMER_FK                */
/*==============================================================*/




create nonclustered index FRAMINGORDER_UITGEVOERD_DOOR_FRAMER_FK on FRAMINGORDER (FRAMER_FRAMERID ASC)
go

/*==============================================================*/
/* Index: LIJST_IN_FRAMINGORDER_FK                              */
/*==============================================================*/




create nonclustered index LIJST_IN_FRAMINGORDER_FK on FRAMINGORDER (LIJST_LIJSTNUMMER ASC)
go

/*==============================================================*/
/* Table: FRAMINGORDERMETPASSEPARTOUT                           */
/*==============================================================*/
create table FRAMINGORDERMETPASSEPARTOUT (
   FRAMINGORDER_ORDERNUMMER FRAMINGORDERNUMMER   not null,
   PASSEPARTOUT_PASSEPARTOUTNUMMER PASSEPARTOUTNUMMER   not null,
   constraint PK_FRAMINGORDERMETPASSEPARTOUT primary key (FRAMINGORDER_ORDERNUMMER)
)
go

/*==============================================================*/
/* Index: PASSEPARTOUTVANFRAMINGORDERMETPASSEPARTOUT_FK         */
/*==============================================================*/




create nonclustered index PASSEPARTOUTVANFRAMINGORDERMETPASSEPARTOUT_FK on FRAMINGORDERMETPASSEPARTOUT (PASSEPARTOUT_PASSEPARTOUTNUMMER ASC)
go

/*==============================================================*/
/* Table: GLAS                                                  */
/*==============================================================*/
create table GLAS (
   GLAS_GLASSOORT       NAAM                 not null,
   constraint PK_GLAS primary key (GLAS_GLASSOORT)
)
go

/*==============================================================*/
/* Table: KLEUR                                                 */
/*==============================================================*/
create table KLEUR (
   KLEUR_KLEURCODE      KLEURCODE            not null,
   KLEUR_KLEUROMSCHRIJVING BESCHRIJVING         not null,
   KLEUR_PGWAARDE       NUMMER               not null,
   constraint PK_KLEUR primary key (KLEUR_KLEURCODE)
)
go

/*==============================================================*/
/* Table: KLEUR_GEMAAKT_VAN_PIGMENT                             */
/*==============================================================*/
create table KLEUR_GEMAAKT_VAN_PIGMENT (
   PIGMENTNAAM          NAAM                 not null,
   KLEUR_KLEURCODE      KLEURCODE            not null,
   constraint PK_KLEUR_GEMAAKT_VAN_PIGMENT primary key (PIGMENTNAAM, KLEUR_KLEURCODE)
)
go

/*==============================================================*/
/* Index: KLEUR_GEMAAKT_VAN_PIGMENT_FK                          */
/*==============================================================*/




create nonclustered index KLEUR_GEMAAKT_VAN_PIGMENT_FK on KLEUR_GEMAAKT_VAN_PIGMENT (PIGMENTNAAM ASC)
go

/*==============================================================*/
/* Index: KLEUR_GEMAAKT_VAN_PIGMENT2_FK                         */
/*==============================================================*/




create nonclustered index KLEUR_GEMAAKT_VAN_PIGMENT2_FK on KLEUR_GEMAAKT_VAN_PIGMENT (KLEUR_KLEURCODE ASC)
go

/*==============================================================*/
/* Table: KUNSTWERKSOORT                                        */
/*==============================================================*/
create table KUNSTWERKSOORT (
   KUNSTWERKSOORT_SOORTNAAM NAAM                 not null,
   constraint PK_KUNSTWERKSOORT primary key (KUNSTWERKSOORT_SOORTNAAM)
)
go

/*==============================================================*/
/* Table: KWASTPRODUCT                                          */
/*==============================================================*/
create table KWASTPRODUCT (
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   MATERIAAL_MATERIAALNAAM NAAM                 not null,
   PRIJSGROEP_PRIJSKLASSE NUMMER               not null,
   KWAST_MAAT           NUMMER               not null,
   KWAST_VORM           NAAM                 null,
   constraint PK_KWASTPRODUCT primary key (PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Index: KWASTPRODUCT_MATERIAAL_FK                             */
/*==============================================================*/




create nonclustered index KWASTPRODUCT_MATERIAAL_FK on KWASTPRODUCT (MATERIAAL_MATERIAALNAAM ASC)
go

/*==============================================================*/
/* Index: PRIJSGROEP_VAN_KWASTPRODUCT_FK                        */
/*==============================================================*/




create nonclustered index PRIJSGROEP_VAN_KWASTPRODUCT_FK on KWASTPRODUCT (PRIJSGROEP_PRIJSKLASSE ASC)
go

/*==============================================================*/
/* Table: KWASTPRODUCT_TECHNIEK                                 */
/*==============================================================*/
create table KWASTPRODUCT_TECHNIEK (
   TECHNIEK_TECHNIEKNAAM NAAM                 not null,
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   constraint PK_KWASTPRODUCT_TECHNIEK primary key (TECHNIEK_TECHNIEKNAAM, PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Index: KWASTPRODUCT_TECHNIEK_FK                              */
/*==============================================================*/




create nonclustered index KWASTPRODUCT_TECHNIEK_FK on KWASTPRODUCT_TECHNIEK (TECHNIEK_TECHNIEKNAAM ASC)
go

/*==============================================================*/
/* Index: KWASTPRODUCT_TECHNIEK2_FK                             */
/*==============================================================*/




create nonclustered index KWASTPRODUCT_TECHNIEK2_FK on KWASTPRODUCT_TECHNIEK (PRODUCT_PRODUCTNR ASC)
go

/*==============================================================*/
/* Table: LIJST                                                 */
/*==============================================================*/
create table LIJST (
   LIJST_LIJSTNUMMER    LIJSTNUMMER          not null,
   LIJSTTYPE_TYPENUMMER TYPENUMMER           null,
   MERKNAAM             NAAM                 null,
   LIJST_PRIJS          PRIJS                null,
   LIJST_LIJSTNAAM      NAAM                 null,
   constraint PK_LIJST primary key (LIJST_LIJSTNUMMER)
)
go

/*==============================================================*/
/* Index: LIJSTDOORMERK_FK                                      */
/*==============================================================*/




create nonclustered index LIJSTDOORMERK_FK on LIJST (MERKNAAM ASC)
go

/*==============================================================*/
/* Index: TYPE_VAN_LIJST_FK                                     */
/*==============================================================*/




create nonclustered index TYPE_VAN_LIJST_FK on LIJST (LIJSTTYPE_TYPENUMMER ASC)
go

/*==============================================================*/
/* Table: LIJSTTYPE                                             */
/*==============================================================*/
create table LIJSTTYPE (
   LIJSTTYPE_TYPENUMMER TYPENUMMER           not null,
   LIJSTTYPE_SOORT      BESCHRIJVING         not null,
   constraint PK_LIJSTTYPE primary key (LIJSTTYPE_TYPENUMMER)
)
go

/*==============================================================*/
/* Table: MATERIAAL                                             */
/*==============================================================*/
create table MATERIAAL (
   MATERIAAL_MATERIAALNAAM NAAM                 not null,
   constraint PK_MATERIAAL primary key (MATERIAAL_MATERIAALNAAM)
)
go

/*==============================================================*/
/* Table: MERK                                                  */
/*==============================================================*/
create table MERK (
   MERKNAAM             NAAM                 not null,
   constraint PK_MERK primary key (MERKNAAM)
)
go

/*==============================================================*/
/* Table: PAPIERSOORT                                           */
/*==============================================================*/
create table PAPIERSOORT (
   PAPIERSOORT_PAPIERNAAM NAAM                 not null,
   constraint PK_PAPIERSOORT primary key (PAPIERSOORT_PAPIERNAAM)
)
go

/*==============================================================*/
/* Table: PASSEPARTOUT                                          */
/*==============================================================*/
create table PASSEPARTOUT (
   PASSEPARTOUT_PASSEPARTOUTNUMMER PASSEPARTOUTNUMMER   not null,
   PAPIERSOORT_PAPIERNAAM NAAM                 not null,
   PASSEPARTOUTSOORT_SOORTNAAM NAAM                 not null,
   PASSEPARTOUT_KLEUR   NAAM                 not null,
   PASSEPARTOUT_PRIJS   PRIJS                not null,
   constraint PK_PASSEPARTOUT primary key (PASSEPARTOUT_PASSEPARTOUTNUMMER)
)
go

/*==============================================================*/
/* Index: PASSEPARTOUT_PASSEPARTOUTSOORT_FK                     */
/*==============================================================*/




create nonclustered index PASSEPARTOUT_PASSEPARTOUTSOORT_FK on PASSEPARTOUT (PASSEPARTOUTSOORT_SOORTNAAM ASC)
go

/*==============================================================*/
/* Index: PASSEPARTOUT_PAPIERSOORT_FK                           */
/*==============================================================*/




create nonclustered index PASSEPARTOUT_PAPIERSOORT_FK on PASSEPARTOUT (PAPIERSOORT_PAPIERNAAM ASC)
go

/*==============================================================*/
/* Table: PASSEPARTOUTSOORT                                     */
/*==============================================================*/
create table PASSEPARTOUTSOORT (
   PASSEPARTOUTSOORT_SOORTNAAM NAAM                 not null,
   constraint PK_PASSEPARTOUTSOORT primary key (PASSEPARTOUTSOORT_SOORTNAAM)
)
go

/*==============================================================*/
/* Table: PIGMENT                                               */
/*==============================================================*/
create table PIGMENT (
   PIGMENTNAAM          NAAM                 not null,
   constraint PK_PIGMENT primary key (PIGMENTNAAM)
)
go

/*==============================================================*/
/* Table: PRIJSGROEP                                            */
/*==============================================================*/
create table PRIJSGROEP (
   PRIJSGROEP_PRIJSKLASSE NUMMER               not null,
   PRIJSGROEP_PRIJS     PRIJS                not null,
   constraint PK_PRIJSGROEP primary key (PRIJSGROEP_PRIJSKLASSE)
)
go

/*==============================================================*/
/* Table: PRODUCT                                               */
/*==============================================================*/
create table PRODUCT (
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   MERKNAAM             NAAM                 not null,
   PRODUCT_TYPE         NAAM                 not null,
   PRODUCT_PRODUCTOMSCHRIJVING BESCHRIJVING         not null,
   PRODUCT_VOORRAAD     AANTAL               not null,
   PRODUCT_VERKOOPPRIJS PRIJS                null,
   constraint PK_PRODUCT primary key (PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Index: PRODUCT_VAN_MERK_FK                                   */
/*==============================================================*/




create nonclustered index PRODUCT_VAN_MERK_FK on PRODUCT (MERKNAAM ASC)
go

/*==============================================================*/
/* Index: PRODUCT_VAN_TYPE_FK                                   */
/*==============================================================*/




create nonclustered index PRODUCT_VAN_TYPE_FK on PRODUCT (PRODUCT_TYPE ASC)
go

/*==============================================================*/
/* Table: PRODUCTTYPE                                           */
/*==============================================================*/
create table PRODUCTTYPE (
   PRODUCT_TYPE         NAAM                 not null,
   constraint PK_PRODUCTTYPE primary key (PRODUCT_TYPE)
)
go

/*==============================================================*/
/* Table: PRODUCT_IN_SET                                        */
/*==============================================================*/
create table PRODUCT_IN_SET (
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   SET_PRODUCT_PRODUCTNR PRODUCTNUMMER        not null,
   PRODUCT_IN_SET_AANTAL AANTAL               not null,
   constraint PK_PRODUCT_IN_SET primary key (PRODUCT_PRODUCTNR, SET_PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Index: PRODUCT_IN_SET_ONDERDEEL_FK                           */
/*==============================================================*/




create nonclustered index PRODUCT_IN_SET_ONDERDEEL_FK on PRODUCT_IN_SET (PRODUCT_PRODUCTNR ASC)
go

/*==============================================================*/
/* Index: ONDERDEEL_ZIT_IN_SET_FK                               */
/*==============================================================*/




create nonclustered index ONDERDEEL_ZIT_IN_SET_FK on PRODUCT_IN_SET (SET_PRODUCT_PRODUCTNR ASC)
go

/*==============================================================*/
/* Table: SETPRODUCT                                            */
/*==============================================================*/
create table SETPRODUCT (
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   SETPRODUCT_VERPAKKING BESCHRIJVING         not null,
   constraint PK_SETPRODUCT primary key (PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Table: TECHNIEK                                              */
/*==============================================================*/
create table TECHNIEK (
   TECHNIEK_TECHNIEKNAAM NAAM                 not null,
   constraint PK_TECHNIEK primary key (TECHNIEK_TECHNIEKNAAM)
)
go

/*==============================================================*/
/* Table: TRANSPARANTIE                                         */
/*==============================================================*/
create table TRANSPARANTIE (
   TRANSPARANTIE_WAARDE NAAM                 not null,
   constraint PK_TRANSPARANTIE primary key (TRANSPARANTIE_WAARDE)
)
go

/*==============================================================*/
/* Table: VERDUNNER                                             */
/*==============================================================*/
create table VERDUNNER (
   VERFPRODUCT_VERDUNNER NAAM                 not null,
   constraint PK_VERDUNNER primary key (VERFPRODUCT_VERDUNNER)
)
go

/*==============================================================*/
/* Table: VERFPRODUCT                                           */
/*==============================================================*/
create table VERFPRODUCT (
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   KLEUR_KLEURCODE      KLEURCODE            not null,
   TRANSPARANTIE_WAARDE NAAM                 not null,
   PRIJSGROEP_PRIJSKLASSE NUMMER               not null,
   VERFPRODUCT_VERDUNNER NAAM                 not null,
   VERFPRODUCT_INHOUD   BESCHRIJVING         not null,
   constraint PK_VERFPRODUCT primary key (PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Index: KLEUR_VAN_VERFPRODUCT_FK                              */
/*==============================================================*/




create nonclustered index KLEUR_VAN_VERFPRODUCT_FK on VERFPRODUCT (KLEUR_KLEURCODE ASC)
go

/*==============================================================*/
/* Index: VERFPRODUCT_TRANSPARANTIE_FK                          */
/*==============================================================*/




create nonclustered index VERFPRODUCT_TRANSPARANTIE_FK on VERFPRODUCT (TRANSPARANTIE_WAARDE ASC)
go

/*==============================================================*/
/* Index: PRIJSGROEP_VAN_VERFPRODUCT_FK                         */
/*==============================================================*/




create nonclustered index PRIJSGROEP_VAN_VERFPRODUCT_FK on VERFPRODUCT (PRIJSGROEP_PRIJSKLASSE ASC)
go

/*==============================================================*/
/* Index: VERFPRODUCT_VERDUNNER_FK                              */
/*==============================================================*/




create nonclustered index VERFPRODUCT_VERDUNNER_FK on VERFPRODUCT (VERFPRODUCT_VERDUNNER ASC)
go

/*==============================================================*/
/* Table: VERFPRODUCT_EIGENSCHAP                                */
/*==============================================================*/
create table VERFPRODUCT_EIGENSCHAP (
   EIGENSCHAP_EIGENSCHAPCODE EIGENSCHAPCODE       not null,
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   constraint PK_VERFPRODUCT_EIGENSCHAP primary key (EIGENSCHAP_EIGENSCHAPCODE, PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Index: VERFPRODUCT_EIGENSCHAP_FK                             */
/*==============================================================*/




create nonclustered index VERFPRODUCT_EIGENSCHAP_FK on VERFPRODUCT_EIGENSCHAP (EIGENSCHAP_EIGENSCHAPCODE ASC)
go

/*==============================================================*/
/* Index: VERFPRODUCT_EIGENSCHAP2_FK                            */
/*==============================================================*/




create nonclustered index VERFPRODUCT_EIGENSCHAP2_FK on VERFPRODUCT_EIGENSCHAP (PRODUCT_PRODUCTNR ASC)
go

/*==============================================================*/
/* Table: VERFPRODUCT_WAARSCHUWING                              */
/*==============================================================*/
create table VERFPRODUCT_WAARSCHUWING (
   WAARSCHUWINGSCODE_WAARSCHUWINGSCODE WAARSCHUWINGSCODE    not null,
   PRODUCT_PRODUCTNR    PRODUCTNUMMER        not null,
   constraint PK_VERFPRODUCT_WAARSCHUWING primary key (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE, PRODUCT_PRODUCTNR)
)
go

/*==============================================================*/
/* Index: VERFPRODUCT_WAARSCHUWING_FK                           */
/*==============================================================*/




create nonclustered index VERFPRODUCT_WAARSCHUWING_FK on VERFPRODUCT_WAARSCHUWING (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE ASC)
go

/*==============================================================*/
/* Index: VERFPRODUCT_WAARSCHUWING2_FK                          */
/*==============================================================*/




create nonclustered index VERFPRODUCT_WAARSCHUWING2_FK on VERFPRODUCT_WAARSCHUWING (PRODUCT_PRODUCTNR ASC)
go

/*==============================================================*/
/* Table: WAARSCHUWINGSCODE                                     */
/*==============================================================*/
create table WAARSCHUWINGSCODE (
   WAARSCHUWINGSCODE_WAARSCHUWINGSCODE WAARSCHUWINGSCODE    not null,
   WAARSCHUWINGSCODE_BESCHRIJVING BESCHRIJVING         not null,
   constraint PK_WAARSCHUWINGSCODE primary key (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE)
)
go

/*==============================================================*/
/* Table: WAARSCHUWING_VAN_PIGMENT                              */
/*==============================================================*/
create table WAARSCHUWING_VAN_PIGMENT (
   PIGMENTNAAM          NAAM                 not null,
   WAARSCHUWINGSCODE_WAARSCHUWINGSCODE WAARSCHUWINGSCODE    not null,
   constraint PK_WAARSCHUWING_VAN_PIGMENT primary key (PIGMENTNAAM, WAARSCHUWINGSCODE_WAARSCHUWINGSCODE)
)
go

/*==============================================================*/
/* Index: WAARSCHUWING_VAN_PIGMENT_FK                           */
/*==============================================================*/




create nonclustered index WAARSCHUWING_VAN_PIGMENT_FK on WAARSCHUWING_VAN_PIGMENT (PIGMENTNAAM ASC)
go

/*==============================================================*/
/* Index: WAARSCHUWING_VAN_PIGMENT2_FK                          */
/*==============================================================*/




create nonclustered index WAARSCHUWING_VAN_PIGMENT2_FK on WAARSCHUWING_VAN_PIGMENT (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE ASC)
go

alter table FRAMINGORDER
   add constraint FK_FRAMINGO_FRAMINGOR_GLAS foreign key (GLAS_GLASSOORT)
      references GLAS (GLAS_GLASSOORT)
go

alter table FRAMINGORDER
   add constraint FK_FRAMINGO_FRAMINGOR_KUNSTWER foreign key (KUNSTWERKSOORT_SOORTNAAM)
      references KUNSTWERKSOORT (KUNSTWERKSOORT_SOORTNAAM)
go

alter table FRAMINGORDER
   add constraint FK_FRAMINGO_FRAMINGOR_FRAMER foreign key (FRAMER_FRAMERID)
      references FRAMER (FRAMER_FRAMERID)
go

alter table FRAMINGORDER
   add constraint FK_FRAMINGO_LIJST_IN__LIJST foreign key (LIJST_LIJSTNUMMER)
      references LIJST (LIJST_LIJSTNUMMER)
go

alter table FRAMINGORDERMETPASSEPARTOUT
   add constraint FK_FRAMINGO_IS_EEN_FR_FRAMINGO foreign key (FRAMINGORDER_ORDERNUMMER)
      references FRAMINGORDER (FRAMINGORDER_ORDERNUMMER)
go

alter table FRAMINGORDERMETPASSEPARTOUT
   add constraint FK_FRAMINGO_PASSEPART_PASSEPAR foreign key (PASSEPARTOUT_PASSEPARTOUTNUMMER)
      references PASSEPARTOUT (PASSEPARTOUT_PASSEPARTOUTNUMMER)
go

alter table KLEUR_GEMAAKT_VAN_PIGMENT
   add constraint FK_KLEUR_GE_KLEUR_GEM_PIGMENT foreign key (PIGMENTNAAM)
      references PIGMENT (PIGMENTNAAM)
go

alter table KLEUR_GEMAAKT_VAN_PIGMENT
   add constraint FK_KLEUR_GE_KLEUR_GEM_KLEUR foreign key (KLEUR_KLEURCODE)
      references KLEUR (KLEUR_KLEURCODE)
go

alter table KWASTPRODUCT
   add constraint FK_KWASTPRO_IS_EEN_PR_PRODUCT foreign key (PRODUCT_PRODUCTNR)
      references PRODUCT (PRODUCT_PRODUCTNR)
go

alter table KWASTPRODUCT
   add constraint FK_KWASTPRO_KWASTPROD_MATERIAA foreign key (MATERIAAL_MATERIAALNAAM)
      references MATERIAAL (MATERIAAL_MATERIAALNAAM)
go

alter table KWASTPRODUCT
   add constraint FK_KWASTPRO_PRIJSGROE_PRIJSGRO foreign key (PRIJSGROEP_PRIJSKLASSE)
      references PRIJSGROEP (PRIJSGROEP_PRIJSKLASSE)
go

alter table KWASTPRODUCT_TECHNIEK
   add constraint FK_KWASTPRO_KWASTPROD_TECHNIEK foreign key (TECHNIEK_TECHNIEKNAAM)
      references TECHNIEK (TECHNIEK_TECHNIEKNAAM)
go

alter table KWASTPRODUCT_TECHNIEK
   add constraint FK_KWASTPRO_KWASTPROD_KWASTPRO foreign key (PRODUCT_PRODUCTNR)
      references KWASTPRODUCT (PRODUCT_PRODUCTNR)
go

alter table LIJST
   add constraint FK_LIJST_LIJSTDOOR_MERK foreign key (MERKNAAM)
      references MERK (MERKNAAM)
go

alter table LIJST
   add constraint FK_LIJST_TYPE_VAN__LIJSTTYP foreign key (LIJSTTYPE_TYPENUMMER)
      references LIJSTTYPE (LIJSTTYPE_TYPENUMMER)
go

alter table PASSEPARTOUT
   add constraint FK_PASSEPAR_PASSEPART_PAPIERSO foreign key (PAPIERSOORT_PAPIERNAAM)
      references PAPIERSOORT (PAPIERSOORT_PAPIERNAAM)
go

alter table PASSEPARTOUT
   add constraint FK_PASSEPAR_PASSEPART_PASSEPAR foreign key (PASSEPARTOUTSOORT_SOORTNAAM)
      references PASSEPARTOUTSOORT (PASSEPARTOUTSOORT_SOORTNAAM)
go

alter table PRODUCT
   add constraint FK_PRODUCT_PRODUCT_V_MERK foreign key (MERKNAAM)
      references MERK (MERKNAAM)
go

alter table PRODUCT
   add constraint FK_PRODUCT_PRODUCT_V_PRODUCTT foreign key (PRODUCT_TYPE)
      references PRODUCTTYPE (PRODUCT_TYPE)
go

alter table PRODUCT_IN_SET
   add constraint FK_PRODUCT__ONDERDEEL_SETPRODU foreign key (SET_PRODUCT_PRODUCTNR)
      references SETPRODUCT (PRODUCT_PRODUCTNR)
go

alter table PRODUCT_IN_SET
   add constraint FK_PRODUCT__PRODUCT_I_PRODUCT foreign key (PRODUCT_PRODUCTNR)
      references PRODUCT (PRODUCT_PRODUCTNR)
go

alter table SETPRODUCT
   add constraint FK_SETPRODU_IS_EEN_PR_PRODUCT foreign key (PRODUCT_PRODUCTNR)
      references PRODUCT (PRODUCT_PRODUCTNR)
go

alter table VERFPRODUCT
   add constraint FK_VERFPROD_IS_EEN_PR_PRODUCT foreign key (PRODUCT_PRODUCTNR)
      references PRODUCT (PRODUCT_PRODUCTNR)
go

alter table VERFPRODUCT
   add constraint FK_VERFPROD_KLEUR_VAN_KLEUR foreign key (KLEUR_KLEURCODE)
      references KLEUR (KLEUR_KLEURCODE)
go

alter table VERFPRODUCT
   add constraint FK_VERFPROD_PRIJSGROE_PRIJSGRO foreign key (PRIJSGROEP_PRIJSKLASSE)
      references PRIJSGROEP (PRIJSGROEP_PRIJSKLASSE)
go

alter table VERFPRODUCT
   add constraint FK_VERFPROD_VERFPRODU_TRANSPAR foreign key (TRANSPARANTIE_WAARDE)
      references TRANSPARANTIE (TRANSPARANTIE_WAARDE)
go

alter table VERFPRODUCT
   add constraint FK_VERFPROD_VERFPRODU_VERDUNNE foreign key (VERFPRODUCT_VERDUNNER)
      references VERDUNNER (VERFPRODUCT_VERDUNNER)
go

alter table VERFPRODUCT_EIGENSCHAP
   add constraint FK_VERFPROD_VERFPRODU_EIGENSCH foreign key (EIGENSCHAP_EIGENSCHAPCODE)
      references EIGENSCHAP (EIGENSCHAP_EIGENSCHAPCODE)
go

alter table VERFPRODUCT_EIGENSCHAP
   add constraint FK_VERFPROD_VERFPRODU_VERFPROD foreign key (PRODUCT_PRODUCTNR)
      references VERFPRODUCT (PRODUCT_PRODUCTNR)
go

alter table VERFPRODUCT_WAARSCHUWING
   add constraint FK_VERFPROD_VERFPRODU_WAARSCHU foreign key (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE)
      references WAARSCHUWINGSCODE (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE)
go

alter table VERFPRODUCT_WAARSCHUWING
   add constraint FK_VERFPRODWAARSHUWING_VERFPRODU_VERFPROD foreign key (PRODUCT_PRODUCTNR)
      references VERFPRODUCT (PRODUCT_PRODUCTNR)
go

alter table WAARSCHUWING_VAN_PIGMENT
   add constraint FK_WAARSCHU_WAARSCHUW_PIGMENT foreign key (PIGMENTNAAM)
      references PIGMENT (PIGMENTNAAM)
go

alter table WAARSCHUWING_VAN_PIGMENT
   add constraint FK_WAARSCHU_WAARSCHUW_WAARSCHU foreign key (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE)
      references WAARSCHUWINGSCODE (WAARSCHUWINGSCODE_WAARSCHUWINGSCODE)
go

