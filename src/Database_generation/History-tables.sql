-- History tables

USE
Artsupplies
GO

CREATE
OR
ALTER PROCEDURE Create_Table_History
    AS
BEGIN
    DECLARE
@Trancounter INT;
    SET
@Trancounter = @@TRANCOUNT;
    IF
@Trancounter > 0
        SAVE TRANSACTION Proceduresave;
ELSE
BEGIN
TRANSACTION;


BEGIN TRY
IF NOT EXISTS(SELECT 1
                      FROM Information_Schema.Schemata
                      WHERE Schema_Name = N'Hist')
            EXEC ('CREATE SCHEMA [hist]')


        DECLARE
@Amountoftables INT = (SELECT COUNT(*) FROM Information_Schema.Tables WHERE Table_Schema = 'dbo')
        DECLARE
@Currentrow INT = 0
        WHILE @Currentrow < @Amountoftables
BEGIN
                SET
@Currentrow = @Currentrow + 1
                DECLARE
@Tablename VARCHAR(255) = (SELECT TOP 1 Table_Name
                                                   FROM (SELECT Table_Name, ROW_NUMBER() OVER (ORDER BY Table_Name DESC) AS Row
                                                         FROM Information_Schema.Tables
                                                         WHERE Table_Schema = 'dbo') AS T
                                                   WHERE T.Row = @Currentrow)
                EXEC Create_Hist_Table @Tablename = @Tablename, @Origintablename = 'dbo'
                EXEC Create_Hist_Triggers @Tablename = @Tablename, @Originschemaname = 'dbo'
END

        IF
@Trancounter = 0
            COMMIT TRANSACTION;
END TRY
BEGIN CATCH
IF @Trancounter = 0
            ROLLBACK TRANSACTION;
ELSE
            IF XACT_STATE() <> -1
                ROLLBACK TRANSACTION Proceduresave;
                ;
        THROW
END CATCH
END
GO

--src generation for empty tables
CREATE
OR
ALTER PROCEDURE Create_Hist_Table(@Tablename SYSNAME, @Origintablename SYSNAME)
    AS
BEGIN
    DECLARE
@Trancounter INT;
    SET
@Trancounter = @@TRANCOUNT;
    IF
@Trancounter > 0
        SAVE TRANSACTION Proceduresave;
ELSE
BEGIN
TRANSACTION;

BEGIN TRY
exec ('DROP TABLE IF EXISTS Hist.'+@Tablename)
        DECLARE
@Amountofcolumns INT = (SELECT COUNT(*)
                                        FROM Information_Schema.Columns
                                        WHERE Table_Name = @Tablename
                                          AND Table_Schema = @Origintablename)
        DECLARE
@Currentrow INT = 0
        DECLARE
@Sql NVARCHAR(MAX) = ''
        WHILE @Currentrow < @Amountofcolumns
BEGIN
                SET
@Currentrow = @Currentrow + 1
                DECLARE
@Columname VARCHAR(255) = (SELECT TOP 1 Column_Name
                                                   FROM (
                                                            SELECT Column_Name, ROW_NUMBER() OVER (ORDER BY Column_Name) AS Row
                                                            FROM Information_Schema.Columns
                                                            WHERE Table_Schema = @Origintablename
                                                              AND Table_Name = @Tablename) AS C
                                                   WHERE C.Row = @Currentrow)

SELECT @Sql = @Sql +
              'ALTER TABLE Hist.' + Table_Name
    + ' ADD ' + @Columname + ' ' + Data_Type
    + IIF(Data_Type = 'varchar', '(' + CAST(Character_Maximum_Length AS VARCHAR) + ') ', ' ')
    + IIF(Data_Type = 'numeric',
          '(' + CAST(Numeric_Precision AS VARCHAR) + ',' + CAST(Numeric_Scale AS VARCHAR) +
          ') ',
          ' ')
    + IIF(Is_Nullable = 'NO', 'NOT NULL', 'NULL') + ';'
FROM Information_Schema.Columns
WHERE Table_Schema = @Origintablename
  AND Table_Name = @Tablename
  AND Column_Name = @Columname
END

        DECLARE
@Pkcolumns NVARCHAR(255)= (SELECT 'timestamp,' + STRING_AGG(C.Column_Name, ',')
                                           FROM Information_Schema.Table_Constraints T
                                                    JOIN Information_Schema.Constraint_Column_Usage C
                                                         ON C.Constraint_Name = T.Constraint_Name
                                           WHERE C.Table_Name = @Tablename
                                             AND T.Constraint_Type = 'PRIMARY KEY')

        DECLARE
@Pkcommand VARCHAR(255)= 'ALTER TABLE Hist.' + @Tablename + ' ADD CONSTRAINT PK_Hist_' + @Tablename +
                                         ' PRIMARY KEY(' + @Pkcolumns + ', action)'


        EXEC ('CREATE TABLE Hist.' + @Tablename + '(timestamp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, username sysname NOT NULL DEFAULT CURRENT_USER, action Varchar(255) NOT NULL)')
        EXEC (@Sql)
        EXEC (@Pkcommand)

        IF @Trancounter = 0
            COMMIT TRANSACTION;
END TRY
BEGIN CATCH
IF @Trancounter = 0
            ROLLBACK TRANSACTION;
ELSE
            IF XACT_STATE() <> -1
                ROLLBACK TRANSACTION Proceduresave;
                ;
        THROW
END CATCH
END
GO

--src generation for triggers
CREATE
OR
ALTER PROCEDURE Create_Hist_Triggers(@Tablename SYSNAME, @Originschemaname SYSNAME) AS
BEGIN
    DECLARE
@Trancounter INT;
    SET
@Trancounter = @@TRANCOUNT;
    IF
@Trancounter > 0
        SAVE TRANSACTION Proceduresave;

ELSE
BEGIN
TRANSACTION;

BEGIN TRY
        DECLARE
@Columns VARCHAR(MAX) = (SELECT STRING_AGG(Column_Name, ', ')
                                         FROM Information_Schema.Columns
                                         WHERE Table_Name = @Tablename
                                           AND Table_Schema = @Originschemaname)
        declare @begin varchar(6) = 'BEGIN ';
        declare @insertIntoHist varchar(17) = 'INSERT INTO Hist.';
        declare @select varchar(7) = 'SELECT ';
        EXEC (
            'CREATE OR ALTER TRIGGER Tr_' + @Tablename + '_Fillhistory ON '+ @Originschemaname + '.'+ @Tablename + ' ' +
            'AFTER INSERT, UPDATE, DELETE ' +
            'AS BEGIN ' +
            'IF (EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED))' +
            @begin+
            @insertIntoHist+ @Tablename + '(' + @Columns + ', action) ' +
            @select + @Columns + ', ''BEFORE UPDATE'' FROM Deleted UNION SELECT ' + @Columns + ', ''AFTER UPDATE'' FROM Inserted ' +
            'END '+

            'IF (EXISTS(SELECT 1 FROM INSERTED) AND NOT EXISTS(SELECT 1 FROM DELETED))' +
            @begin +
            @insertIntoHist + @Tablename + '(' + @Columns + ', action) ' +
            @select + @Columns + ', ''INSERT'' FROM Inserted ' +
            'END '+

            'IF (NOT EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED))' +
            @begin +
            @insertIntoHist + @Tablename + '(' + @Columns + ', action) ' +
            @select + @Columns + ', ''DELETE'' FROM Deleted ' +
            'END '+
            'END'
            )
        IF @Trancounter = 0
            COMMIT TRANSACTION;
END TRY
BEGIN CATCH
IF @Trancounter = 0
            ROLLBACK TRANSACTION;
ELSE
            IF XACT_STATE() <> -1
                ROLLBACK TRANSACTION Proceduresave;
                ;
        THROW
END CATCH

END
GO

print 'create_table_history'
EXEC Create_Table_History