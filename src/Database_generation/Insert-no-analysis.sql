USE Artsupplies
SET NOCOUNT ON

DELETE
FROM Framingordermetpassepartout
DELETE
FROM Framingorder

DELETE
FROM Lijst
DELETE
FROM Framer
DELETE
FROM Passepartout
DELETE
FROM Lijsttype
DELETE
FROM Glas
DELETE
FROM Passepartoutsoort
DELETE
FROM Papiersoort
DELETE
FROM Kunstwerksoort

DELETE
FROM Kleur_Gemaakt_Van_Pigment
DELETE
FROM Waarschuwing_Van_Pigment
DELETE
FROM Verfproduct_Waarschuwing
DELETE
FROM Waarschuwingscode
DELETE
FROM Pigment
DELETE
FROM Kwastproduct_Techniek
DELETE
FROM Product_In_Set
DELETE
FROM Techniek
DELETE
FROM Kwastproduct
DELETE
FROM VERFPRODUCT_EIGENSCHAP
DELETE
FROM Verfproduct
DELETE
FROM Setproduct
DELETE
FROM Eigenschap


DELETE
FROM Product
DELETE
FROM Producttype
DELETE
FROM Kleur
DELETE
FROM Prijsgroep
DELETE
FROM Materiaal
DELETE
FROM Merk
DELETE
FROM Transparantie
DELETE
FROM Verdunner


INSERT
INTO Merk (Merknaam)
VALUES ('Izio');
INSERT
INTO Merk (Merknaam)
VALUES ('Ainyx');
INSERT
INTO Merk (Merknaam)
VALUES ('Aimbu');
INSERT
INTO Merk (Merknaam)
VALUES ('Youbridge');
INSERT
INTO Merk (Merknaam)
VALUES ('Agimba');
INSERT
INTO Merk (Merknaam)
VALUES ('Thoughtblab');
INSERT
INTO Merk (Merknaam)
VALUES ('Yacero');
INSERT
INTO Merk (Merknaam)
VALUES ('Thoughtstorm');
INSERT
INTO Merk (Merknaam)
VALUES ('Riffpath');
INSERT
INTO Merk (Merknaam)
VALUES ('Wikido');
INSERT
INTO Merk (Merknaam)
VALUES ('Jaxbean');
INSERT
INTO Merk (Merknaam)
VALUES ('Brightdog');
INSERT
INTO Merk (Merknaam)
VALUES ('Devshare');
INSERT
INTO Merk (Merknaam)
VALUES ('Quatz');
INSERT
INTO Merk (Merknaam)
VALUES ('Skibox');
INSERT
INTO Merk (Merknaam)
VALUES ('Voonyx');
INSERT
INTO Merk (Merknaam)
VALUES ('Nlounge');
INSERT
INTO Merk (Merknaam)
VALUES ('Tagcat');
INSERT
INTO Merk (Merknaam)
VALUES ('Oyondu');
INSERT
INTO Merk (Merknaam)
VALUES ('Avaveo');
INSERT
INTO Merk (Merknaam)
VALUES ('Demimbu');
INSERT
INTO Merk (Merknaam)
VALUES ('Talane');
INSERT
INTO Merk (Merknaam)
VALUES ('Shufflester');
INSERT
INTO Merk (Merknaam)
VALUES ('Chatterpoint');
INSERT
INTO Merk (Merknaam)
VALUES ('Eadel');
INSERT
INTO Merk (Merknaam)
VALUES ('Buzzshare');
INSERT
INTO Merk (Merknaam)
VALUES ('Quinu');
INSERT
INTO Merk (Merknaam)
VALUES ('Meembee');
INSERT
INTO Merk (Merknaam)
VALUES ('Quaxo');
INSERT
INTO Merk (Merknaam)
VALUES ('Skyvu');
INSERT
INTO Merk (Merknaam)
VALUES ('Npath');
INSERT
INTO Merk (Merknaam)
VALUES ('Eazzy');
INSERT
INTO Merk (Merknaam)
VALUES ('Wikivu');
INSERT
INTO Merk (Merknaam)
VALUES ('Mynte');
INSERT
INTO Merk (Merknaam)
VALUES ('Voonix');
INSERT
INTO Merk (Merknaam)
VALUES ('Edgepulse');
INSERT
INTO Merk (Merknaam)
VALUES ('Edgeblab');
INSERT
INTO Merk (Merknaam)
VALUES ('Jazzy');
INSERT
INTO Merk (Merknaam)
VALUES ('Edgetag');
INSERT
INTO Merk (Merknaam)
VALUES ('Babbleopia');
INSERT
INTO Merk (Merknaam)
VALUES ('Cogilith');
INSERT
INTO Merk (Merknaam)
VALUES ('Yadel');
INSERT
INTO Merk (Merknaam)
VALUES ('Kazio');

INSERT
INTO Producttype (Product_Type)
VALUES ('VERF');

INSERT
INTO Producttype (Product_Type)
VALUES ('SET');

INSERT
INTO Producttype (Product_Type)
VALUES ('KWAST');
INSERT
INTO Producttype (Product_Type)
VALUES ('PRODUCT');

INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Madagascar hawk owl');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Lesser double-collared sunbird');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Heron, yellow-crowned night');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Squirrel, indian giant');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Three-banded plover');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Grey lourie');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Bee-eater, carmine');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Dik, kirk''s dik');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Lemur, lesser mouse');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('African pied wagtail');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Woodpecker, downy');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Squirrel, pine');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Reedbuck, bohor');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Two-banded monitor');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Kelp gull');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Rat, arboral spiny');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Waterbuck, defassa');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Coatimundi, white-nosed');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Eastern dwarf mongoose');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Turkey vulture');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Brush-tailed bettong');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Green heron');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Vulture, black');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Pied avocet');
INSERT
INTO Materiaal (Materiaal_Materiaalnaam)
VALUES ('Deer, mule');

INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Macaque, pig-tailed');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Squirrel, grey-footed');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Ring-tailed lemur');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Marshbird, brown and yellow');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Purple moorhen');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Bleu, red-cheeked cordon');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Grenadier, common');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Pigeon, feral rock');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Silver gull');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Tsessebe');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Giant otter');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Coatimundi, white-nosed');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Pale white-eye');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Tree porcupine');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Black-backed jackal');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Cat, jungle');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Anaconda (unidentified)');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Bison, american');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Pronghorn');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Carpet python');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Violet-eared waxbill');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Bat-eared fox');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Black-tailed prairie dog');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Jabiru stork');
INSERT
INTO Papiersoort (Papiersoort_Papiernaam)
VALUES ('Elk, Wapiti');
GO


INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (1, 'Avaveo', 'SET', 'Curabitur at ipsum ac tellus semper interdum.', 2, 130);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (2, 'Avaveo', 'KWAST', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', 22, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (3, 'Avaveo', 'VERF', 'Aliquam sit amet diam in magna bibendum imperdiet.', 26, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (4, 'Avaveo', 'SET', 'Cum sociis natoque penatibuAenean auctor gravida sem.', 6, 249);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (5, 'Avaveo', 'KWAST', 'Morbi quis tortor id nu', 8, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (6, 'Avaveo', 'VERF', 'Vestibulum ac est laciniaien arcu sed augue. Aliquam erat volutpat. In congue.', 4, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (7, 'Avaveo', 'SET', 'Aenean lectus. Pellentesqu ', 8, 35);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (8, 'Avaveo', 'KWAST', 'Mauris ullamcorper purust.', 28, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (9, 'Avaveo', 'VERF', 'Cras pellentesque volutpat', 27, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (10, 'Avaveo', 'SET', 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.', 11, 68);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (11, 'Avaveo', 'KWAST', 'n sit amet justo. Morbi ut odio.', 21, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (12, 'Chatterpoint', 'VERF', 'Praesent blandit. ligula.', 13, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (13, 'Chatterpoint', 'SET', 'Vivamus vestibulu', 3, 204);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (14, 'Chatterpoint', 'KWAST', 'Nunc rhoncus du', 27, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (15, 'Chatterpoint', 'VERF', 'Duis faucibu', 14, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (16, 'Chatterpoint', 'SET', 'Cum sociis na', 29, 92);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (17, 'Chatterpoint', 'KWAST', 'Vestibulum .', 20, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (18, 'Chatterpoint', 'VERF', 'Integer non.', 26, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (19, 'Chatterpoint', 'SET', 'Integer non ', 29, 48);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (20, 'Chatterpoint', 'KWAST', 'Nulla ac e', 24, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (21, 'Chatterpoint', 'VERF', 'Vestibulum ', 18, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (22, 'Quaxo', 'SET', 'Integer a nibh. In ', 9, 86);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (23, 'Quaxo', 'KWAST', 'Integer aliquet,', 4, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (24, 'Quaxo', 'VERF', 'Morbi non quam necis dui vel nisl.', 7, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (25, 'Quaxo', 'SET', 'Aliquam quis turpitortor quis turpis. Sed ante.', 13, 171);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (26, 'Quaxo', 'KWAST', 'Quisque id justo', 6, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (27, 'Quaxo', 'VERF', 'Nulla neque liberc est lacinia nisi venenatis tristique.', 16, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (28, 'Quaxo', 'SET', 'Suspendisse ornare consequat lectus.', 24, 259);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (29, 'Quaxo', 'KWAST', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Duis bibendum.',
        16, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (30, 'Quaxo', 'VERF', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.', 23, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (31, 'Quaxo', 'SET', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget', 3, 223);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (32, 'Quaxo', 'KWAST', 'Sed sagittis. Nam congue, risus semper porta volutpat, quamque at nulla.', 18, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (33, 'Quaxo', 'VERF', 'Quisquvel ipsum.', 27, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (34, 'Quaxo', 'SET',
        'Curabitur gravida isi at nibh. In hac habitasse platea dictuvel ipsum. Praesent blandit lacinia erat.', 29,
        87);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (35, 'Quaxo', 'KWAST',
        'Veluctus et ulti volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 24, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (36, 'Talane', 'VERF', 'Etiam faucibus crsus urna. Ut tellus. Nulla ut erat id mau', 2, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (37, 'Talane', 'SET',
        'Quisque id bilia Curae; dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',
        9, 136);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (38, 'Talane', 'KWAST', 'Integer ac nulla tellus.', 2, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (39, 'Talane', 'VERF', 'Quisqutrum, nulla. Nunc purus. Phasellus in felis.', 20, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (40, 'Talane', 'SET', 'Curabit dolor.', 17, 137);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (41, 'Talane', 'KWAST', 'Nulla In hac habitasse platea dictumst.', 23, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (42, 'Talane', 'VERF', 'Praesnt eget, tempus vel, pede. Morbi porttitor lorem id ligula.', 29, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (43, 'Talane', 'SET', 'Suspendisse accumsan tortor quis turpis.', 5, 237);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (44, 'Talane', 'KWAST', 'Fusce posnar sed, nisl. Nunc rhoncus dui vel sem.', 6, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (45, 'Talane', 'VERF', 'Nulla tempus.', 29, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (46, 'Talane', 'SET', 'Morbi quis tortor id nulla ultrices aliquet.', 29, 23);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (47, 'Talane', 'KWAST', 'Crasperdiet et, ', 10, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (48, 'Talane', 'VERF', 'Maecenas trs semper, ', 16, NULL);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (49, 'Talane', 'SET', 'Fuscerdiet, sapi', 11, 278);
INSERT
INTO Product (Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
              Product_Verkoopprijs)
VALUES (50, 'Shufflester', 'KWAST', 'Maecenas pulvinar lobortis est.', 15, NULL);

INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (1, 'Radiated tortoise');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (2, 'Moorhen, purple');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (3, 'Netted rock dragon');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (4, 'Anteater, australian spiny');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (5, 'Waxbill, black-cheeked');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (6, 'Leadbeateri''s ground hornbill');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (7, 'Anteater, australian spiny');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (8, 'Hawk, galapagos');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (9, 'Red-necked phalarope');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (10, 'Sloth, two-toed');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (11, 'Yellow-billed stork');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (12, 'Gray duiker');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (13, 'European red squirrel');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (14, 'Wagtail, african pied');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (15, 'African wild dog');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (16, 'Brown hyena');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (17, 'Goose, egyptian');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (18, 'Heron, black-crowned night');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (19, 'Quoll, spotted-tailed');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (20, 'Beisa oryx');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (21, 'Dove, laughing');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (22, 'Lemur, ring-tailed');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (23, 'Great horned owl');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (24, 'Stanley crane');
INSERT
INTO Lijsttype (Lijsttype_Typenummer, Lijsttype_Soort)
VALUES (25, 'Rose-ringed parakeet');
GO

INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (1, 1, 'Agimba', 2.27, 'Tammar wallaby');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (2, 2, 'Aimbu', 4.96, 'Numbat');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (3, 3, 'Ainyx', 2.58, 'Cape wild cat');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (4, 4, 'Avaveo', 5.75, 'Mountain goat');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (5, 5, 'Babbleopia', 8.94, 'Cattle egret');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (6, 6, 'Brightdog', 7.11, 'Steenbuck');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (7, 7, 'Buzzshare', 3.03, 'Salmon, sockeye');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (8, 8, 'Chatterpoint', 4.08, 'Squirrel, palm');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (9, 9, 'Agimba', 7.44, 'Polar bear');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (10, 10, 'Aimbu', 5.79, 'Goose, canada');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (11, 11, 'Ainyx', 1.84, 'Blue-tongued skink');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (12, 12, 'Avaveo', 3.13, 'Gull, swallow-tail');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (13, 13, 'Babbleopia', 9.15, 'Lapwing (unidentified)');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (14, 14, 'Brightdog', 5.31, 'Pale-throated three-toed sloth');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (15, 15, 'Buzzshare', 9.45, 'Rhinoceros, black');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (16, 16, 'Chatterpoint', 2.11, 'Red-tailed hawk');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (17, 17, 'Agimba', 1.56, 'Baboon, savanna');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (18, 18, 'Aimbu', 8.89, 'Eagle, pallas''s fish');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (19, 19, 'Ainyx', 2.99, 'Weaver, chestnut');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (20, 20, 'Avaveo', 9.58, 'Red-tailed hawk');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (21, 21, 'Babbleopia', 8.08, 'Meerkat, red');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (22, 22, 'Brightdog', 6.20, 'Macaque, bonnet');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (23, 23, 'Buzzshare', 5.81, 'Brolga crane');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (24, 24, 'Chatterpoint', 2.07, 'American woodcock');
INSERT
INTO Lijst (Lijst_Lijstnummer, Lijsttype_Typenummer, Merknaam, Lijst_Prijs, Lijst_Lijstnaam)
VALUES (25, 25, 'Agimba', 2.03, 'Black-faced kangaroo');


INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (1, 'process improvement');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (2, 'Integrated');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (3, 'Multi-layered');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (4, 'forecast');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (5, '3rd generation');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (6, 'Innovative');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (7, 'Extended');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (8, 'Versatile');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (9, 'local');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (10, 'Organized');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (11, 'definition');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (12, 'leading edge');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (13, 'algorithm');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (14, 'zero administration');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (15, 'Extended');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (16, 'Upgradable');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (17, 'Organic');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (18, 'utilisation');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (19, 'Re-contextualized');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (20, 'algorithm');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (21, 'motivating');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (22, 'collaboration');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (23, 'bi-directional');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (24, 'Pre-emptive');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (25, 'firmware');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (26, 'Object-based');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (27, 'interface');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (28, 'installation');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (29, 'actuating');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (30, 'Upgradable');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (31, 'Synchronised');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (32, 'product');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (33, 'value-added');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (34, 'Innovative');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (35, 'tangible');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (36, '24/7');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (37, 'application');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (38, 'interactive');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (39, 'Universal');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (40, 'policy');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (41, 'actuating');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (42, 'protocol');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (43, 'multi-tasking');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (44, 'scalable');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (45, 'responsive');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (46, 'Persevering');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (47, 'Polarised');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (48, 'approach');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (49, 'Managed');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (50, 'secondary');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (51, 'Open-architected');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (52, 'Triple-buffered');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (53, '24 hour');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (54, 'Cloned');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (55, 'Decentralized');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (56, 'radical');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (57, 'disintermediate');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (58, 'zero administration');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (59, 'focus group');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (60, 'Pre-emptive');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (61, 'Organized');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (62, 'demand-driven');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (63, 'website');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (64, 'Persistent');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (65, 'Extended');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (66, 'executive');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (67, 'Future-proofed');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (68, 'Diverse');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (69, 'User-centric');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (70, 'productivity');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (71, 'groupware');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (72, 'throughput');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (73, 'Virtual');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (74, 'Reverse-engineered');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (75, 'empowering');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (76, 'success');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (77, 'Team-oriented');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (78, 'actuating');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (79, 'bottom-line');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (80, 'Future-proofed');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (81, 'incremental');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (82, 'Triple-buffered');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (83, 'time-frame');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (84, 'bandwidth-monitored');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (85, 'orchestration');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (86, 'paradigm');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (87, 'coherent');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (88, 'Synergized');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (89, 'User-friendly');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (90, 'high-level');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (91, 'Organic');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (92, 'algorithm');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (93, 'forecast');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (94, 'Polarised');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (95, 'Decentralized');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (96, 'Reverse-engineered');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (97, 'User-centric');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (98, 'Pre-emptive');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (99, 'forecast');
INSERT
INTO Eigenschap (Eigenschap_Eigenschapcode, Eigenschap_Beschrijving)
VALUES (100, 'Proactive');
GO

INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (1, 'Maighdiln');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (2, 'Cassie');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (3, 'Hoyt');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (4, 'Gerry');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (5, 'Salomo');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (6, 'Prescott');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (7, 'Gretel');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (8, 'Milissent');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (9, 'Dolly');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (10, 'Tiphani');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (11, 'Editha');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (12, 'Guilbert');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (13, 'Syman');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (14, 'Nial');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (15, 'Kittie');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (16, 'Krissy');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (17, 'Glenna');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (18, 'Quintina');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (19, 'Lyell');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (20, 'Savina');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (21, 'Annaliese');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (22, 'Renata');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (23, 'Ruby');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (24, 'Debee');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (25, 'Julita');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (26, 'Brandy');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (27, 'Cassondra');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (28, 'Janith');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (29, 'Germaine');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (30, 'Atlante');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (31, 'Tally');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (32, 'Silvia');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (33, 'Ediva');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (34, 'Carlie');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (35, 'Lilian');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (36, 'Leif');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (37, 'Selle');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (38, 'Adelheid');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (39, 'Yovonnda');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (40, 'Jasun');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (41, 'Penrod');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (42, 'Quinlan');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (43, 'Rosalynd');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (44, 'Atalanta');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (45, 'Humfried');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (46, 'Sasha');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (47, 'Lanita');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (48, 'Jacobo');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (49, 'Ivonne');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (50, 'Sibelle');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (51, 'Granny');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (52, 'Barnebas');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (53, 'Cassie');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (54, 'Jammie');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (55, 'Vitia');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (56, 'Sallyann');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (57, 'Jarad');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (58, 'Jada');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (59, 'Even');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (60, 'Yalonda');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (61, 'Roxana');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (62, 'Jena');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (63, 'Sylas');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (64, 'Avrit');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (65, 'Licha');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (66, 'Noll');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (67, 'Benji');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (68, 'Patton');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (69, 'Freda');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (70, 'Ediva');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (71, 'Carlin');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (72, 'Jeremiah');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (73, 'Wandie');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (74, 'Kenn');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (75, 'Waly');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (76, 'Clerissa');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (77, 'Stesha');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (78, 'Von');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (79, 'Vince');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (80, 'Devonna');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (81, 'Reinold');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (82, 'Callean');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (83, 'Wes');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (84, 'Amandy');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (85, 'Rich');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (86, 'Ave');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (87, 'Elvira');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (88, 'Kristoffer');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (89, 'Pavlov');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (90, 'Cecil');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (91, 'Tann');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (92, 'Christina');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (93, 'Jamal');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (94, 'Michel');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (95, 'Ludwig');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (96, 'Carolee');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (97, 'Ivie');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (98, 'Joey');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (99, 'Ad');
INSERT
INTO Framer (Framer_Framerid, Framer_Naam)
VALUES (100, 'Natassia');


INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Quoll, eastern');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Bear, sloth');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Turkey, common');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Duck, blue');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Hawk, galapagos');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Manatee');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Vulture, lappet-faced');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Red and blue macaw');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Ring-tailed lemur');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Trumpeter, green-winged');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Common boubou shrike');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Genet, small-spotted');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Kangaroo, jungle');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('White-faced tree rat');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Defassa waterbuck');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Red-tailed phascogale');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Tasmanian devil');
INSERT
INTO Glas (Glas_Glassoort)
VALUES ('Lizard, mexican beaded');
GO

INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#835b40', 'Red-winged hawk (unidentified)', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#76756f', 'Ibis, glossy', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#a303b1', 'Squirrel, thirteen-lined', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#b9f8a2', 'European badger', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#d0dbc5', 'Praying mantis (unidentified)', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#344bc2', 'Grey lourie', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#ec64be', 'Kalahari scrub robin', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#67eaca', 'Azara''s zorro', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#df0814', 'Quail, gambel''s', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#53317b', 'Woodcock, american', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#ff6e55', 'Seal, common', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#37d478', 'Sambar', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#bb46fb', 'Lion, galapagos sea', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#d51556', 'Red-headed woodpecker', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#b478d2', 'Malachite kingfisher', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#f95730', 'Red-billed hornbill', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#4932e5', 'Gray rhea', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#e8e3a1', 'Warthog', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#68a306', 'Gorilla, western lowland', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#3a41c4', 'Javanese cormorant', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#922df6', 'Monitor, water', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#bfd025', 'Arctic fox', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#0b1df7', 'Gray duiker', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#5e5ae2', 'Magistrate black colobus', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#78c66f', 'Lizard, collared', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#cac8ba', 'Chestnut weaver', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#b7d8bc', 'Cat, native', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#f5f890', 'Heron, goliath', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#4078e8', 'Emu', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#b4226e', 'Salmon pink bird eater tarantula', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#18ce76', 'Eastern box turtle', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#2fab22', 'Sheep, american bighorn', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#5a2ee4', 'Painted stork', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#16d35a', 'Tern, arctic', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#d2f3ba', 'Prehensile-tailed porcupine', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#cca82a', 'White-browed sparrow weaver', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#879ca6', 'Whale, baleen', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#89bc89', 'Cape starling', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#38e963', 'Bear, american black', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#c9dfa5', 'Dragon, ornate rock', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#47715f', 'Bandicoot, short-nosed', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#21d848', 'Antechinus, brown', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#452dab', 'Little brown bat', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#e12a4a', 'Sheathbill, snowy', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#9a660c', 'Screamer, crested', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#7c812b', 'Spotted deer', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#2bcb7c', 'Secretary bird', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#19830c', 'Coatimundi, ring-tailed', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#02ae42', 'Wallaroo, common', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#7b48a0', 'Agile wallaby', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#117602', 'Springbok', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#cec162', 'Asiatic jackal', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#77f580', 'Saddle-billed stork', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#a7a7cb', 'Opossum, american virginia', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#704fdc', 'Common eland', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#0593ad', 'Kelp gull', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#aaaedd', 'White-browed owl', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#9e2b2c', 'Collared peccary', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#f967e3', 'Iguana, land', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#cdbc3b', 'Mexican beaded lizard', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#8444a4', 'Kookaburra, laughing', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#b3ac8d', 'Goose, canada', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#f7d474', 'Long-finned pilot whale', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#f387a8', 'Little blue penguin', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#2e9eed', 'Agouti', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#e43cfa', 'Chimpanzee', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#bbbe27', 'Egret, cattle', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#70ebaf', 'Wallaby, red-necked', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#140482', 'Monitor lizard (unidentified)', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#8f0c75', 'Ovenbird', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#59d279', 'Wallaby, whip-tailed', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#482a27', 'Porcupine, indian', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#9f092b', 'Puma, south american', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#6f41a8', 'Southern right whale', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#1e7ff3', 'Netted rock dragon', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#d6d1b1', 'Goose, egyptian', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#d413a2', 'Toucan, red-billed', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#4df135', 'Western spotted skunk', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#a26925', 'Hawk-headed parrot', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#2db67a', 'Jackal, asiatic', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#574ac3', 'Caracara, yellow-headed', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#bc728a', 'Dingo', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#542916', 'Crake, african black', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#3fe856', 'Giant armadillo', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#88feb6', 'Snowy owl', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#c087be', 'Lesser mouse lemur', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#81fab4', 'Madagascar hawk owl', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#9ad470', 'Lion, galapagos sea', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#db0b9e', 'Northern fur seal', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#c0bb6a', 'Great egret', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#425c61', 'Colobus, white-mantled', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#32c93f', 'Blue-faced booby', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#97b78f', 'Ground monitor (unidentified)', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#2a4725', 'Vine snake (unidentified)', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#2259e9', 'White-winged tern', 2);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#9db137', 'Possum, pygmy', 3);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#64c9af', 'Flamingo, roseat', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#2c1a4c', 'Cormorant, king', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#07f07a', 'Tiger cat', 1);
INSERT
INTO Kleur (Kleur_Kleurcode, Kleur_Kleuromschrijving, Kleur_Pgwaarde)
VALUES ('#8a07d8', 'Chuckwalla', 3);

INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (1, 2.43);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (2, 4.86);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (3, 7.29);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (4, 9.72);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (5, 12.15);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (6, 14.58);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (7, 17.01);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (8, 19.44);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (9, 21.87);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (10, 24.3);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (11, 26.73);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (12, 29.16);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (13, 31.59);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (14, 34.02);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (15, 36.45);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (16, 38.88);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (17, 41.31);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (18, 43.74);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (19, 46.17);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (20, 48.6);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (21, 51.03);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (22, 53.46);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (23, 55.89);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (24, 58.32);
INSERT
INTO Prijsgroep (Prijsgroep_Prijsklasse, Prijsgroep_Prijs)
VALUES (25, 60.75);

INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (2, 'African pied wagtail', 3, 'Rond', 2);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (5, 'African pied wagtail', 1, 'Ovaal', 3);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (8, 'African pied wagtail', 3, 'Rond', 2);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (11, 'African pied wagtail', 2, 'Ovaal', 7);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (14, 'African pied wagtail', 1, 'Rond', 7);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (17, 'African pied wagtail', 2, 'Ovaal', 5);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (20, 'African pied wagtail', 2, 'Rond', 5);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (23, 'African pied wagtail', 1, 'Ovaal', 5);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (26, 'African pied wagtail', 1, 'Rond', 4);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (29, 'African pied wagtail', 3, 'Ovaal', 4);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (32, 'African pied wagtail', 1, 'Rond', 3);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (35, 'African pied wagtail', 2, 'Ovaal', 3);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (38, 'African pied wagtail', 3, 'Rond', 1);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (41, 'African pied wagtail', 2, 'Ovaal', 7);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (44, 'African pied wagtail', 1, 'Rond', 5);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (47, 'African pied wagtail', 1, 'Ovaal', 8);
INSERT
INTO Kwastproduct (Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Vorm, Kwast_Maat)
VALUES (50, 'Pied avocet', 1, 'Rond', 1);

INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Lesser double-collared sunbird');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Bandicoot, short-nosed');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Jungle cat');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Asian lion');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Spotted hyena');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Badger, european');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Mynah, common');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Macaw, blue and yellow');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Owl, snowy');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Cape Barren goose');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Tern, white-winged');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Fat-tailed dunnart');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Crested bunting');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Stork, painted');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Green-winged trumpeter');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('African porcupine');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Eagle, golden');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Brown brocket');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Zorro, azara''s');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Leopard');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Quail, gambel''s');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Bleu, blue-breasted cordon');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Parakeet, rose-ringed');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('European red squirrel');
INSERT
INTO Pigment (Pigmentnaam)
VALUES ('Lappet-faced vulture');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('circuit');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('systemic');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('projection');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('multi-tasking');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('collaboration');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('function');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('hierarchy');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('interface');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('coherent');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('Enhanced');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('Seamless');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('Public-key');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('well-modulated');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('adapter');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('Secured');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('fresh-thinking');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('emulation');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('product');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('portal');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('cohesive');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('benchmark');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('Open-architected');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('multimedia');
INSERT
INTO Techniek (Techniek_Technieknaam)
VALUES ('transitional');


INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 2);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 5);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 8);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 11);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 14);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 17);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 20);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 23);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 26);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('benchmark', 29);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 29);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 32);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 35);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 38);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 41);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 44);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 47);
INSERT
INTO Kwastproduct_Techniek (Techniek_Technieknaam, Product_Productnr)
VALUES ('transitional', 50);

INSERT
INTO Kunstwerksoort (Kunstwerksoort_Soortnaam)
VALUES ('Tekening');
INSERT
INTO Kunstwerksoort (Kunstwerksoort_Soortnaam)
VALUES ('Schilderij');

INSERT
INTO Transparantie(Transparantie_Waarde)
VALUES ('HALF'),
       ('VOLLEDIG'),
       ('ONDOORZICHTIG');

INSERT
INTO Verdunner(Verfproduct_Verdunner)
VALUES ('WASBENZINE');
INSERT
INTO Verdunner(Verfproduct_Verdunner)
VALUES ('THINNER');
INSERT
INTO Verdunner(Verfproduct_Verdunner)
VALUES ('TERPETINE');
INSERT
INTO Verdunner(Verfproduct_Verdunner)
VALUES ('AROMAATVRIJE-TERPETINE');
INSERT
INTO Verdunner(Verfproduct_Verdunner)
VALUES ('WATER');



INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (3, '#2259e9', 'HALF', 18, '12 x 5ml tubes', 'WASBENZINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (6, '#2259e9', 'VOLLEDIG', 14, '9x 10 ml tubes', 'THINNER');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (9, '#2259e9', 'VOLLEDIG', 16, '3x 15ml tubes', 'THINNER');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (12, '#02ae42', 'VOLLEDIG', 19, '9x 10 ml tubes', 'WASBENZINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (15, '#2259e9', 'ONDOORZICHTIG', 10, '12 x 15ml tubes', 'WATER');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (18, '#2259e9', 'VOLLEDIG', 17, '12 x 5ml tubes', 'WASBENZINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (21, '#2259e9', 'ONDOORZICHTIG', 11, '12 x 15ml tubes', 'TERPETINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (24, '#2e9eed', 'ONDOORZICHTIG', 8, '12 x 10 ml tubes', 'TERPETINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (27, '#02ae42', 'ONDOORZICHTIG', 17, '9x 10 ml tubes', 'WATER');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (30, '#02ae42', 'VOLLEDIG', 2, '12 x 5ml tubes', 'TERPETINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (33, '#02ae42', 'HALF', 3, '12 x 10 ml tubes', 'AROMAATVRIJE-TERPETINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (36, '#2e9eed', 'HALF', 20, '3x 15ml tubes', 'TERPETINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (39, '#2259e9', 'VOLLEDIG', 3, '12 x 10 ml tubes', 'AROMAATVRIJE-TERPETINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (42, '#2e9eed', 'HALF', 17, '12 x 10 ml tubes', 'WASBENZINE');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (45, '#2259e9', 'VOLLEDIG', 15, '12 x 15ml tubes', 'THINNER');
INSERT
INTO Verfproduct (Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                  Verfproduct_Inhoud, Verfproduct_Verdunner)
VALUES (48, '#02ae42', 'ONDOORZICHTIG', 5, '9x 10 ml tubes', 'THINNER');

INSERT
INTO Waarschuwingscode (Waarschuwingscode_Waarschuwingscode, Waarschuwingscode_Beschrijving)
VALUES (N'H411', N'Toxic to aquatic life with long-lasting effects ');

INSERT
INTO Waarschuwingscode (Waarschuwingscode_Waarschuwingscode, Waarschuwingscode_Beschrijving)
VALUES (N'EUH208', N'Contains <name of sensitising substance>. May produce an allergic reaction.');

INSERT
INTO Waarschuwingscode (Waarschuwingscode_Waarschuwingscode, Waarschuwingscode_Beschrijving)
VALUES (N'EUH006', N'Explosive with or without contact with air');
GO


INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (1, 'Hawk, galapagos', 1, 1, 44.4, 48.3, 87.25, '2022-02-12 10:24:50', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (2, 'Hawk, galapagos', 2, 2, 79.5, 94.1, 43.69, '2022-07-28 09:14:48', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (3, 'Hawk, galapagos', 3, 3, 55.1, 69.1, 87.85, '2022-07-23 08:27:58', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (4, 'Hawk, galapagos', 4, 4, 90.6, 7.4, 45.21, '2022-08-10 06:07:48', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (5, 'Hawk, galapagos', 5, 5, 87.4, 88.2, 49.30, '2022-11-07 13:33:02', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (6, 'Hawk, galapagos', 6, 6, 76.7, 76.0, 70.70, '2022-07-19 14:45:24', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (7, 'Hawk, galapagos', 7, 7, 74.5, 87.8, 30.04, '2022-12-03 14:32:08', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (8, 'Hawk, galapagos', 8, 8, 57.6, 31.7, 30.65, '2022-02-19 17:57:57', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (9, 'Hawk, galapagos', 9, 9, 11.9, 93.7, 38.05, '2022-09-24 06:30:06', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (10, 'Hawk, galapagos', 10, 10, 69.5, 25.9, 42.34, '2022-06-03 02:05:06', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (11, 'Hawk, galapagos', 11, 11, 87.6, 7.4, 59.99, '2022-09-18 23:58:10', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (12, 'Quoll, eastern', 12, 12, 2.1, 48.0, 89.57, '2022-03-25 23:01:52', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (13, 'Quoll, eastern', 13, 13, 38.4, 3.6, 85.08, '2022-01-24 11:56:12', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (14, 'Quoll, eastern', 14, 14, 73.9, 66.4, 91.44, '2022-01-18 20:42:42', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (15, 'Quoll, eastern', 15, 15, 7.3, 31.2, 28.20, '2022-06-06 18:47:25', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (16, 'Quoll, eastern', 16, 16, 97.5, 59.6, 36.62, '2022-06-30 12:29:36', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (17, 'Quoll, eastern', 17, 17, 87.5, 25.7, 93.81, '2022-01-01 11:24:32', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (18, 'Quoll, eastern', 18, 18, 61.0, 65.5, 93.35, '2022-06-02 16:25:16', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (19, 'Quoll, eastern', 19, 19, 50.4, 59.0, 53.60, '2022-10-22 21:43:10', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (20, 'Quoll, eastern', 20, 20, 45.7, 5.6, 54.38, '2022-11-09 18:51:10', GETDATE(), 'Tekening');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (21, 'Turkey, common', 21, 21, 72.5, 32.1, 91.73, '2022-03-18 05:04:36', GETDATE(), 'Schilderij');
INSERT
INTO Framingorder (Framingorder_Ordernummer, Glas_Glassoort, Lijst_Lijstnummer, Framer_Framerid,
                   Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk, Framingorder_Prijs,
                   Framingorder_Opleveringsdatum,Framingorder_Aanvraagdatum, Kunstwerksoort_Soortnaam)
VALUES (22, 'Turkey, common', 22, 22, 30.9, 38.7, 33.09, '2022-01-26 13:31:38', GETDATE(), 'Schilderij');
GO

INSERT
INTO Kleur_Gemaakt_Van_Pigment(Pigmentnaam, Kleur_Kleurcode)
SELECT Pigmentnaam, Kleur_Kleurcode
FROM Kleur,
     Pigment
GO

INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH006', 3);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH208', 6);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('H411', 9);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH208', 12);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('H411', 15);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH208', 18);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH208', 21);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH208', 24);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('H411', 27);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('H411', 30);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH208', 33);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH006', 36);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH006', 39);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH006', 42);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('H411', 45);
INSERT
INTO Verfproduct_Waarschuwing (Waarschuwingscode_Waarschuwingscode, Product_Productnr)
VALUES ('EUH208', 48);
GO

INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Southern boubou');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Ring-tailed coatimundi');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Red deer');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Puna ibis');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Tortoise, indian star');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Heron, green-backed');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Greater roadrunner');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Rat, desert kangaroo');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Orca');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Nilgai');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Ostrich');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Swallow-tail gull');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Kite, black');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Brown pelican');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Bulbul, black-eyed');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Bird, red-billed tropic');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Rhea, common');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Crested bunting');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Savannah deer (unidentified)');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Dolphin, striped');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Marmot, yellow-bellied');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Comb duck');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Spoonbill, european');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Fork-tailed drongo');
INSERT
INTO Passepartoutsoort (Passepartoutsoort_Soortnaam)
VALUES ('Asian elephant');


INSERT
INTO Waarschuwing_Van_Pigment (Waarschuwingscode_Waarschuwingscode, Pigmentnaam)
VALUES ('EUH208', 'Bleu, blue-breasted cordon');
INSERT
INTO Waarschuwing_Van_Pigment (Waarschuwingscode_Waarschuwingscode, Pigmentnaam)
VALUES ('EUH006', 'Bleu, blue-breasted cordon');
INSERT
INTO Waarschuwing_Van_Pigment (Waarschuwingscode_Waarschuwingscode, Pigmentnaam)
VALUES ('H411', 'Bleu, blue-breasted cordon');

INSERT
INTO Setproduct(Product_Productnr, Setproduct_Verpakking)
VALUES (1, 'Metalen doos voorzien van scharnierende deksels om als mengpaletten te gebruiken'),
       (4, 'Set product 2'),
       (7, 'Set product 3'),
       (10, 'Set product 4'),
       (13, 'Set product 5'),
       (16, 'Set product 6'),
       (19, 'Set product 7'),
       (22, 'Set product 8'),
       (25, 'Set product 9'),
       (28, 'Set product 10'),
       (31, 'Set product 11'),
       (34, 'Set product 12'),
       (37, 'Set product 13'),
       (40, 'Set product 14'),
       (43, 'Set product 15'),
       (46, 'Set product 16'),
       (49, 'Set product 17')


INSERT
INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
VALUES (15, 1, 5),
       (20, 4, 2),
       (17, 7, 1),
       (20, 10, 1),
       (21, 13, 2)
GO
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (1, 'Pronghorn', 'Southern boubou', 'Yellow', 0.29);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (2, 'Ring-tailed lemur', 'Ring-tailed coatimundi', 'Indigo', 6.91);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (3, 'Giant otter', 'Orca', 'Goldenrod', 7.21);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (4, 'Giant otter', 'Ostrich', 'Yellow', 9.28);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (5, 'Giant otter', 'Southern boubou', 'Teal', 1.35);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (6, 'Purple moorhen', 'Ring-tailed coatimundi', 'Mauv', 7.00);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (7, 'Ring-tailed lemur', 'Asian elephant', 'Fuscia', 2.10);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (8, 'Ring-tailed lemur', 'Fork-tailed drongo', 'Green', 8.23);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (9, 'Bat-eared fox', 'Southern boubou', 'Maroon', 6.68);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (10, 'Giant otter', 'Southern boubou', 'Indigo', 9.43);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (11, 'Giant otter', 'Fork-tailed drongo', 'Purple', 2.09);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (12, 'Bat-eared fox', 'Ostrich', 'Red', 3.67);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (13, 'Pronghorn', 'Fork-tailed drongo', 'Indigo', 4.85);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (14, 'Giant otter', 'Fork-tailed drongo', 'Teal', 6.87);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (15, 'Giant otter', 'Fork-tailed drongo', 'Purple', 5.11);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (16, 'Giant otter', 'Asian elephant', 'Aquamarine', 0.84);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (17, 'Pronghorn', 'Asian elephant', 'Blue', 3.37);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (18, 'Bat-eared fox', 'Asian elephant', 'Puce', 6.51);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (19, 'Giant otter', 'Ostrich', 'Puce', 3.52);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (20, 'Pronghorn', 'Asian elephant', 'Blue', 8.42);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (21, 'Purple moorhen', 'Ostrich', 'Puce', 7.51);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (22, 'Purple moorhen', 'Ring-tailed coatimundi', 'Green', 8.32);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (23, 'Giant otter', 'Southern boubou', 'Yellow', 0.15);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (24, 'Bat-eared fox', 'Ostrich', 'Mauv', 5.47);
INSERT
INTO Passepartout (Passepartout_Passepartoutnummer, Papiersoort_Papiernaam, Passepartoutsoort_Soortnaam,
                   Passepartout_Kleur, Passepartout_Prijs)
VALUES (25, 'Giant otter', 'Ostrich', 'Red', 1.45);

INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (1, 1);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (2, 2);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (3, 3);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (4, 4);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (5, 5);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (6, 6);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (7, 7);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (8, 8);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (9, 9);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (10, 10);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (11, 11);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (12, 12);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (13, 13);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (14, 14);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (15, 15);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (16, 16);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (17, 17);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (18, 18);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (19, 19);
INSERT
INTO Framingordermetpassepartout (Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
VALUES (20, 20);

INSERT INTO Verfproduct_Eigenschap(Eigenschap_Eigenschapcode, Product_Productnr)
SELECT E.EIGENSCHAP_EIGENSCHAPCODE, V.PRODUCT_PRODUCTNR FROM Eigenschap E, Verfproduct V
GO
