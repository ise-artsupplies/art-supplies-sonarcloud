[comment]: <Titel> (  Zoeken naar product)
[comment]: <Auteur> (  Auteur: Enes yildiz)
[comment]: <Datum> (  Datum: 26-05-2021)

# Hoe dit document te gebruiken
Dit document bevat een aantal voorbeeld requests. Er staat hier wat meer toelichting per aanvraag.  
De aanvragen kunnen makkelijk worden ingeladen door "test.postman_collection.json" te importeren in Postman.
Om de aanvragen te kunnen gebruiken moet Basic Authentication worden ingesteld van Restheart. Standaard is de gebruiker admin met wachtwoord secret.  
De basic authentication kan worden ingesteld in Postman onder Authorization bij elke request.



# Alle producten tonen
```
GET http://localhost:8080/products
```

## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

//Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten zonder voorraad
```
GET http://localhost:8080/products
    ?filter={
    "PRODUCT_VOORRAAD":
                        {"$lte":0}
    }
```

##Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Test if returned keys/records are equal to 0 to know if all products are on stock
var data = pm.response.json();

pm.test('Number of items returned = ' + data.length, function () {
    pm.expect(data.length).to.equal(0);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met voorraad

```
GET http://localhost:8080/products

    ?filter={
    "PRODUCT_VOORRAAD":
                        {"$gte":0}
    }
```

## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met kwast vorm rond
```
GET http://localhost:8080/products

    ?filter=
    {"KWAST_VORM":
                    {"$eq": "Rond"}
    }
```
# Tests
```javascript
//Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

//Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met kwast vorm ovaal

```
GET http://localhost:8080/products

    ?filter=
    {"KWAST_VORM":
                    {"$eq": "Ovaal"}
    }
```
## Tests
```javascript


// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

//Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);

```

# Producten met type kwast

```
GET http://localhost:8080/products

    ?filter=
    {"PRODUCT_TYPE":
                    {"$eq": "KWAST"}
    }
```

## Tests

```javascript
//Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```


# Producten met type verf

```
GET http://localhost:8080/products

    ?filter=
    {"PRODUCT_TYPE":
                    {"$eq": "VERF"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```


# Producten met type set

```
GET http://localhost:8080/products

    ?filter=
    {"PRODUCT_TYPE":
                    {"$eq": "SET"}
    }
```
## Tests

```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);

```

# Product materiaalnaam  

```
GET http://localhost:8080/products

    ?filter=
    {"MATERIAAL_MATERIAALNAAM":
                    {"$regex": "(?i)^Pied avocet"}
    }
```
## Tests

```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```


# Producten met merknaam Shufflester  

```
GET http://localhost:8080/products

    ?filter=
    {"MERKNAAM":
                    {"$eq": "Shufflester"}
    }
```
## Tests

```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met merknaam Talane

```
GET http://localhost:8080/products

    ?filter=
    {"MERKNAAM":
                    {"$eq": "Talane"}
    }
```
## Tests

```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```


# Producten met merknaam Quaxo

```
GET http://localhost:8080/products

    ?filter=
    {"MERKNAAM":
                    {"$eq": "Quaxo"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met merknaam Chatterpoint

```
GET http://localhost:8080/products

    ?filter=
    {"MERKNAAM":
                    {"$eq": "Chatterpoint"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met merknaam Avaveo

```
GET http://localhost:8080/products

    ?filter=
    {"MERKNAAM":
                    {"$eq": "Avaveo"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met technieken transitional

```
GET http://localhost:8080/products

    ?filter=
    {"TECHNIEKEN":
                    {"$eq": "transitional"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met technieken benchmark

```
GET http://localhost:8080/products

    ?filter=
    {"TECHNIEKEN":
                    {"$eq": "benchmark"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Sets zonder producten

```
GET http://localhost:8080/products

    ?filter=
    {"PRODUCTS IN SET":
                    {"$eq": ""}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Sets met producten

```
GET http://localhost:8080/products

    ?filter=
    {"PRODUCTS IN SET":
                    {$exists: true,
                     $not: {$eq: ""}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Producten met verf eigenschappen

```
GET http://localhost:8080/products

    ?filter=
    {"EIGENSCHAPPEN":
                    {$exists: true,
                     $not: {$eq: ""}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);

```

# Producten met geen/leeg verf eigenschappen

```
GET http://localhost:8080/products

    ?filter=
    {"EIGENSCHAPPEN":
                    {$exists: true,
                     $eq: ""}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Verfproducten met kleur
```
GET http://localhost:8080/products

    ?filter=
    {"KLEUR":
                    {$exists: true,
                     $not: {$eq: ""}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Verfproducten met leeg kleur

```
GET http://localhost:8080/products

    ?filter=
    {"KLEUR":
                    {$exists: true,
                     $not: {$eq: ""}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```


# Verfproducten met pgwaarde 1

```
GET http://localhost:8080/products

    ?filter=
    {"KLEUR
        .KLEUR_PGWAARDE":
                    {$eq: 1}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Verfproducten met pgwaarde 2

```
GET http://localhost:8080/products

    ?filter=
    {"KLEUR
        .KLEUR_PGWAARDE":
                    {$eq: 2}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Verfproducten met pgwaarde 3

```
GET http://localhost:8080/products

    ?filter=
    {"KLEUR
        .KLEUR_PGWAARDE":
                    {$eq: 3}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Verfproducten met pigment Zorro, Azara's


```
GET http://localhost:8080/products

    ?filter=
    {"KLEUR
        .PIGMENT
            .PIGMENTNAAM":
                    {$eq: "Zorro, azara's"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```


# Zoeken op producten met waarschuwingscode EUH006

```
GET http://localhost:8080/products

    ?filter=
    {"VERFPRODUCT_WAARSCHUWINGEN
        .WAARSCHUWINGSCODE_WAARSCHUWINGSCODE":
                    {$eq: "EUH006"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```

# Zoeken op producten met waarschuwingscode EUH208

```
GET http://localhost:8080/products

    ?filter=
    {"VERFPRODUCT_WAARSCHUWINGEN
        .WAARSCHUWINGSCODE_WAARSCHUWINGSCODE":
                    {$eq: "EUH208"}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);

```


# Zoeken op producten zonder waarschuwingscode


```
GET http://localhost:8080/products

    ?filter=
    {"VERFPRODUCT_WAARSCHUWINGEN
        .WAARSCHUWINGSCODE_WAARSCHUWINGSCODE":
                    {$exists: true,
                     $eq: ""}
    }
```
## Tests
```javascript
// Test if status code is OK
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});

// Shows amount of keys/records returned
const responseJson = pm.response.json();

var count = Object.keys(responseJson).length;

console.log("The number of expected keys in the response body is: " + count);
```


# Waarschuwingscode toevoegen
Post deze request om te zoeken op waarschuwingscode

```
POST Body [
    {
        "_id": 3,
        "PRODUCT_PRODUCTNR": 3,
        "MERKNAAM": "Avaveo",
        "PRODUCT_TYPE": "VERF",
        "PRODUCT_PRODUCTOMSCHRIJVING": "Aliquam sit amet diam in magna bibendum imperdiet.",
        "PRODUCT_VOORRAAD": 26,
        "VERKOOPPRIJS": 43.7400,
        "PRIJSGROEP_PRIJSKLASSE": 18,
        "VERFPRODUCT_WAARSCHUWINGEN": [
            {
                "WAARSCHUWINGSCODE_WAARSCHUWINGSCODE": "EUH006",
                "WAARSCHUWINGSCODE_BESCHRIJVING": "Explosive with or without contact with air"
            },
            {
                "WAARSCHUWINGSCODE_WAARSCHUWINGSCODE": "EUH208",
                "WAARSCHUWINGSCODE_BESCHRIJVING": "Contains <name of sensitising substance>. May produce an allergic reaction."
            }
        ],
        "EIGENSCHAPPEN": [
            "process improvement",
            "Organized",
            "Proactive",
        ],
        "KLEUR": [
            {
                "KLEUR_KLEURCODE": "#2259e9",
                "KLEUR_KLEUROMSCHRIJVING": "White-winged tern",
                "KLEUR_PGWAARDE": 2,
                "PIGMENT": [
                    {
                        "PIGMENTNAAM": "Zorro, azara's",
                        "WAARSCHUWINGEN": [
                            {
                                "WAARSCHUWINGSCODE_WAARSCHUWINGSCODE": "EUH208",
                                "WAARSCHUWINGSCODE_BESCHRIJVING": "Contains <name of sensitising substance>. May produce an allergic reaction."
                            },
                            {
                                "WAARSCHUWINGSCODE_WAARSCHUWINGSCODE": "EUH006",
                                "WAARSCHUWINGSCODE_BESCHRIJVING": "Explosive with or without contact with air"
                            },
                            {
                                "WAARSCHUWINGSCODE_WAARSCHUWINGSCODE": "H411",
                                "WAARSCHUWINGSCODE_BESCHRIJVING": "Toxic to aquatic life with long-lasting effects "
                            }
                        ]
                    }
                ]
            }
        ]
    }
]
```