USE Artsupplies
GO

CREATE OR
ALTER PROCEDURE Sp_Deleteproduct(
    @Productnr PRODUCTNUMMER) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    DECLARE @Product_Type NAAM;
    SELECT @Product_Type = Product_Type FROM Product WHERE Product_Productnr = @Productnr
    SET @Trancounter = @@TRANCOUNT;

    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;
    BEGIN TRY
        IF EXISTS(SELECT 1 FROM Product_In_Set WHERE Product_Productnr = @Productnr)
            BEGIN
                ; THROW 50006, 'Product cannot be deleted as it is part of a set', 1
            END
        IF @Product_Type = 'VERF'
            BEGIN
                DELETE
                FROM Verfproduct_Waarschuwing
                WHERE Product_Productnr = @Productnr
                DELETE
                FROM Verfproduct_Eigenschap
                WHERE Product_Productnr = @Productnr
                DELETE
                FROM Verfproduct
                WHERE Product_Productnr = @Productnr
            END
        IF @Product_Type = 'KWAST'
            BEGIN
                DELETE
                FROM Kwastproduct_Techniek
                WHERE Product_Productnr = @Productnr
                DELETE
                FROM Kwastproduct
                WHERE Product_Productnr = @Productnr
            END
        IF @Product_Type = 'SET'
            BEGIN
                DELETE
                FROM Product_In_Set
                WHERE Set_Product_Productnr = @Productnr
                DELETE
                FROM Setproduct
                WHERE Product_Productnr = @Productnr
            END
        DELETE
        FROM Product
        WHERE Product_Productnr = @Productnr
        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO