/*
  Update product
  Author: Romy de Weijer
  Date: 27-05-2021
 */
USE Artsupplies
GO


/* =================================
          Update product
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Updateproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Merknaam NAAM,
    @Product_Productomschrijving BESCHRIJVING,
    @Product_Voorraad AANTAL,
    @Product_Verkoopprijs PRIJS = NULL) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF EXISTS(SELECT 1 FROM Product WHERE Product_Productnr = @Product_Productnr)
            BEGIN
                UPDATE Product
                SET Merknaam                    = @Merknaam,
                    Product_Productomschrijving = @Product_Productomschrijving,
                    Product_Voorraad            = @Product_Voorraad,
                    Product_Verkoopprijs        = @Product_Verkoopprijs
                WHERE Product_Productnr = @Product_Productnr
            END
        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO

/* =================================
          Update verfproduct
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Updateverfproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Kleur_Kleurcode KLEURCODE,
    @Transparantie_Waarde NAAM = NULL,
    @Prijsgroep_Prijsklasse NUMMER,
    @Verfproduct_Inhoud BESCHRIJVING,
    @Verfproduct_Verdunner NAAM = NULL
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF EXISTS(SELECT 1 FROM Verfproduct WHERE Product_Productnr = @Product_Productnr)
            BEGIN
                UPDATE Verfproduct
                SET Kleur_Kleurcode        = @Kleur_Kleurcode,
                    Transparantie_Waarde   = @Transparantie_Waarde,
                    Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse,
                    Verfproduct_Inhoud     = @Verfproduct_Inhoud,
                    Verfproduct_Verdunner  = @Verfproduct_Verdunner
                WHERE Product_Productnr = @Product_Productnr
            END

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO

/* =================================
          Update setproduct
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Updatesetproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Setproduct_Verpakking BESCHRIJVING
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF EXISTS(SELECT 1 FROM Setproduct WHERE Product_Productnr = @Product_Productnr)
            BEGIN
                UPDATE Setproduct
                SET Setproduct_Verpakking = @Setproduct_Verpakking
                WHERE Product_Productnr = @Product_Productnr
            END

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO


/* =================================
          Update kwastproduct
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Updatekwastproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Materiaal_Materiaalnaam NAAM,
    @Prijsgroep_Prijsklasse NUMMER,
    @Kwast_Maat NUMMER,
    @Kwast_Vorm NAAM
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF EXISTS(SELECT 1 FROM Kwastproduct WHERE Product_Productnr = @Product_Productnr)
            BEGIN
                UPDATE Kwastproduct
                SET Materiaal_Materiaalnaam = @Materiaal_Materiaalnaam,
                    Prijsgroep_Prijsklasse  = @Prijsgroep_Prijsklasse,
                    Kwast_Maat              = @Kwast_Maat,
                    Kwast_Vorm              = @Kwast_Vorm
                WHERE Product_Productnr = @Product_Productnr
            END

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO



/* =================================
  Update product to kwastproduct
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Updateproducttokwastproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Materiaal_Materiaalnaam NAAM,
    @Prijsgroep_Prijsklasse NUMMER,
    @Kwast_Maat NUMMER,
    @Kwast_Vorm NAAM
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF ((SELECT Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr) = 'SET')
            BEGIN
                DELETE FROM Product_In_Set WHERE Set_Product_Productnr = @Product_Productnr
                DELETE FROM Setproduct WHERE Product_Productnr = @Product_Productnr
            END
        IF ((SELECT Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr) = 'VERF')
            BEGIN
                DELETE FROM Verfproduct_Eigenschap WHERE Product_Productnr = @Product_Productnr
                DELETE FROM Verfproduct_Waarschuwing WHERE Product_Productnr = @Product_Productnr
                DELETE FROM Verfproduct WHERE Product_Productnr = @Product_Productnr
            END
        UPDATE Product
        SET Product_Type = 'KWAST'
        WHERE Product_Productnr = @Product_Productnr

        INSERT
        INTO Kwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat, Kwast_Vorm)
        VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO

/* =================================
   Update product to setproduct
 ==================================*/

CREATE OR
ALTER PROCEDURE Sp_Updateproducttosetproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Setproduct_Verpakking BESCHRIJVING
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF ((SELECT Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr) = 'KWAST')
            BEGIN
                DELETE FROM Kwastproduct_Techniek WHERE Product_Productnr = @Product_Productnr
                DELETE FROM Kwastproduct WHERE Product_Productnr = @Product_Productnr
            END
        IF ((SELECT Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr) = 'VERF')
            BEGIN
                DELETE FROM Verfproduct_Eigenschap WHERE Product_Productnr = @Product_Productnr
                DELETE FROM Verfproduct_Waarschuwing WHERE Product_Productnr = @Product_Productnr
                DELETE FROM Verfproduct WHERE Product_Productnr = @Product_Productnr
            END

        UPDATE Product
        SET Product_Type = 'SET'
        WHERE Product_Productnr = @Product_Productnr

        INSERT
        INTO Setproduct(Product_Productnr, Setproduct_Verpakking)
        VALUES (@Product_Productnr, @Setproduct_Verpakking)

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO

/* =================================
   Update product to verfproduct
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Updateproducttoverfproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Kleur_Kleurcode KLEURCODE,
    @Transparantie_Waarde NAAM = NULL,
    @Prijsgroep_Prijsklasse NUMMER,
    @Verfproduct_Inhoud BESCHRIJVING,
    @Verfproduct_Verdunner NAAM = NULL
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF ((SELECT Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr) = 'KWAST')
            BEGIN
                DELETE FROM Kwastproduct_Techniek WHERE Product_Productnr = @Product_Productnr
                DELETE FROM Kwastproduct WHERE Product_Productnr = @Product_Productnr
            END
        IF ((SELECT Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr) = 'SET')
            BEGIN
                DELETE FROM Product_In_Set WHERE Set_Product_Productnr = @Product_Productnr
                DELETE FROM Setproduct WHERE Product_Productnr = @Product_Productnr
            END

        UPDATE Product
        SET Product_Type = 'VERF'
        WHERE Product_Productnr = @Product_Productnr

        INSERT
        INTO Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                         Verfproduct_Inhoud, Verfproduct_Verdunner)
        VALUES (@Product_Productnr, @Kleur_Kleurcode, @Transparantie_Waarde, @Prijsgroep_Prijsklasse,
                @Verfproduct_Inhoud, @Verfproduct_Verdunner)

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO
