/*
  Aanmaken product tests
  Auteur: Stein Jonker
  Datum: 25-05-2021
 */

/*  ========================
        Aanmaken product
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Create_Product'
GO

CREATE OR
ALTER PROCEDURE Test_Create_Product.[test creates a product]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'

    DROP TABLE IF EXISTS Expected;
    CREATE TABLE Expected
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'Product',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Verkoopprijs PRIJS = 1.51

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expected(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                  Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Verkoopprijs)

    EXEC Sp_Createproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam, @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Product_Verkoopprijs = @Product_Verkoopprijs

    EXEC Tsqlt.Assertequalstable @Expected = 'Expected', @Actual = 'dbo.Product'
END
GO

EXEC Tsqlt.Run 'Test_Create_Product'


/*  ========================
        Aanmaken verfproduct
 ===========================*/


EXEC Tsqlt.Newtestclass 'Test_Create_VerfProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Create_Verfproduct.[test creates verfproduct and product]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Verfproduct'

    IF OBJECT_ID('ExpectedProduct') IS NOT NULL DROP TABLE Expectedproduct;
    CREATE TABLE Expectedproduct
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    CREATE TABLE Expectedverfproduct
    (
        Product_Productnr      PRODUCTNUMMER NOT NULL,
        Kleur_Kleurcode        KLEURCODE     NOT NULL,
        Transparantie_Waarde   NAAM,
        Prijsgroep_Prijsklasse NUMMER        NOT NULL,
        Verfproduct_Inhoud     BESCHRIJVING  NOT NULL,
        Verfproduct_Verdunner  NAAM
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'VERF',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Kleur_Kleurcode KLEURCODE = '#ffffff',
        @Transparantie_Waarde NAAM = 'HALF',
        @Prijsgroep_Prijsklasse NUMMER = 10 ,
        @Verfproduct_Inhoud BESCHRIJVING = 'Inhoud',
        @Verfproduct_Verdunner NAAM = 'Terpetine'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expectedproduct(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                         Product_Voorraad,
                         Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            NULL)

    INSERT
    INTO Expectedverfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                             Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, @Transparantie_Waarde, @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud,
            @Verfproduct_Verdunner)


    EXEC Sp_Createverfproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Kleur_Kleurcode = @Kleur_Kleurcode, @Transparantie_Waarde = @Transparantie_Waarde,
         @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud = @Verfproduct_Inhoud,
         @Verfproduct_Verdunner = @Verfproduct_Verdunner

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedProduct', @Actual = 'dbo.Product'
    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedVerfProduct', @Actual = 'dbo.Verfproduct'
END
GO

CREATE OR
ALTER PROCEDURE Test_Create_Verfproduct.[test creates verfproduct and product with all optional fields empty]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Verfproduct'

    DROP TABLE IF EXISTS Expectedproduct;
    DROP TABLE IF EXISTS Expectedverfproduct
    CREATE TABLE Expectedproduct
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    CREATE TABLE Expectedverfproduct
    (
        Product_Productnr      PRODUCTNUMMER NOT NULL,
        Kleur_Kleurcode        KLEURCODE     NOT NULL,
        Transparantie_Waarde   NAAM,
        Prijsgroep_Prijsklasse NUMMER        NOT NULL,
        Verfproduct_Inhoud     BESCHRIJVING  NOT NULL,
        Verfproduct_Verdunner  NAAM
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'VERF',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Kleur_Kleurcode KLEURCODE = '#ffffff',
        @Prijsgroep_Prijsklasse NUMMER = 10 ,
        @Verfproduct_Inhoud BESCHRIJVING = 'Inhoud'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expectedproduct(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                         Product_Voorraad,
                         Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            NULL)

    INSERT
    INTO Expectedverfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                             Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, NULL, @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud,
            NULL)


    EXEC Sp_Createverfproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Kleur_Kleurcode = @Kleur_Kleurcode, @Transparantie_Waarde = NULL,
         @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud = @Verfproduct_Inhoud,
         @Verfproduct_Verdunner = NULL

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedProduct', @Actual = 'dbo.Product'
    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedVerfProduct', @Actual = 'dbo.Verfproduct'
END
GO

CREATE OR
ALTER PROCEDURE Test_Create_Verfproduct.[test only creates verfproduct, because product already exists]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Verfproduct'

    DROP TABLE IF EXISTS Expectedproduct;
    DROP TABLE IF EXISTS Expectedverfproduct
    CREATE TABLE Expectedproduct
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    CREATE TABLE Expectedverfproduct
    (
        Product_Productnr      PRODUCTNUMMER NOT NULL,
        Kleur_Kleurcode        KLEURCODE     NOT NULL,
        Transparantie_Waarde   NAAM,
        Prijsgroep_Prijsklasse NUMMER        NOT NULL,
        Verfproduct_Inhoud     BESCHRIJVING  NOT NULL,
        Verfproduct_Verdunner  NAAM
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'VERF',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Kleur_Kleurcode KLEURCODE = '#ffffff',
        @Prijsgroep_Prijsklasse NUMMER = 10 ,
        @Verfproduct_Inhoud BESCHRIJVING = 'Inhoud'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                 Product_Voorraad,
                 Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            NULL)

    INSERT
    INTO Expectedproduct(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                         Product_Voorraad,
                         Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            NULL)

    INSERT
    INTO Expectedverfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                             Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, NULL, @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud,
            NULL)


    EXEC Sp_Createverfproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Kleur_Kleurcode = @Kleur_Kleurcode, @Transparantie_Waarde = NULL,
         @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud = @Verfproduct_Inhoud,
         @Verfproduct_Verdunner = NULL

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedProduct', @Actual = 'dbo.Product'
    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedVerfProduct', @Actual = 'dbo.Verfproduct'
END
GO

EXEC Tsqlt.Run 'Test_Create_VerfProduct'


/*====================================
   Toevoegen waarschuwing verfproduct
 =====================================*/
EXEC Tsqlt.Newtestclass 'Sp_Verfproductwaarschuwing_tests'
GO

CREATE OR
ALTER PROCEDURE Sp_Verfproductwaarschuwing_Tests.[test insert waarschuwing for verfproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Verfproduct'
    EXEC Tsqlt.Faketable 'dbo.Waarschuwingscode'
    EXEC Tsqlt.Faketable 'dbo.Verfproduct_waarschuwing'

    INSERT
    INTO Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                     Verfproduct_Verdunner, Verfproduct_Inhoud)
    VALUES (1, '#ffffff', 'HALF', 10, 'Terpetine', 'inhoud')
    INSERT
    INTO Waarschuwingscode(Waarschuwingscode_Waarschuwingscode, Waarschuwingscode_Beschrijving)
    VALUES (1, 'heel gevaarlijk')

    EXEC Tsqlt.Expectnoexception

    EXEC Sp_Verfproductwaarschuwing 1, 1
END
GO

EXEC Tsqlt.Run Sp_Verfproductwaarschuwing_Tests


/*====================================
   Toevoegen eigenschap verfproduct
 =====================================*/
EXEC Tsqlt.Newtestclass 'Sp_Verfproducteigenschap_tests'
GO

CREATE OR
ALTER PROCEDURE Sp_Verfproducteigenschap_Tests.[test insert waarschuwing for verfproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Verfproduct'
    EXEC Tsqlt.Faketable 'dbo.Waarschuwingscode'
    EXEC Tsqlt.Faketable 'dbo.Verfproduct_eigenschap'

    INSERT
    INTO Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                     Verfproduct_Verdunner, Verfproduct_Inhoud)
    VALUES (1, '#ffffff', 'HALF', 10, 'Terpetine', 'inhoud')
    INSERT
    INTO Waarschuwingscode(Waarschuwingscode_Waarschuwingscode, Waarschuwingscode_Beschrijving)
    VALUES (1, 'heel gevaarlijk')

    EXEC Tsqlt.Expectnoexception

    EXEC Sp_Verfproducteigenschap 1, 1
END
GO

EXEC Tsqlt.Run Sp_Verfproducteigenschap_Tests


/*  ========================
        Aanmaken Setproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Create_SetProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Create_Setproduct.[test creates setproduct and product]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Setproduct'

    DROP TABLE IF EXISTS Expectedproduct;
    DROP TABLE IF EXISTS Expectedsetproduct;
    CREATE TABLE Expectedproduct
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    CREATE TABLE Expectedsetproduct
    (
        Product_Productnr     PRODUCTNUMMER NOT NULL,
        Setproduct_Verpakking KLEURCODE     NOT NULL,
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'SET',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Setproduct_Verpakking BESCHRIJVING = 'Houten doosje',
        @Product_Prijs PRIJS = 1.10
    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expectedproduct(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                         Product_Voorraad,
                         Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Prijs)

    INSERT
    INTO Expectedsetproduct(Product_Productnr, Setproduct_Verpakking)
    VALUES (@Product_Productnr, @Setproduct_Verpakking)


    EXEC Sp_Createsetproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Setproduct_Verpakking = @Setproduct_Verpakking, @Prijs = @Product_Prijs

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedProduct', @Actual = 'dbo.Product'
    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedSetProduct', @Actual = 'dbo.Setproduct'
END
GO

CREATE OR
ALTER PROCEDURE Test_Create_Setproduct.[test only creates setproduct, because product already exists]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Setproduct'

    DROP TABLE IF EXISTS Expectedproduct;
    DROP TABLE IF EXISTS Expectedsetproduct;
    CREATE TABLE Expectedproduct
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    CREATE TABLE Expectedsetproduct
    (
        Product_Productnr     PRODUCTNUMMER NOT NULL,
        Setproduct_Verpakking KLEURCODE     NOT NULL,
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'SET',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Setproduct_Verpakking BESCHRIJVING = 'Houten doosje',
        @Product_Prijs PRIJS = 1.10
    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                 Product_Voorraad,
                 Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Prijs)

    INSERT
    INTO Expectedproduct(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                         Product_Voorraad,
                         Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Prijs)

    INSERT
    INTO Expectedsetproduct(Product_Productnr, Setproduct_Verpakking)
    VALUES (@Product_Productnr, @Setproduct_Verpakking)


    EXEC Sp_Createsetproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Setproduct_Verpakking = @Setproduct_Verpakking, @Prijs = @Product_Prijs

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedProduct', @Actual = 'dbo.Product'
    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedSetProduct', @Actual = 'dbo.Setproduct'
END
GO

EXEC Tsqlt.Run 'Test_Create_SetProduct'

/*  ========================
    Toevoegen product in set
 ===========================*/
EXEC Tsqlt.Newtestclass 'Sp_Productinset_tests'
GO

CREATE OR
ALTER PROCEDURE Sp_Productinset_Tests.[test insert waarschuwing for verfproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product'
    EXEC Tsqlt.Faketable 'dbo.Product_in_set'

    INSERT
    INTO Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                 Product_Verkoopprijs)
    VALUES (1, 'Agimba', 'SET', 'test', 1, 25.00),
           (2, 'Agimba', 'PRODUCT', 'test', 1, 25.00)


    EXEC Tsqlt.Expectnoexception

    EXEC Sp_Productinset 2, 1, 2
END
GO

EXEC Tsqlt.Run Sp_Productinset_Tests


/*  ========================
        Aanmaken Kwastproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Create_KwastProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Create_Kwastproduct.[test creates kwastproduct and product]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Kwastproduct'

    DROP TABLE IF EXISTS Expectedproduct;
    DROP TABLE IF EXISTS Expectedkwastproduct;
    CREATE TABLE Expectedproduct
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    CREATE TABLE Expectedkwastproduct
    (
        Product_Productnr       PRODUCTNUMMER NOT NULL,
        Materiaal_Materiaalnaam NAAM          NOT NULL,
        Prijsgroep_Prijsklasse  NUMMER        NOT NULL,
        Kwast_Maat              NUMMER        NOT NULL,
        Kwast_Vorm              NAAM
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'SET',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Prijs PRIJS = NULL,
        @Materiaal_Materiaalnaam NAAM = 'Materiaalnaam',
        @Prijsgroep_Prijsklasse NUMMER = 10,
        @Kwast_Maat NUMMER = 8 ,
        @Kwast_Vorm NAAM = 'Rond'
    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expectedproduct(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                         Product_Voorraad,
                         Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Prijs)

    INSERT
    INTO Expectedkwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat,
                              Kwast_Vorm)
    VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)


    EXEC Sp_Createkwastproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Materiaal_Materiaalnaam = @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse,
         @Kwast_Maat = @Kwast_Maat, @Kwast_Vorm = @Kwast_Vorm

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedProduct', @Actual = 'dbo.Product'
    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedKwastProduct', @Actual = 'dbo.Kwastproduct'
END
GO

CREATE OR
ALTER PROCEDURE Test_Create_Kwastproduct.[test only creates kwastproduct, because product already exists]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Kwastproduct'

    DROP TABLE IF EXISTS Expectedproduct;
    DROP TABLE IF EXISTS Expectedkwastproduct;
    CREATE TABLE Expectedproduct
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    CREATE TABLE Expectedkwastproduct
    (
        Product_Productnr       PRODUCTNUMMER NOT NULL,
        Materiaal_Materiaalnaam NAAM          NOT NULL,
        Prijsgroep_Prijsklasse  NUMMER        NOT NULL,
        Kwast_Maat              NUMMER        NOT NULL,
        Kwast_Vorm              NAAM
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'SET',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Prijs PRIJS = NULL,
        @Materiaal_Materiaalnaam NAAM = 'Materiaalnaam',
        @Prijsgroep_Prijsklasse NUMMER = 10,
        @Kwast_Maat NUMMER = 8 ,
        @Kwast_Vorm NAAM = 'Rond'
    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                 Product_Voorraad,
                 Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Prijs)

    INSERT
    INTO Expectedproduct(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving,
                         Product_Voorraad,
                         Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Prijs)

    INSERT
    INTO Expectedkwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat,
                              Kwast_Vorm)
    VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)


    EXEC Sp_Createkwastproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Type = @Product_Type,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
         @Materiaal_Materiaalnaam = @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse,
         @Kwast_Maat = @Kwast_Maat, @Kwast_Vorm = @Kwast_Vorm

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedProduct', @Actual = 'dbo.Product'
    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedKwastProduct', @Actual = 'dbo.Kwastproduct'
END
GO


EXEC Tsqlt.Run 'Test_Create_KwastProduct'


