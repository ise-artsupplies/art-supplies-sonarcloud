/*
  Update product tests
  Auteur: Romy de Weijer
  Datum: 28-05-2021
 */

/*  ========================
        Aanmaken product
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Update_Product'
GO

CREATE OR
ALTER PROCEDURE Test_Update_Product.[test Update product]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'

    DROP TABLE IF EXISTS Expected;
    CREATE TABLE Expected
    (
        Product_Productnr           PRODUCTNUMMER NOT NULL,
        Merknaam                    NAAM          NOT NULL,
        Product_Type                NAAM          NOT NULL,
        Product_Productomschrijving BESCHRIJVING  NOT NULL,
        Product_Voorraad            AANTAL        NOT NULL,
        Product_Verkoopprijs        PRIJS
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'Product',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Verkoopprijs PRIJS = 1.51,
        @Nieuwe_Voorraad AANTAL = 3

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expected(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                  Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Nieuwe_Voorraad,
            @Product_Verkoopprijs)

    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Verkoopprijs)

    EXEC Sp_Updateproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
         @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Nieuwe_Voorraad,
         @Product_Verkoopprijs = @Product_Verkoopprijs

    EXEC Tsqlt.Assertequalstable @Expected = 'Expected', @Actual = 'dbo.Product'
END
GO

EXEC Tsqlt.Run 'Test_Update_Product'


/*  ========================
        Update verfproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Update_VerfProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Update_Verfproduct.[test Update verfproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Verfproduct'

    CREATE TABLE Expectedverfproduct
    (
        Product_Productnr      PRODUCTNUMMER NOT NULL,
        Kleur_Kleurcode        KLEURCODE     NOT NULL,
        Transparantie_Waarde   NAAM,
        Prijsgroep_Prijsklasse NUMMER        NOT NULL,
        Verfproduct_Inhoud     BESCHRIJVING  NOT NULL,
        Verfproduct_Verdunner  NAAM
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Kleur_Kleurcode KLEURCODE = '#ffffff',
        @Transparantie_Waarde NAAM = 'HALF',
        @Nieuwe_Transparantie_Waarde NAAM = 'NIET',
        @Prijsgroep_Prijsklasse NUMMER = 10 ,
        @Verfproduct_Inhoud BESCHRIJVING = 'Inhoud',
        @Verfproduct_Verdunner NAAM = 'Terpetine'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expectedverfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                             Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, @Nieuwe_Transparantie_Waarde, @Prijsgroep_Prijsklasse,
            @Verfproduct_Inhoud, @Verfproduct_Verdunner)

    INSERT
    INTO Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                     Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, @Transparantie_Waarde, @Prijsgroep_Prijsklasse,
            @Verfproduct_Inhoud, @Verfproduct_Verdunner)


    EXEC Sp_Updateverfproduct @Product_Productnr = @Product_Productnr,
         @Kleur_Kleurcode = @Kleur_Kleurcode, @Transparantie_Waarde = @Nieuwe_Transparantie_Waarde,
         @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud = @Verfproduct_Inhoud,
         @Verfproduct_Verdunner = @Verfproduct_Verdunner

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedVerfProduct', @Actual = 'dbo.Verfproduct'
END
GO


EXEC Tsqlt.Run 'Test_Update_VerfProduct'


/*  ========================
        Update Setproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Update_SetProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Update_Setproduct.[test Update setproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Setproduct'

    DROP TABLE IF EXISTS Expectedsetproduct;

    CREATE TABLE Expectedsetproduct
    (
        Product_Productnr     PRODUCTNUMMER NOT NULL,
        Setproduct_Verpakking KLEURCODE     NOT NULL,
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Setproduct_Verpakking BESCHRIJVING = 'Houten doosje',
        @Nieuwe_Setproduct_Verpakking BESCHRIJVING = 'plastic'
    EXEC Tsqlt.Expectnoexception


    INSERT
    INTO Expectedsetproduct(Product_Productnr, Setproduct_Verpakking)
    VALUES (@Product_Productnr, @Nieuwe_Setproduct_Verpakking)

    INSERT
    INTO Setproduct(Product_Productnr, Setproduct_Verpakking)
    VALUES (@Product_Productnr, @Setproduct_Verpakking)


    EXEC Sp_Updatesetproduct @Product_Productnr = @Product_Productnr,
         @Setproduct_Verpakking = @Nieuwe_Setproduct_Verpakking

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedSetProduct', @Actual = 'dbo.Setproduct'
END
GO


EXEC Tsqlt.Run 'Test_Update_SetProduct'


/*  ========================
    Update Kwastproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Update_KwastProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Update_Kwastproduct.[test Update kwastproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Kwastproduct'

    DROP TABLE IF EXISTS Expectedkwastproduct;
    CREATE TABLE Expectedkwastproduct
    (
        Product_Productnr       PRODUCTNUMMER NOT NULL,
        Materiaal_Materiaalnaam NAAM          NOT NULL,
        Prijsgroep_Prijsklasse  NUMMER        NOT NULL,
        Kwast_Maat              NUMMER        NOT NULL,
        Kwast_Vorm              NAAM
    )

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Materiaal_Materiaalnaam NAAM = 'Materiaalnaam',
        @Nieuwe_Materiaal_Materiaalnaam NAAM = 'Paardenhaar',
        @Prijsgroep_Prijsklasse NUMMER = 10,
        @Kwast_Maat NUMMER = 8 ,
        @Kwast_Vorm NAAM = 'Rond'
    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Expectedkwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat,
                              Kwast_Vorm)
    VALUES (@Product_Productnr, @Nieuwe_Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)

    INSERT
    INTO Kwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat, Kwast_Vorm)
    VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)


    EXEC Sp_Updatekwastproduct @Product_Productnr = @Product_Productnr,
         @Materiaal_Materiaalnaam = @Nieuwe_Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse,
         @Kwast_Maat = @Kwast_Maat, @Kwast_Vorm = @Kwast_Vorm

    EXEC Tsqlt.Assertequalstable @Expected = 'ExpectedKwastProduct', @Actual = 'dbo.Kwastproduct'
END
GO


EXEC Tsqlt.Run 'Test_Update_Product_Naar_KwastProduct'

/*  ========================
Update Product to Kwastproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Update_Product_To_KwastProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Update_Product_To_Kwastproduct.[test Update product to kwastproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Kwastproduct'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Verfproduct'

    DROP TABLE IF EXISTS Expectedkwastproduct;
    CREATE TABLE Expectedkwastproduct
    (
        Product_Productnr       PRODUCTNUMMER NOT NULL,
        Materiaal_Materiaalnaam NAAM          NOT NULL,
        Prijsgroep_Prijsklasse  NUMMER        NOT NULL,
        Kwast_Maat              NUMMER        NOT NULL,
        Kwast_Vorm              NAAM
    )
    DECLARE @Nieuwe_Product_Type NAAM

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Materiaal_Materiaalnaam NAAM = 'Materiaalnaam',
        @Prijsgroep_Prijsklasse NUMMER = 10,
        @Kwast_Maat NUMMER = 8 ,
        @Kwast_Vorm NAAM = 'Rond'

    DECLARE @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'VERF',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Verkoopprijs PRIJS = 1.51

    DECLARE @Kleur_Kleurcode KLEURCODE = '#ffffff',
        @Transparantie_Waarde NAAM = 'HALF',
        @Verfproduct_Inhoud BESCHRIJVING = 'Inhoud',
        @Verfproduct_Verdunner NAAM = 'Terpetine'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Verkoopprijs)

    INSERT
    INTO Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                     Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, @Transparantie_Waarde, @Prijsgroep_Prijsklasse,
            @Verfproduct_Inhoud, @Verfproduct_Verdunner)

    INSERT
    INTO Expectedkwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat,
                              Kwast_Vorm)
    VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)


    EXEC Sp_Updateproducttokwastproduct @Product_Productnr = @Product_Productnr,
         @Materiaal_Materiaalnaam = @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse,
         @Kwast_Maat = @Kwast_Maat, @Kwast_Vorm = @Kwast_Vorm

    SELECT @Nieuwe_Product_Type = Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr

    EXEC Tsqlt.Assertemptytable 'dbo.Verfproduct'
    EXEC Tsqlt.Assertequalstable 'Expectedkwastproduct', 'Kwastproduct'
    EXEC Tsqlt.Assertequals 'KWAST', @Nieuwe_Product_Type

END
GO

EXEC Tsqlt.Run 'Test_Update_Product_To_KwastProduct'

/*  ========================
Update Product to Verfproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Update_Product_To_VerfProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Update_Product_To_Verfproduct.[test Update kwast product to verfproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Kwastproduct'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Verfproduct'

    DROP TABLE IF EXISTS Expectedverfproduct;
    CREATE TABLE Expectedverfproduct
    (
        Product_Productnr      PRODUCTNUMMER NOT NULL,
        Kleur_Kleurcode        KLEURCODE     NOT NULL,
        Transparantie_Waarde   NAAM,
        Prijsgroep_Prijsklasse NUMMER        NOT NULL,
        Verfproduct_Inhoud     BESCHRIJVING  NOT NULL,
        Verfproduct_Verdunner  NAAM
    )
    DECLARE @Nieuwe_Product_Type NAAM

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Materiaal_Materiaalnaam NAAM = 'Materiaalnaam',
        @Prijsgroep_Prijsklasse NUMMER = 10,
        @Kwast_Maat NUMMER = 8 ,
        @Kwast_Vorm NAAM = 'Rond'

    DECLARE @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'KWAST',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Verkoopprijs PRIJS = 1.51

    DECLARE @Kleur_Kleurcode KLEURCODE = '#ffffff',
        @Transparantie_Waarde NAAM = 'HALF',
        @Verfproduct_Inhoud BESCHRIJVING = 'Inhoud',
        @Verfproduct_Verdunner NAAM = 'Terpetine'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Verkoopprijs)

    INSERT
    INTO Kwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat, Kwast_Vorm)
    VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)

    INSERT
    INTO Expectedverfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                             Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, @Transparantie_Waarde, @Prijsgroep_Prijsklasse,
            @Verfproduct_Inhoud,
            @Verfproduct_Verdunner)


    EXEC Sp_Updateproducttoverfproduct @Product_Productnr = @Product_Productnr,
         @Kleur_Kleurcode = @Kleur_Kleurcode, @Transparantie_Waarde = @Transparantie_Waarde,
         @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud = @Verfproduct_Inhoud,
         @Verfproduct_Verdunner = @Verfproduct_Verdunner


    SELECT @Nieuwe_Product_Type = Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr

    EXEC Tsqlt.Assertemptytable 'dbo.Kwastproduct'
    EXEC Tsqlt.Assertequalstable 'Expectedverfproduct', 'Verfproduct'
    EXEC Tsqlt.Assertequals 'VERF', @Nieuwe_Product_Type

END
GO

CREATE OR
ALTER PROCEDURE Test_Update_Product_To_Verfproduct.[test Update normal product to verfproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Kwastproduct'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Verfproduct'

    DROP TABLE IF EXISTS Expectedverfproduct;
    CREATE TABLE Expectedverfproduct
    (
        Product_Productnr      PRODUCTNUMMER NOT NULL,
        Kleur_Kleurcode        KLEURCODE     NOT NULL,
        Transparantie_Waarde   NAAM,
        Prijsgroep_Prijsklasse NUMMER        NOT NULL,
        Verfproduct_Inhoud     BESCHRIJVING  NOT NULL,
        Verfproduct_Verdunner  NAAM
    )
    DECLARE @Nieuwe_Product_Type NAAM

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Prijsgroep_Prijsklasse NUMMER = 10


    DECLARE @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'PRODUCT',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Verkoopprijs PRIJS = 1.51

    DECLARE @Kleur_Kleurcode KLEURCODE = '#ffffff',
        @Transparantie_Waarde NAAM = 'HALF',
        @Verfproduct_Inhoud BESCHRIJVING = 'Inhoud',
        @Verfproduct_Verdunner NAAM = 'Terpetine'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Verkoopprijs)

    INSERT
    INTO Expectedverfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                             Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (@Product_Productnr, @Kleur_Kleurcode, @Transparantie_Waarde, @Prijsgroep_Prijsklasse,
            @Verfproduct_Inhoud,
            @Verfproduct_Verdunner)


    EXEC Sp_Updateproducttoverfproduct @Product_Productnr = @Product_Productnr,
         @Kleur_Kleurcode = @Kleur_Kleurcode, @Transparantie_Waarde = @Transparantie_Waarde,
         @Prijsgroep_Prijsklasse = @Prijsgroep_Prijsklasse, @Verfproduct_Inhoud = @Verfproduct_Inhoud,
         @Verfproduct_Verdunner = @Verfproduct_Verdunner


    SELECT @Nieuwe_Product_Type = Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr

    EXEC Tsqlt.Assertemptytable 'dbo.Kwastproduct'
    EXEC Tsqlt.Assertequalstable 'Expectedverfproduct', 'Verfproduct'
    EXEC Tsqlt.Assertequals 'VERF', @Nieuwe_Product_Type

END
GO


EXEC Tsqlt.Run 'Test_Update_Product_To_VerfProduct'

/*  ========================
Update Product to setproduct
 ===========================*/
EXEC Tsqlt.Newtestclass 'Test_Update_Product_To_SetProduct'
GO

CREATE OR
ALTER PROCEDURE Test_Update_Product_To_Setproduct.[test Update product to setproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Product'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Kwastproduct'
    EXEC Tsqlt.Faketable @Tablename = 'dbo.Setproduct'

    DROP TABLE IF EXISTS Expectedsetproduct;
    CREATE TABLE Expectedsetproduct
    (
        Product_Productnr     PRODUCTNUMMER NOT NULL,
        Setproduct_Verpakking KLEURCODE     NOT NULL,
    )

    DECLARE @Nieuwe_Product_Type NAAM

    DECLARE @Product_Productnr PRODUCTNUMMER = 1,
        @Materiaal_Materiaalnaam NAAM = 'Materiaalnaam',
        @Prijsgroep_Prijsklasse NUMMER = 10,
        @Kwast_Maat NUMMER = 8 ,
        @Kwast_Vorm NAAM = 'Rond'

    DECLARE @Merknaam NAAM = 'Merk',
        @Product_Type NAAM = 'KWAST',
        @Product_Productomschrijving BESCHRIJVING = 'Beschrijving',
        @Product_Voorraad AANTAL = 1,
        @Product_Verkoopprijs PRIJS = 1.51

    DECLARE @Setproduct_Verpakking BESCHRIJVING = 'Houten doosje'

    EXEC Tsqlt.Expectnoexception

    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
            @Product_Verkoopprijs)

    INSERT
    INTO Kwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat, Kwast_Vorm)
    VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)

    INSERT
    INTO Expectedsetproduct(Product_Productnr, Setproduct_Verpakking)
    VALUES (@Product_Productnr, @Setproduct_Verpakking)

    EXEC Sp_Updateproducttosetproduct @Product_Productnr = @Product_Productnr,
         @Setproduct_Verpakking = @Setproduct_Verpakking

    SELECT @Nieuwe_Product_Type = Product_Type FROM Product WHERE Product_Productnr = @Product_Productnr

    EXEC Tsqlt.Assertemptytable 'dbo.Kwastproduct'
    EXEC Tsqlt.Assertequalstable 'Expectedsetproduct', 'Setproduct'
    EXEC Tsqlt.Assertequals 'SET', @Nieuwe_Product_Type

END
GO

EXEC Tsqlt.Run 'Test_Update_Product_To_SetProduct'
