USE Artsupplies

EXEC Tsqlt.Newtestclass 'Delete_ProductTests'
GO

CREATE OR
ALTER PROCEDURE Delete_Producttests.[test delete product]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'ezel', 'test', 3, 65)

    EXEC Tsqlt.Expectnoexception

    EXEC Sp_Deleteproduct 1

    EXEC Tsqlt.Assertemptytable 'dbo.Product'
END
GO


CREATE OR
ALTER PROCEDURE Delete_Producttests.[test delete verfproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product'
    EXEC Tsqlt.Faketable 'dbo.Verfproduct'
    EXEC Tsqlt.Faketable 'dbo.Verfproduct_eigenschap'
    EXEC Tsqlt.Faketable 'dbo.Verfproduct_waarschuwing'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'verf', 'test', 3, 65)
    INSERT
    INTO Dbo.Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                         Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (1, '#2259e9', 'half', 18, '4 napjes', 'thinner')
    INSERT
    INTO Dbo.Verfproduct_Eigenschap(Eigenschap_Eigenschapcode, Product_Productnr)
    VALUES (1, 1)
    INSERT
    INTO Dbo.Verfproduct_Waarschuwing(Waarschuwingscode_Waarschuwingscode, Product_Productnr)
    VALUES (1, 1)
    EXEC Tsqlt.Expectnoexception

    EXEC Sp_Deleteproduct 1

    EXEC Tsqlt.Assertemptytable 'dbo.Product'
    EXEC Tsqlt.Assertemptytable 'dbo.Verfproduct'
    EXEC Tsqlt.Assertemptytable 'dbo.Verfproduct_eigenschap'
    EXEC Tsqlt.Assertemptytable 'dbo.Verfproduct_waarschuwing'
END
GO



CREATE OR
ALTER PROCEDURE Delete_Producttests.[test delete kwastproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product'
    EXEC Tsqlt.Faketable 'dbo.Kwastproduct'
    EXEC Tsqlt.Faketable 'dbo.Kwastproduct_techniek'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'kwast', 'test', 3, 65)
    INSERT
    INTO Dbo.Kwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat, Kwast_Vorm)
    VALUES (1, 'paardenhaar', 1, 2, 'rond')
    INSERT
    INTO Dbo.Kwastproduct_Techniek(Techniek_Technieknaam, Product_Productnr)
    VALUES ('test', 1)
    EXEC Tsqlt.Expectnoexception

    EXEC Sp_Deleteproduct 1

    EXEC Tsqlt.Assertemptytable 'dbo.Product'
    EXEC Tsqlt.Assertemptytable 'dbo.Kwastproduct'
    EXEC Tsqlt.Assertemptytable 'dbo.Kwastproduct_techniek'
END
GO

CREATE OR
ALTER PROCEDURE Delete_Producttests.[test delete setproduct]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product'
    EXEC Tsqlt.Faketable 'dbo.Setproduct'
    EXEC Tsqlt.Faketable 'dbo.Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'set', 'test', 3, 65)
    INSERT
    INTO Dbo.Setproduct(Product_Productnr, Setproduct_Verpakking)
    VALUES (1, 'het zit in een vuilniszak')
    INSERT
    INTO Dbo.Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (12, 1, 4)

    EXEC Tsqlt.Expectnoexception

    EXEC Sp_Deleteproduct 1

    EXEC Tsqlt.Assertemptytable 'dbo.Product'
    EXEC Tsqlt.Assertemptytable 'dbo.Setproduct'
    EXEC Tsqlt.Assertemptytable 'dbo.Product_In_Set'
END
GO

CREATE OR
ALTER PROCEDURE Delete_Producttests.[test invalid delete because the product is part of a set]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product'
    EXEC Tsqlt.Faketable 'dbo.Product_In_Set'
    INSERT
    INTO Dbo.Product(Product_Productnr, Merknaam, Product_Type, Product_Productomschrijving, Product_Voorraad,
                     Product_Verkoopprijs)
    VALUES (1, 'test', 'ezel', 'test', 3, 65)
    INSERT
    INTO Dbo.Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
    VALUES (1, 7, 15)

    EXEC Tsqlt.Expectexception @Expectederrornumber = 50006,
         @Expectedmessage = 'Product cannot be deleted as it is part of a set'

    EXEC Sp_Deleteproduct 1

END
GO

EXEC Tsqlt.Run 'Delete_ProductTests'

