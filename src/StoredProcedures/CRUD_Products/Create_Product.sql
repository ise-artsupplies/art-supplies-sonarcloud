/*
  create product
  Auteur: Stein Jonker
  Datum: 25-05-2021
 */
USE Artsupplies
GO


/* =================================
          create product
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Createproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Merknaam NAAM,
    @Product_Type NAAM,
    @Product_Productomschrijving BESCHRIJVING,
    @Product_Voorraad AANTAL,
    @Product_Verkoopprijs PRIJS = NULL) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        INSERT
        INTO Product(Product_Productnr,
                     Merknaam,
                     Product_Type,
                     Product_Productomschrijving,
                     Product_Voorraad,
                     Product_Verkoopprijs)
        VALUES (@Product_Productnr, @Merknaam, @Product_Type, @Product_Productomschrijving, @Product_Voorraad,
                @Product_Verkoopprijs)

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO


/* =================================
          create verfproduct
 ==================================*/

CREATE OR
ALTER PROCEDURE Sp_Createverfproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Merknaam NAAM,
    @Product_Type NAAM,
    @Product_Productomschrijving BESCHRIJVING,
    @Product_Voorraad AANTAL,
    @Kleur_Kleurcode KLEURCODE,
    @Transparantie_Waarde NAAM = NULL,
    @Prijsgroep_Prijsklasse NUMMER,
    @Verfproduct_Inhoud BESCHRIJVING,
    @Verfproduct_Verdunner NAAM = NULL
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF NOT EXISTS(SELECT 1 FROM Product WHERE Product_Productnr = @Product_Productnr)
            BEGIN
                EXEC Sp_Createproduct @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
                     @Product_Type = @Product_Type,
                     @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
                     @Product_Verkoopprijs = NULL
            END
        INSERT
        INTO Dbo.Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde, Prijsgroep_Prijsklasse,
                             Verfproduct_Verdunner, Verfproduct_Inhoud)
        VALUES (@Product_Productnr, @Kleur_Kleurcode, @Transparantie_Waarde, @Prijsgroep_Prijsklasse,
                @Verfproduct_Verdunner, @Verfproduct_Inhoud)

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO

CREATE OR
ALTER PROCEDURE Sp_Verfproductwaarschuwing(
    @Productnr PRODUCTNUMMER, @Waarschuwingscode WAARSCHUWINGSCODE) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;

    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION
    BEGIN TRY

        IF NOT EXISTS(SELECT 1
                      FROM Verfproduct_Waarschuwing
                      WHERE Waarschuwingscode_Waarschuwingscode = @Waarschuwingscode
                        AND Product_Productnr = @Productnr)
            BEGIN
                INSERT
                INTO Verfproduct_Waarschuwing(Waarschuwingscode_Waarschuwingscode, Product_Productnr)
                VALUES (@Waarschuwingscode, @Productnr)
            END
        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        THROW
    END CATCH
END
GO

CREATE OR
ALTER PROCEDURE Sp_Verfproducteigenschap(
    @Productnr PRODUCTNUMMER, @Eigenschapscode WAARSCHUWINGSCODE) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;

    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION
    BEGIN TRY

        IF NOT EXISTS(SELECT 1
                      FROM Verfproduct_Eigenschap
                      WHERE Product_Productnr = @Productnr
                        AND Eigenschap_Eigenschapcode = @Eigenschapscode)
            BEGIN
                INSERT
                INTO Verfproduct_Eigenschap(Eigenschap_Eigenschapcode, Product_Productnr)
                VALUES (@Eigenschapscode, @Productnr)
            END

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        THROW
    END CATCH
END
GO



/* =================================
          create setproduct
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Createsetproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Merknaam NAAM,
    @Product_Type NAAM,
    @Product_Productomschrijving BESCHRIJVING,
    @Product_Voorraad AANTAL,
    @Prijs PRIJS,
    @Setproduct_Verpakking BESCHRIJVING
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF NOT EXISTS(SELECT 1 FROM Product WHERE Product_Productnr = @Product_Productnr)
            BEGIN
                EXEC Sp_Createproduct
                     @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
                     @Product_Type = @Product_Type,
                     @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
                     @Product_Verkoopprijs = @Prijs
            END
        INSERT
        INTO Setproduct(Product_Productnr, Setproduct_Verpakking)
        VALUES (@Product_Productnr, @Setproduct_Verpakking)


        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO

CREATE OR
ALTER PROCEDURE Sp_Productinset(
    @Productnr PRODUCTNUMMER, @Setnr PRODUCTNUMMER, @Aantal AANTAL) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;

    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION
    BEGIN TRY

        IF NOT EXISTS(
                SELECT 1 FROM Product_In_Set WHERE Product_Productnr = @Productnr AND Set_Product_Productnr = @Setnr)
            BEGIN
                INSERT
                INTO Product_In_Set(Product_Productnr, Set_Product_Productnr, Product_In_Set_Aantal)
                VALUES (@Productnr, @Setnr, @Aantal)
            END
        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        THROW
    END CATCH
END
GO


/* =================================
          create kwastproduct
 ==================================*/
CREATE OR
ALTER PROCEDURE Sp_Createkwastproduct(
    @Product_Productnr PRODUCTNUMMER,
    @Merknaam NAAM,
    @Product_Type NAAM,
    @Product_Productomschrijving BESCHRIJVING,
    @Product_Voorraad AANTAL,
    @Materiaal_Materiaalnaam NAAM,
    @Prijsgroep_Prijsklasse NUMMER,
    @Kwast_Maat NUMMER,
    @Kwast_Vorm NAAM
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;
    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION;

    BEGIN TRY
        IF NOT EXISTS(SELECT 1 FROM Product WHERE Product_Productnr = @Product_Productnr)
            BEGIN
                EXEC Sp_Createproduct
                     @Product_Productnr = @Product_Productnr, @Merknaam = @Merknaam,
                     @Product_Type = @Product_Type,
                     @Product_Productomschrijving = @Product_Productomschrijving, @Product_Voorraad = @Product_Voorraad,
                     @Product_Verkoopprijs = NULL
            END
        INSERT
        INTO Kwastproduct(Product_Productnr, Materiaal_Materiaalnaam, Prijsgroep_Prijsklasse, Kwast_Maat,
                          Kwast_Vorm)
        VALUES (@Product_Productnr, @Materiaal_Materiaalnaam, @Prijsgroep_Prijsklasse, @Kwast_Maat, @Kwast_Vorm)


        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        ELSE
            IF XACT_STATE() <> -1
                BEGIN
                    ROLLBACK TRANSACTION Proceduresave;
                END;
        THROW
    END CATCH
END
GO

CREATE OR
ALTER PROCEDURE Sp_Kwastproducttechniek(
    @Productnr PRODUCTNUMMER, @Technieknaam NAAM) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;

    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION
    BEGIN TRY
        IF NOT EXISTS(SELECT 1
                      FROM Kwastproduct_Techniek
                      WHERE Product_Productnr = @Productnr AND Techniek_Technieknaam = @Technieknaam)
            BEGIN
                INSERT
                INTO Kwastproduct_Techniek(Techniek_Technieknaam, Product_Productnr)
                VALUES (@Technieknaam, @Productnr)
            END
        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        THROW
    END CATCH
END
GO
