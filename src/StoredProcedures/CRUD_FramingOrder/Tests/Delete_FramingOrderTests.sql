EXEC Tsqlt.Newtestclass 'Test_Delete_FramingOrder'
GO

CREATE OR
ALTER PROCEDURE Test_Delete_Framingorder.[test delete existing framingOrder]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Framingorder'
    EXEC Tsqlt.Faketable 'dbo.FramingOrderMetPassePartout'

    INSERT
    INTO Framingorder(Framingorder_Ordernummer, Kunstwerksoort_Soortnaam, Glas_Glassoort, Lijst_Lijstnummer,
                      Framer_Framerid, Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk,
                      Framingorder_Prijs, Framingorder_Opleveringsdatum)
    VALUES (1, 'Schilderij', 'Hawk, galapagos', 4, 4, 90.6, 7.4, 45.2100, '2021-08-10 06:07:48.000')

    EXEC Tsqlt.Expectnoexception
    EXEC Sp_Deleteframingorder 1
END
GO

CREATE OR
ALTER PROCEDURE Test_Delete_Framingorder.[test delete non existing framingOrder]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Framingorder'
    EXEC Tsqlt.Faketable 'dbo.FramingOrderMetPassePartout'

    INSERT
    INTO Framingorder(Framingorder_Ordernummer, Kunstwerksoort_Soortnaam, Glas_Glassoort, Lijst_Lijstnummer,
                      Framer_Framerid, Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk,
                      Framingorder_Prijs, Framingorder_Opleveringsdatum)
    VALUES (1, 'Schilderij', 'Hawk, galapagos', 4, 4, 90.6, 7.4, 45.2100, '2021-08-10 06:07:48.000')

    EXEC Tsqlt.Expectexception @Expectederrornumber = 50009, @Expectedmessage = 'Framing order does not exist'
    EXEC Sp_Deleteframingorder 2
END
GO

CREATE OR
ALTER PROCEDURE Test_Delete_Framingorder.[test delete existing framingOrderMetPassePartout]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Framingorder'
    EXEC Tsqlt.Faketable 'dbo.FramingOrderMetPassePartout'

    INSERT
    INTO Framingorder(Framingorder_Ordernummer, Kunstwerksoort_Soortnaam, Glas_Glassoort, Lijst_Lijstnummer,
                      Framer_Framerid, Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk,
                      Framingorder_Prijs, Framingorder_Opleveringsdatum)
    VALUES (1, 'Schilderij', 'Hawk, galapagos', 4, 4, 90.6, 7.4, 45.2100, '2021-08-10 06:07:48.000')

    INSERT
    INTO Framingordermetpassepartout(Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
    VALUES (1, 2)

    EXEC Tsqlt.Expectnoexception
    EXEC Sp_Deleteframingorder 1
END
GO

CREATE OR
ALTER PROCEDURE Test_Delete_Framingorder.[test delete non existing framingOrderMetPassePartout]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Framingorder'
    EXEC Tsqlt.Faketable 'dbo.FramingOrderMetPassePartout'

    INSERT
    INTO Framingorder(Framingorder_Ordernummer, Kunstwerksoort_Soortnaam, Glas_Glassoort, Lijst_Lijstnummer,
                      Framer_Framerid, Framingorder_Breedte_Kunstwerk, Framingorder_Hoogte_Kunstwerk,
                      Framingorder_Prijs, Framingorder_Opleveringsdatum)
    VALUES (1, 'Schilderij', 'Hawk, galapagos', 4, 4, 90.6, 7.4, 45.2100, '2021-08-10 06:07:48.000')

    INSERT
    INTO Framingordermetpassepartout(Framingorder_Ordernummer, Passepartout_Passepartoutnummer)
    VALUES (1, 2)

    EXEC Tsqlt.Expectexception @Expectederrornumber = 50009, @Expectedmessage = 'Framing order does not exist'
    EXEC Sp_Deleteframingorder 2
END
GO

EXEC Tsqlt.Run 'Test_Delete_FramingOrder'
GO
