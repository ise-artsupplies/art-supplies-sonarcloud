/*
 Author: Rens Harinck
 Description: Tests for sp_update_framingOrder and sp_update_framingOrderMetPassePartout
 Created on: 1-6-2021
 */
use ArtSupplies
go

exec tsqlt.DropClass 'sp_update_framingOrderTests';
go

exec tsqlt.newtestclass 'sp_update_framingOrderTests';
go

create or
alter procedure sp_update_framingOrderTests.[Test if error is thrown when record does not exist]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'

    -- Assertion
    exec tSQLt.ExpectException @ExpectedMessage = 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER',
         @ExpectedErrorNumber = 50012

    -- Execution
    exec sp_update_framingOrder
         @FRAMINGORDER_ORDERNUMMER = 1,
         @KUNSTWERKSOORT_SOORTNAAM = 'kunstwerksoort',
         @GLAS_GLASSOORT = 'glassoort',
         @LIJST_LIJSTNUMMER = 1,
         @FRAMER_FRAMERID = 1,
         @FRAMINGORDER_BREEDTE_KUNSTWERK = 1,
         @FRAMINGORDER_HOOGTE_KUNSTWERK = 1,
         @FRAMINGORDER_PRIJS = 1,
         @FRAMINGORDER_OPLEVERINGSDATUM = '2050-01-01',
        @FRAMINGORDER_AANVRAAGDATUM = '2020-01-01'

end
go

create or
alter procedure sp_update_framingOrderTests.[Test if record is updated in main succes scenario]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'

    insert into dbo.FRAMINGORDER
    values (1, 'kunstwerksoort 1', 'glassoort', 1, 1, 1, 1, 1, '2050-01-01', '2020-01-01')

    -- Execution
    exec sp_update_framingOrder
         @FRAMINGORDER_ORDERNUMMER = 1,
         @KUNSTWERKSOORT_SOORTNAAM = 'kunstwerksoort 2',
         @GLAS_GLASSOORT = 'glassoort',
         @LIJST_LIJSTNUMMER = 1,
         @FRAMER_FRAMERID = 1,
         @FRAMINGORDER_BREEDTE_KUNSTWERK = 1,
         @FRAMINGORDER_HOOGTE_KUNSTWERK = 1,
         @FRAMINGORDER_PRIJS = 1,
         @FRAMINGORDER_OPLEVERINGSDATUM = '2050-01-01',
         @FRAMINGORDER_AANVRAAGDATUM = '2020-01-01'

    -- Assertion
    declare @exists bit = 0

    if (exists(
            select 1
            from dbo.FRAMINGORDER
            where KUNSTWERKSOORT_SOORTNAAM = 'kunstwerksoort 2'
        )
        )
        set @exists = 1

    exec tSQLt.AssertEquals 1, @exists

end
go

exec tSQLt.RunTestClass @TestClassName = 'sp_update_framingOrderTests'
go

exec tsqlt.DropClass 'sp_update_framingOrderMetPassePartoutTests';
go

exec tsqlt.newtestclass 'sp_update_framingOrderMetPassePartoutTests';
go

create or
alter procedure sp_update_framingOrderMetPassePartoutTests.[Test if error is thrown when record does not exist in FRAMINGORDER]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    -- Assertion
    exec tSQLt.ExpectException @ExpectedMessage = 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER',
         @ExpectedErrorNumber = 50012

    -- Execution
    exec sp_update_framingOrderMetPassePartout
         @FRAMINGORDER_ORDERNUMMER = 1,
         @KUNSTWERKSOORT_SOORTNAAM = 'kunstwerksoort',
         @GLAS_GLASSOORT = 'glassoort',
         @LIJST_LIJSTNUMMER = 1,
         @FRAMER_FRAMERID = 1,
         @FRAMINGORDER_BREEDTE_KUNSTWERK = 1,
         @FRAMINGORDER_HOOGTE_KUNSTWERK = 1,
         @FRAMINGORDER_PRIJS = 1,
         @FRAMINGORDER_OPLEVERINGSDATUM = '2050-01-01',
         @PASSEPARTOUT_PASSEPARTOUTNUMMER = 1,
        @FRAMINGORDER_AANVRAAGDATUM = '2020-01-01'

end
go

create or
alter procedure sp_update_framingOrderMetPassePartoutTests.[Test if error is thrown when record does not exist in FRAMINGORDERMETPASSEPARTOUT]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    insert into dbo.FRAMINGORDER
    values (1, 'kunstwerksoort 1', 'glassoort', 1, 1, 1, 1, 1, '2050-01-01', '2020-01-01')

    -- Assertion
    exec tSQLt.ExpectException
         @ExpectedMessage = 'No FRAMINGORDERMETPASSEPARTOUT exists with the given FRAMINGORDER_ORDERNUMMER',
         @ExpectedErrorNumber = 50007

    -- Execution
    exec sp_update_framingOrderMetPassePartout
         @FRAMINGORDER_ORDERNUMMER = 1,
         @KUNSTWERKSOORT_SOORTNAAM = 'kunstwerksoort',
         @GLAS_GLASSOORT = 'glassoort',
         @LIJST_LIJSTNUMMER = 1,
         @FRAMER_FRAMERID = 1,
         @FRAMINGORDER_BREEDTE_KUNSTWERK = 1,
         @FRAMINGORDER_HOOGTE_KUNSTWERK = 1,
         @FRAMINGORDER_PRIJS = 1,
         @FRAMINGORDER_OPLEVERINGSDATUM = '2050-01-01',
         @PASSEPARTOUT_PASSEPARTOUTNUMMER = 1,
         @FRAMINGORDER_AANVRAAGDATUM = '2020-01-01'

end
go

create or
alter procedure sp_update_framingOrderMetPassePartoutTests.[Test if record is updated in main succes scenario]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    insert into dbo.FRAMINGORDER
    values (1, 'kunstwerksoort 1', 'glassoort', 1, 1, 1, 1, 1, '2050-01-01', '2020-01-01')

    insert into dbo.FRAMINGORDERMETPASSEPARTOUT
    values (1, 1)

    -- Execution
    exec sp_update_framingOrderMetPassePartout
         @FRAMINGORDER_ORDERNUMMER = 1,
         @KUNSTWERKSOORT_SOORTNAAM = 'kunstwerksoort 2',
         @GLAS_GLASSOORT = 'glassoort',
         @LIJST_LIJSTNUMMER = 1,
         @FRAMER_FRAMERID = 1,
         @FRAMINGORDER_BREEDTE_KUNSTWERK = 1,
         @FRAMINGORDER_HOOGTE_KUNSTWERK = 1,
         @FRAMINGORDER_PRIJS = 1,
         @FRAMINGORDER_OPLEVERINGSDATUM = '2050-01-01',
         @PASSEPARTOUT_PASSEPARTOUTNUMMER = 2,
         @FRAMINGORDER_AANVRAAGDATUM = '2020-01-01'

    -- Assertion
    declare @exists bit = 0

    if (exists(
                select 1
                from dbo.FRAMINGORDER
                where KUNSTWERKSOORT_SOORTNAAM = 'kunstwerksoort 2'
            )
        and
        exists(
                select 1
                from dbo.FRAMINGORDERMETPASSEPARTOUT
                where PASSEPARTOUT_PASSEPARTOUTNUMMER = 2
            )
        )
        set @exists = 1

    exec tSQLt.AssertEquals 1, @exists

end
go

exec tSQLt.RunTestClass @TestClassName = 'sp_update_framingOrderMetPassePartoutTests'
go

exec tsqlt.DropClass 'sp_setFramingOrderToFramingOrderMetPassepartoutTests';
go

exec tsqlt.newtestclass 'sp_setFramingOrderToFramingOrderMetPassepartoutTests';
go

create or
alter procedure sp_setFramingOrderToFramingOrderMetPassepartoutTests.[Test if error is thrown when record doesn't exist in FRAMINGORDER]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    -- Assertion
    exec tSQLt.ExpectException @ExpectedErrorNumber = 50012,
         @ExpectedMessage = 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER'

    -- Execution
    exec sp_setFramingOrderToFramingOrderMetPassepartout @FRAMINGORDER_ORDERNUMMER = 1,
         @PASSEPARTOUT_PASSEPARTOUTNUMMER = 1

end
go

create or
alter procedure sp_setFramingOrderToFramingOrderMetPassepartoutTests.[Test if error is thrown when record already exists in FRAMINGORDERMETPASSEPARTOUT]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    insert into dbo.FRAMINGORDER
    values (1, 'test_kunstwerksoort', 'test_glassoort', 1, 1, 1, 1, 1, '2050-01-01','2050-01-01')

    insert into dbo.FRAMINGORDERMETPASSEPARTOUT
    values (1, 1)

    -- Assertion
    exec tSQLt.ExpectException @ExpectedErrorNumber = 50010,
         @ExpectedMessage = 'FRAMINGORDER with the given FRAMINGORDER_ORDERNUMMER already is of type FRAMINGORDERMETPASSEPARTOUT'

    -- Execution
    exec sp_setFramingOrderToFramingOrderMetPassepartout @FRAMINGORDER_ORDERNUMMER = 1,
         @PASSEPARTOUT_PASSEPARTOUTNUMMER = 2
end
go

create or
alter procedure sp_setFramingOrderToFramingOrderMetPassepartoutTests.[Test if record is inserted in main success scenario]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    insert into dbo.FRAMINGORDER
    values (1, 'test_kunstwerksoort', 'test_glassoort', 1, 1, 1, 1, 1, '2050-01-01','2050-01-01')

    -- Execution
    exec sp_setFramingOrderToFramingOrderMetPassepartout @FRAMINGORDER_ORDERNUMMER = 1,
         @PASSEPARTOUT_PASSEPARTOUTNUMMER = 2

    -- Assertion
    declare @exists bit = 0
    if (exists(select 1 from dbo.FRAMINGORDERMETPASSEPARTOUT where FRAMINGORDER_ORDERNUMMER = 1))
        set @exists = 1

    exec tSQLt.AssertEquals 1, @exists
end
go

exec tSQLt.RunTestClass @TestClassName = 'sp_setFramingOrderToFramingOrderMetPassepartoutTests'
go

exec tsqlt.DropClass 'sp_setFramingOrderMetPassepartoutToFramingOrderTests';
go

exec tsqlt.newtestclass 'sp_setFramingOrderMetPassepartoutToFramingOrderTests';
go

create or
alter procedure sp_setFramingOrderMetPassepartoutToFramingOrderTests.[Test if error is thrown when record doesn't exist in FRAMINGORDER]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    -- Assertion
    exec tSQLt.ExpectException @ExpectedErrorNumber = 50012,
         @ExpectedMessage = 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER'

    -- Execution
    exec sp_setFramingOrderMetPassepartoutToFramingOrder @FRAMINGORDER_ORDERNUMMER = 1
end
go

create or
alter procedure sp_setFramingOrderMetPassepartoutToFramingOrderTests.[Test if error is thrown when record doesn't exists in FRAMINGORDERMETPASSEPARTOUT]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    insert into dbo.FRAMINGORDER
    values (1, 'test_kunstwerksoort', 'test_glassoort', 1, 1, 1, 1, 1, '2050-01-01','2050-01-01')

    -- Assertion
    exec tSQLt.ExpectException @ExpectedErrorNumber = 50007,
         @ExpectedMessage = 'No FRAMINGORDERMETPASSEPARTOUT exists with the given FRAMINGORDER_ORDERNUMMER'

    -- Execution
    exec sp_setFramingOrderMetPassepartoutToFramingOrder @FRAMINGORDER_ORDERNUMMER = 1
end
go

create or
alter procedure sp_setFramingOrderMetPassepartoutToFramingOrderTests.[Test if record is deleted in FRAMINGORDERMETPASSEPARTOUT]
as
begin
    -- Setup
    exec tSQLt.FakeTable 'dbo.FRAMINGORDER'
    exec tSQLt.FakeTable 'dbo.FRAMINGORDERMETPASSEPARTOUT'

    insert into dbo.FRAMINGORDER
    values (1, 'test_kunstwerksoort', 'test_glassoort', 1, 1, 1, 1, 1, '2050-01-01','2050-01-01')

    insert into dbo.FRAMINGORDERMETPASSEPARTOUT
        values (1, 1)

    -- Execution
    exec sp_setFramingOrderMetPassepartoutToFramingOrder  @FRAMINGORDER_ORDERNUMMER = 1

    -- Assertion
    declare @exists bit = 0
    if (exists(select 1 from dbo.FRAMINGORDERMETPASSEPARTOUT where FRAMINGORDER_ORDERNUMMER = 1))
        set @exists = 1

    exec tSQLt.AssertEquals 0, @exists

end
go

exec tSQLt.RunTestClass @TestClassName = 'sp_setFramingOrderMetPassepartoutToFramingOrderTests'
go

