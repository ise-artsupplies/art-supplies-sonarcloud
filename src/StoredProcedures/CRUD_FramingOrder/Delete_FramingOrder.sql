USE Artsupplies
GO

CREATE OR
ALTER PROCEDURE Sp_Deleteframingorder(
    @Ordernr FRAMINGORDERNUMMER) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Trancounter INT;
    SET @Trancounter = @@TRANCOUNT;

    IF @Trancounter > 0
        SAVE TRANSACTION Proceduresave;
    ELSE
        BEGIN TRANSACTION
    BEGIN TRY
        IF NOT EXISTS(SELECT 1 FROM Framingorder WHERE Framingorder_Ordernummer = @Ordernr)
            BEGIN
                ;THROW 50009, 'Framing order does not exist', 1
            END

        IF EXISTS(SELECT 1 FROM Framingordermetpassepartout WHERE Framingorder_Ordernummer = @Ordernr)
            BEGIN
                DELETE FROM Framingordermetpassepartout WHERE Framingorder_Ordernummer = @Ordernr
            END
        DELETE FROM Framingorder WHERE Framingorder_Ordernummer = @Ordernr

        IF @Trancounter = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @Trancounter = 0
            ROLLBACK TRANSACTION;
        THROW
    END CATCH
END
GO
