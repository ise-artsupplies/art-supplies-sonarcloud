/*
 Author: Rens Harinck
 Description: Updates a record of table FRAMINGORDER or FRAMINGORDERMETPASSEPARTOUT
 Created on: 1-6-2021
 */
use ArtSupplies
go

-- stored procedure for updating FRAMINGORDER

create or
alter procedure sp_update_framingOrder @FRAMINGORDER_ORDERNUMMER framingordernummer,
                                       @KUNSTWERKSOORT_SOORTNAAM naam,
                                       @GLAS_GLASSOORT naam,
                                       @LIJST_LIJSTNUMMER lijstnummer,
                                       @FRAMER_FRAMERID nummer,
                                       @FRAMINGORDER_BREEDTE_KUNSTWERK lijstafmeting,
                                       @FRAMINGORDER_HOOGTE_KUNSTWERK lijstafmeting,
                                       @FRAMINGORDER_PRIJS prijs,
                                       @FRAMINGORDER_OPLEVERINGSDATUM datum,
                                       @FRAMINGORDER_AANVRAAGDATUM datum
as
declare @trancounter int;
    set @trancounter = @@trancount;
    if @trancounter > 0
        save transaction proceduresave1;

    else
        begin transaction;

begin try
    set nocount on
    if (
        not exists(
                select 1
                from dbo.FRAMINGORDER
                where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER
            )
        )
        begin
            ;throw 50012, 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER' , 1
        end

    update FRAMINGORDER
    set KUNSTWERKSOORT_SOORTNAAM       = @KUNSTWERKSOORT_SOORTNAAM,
        GLAS_GLASSOORT                 = @GLAS_GLASSOORT,
        LIJST_LIJSTNUMMER              = @LIJST_LIJSTNUMMER,
        FRAMER_FRAMERID                = @FRAMER_FRAMERID,
        FRAMINGORDER_BREEDTE_KUNSTWERK = @FRAMINGORDER_BREEDTE_KUNSTWERK,
        FRAMINGORDER_HOOGTE_KUNSTWERK  = @FRAMINGORDER_HOOGTE_KUNSTWERK,
        FRAMINGORDER_PRIJS             = @FRAMINGORDER_PRIJS,
        FRAMINGORDER_OPLEVERINGSDATUM  = @FRAMINGORDER_OPLEVERINGSDATUM,
        FRAMINGORDER_AANVRAAGDATUM = @FRAMINGORDER_AANVRAAGDATUM
    where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER

    if @trancounter = 0
        commit transaction;
end try
begin catch
    if @trancounter = 0
        rollback transaction;
    else
        if xact_state() <> -1
            rollback transaction proceduresave1;
            ;
    throw
end catch
go

-- stored procedure for updating FRAMINGORDERMETPASSEPARTOUT
create or
alter procedure sp_update_framingOrderMetPassePartout @FRAMINGORDER_ORDERNUMMER framingordernummer,
                                                      @KUNSTWERKSOORT_SOORTNAAM naam,
                                                      @GLAS_GLASSOORT naam,
                                                      @LIJST_LIJSTNUMMER lijstnummer,
                                                      @FRAMER_FRAMERID nummer,
                                                      @FRAMINGORDER_BREEDTE_KUNSTWERK lijstafmeting,
                                                      @FRAMINGORDER_HOOGTE_KUNSTWERK lijstafmeting,
                                                      @FRAMINGORDER_PRIJS prijs,
                                                      @FRAMINGORDER_OPLEVERINGSDATUM datum,
                                                      @PASSEPARTOUT_PASSEPARTOUTNUMMER passepartoutnummer,
                                                      @FRAMINGORDER_AANVRAAGDATUM datum
as
declare @trancounter int;
    set @trancounter = @@trancount;
    if @trancounter > 0
        save transaction proceduresave1;

    else
        begin transaction;

begin try
    set nocount on
    if (
        not exists(
                select 1
                from dbo.FRAMINGORDER
                where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER
            )
        )
        begin
            ;throw 50012, 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER' , 1
        end

    if (
        not exists(
                select 1
                from dbo.FRAMINGORDERMETPASSEPARTOUT
                where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER
            )
        )
        begin
            ;throw 50007, 'No FRAMINGORDERMETPASSEPARTOUT exists with the given FRAMINGORDER_ORDERNUMMER' , 1
        end

    update dbo.FRAMINGORDER
    set KUNSTWERKSOORT_SOORTNAAM       = @KUNSTWERKSOORT_SOORTNAAM,
        GLAS_GLASSOORT                 = @GLAS_GLASSOORT,
        LIJST_LIJSTNUMMER              = @LIJST_LIJSTNUMMER,
        FRAMER_FRAMERID                = @FRAMER_FRAMERID,
        FRAMINGORDER_BREEDTE_KUNSTWERK = @FRAMINGORDER_BREEDTE_KUNSTWERK,
        FRAMINGORDER_HOOGTE_KUNSTWERK  = @FRAMINGORDER_HOOGTE_KUNSTWERK,
        FRAMINGORDER_PRIJS             = @FRAMINGORDER_PRIJS,
        FRAMINGORDER_OPLEVERINGSDATUM  = @FRAMINGORDER_OPLEVERINGSDATUM,
        FRAMINGORDER_AANVRAAGDATUM = @FRAMINGORDER_AANVRAAGDATUM
    where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER

    update dbo.FRAMINGORDERMETPASSEPARTOUT
    set PASSEPARTOUT_PASSEPARTOUTNUMMER = @PASSEPARTOUT_PASSEPARTOUTNUMMER
    where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER

    if @trancounter = 0
        commit transaction;
end try
begin catch
    if @trancounter = 0
        rollback transaction;
    else
        if xact_state() <> -1
            rollback transaction proceduresave1;
            ;
    throw
end catch
go

-- stored procedure for changing a FRAMINGORDER to a FRAMINGORDERMETPASSEPARTOUT
go
create or
alter procedure sp_setFramingOrderToFramingOrderMetPassepartout @FRAMINGORDER_ORDERNUMMER framingordernummer,
                                                                @PASSEPARTOUT_PASSEPARTOUTNUMMER passepartoutnummer
as
declare @trancounter int;
    set @trancounter = @@trancount;
    if @trancounter > 0
        save transaction proceduresave1;

    else
        begin transaction;

begin try
    set nocount on
    if (
        not exists(
                select 1
                from dbo.FRAMINGORDER
                where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER
            )
        )
        begin
            ;throw 50012, 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER' , 1
        end

    if (
        exists(
                select 1
                from dbo.FRAMINGORDERMETPASSEPARTOUT
                where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER
            )
        )
        begin
            ;throw 50010, 'FRAMINGORDER with the given FRAMINGORDER_ORDERNUMMER already is of type FRAMINGORDERMETPASSEPARTOUT' , 1
        end

    insert into dbo.FRAMINGORDERMETPASSEPARTOUT
    values (@FRAMINGORDER_ORDERNUMMER, @PASSEPARTOUT_PASSEPARTOUTNUMMER)

    if @trancounter = 0
        commit transaction;
end try
begin catch
    if @trancounter = 0
        rollback transaction;
    else
        if xact_state() <> -1
            rollback transaction proceduresave1;
            ;
    throw
end catch
go

-- stored procedure for changing a  FRAMINGORDERMETPASSEPARTOUT to a FRAMINGORDER
create or
alter procedure sp_setFramingOrderMetPassepartoutToFramingOrder @FRAMINGORDER_ORDERNUMMER framingordernummer
as
declare @trancounter int;
    set @trancounter = @@trancount;
    if @trancounter > 0
        save transaction proceduresave1;

    else
        begin transaction;

begin try
    set nocount on
    if (
        not exists(
                select 1
                from dbo.FRAMINGORDER
                where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER
            )
        )
        begin
            ;throw 50012, 'No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER' , 1
        end

    if (
        not exists(
                select 1
                from dbo.FRAMINGORDERMETPASSEPARTOUT
                where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER
            )
        )
        begin
            ;throw 50007, 'No FRAMINGORDERMETPASSEPARTOUT exists with the given FRAMINGORDER_ORDERNUMMER' , 1
        end

    delete from dbo.FRAMINGORDERMETPASSEPARTOUT
    where FRAMINGORDER_ORDERNUMMER = @FRAMINGORDER_ORDERNUMMER

    if @trancounter = 0
        commit transaction;
end try
begin catch
    if @trancounter = 0
        rollback transaction;
    else
        if xact_state() <> -1
            rollback transaction proceduresave1;
            ;
    throw
end catch
