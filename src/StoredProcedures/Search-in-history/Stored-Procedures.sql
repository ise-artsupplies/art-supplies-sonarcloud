USE Artsupplies
GO

CREATE OR
ALTER PROCEDURE SpChanges_In_Pigment(
    @Productnr INT
)
AS
BEGIN

    DECLARE @Kleurcode VARCHAR(255)
    SELECT @Kleurcode = P.Kleur_Kleurcode FROM Verfproduct P WHERE P.Product_Productnr = @Productnr

    BEGIN TRY
        IF NOT EXISTS(
                SELECT 1
                FROM Hist.Kleur_Gemaakt_Van_Pigment
                WHERE Kleur_Kleurcode = @Kleurcode
                  AND Action LIKE '%UPDATE')
            BEGIN
                ; THROW 50003, 'No changes were made in pigment for this color', 1
            END
        SELECT * FROM Hist.Kleur_Gemaakt_Van_Pigment WHERE Kleur_Kleurcode = @Kleurcode AND Action LIKE '%UPDATE'
    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
END
GO

CREATE OR
ALTER PROCEDURE SpChanges_By_User(
    @Username VARCHAR(255),
    @Table SYSNAME
)
AS
BEGIN
    BEGIN TRY
        EXEC ('SELECT * FROM hist.' + @Table + ' T WHERE T.username LIKE ''' + @Username + '''')
    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
END
GO

CREATE OR
ALTER PROCEDURE SpChanges_In_Price(
    @Productnr PRODUCTNUMMER
)
AS
BEGIN

    DECLARE @Prijs VARCHAR(255)
    SELECT @Prijs = P.Product_Verkoopprijs FROM Product P WHERE P.Product_Productnr = @Productnr

    BEGIN TRY
        IF (@Prijs IS NOT NULL)
            BEGIN
                SELECT timestamp, username, Product_Verkoopprijs, action
                FROM Hist.Product
                WHERE Product_Productnr = @Productnr
                ORDER BY Product.Timestamp DESC
            END
        ELSE
            BEGIN
                IF EXISTS(SELECT 1
                          FROM Hist.Kwastproduct
                          WHERE Prijsgroep_Prijsklasse IS NOT NULL
                            AND Product_Productnr = @Productnr
                    )
                    BEGIN
                        SELECT timestamp, username, Prijsgroep_Prijsklasse, action
                        FROM Hist.Kwastproduct
                        WHERE Prijsgroep_Prijsklasse IS NOT NULL
                          AND Product_Productnr = @Productnr
                        ORDER BY Timestamp
                    END
                ELSE
                    IF EXISTS(SELECT 1
                              FROM Hist.Verfproduct
                              WHERE Prijsgroep_Prijsklasse IS NOT NULL
                                AND Product_Productnr = @Productnr
                        )
                        BEGIN
                            SELECT timestamp, username, Prijsgroep_Prijsklasse, action
                            FROM Hist.Verfproduct
                            WHERE Prijsgroep_Prijsklasse IS NOT NULL
                              AND Product_Productnr = @Productnr
                            ORDER BY Timestamp
                        END
            END
    END TRY
    BEGIN CATCH
        ; THROW
    END CATCH
END
GO