use ArtSupplies

EXEC Tsqlt.Newtestclass 'Test_SpChanges_In_Pigment'
GO

CREATE OR
ALTER PROCEDURE Test_SpChanges_In_Pigment.[test error when inserting]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Verfproduct';
    EXEC Tsqlt.Faketable 'dbo', 'Kleur_Gemaakt_Van_Pigment';

    EXEC Tsqlt.Faketable 'hist', 'Verfproduct';
    EXEC Tsqlt.Faketable 'hist', 'Kleur_Gemaakt_Van_Pigment'

    EXEC Tsqlt.Applytrigger 'dbo.Kleur_Gemaakt_Van_Pigment', Tr_Kleur_Gemaakt_Van_Pigment_Fillhistory

    INSERT
    INTO Dbo.Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                         Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (1, 123, 'test', 1, 20, 'TERPENTINE')

    INSERT
    INTO Dbo.Kleur_Gemaakt_Van_Pigment
    VALUES ('test2', '123')


    EXEC Tsqlt.Expectexception @Expectederrornumber = 50003,
         @Expectedmessage = 'No changes were made in pigment for this color'

    EXEC Dbo.SpChanges_In_Pigment 1
END
GO

CREATE OR
ALTER PROCEDURE Test_SpChanges_In_Pigment.[test error when doing double insert]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Verfproduct';
    EXEC Tsqlt.Faketable 'dbo', 'Kleur_Gemaakt_Van_Pigment';

    EXEC Tsqlt.Faketable 'hist', 'Verfproduct';
    EXEC Tsqlt.Faketable 'hist', 'Kleur_Gemaakt_Van_Pigment'

    EXEC Tsqlt.Applytrigger 'dbo.Kleur_Gemaakt_Van_Pigment', Tr_Kleur_Gemaakt_Van_Pigment_Fillhistory

    INSERT
    INTO Dbo.Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                         Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (1, 123, 'test', 1, 20, 'TERPENTINE')

      INSERT
    INTO Dbo.Kleur_Gemaakt_Van_Pigment
    VALUES ('test2', '123')


    EXEC Tsqlt.Expectexception @Expectederrornumber = 50003,
         @Expectedmessage = 'No changes were made in pigment for this color'

    EXEC Dbo.SpChanges_In_Pigment 1
END
GO

CREATE OR
ALTER PROCEDURE Test_SpChanges_In_Pigment.[test error when deleting]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Verfproduct';
    EXEC Tsqlt.Faketable 'dbo', 'Kleur_Gemaakt_Van_Pigment';
    EXEC Tsqlt.Faketable 'dbo', 'Verdunner'

    EXEC Tsqlt.Faketable 'hist', 'Verfproduct';
    EXEC Tsqlt.Faketable 'hist', 'Kleur_Gemaakt_Van_Pigment'

    EXEC Tsqlt.Applytrigger 'dbo.Kleur_Gemaakt_Van_Pigment', Tr_Kleur_Gemaakt_Van_Pigment_Fillhistory

    INSERT
    INTO Dbo.Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                         Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (1, 123, 'test', 1, 20, 'TERPENTINE')

    INSERT
    INTO Dbo.Kleur_Gemaakt_Van_Pigment
    VALUES ('test2', '123')

    DELETE
    FROM Dbo.Kleur_Gemaakt_Van_Pigment
    WHERE Kleur_Kleurcode = 123

    EXEC Tsqlt.Expectexception @Expectederrornumber = 50003,
         @Expectedmessage = 'No changes were made in pigment for this color'

    EXEC Dbo.SpChanges_In_Pigment 1
END
GO

CREATE OR
ALTER PROCEDURE Test_SpChanges_In_Pigment.[test error when doing double delete]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Verfproduct';
    EXEC Tsqlt.Faketable 'dbo', 'Kleur_Gemaakt_Van_Pigment';

    EXEC Tsqlt.Faketable 'hist', 'Verfproduct';
    EXEC Tsqlt.Faketable 'hist', 'Kleur_Gemaakt_Van_Pigment'

    EXEC Tsqlt.Applytrigger 'dbo.Kleur_Gemaakt_Van_Pigment', Tr_Kleur_Gemaakt_Van_Pigment_Fillhistory

    INSERT
    INTO Dbo.Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                         Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (1, 123, 'test', 1, 20, 'TERPENTINE')

    INSERT
    INTO Dbo.Kleur_Gemaakt_Van_Pigment
    VALUES ('test2', '123'),
           ('test', '123')

    INSERT
    INTO Dbo.Verdunner
    VALUES ('test')

    DELETE
    FROM Dbo.Kleur_Gemaakt_Van_Pigment
    WHERE Kleur_Kleurcode = 123

    EXEC Tsqlt.Expectexception @Expectederrornumber = 50003,
         @Expectedmessage = 'No changes were made in pigment for this color'

    EXEC Dbo.SpChanges_In_Pigment 1
END
GO

CREATE OR
ALTER PROCEDURE Test_SpChanges_In_Pigment.[test no error when updating]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo', 'Verfproduct';
    EXEC Tsqlt.Faketable 'dbo', 'Kleur_Gemaakt_Van_Pigment';

    EXEC Tsqlt.Faketable 'hist', 'Verfproduct';
    EXEC Tsqlt.Faketable 'hist', 'Kleur_Gemaakt_Van_Pigment'

    EXEC Tsqlt.Applytrigger 'dbo.Kleur_Gemaakt_Van_Pigment', Tr_Kleur_Gemaakt_Van_Pigment_Fillhistory

    INSERT
    INTO Dbo.Verfproduct(Product_Productnr, Kleur_Kleurcode, Transparantie_Waarde,
                         Prijsgroep_Prijsklasse, Verfproduct_Inhoud, Verfproduct_Verdunner)
    VALUES (1, 123, 'test', 1, 20, 'TERPENTINE')

    INSERT
    INTO Dbo.Kleur_Gemaakt_Van_Pigment
    VALUES ('test2', '123')


    UPDATE Kleur_Gemaakt_Van_Pigment
    SET Pigmentnaam = 'nieuw'
    WHERE Kleur_Kleurcode = '123'

    EXEC Tsqlt.Expectnoexception

    EXEC Dbo.SpChanges_In_Pigment 1
END
GO

EXEC Tsqlt.Run 'Test_SpChanges_In_Pigment'
