use ArtSupplies

EXEC Tsqlt.Newtestclass 'SpChanges_By_UserTests';
GO

CREATE OR
ALTER PROCEDURE SpChanges_By_Usertests.[Test if results of sp are equal to expected. All values in table]
AS
BEGIN
    IF OBJECT_ID('Expected') IS NOT NULL DROP TABLE #Expected;
    CREATE TABLE #Expected
    (
        Timestamp                 DATETIME DEFAULT GETDATE()   NOT NULL,
        Username                  SYSNAME  DEFAULT USER_NAME() NOT NULL,
        Action                    VARCHAR(255)                 NOT NULL,
        Eigenschap_Beschrijving   VARCHAR(256),
        Eigenschap_Eigenschapcode VARCHAR(256)                 NOT NULL
    )


    EXEC Tsqlt.Faketable @Tablename = 'hist.EIGENSCHAP', @Defaults = 1

    EXEC Tsqlt.Faketable 'dbo.EIGENSCHAP'

    EXEC Tsqlt.Applytrigger 'dbo.EIGENSCHAP', 'tr_eigenschap_fillhistory'

    INSERT INTO Dbo.Eigenschap VALUES (1, 'Beschrijving'), (2, 'Nog een beschrijving')

    INSERT
    INTO #Expected
        EXEC SpChanges_By_User '%', Eigenschap

    DECLARE @Exists BIT
    IF NOT EXISTS(SELECT * FROM Hist.Eigenschap WHERE Username LIKE '%' EXCEPT SELECT * FROM #Expected)
        SET @Exists = 0

    EXEC Tsqlt.Assertequals @Exists, 0

END
GO

CREATE OR
ALTER PROCEDURE SpChanges_By_Usertests.[Test if results of sp are equal to expected. Values in table with username Admin]
AS
BEGIN
    IF OBJECT_ID('Expected') IS NOT NULL DROP TABLE #Expected;
    CREATE TABLE #Expected
    (
        Timestamp                 DATETIME DEFAULT GETDATE()   NOT NULL,
        Username                  SYSNAME  DEFAULT USER_NAME() NOT NULL,
        Action                    VARCHAR(255)                 NOT NULL,
        Eigenschap_Beschrijving   VARCHAR(256),
        Eigenschap_Eigenschapcode VARCHAR(256)                 NOT NULL
    )

    EXEC Tsqlt.Faketable @Tablename = 'hist.EIGENSCHAP', @Defaults = 1

    EXEC Tsqlt.Faketable 'dbo.EIGENSCHAP'

    EXEC Tsqlt.Applytrigger 'dbo.EIGENSCHAP', 'tr_eigenschap_fillhistory'


    INSERT INTO Dbo.Eigenschap VALUES (1, 'Beschrijving')

    declare @admin varchar(5) = 'Admin';
    EXECUTE AS USER = @admin
    INSERT INTO Dbo.Eigenschap VALUES (2, 'Andere beschrijving')

    INSERT
    INTO #Expected
        EXEC SpChanges_By_User @admin, 'Eigenschap'

    DECLARE @Exists BIT
    IF NOT EXISTS(SELECT * FROM Hist.Eigenschap WHERE Username LIKE @admin EXCEPT SELECT * FROM #Expected)
        SET @Exists = 0

    EXEC Tsqlt.Assertequals @Exists, 0
END
GO

EXEC Tsqlt.Runtestclass @Testclassname = 'SpChanges_By_UserTests'
GO
