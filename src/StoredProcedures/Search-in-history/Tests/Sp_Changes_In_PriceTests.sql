use ArtSupplies

EXEC Tsqlt.Newtestclass 'SpChanges_In_PriceTests'
GO

CREATE OR ALTER PROCEDURE SpChanges_In_PriceTests.[Test insert Product where @prijs is not null]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product'
    EXEC Tsqlt.Faketable 'hist.Product', @defaults = 1;

    insert into dbo.PRODUCT(product_productnr, merknaam, product_type, product_productomschrijving, product_voorraad, product_verkoopprijs)
    VALUES (1,'test','test','test',1,20)

    insert into hist.PRODUCT (PRODUCT_PRODUCTNR, MERKNAAM, PRODUCT_TYPE, PRODUCT_PRODUCTOMSCHRIJVING, PRODUCT_VOORRAAD, PRODUCT_VERKOOPPRIJS, action)
    VALUES (1,'test','test','test',1,20, 'INSERT'),
           (1,'test','test','test',2,18, 'INSERT')
    EXEC Tsqlt.ExpectNoException

    EXEC dbo.SpChanges_In_Price 1
END
GO


CREATE OR ALTER PROCEDURE SpChanges_In_PriceTests.[Test insert Verfproduct where @prijs is null]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product';
    EXEC Tsqlt.Faketable 'hist.Product', @defaults = 1;
    EXEC Tsqlt.Faketable 'dbo.Verfproduct';
    EXEC Tsqlt.Faketable 'hist.Verfproduct', @defaults = 1;

    insert into dbo.PRODUCT(product_productnr, merknaam, product_type, product_productomschrijving, product_voorraad, product_verkoopprijs)
    VALUES (1,'test','Verf','test',1,NULL)

    declare @insert varchar(6) = 'INSERT';
    insert into dbo.VERFPRODUCT(product_productnr, kleur_kleurcode, transparantie_waarde, prijsgroep_prijsklasse, verfproduct_inhoud, verfproduct_verdunner)
    VALUES (1,'test','test',3,'test','test')

    insert into hist.PRODUCT (PRODUCT_PRODUCTNR, MERKNAAM, PRODUCT_TYPE, PRODUCT_PRODUCTOMSCHRIJVING, PRODUCT_VOORRAAD, PRODUCT_VERKOOPPRIJS, action)
    VALUES (1,'test','Verf','test',1,NULL, @insert)

    insert into hist.VERFPRODUCT(product_productnr, kleur_kleurcode, transparantie_waarde, prijsgroep_prijsklasse, verfproduct_inhoud, verfproduct_verdunner, action)
    VALUES (1,'test','test',2,'test','test', @insert)

    insert into hist.VERFPRODUCT(product_productnr, kleur_kleurcode, transparantie_waarde, prijsgroep_prijsklasse, verfproduct_inhoud, verfproduct_verdunner, action)
    VALUES (1,'test','test',3,'test','test', @insert)

    EXEC Tsqlt.ExpectNoException

    EXEC dbo.SpChanges_In_Price 1
END
GO



CREATE OR ALTER PROCEDURE SpChanges_In_PriceTests.[Test insert Kwastproduct where @prijs is null]
AS
BEGIN
    EXEC Tsqlt.Faketable 'dbo.Product';
    EXEC Tsqlt.Faketable 'hist.Product', @defaults = 1;
    EXEC Tsqlt.Faketable 'dbo.Kwastproduct';
    EXEC Tsqlt.Faketable 'hist.Kwastproduct', @defaults = 1;

    insert into dbo.PRODUCT(product_productnr, merknaam, product_type, product_productomschrijving, product_voorraad, product_verkoopprijs)
    VALUES (1,'test','Kwast','test',1,NULL)

    insert into dbo.KWASTPRODUCT(PRODUCT_PRODUCTNR, MATERIAAL_MATERIAALNAAM, PRIJSGROEP_PRIJSKLASSE, KWAST_MAAT, KWAST_VORM)
    VALUES (1,'test',3,3,'test')

    declare @insert varchar(6) = 'INSERT';
    insert into hist.PRODUCT (PRODUCT_PRODUCTNR, MERKNAAM, PRODUCT_TYPE, PRODUCT_PRODUCTOMSCHRIJVING, PRODUCT_VOORRAAD, PRODUCT_VERKOOPPRIJS, action)
    VALUES (1,'test','Kwast','test',1,NULL, @insert)

    insert into hist.KWASTPRODUCT(PRODUCT_PRODUCTNR, MATERIAAL_MATERIAALNAAM, PRIJSGROEP_PRIJSKLASSE, KWAST_MAAT, KWAST_VORM, action)
    VALUES (1,'test',2,3,'test', @insert)

    insert into hist.KWASTPRODUCT(PRODUCT_PRODUCTNR, MATERIAAL_MATERIAALNAAM, PRIJSGROEP_PRIJSKLASSE, KWAST_MAAT, KWAST_VORM, action)
    VALUES (1,'test',3,3,'test', @insert)

    EXEC Tsqlt.ExpectNoException

    EXEC dbo.SpChanges_In_Price 1
END
GO

EXEC Tsqlt.Run 'SpChanges_In_PriceTests'
