---
title: |
        Iteratieplan\
        Art Supplies\
        ISE Project\
        Groep A4
date: 08-06-2021
---

# Inleiding  
In dit document zal er per constructiefase worden beschreven welke functionaliteit gebouwd zal gaan worden.  
Dit document zal daarom bijgewerkt worden voordat elke constructiefase begint.  

# Constructiefase 1  
Voor de eerste constructiefase zijn de must-have functionaliteiten opgenomen, met een tweetal should-haves. 
De opgenomen functionaliteiten zijn:  
- Zoeken naar producten  
- Exporteren naar MongoDB  
- Data geschiedenis bijhouden  
- Zoeken in datageschiedenis  
  
Om deze functionaliteit goed te implementeren, zullen tests worden gemaakt. Ook zullen het functioneel en technisch ontwerp aangevuld worden.  

De eerste constructiefase loopt af op 21 mei, maar er vallen werkdagen uit.  
Er valt bijvoorbeeld een werkweek uit i.v.m. de meivakantie en een aantal dagen door hemelvaart en Pinksteren.  

## Afronding constructiefase:
De volgende functionaliteiten zijn afgerond in deze constructiefase:  

- Datageschiedenis bijhouden
- Zoeken in de datageschiedenis
- Exporteren naar MongoDB

Doordat het zoeken op producten technisch een uitdaging was, is het niet gelukt om deze volledig te implementeren.  
Wel zijn er al grote stappen gezet in de oplossing die is bedacht, waardoor deze functionaliteit makkelijker voltooid kan worden in de volgende constructiefase.  

# Constructiefase 2
De te realiseren functionaliteiten voor de constructiefase 2 zijn als volgt:  

- Afronden zoeken product
- Beheren producten (CRUD)

Het team zal, als er tijd over is in de constructiefase, ook beginnen met het vastleggen van informatie over de inlijstservice.  

Deze constructiefase start dinsdag 25 mei en eindigt woensdag 2 juni.  
Maandag 24 mei is het tweede Pinksterdag en zal het team niet werken  

## Afronding constructiefase:
In constructiefase 2 is de bovengenoemde functionaliteit af, maar nog niet gedemonstreerd aan de opdrachtgever. De demonstratie zal 03-06 plaatsvinden.  
Uiteraard worden functionaliteiten die niet voldoen aan de verwachting meegenomen naar constructie 3.  

# Constructiefase 3  
Constructiefase 3 is de laatste constructiefase van het project.  
De constructiefase loopt van 1-06 tot en met 11-06. Er zijn geen bijzondere dagen waardoor werkdagen uitvallen.  
  
De planning voor constructiefase 3 is als volgt:  

- Use case omtrent inlijstservice implementeren.  
- Afronding en afwerking van producten. (Details & Testen)  
- Eventueel functionaliteiten constructie 2 als niet naar wens.  
  

Opvallend hier is dat we in deze constructiefase ook met afronding bezig zullen zijn. Dit is i.v.m. de deadline die op 11-06 ligt.
Het is toegestaan na de deadline nog wel wijzigingen maken, maar deze moeten dan gepresenteerd worden. Het is lastig om de details te presenteren.  
Om te voorkomen dat we details moeten presenteren nemen we de tijd om de details in de constructiefase goed te maken.  
Grotere wijzigingen in de volgende fase, de transitiefase, kunnen dan makkelijk worden gepresenteerd.

## Afronding constructiefase 3
In de laatste constructiefase zijn alle beloofde functionaliteiten zoals hierboven genoemd afgerond.  
Tijdens de productreview van constructiefase 2 bleek dat er nog een aantal onderdelen mistte betreft het beheren van producten.  
Deze verbeteringen zijn opgepakt, samen met het beheren van de inlijstservice.  
  
Ook zijn er nieuwe testen ontwikkeld. Dit zijn integratietesten, waardoor we kunnen bevestigen dat de koppeling met MongoDB via Restheart goed werkt.  
Deze testen dragen bij aan de afronding van het project, net zoals de kwaliteitscontroles die we hebben uitgevoerd op zowel code als documentatie.  
  
Constructiefase 3 is de laatste constructiefase in het project.  
