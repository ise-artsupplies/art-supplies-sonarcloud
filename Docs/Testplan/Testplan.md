---
title: |
        Testplan\
        Art Supplies\
        ISE Project\
        Groep A4
date: 08-06-2021
---  
  
# Inleiding

Dit Test Plan beschrijft de testaanpak die binnen het ontwikkelteam zal worden gehanteerd. Het besteedt aandacht aan: testscope, testorganisatie, testtypen, 
testhulpmiddelen, testtooling en testomgeving.

Dit document bestaat uit:
1. Opdrachtformulering
2. Testbasis
3. Testaanpak
4. Tooling


# Opdrachtformulering

De testinspanningen binnen het ontwikkelteam hebben de volgende doelen:
- Een bijdrage leveren aan de kwaliteit van werkproducten.
- Meetbaar maken van de kwaliteit van werkproducten.
- Beoordelen van de geschiktheid voor oplevering van werkproducten.

## Binnen scope

De volgende zaken liggen binnen de scope om getest te worden.
- Alle use cases worden getest.  
- Integratie tussen SQL en MongoDB via Restheart.  
  + Ook de onderdelen die dit geheel mogelijk maken worden getest.  
- In SQL worden de rechten van verschillende gebruikers getest.

## Buiten scope

De volgende zaken liggen buiten de scope van dit document en worden niet opgepakt:
- Performance van de applicatie wordt niet getest.  
- Meerdere gebruikers tegelijk wordt niet getest.
- Er wordt niet getest met een andere restheart gebruiker. Alleen het standaard account wordt getest.  

# Testbasis 

In dit hoofdstuk wordt beschreven uit welke werkproducten de testgevallen worden afgeleid.

De volgende werkproducten worden gebruikt als basis voor testgevallen:
- Use Cases  
- Technisch ontwerp
- Functioneel ontwerp  
  
De use cases worden getest op basis van de verschillende flows in de use case beschrijving. Deze staat in het functioneel ontwerp genoteerd.  
Er wordt ook gekeken naar het technisch ontwerp, omdat hier belangrijke technische keuzes worden toegelicht en dus getest moeten worden.
 
De integratietest vangen onze use case af met het meeste technische risico. Deze risicolijst te vinden in het architectureel prototype.  

# Testaanpak  

De usecases worden getest d.m.v. unittesten. Dit betekent dat de testen geïsoleerd zijn, waardoor er geen invloeden van elders in de applicatie zijn.  
De unittests worden geschreven voordat de functionaliteit geimplementeerd is. Zo maken we gebruik van Test Driven Development.  
Na het implementeren van de functionaliteit moeten alle geschreven tests slagen. Ook moeten de andere testen nog slagen.  
 
De integratie tussen SQL en MongoDB wordt getest d.m.v. integratietesten. Het is hier van belang dat de data die in beide databases staan voorspelbaar zijn.  
Er zal tijdens het project een script worden gemaakt die data aanmaakt, met deze data zal worden getest omdat hier nog geen verdere wijzigingen op zijn gedaan.  
De integratietest zal werken op de volgende manier:
- Het script voor de voorspelbare data wordt uitgevoerd.        
  + Door het uitvoeren van dit script maakt restheart de MongoDB database opnieuw aan. Wanneer de database opnieuw aangemaakt word is de database leeg.  
  + Restheart zet alle data van de SQL database om naar de MongoDB database.
- In Postman ([zie: Tooling](#tooling)) worden aanvragen gemaakt die tests bevatten. Als de data niet is zoals verwacht is er iets fout gegaan en geeft het een foutmelding weer.  

# Tooling
Om het testproces te bevorderen maken we gebruik van een aantal tools.  
Een belangrijke tool (framework) is tSQLt. Om de testen uit te kunnen voeren moet tSqlT geïnstalleerd zijn op de betreffende database. 
Het testframework tSQLt zorgt ervoor dat we makkelijk tests kunnen schrijven. Er wordt functionaliteit aangeboden om een tabel na te maken, waardoor alleen de structuur van de tabel met de datatypen overblijft. 
De data is dus ook verwijderd en er staan geen beperkingen meer op de tabel. Deze beperkingen kunnen worden teruggezet met een ander commando, waarmee die 
specifieke beperking goed getest kunnen worden. Je weet dan zeker dat er geen andere invloeden zijn die de tests kunnen beïnvloeden.   
Daardoor is de test geïsoleerd, dat is typerend voor een unittest. Alle functionaliteiten van tSqlT hebben geen invloed op de echte database, waardoor nooit data verloren gaat.  

  
Ook is het belangrijk om restheart & mongodb werkend te hebben, zodat we later de integratietests kunnen uitvoeren.
De integratietests worden gemaakt in een applicatie genaamd "Postman". Met deze applicatie kunnen API's automatisch worden aangeroepen, en kan de inhoud van de reactie worden getest.  


## Bevindingsprocedure  
Na het implementeren van de functionaliteit worden alle tests uitgevoerd. Alle tests moeten dan nog slagen, zowel de unittests als de integratietests.  
Als een test faalt is het aan de ontwikkelaar om dit op te lossen voordat de functionaliteit wordt gereviewed.  

Als blijkt dat een test ongerelateerd aan de functionaliteit faalt, wordt onderzocht of dit inderdaad volledig ongerelateerd is aan de functionaliteit.
Wanneer is vastgesteld dat de test ongerelateerd is, moet worden onderzocht waarom deze test faalt. Om dit snel uit te zoeken wordt opgezocht wie de test in kwestie heeft gemaakt.  
De ontwikkelaar van test kan dan vervolgens de functionaliteit bekijken en tot een oplossing komen om de test te laten slagen.  
