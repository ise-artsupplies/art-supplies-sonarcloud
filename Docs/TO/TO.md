---
title: |
        Technisch Ontwerp\
        Art Supplies\
        ISE Project\
        Groep A4
date: 12-05-2021
---    

# Inleiding

Dit document bevat het technische ontwerp per functionaliteit. Het ontwerp wordt eerst gedocumenteerd in dit document voordat het gemaakt wordt in de code.  
Dit zorgt ervoor dat er eerst goed nagedacht moet worden voordat je begint, waardoor je mogelijke problemen in het denkproces er al uit haalt, in plaats van dat je in de code vastloopt.  
Dat scheelt veel tijd.

Bovendien dient het technisch ontwerp als koppeling tussen de functionaliteit uit het FO en de implementatie in de code. Dit zorgt voor goede traceability.  
Dit betekent dat je vanuit de code terug naar de requirement kan navigeren en vanaf de requirement naar de code.  
Zo is altijd duidelijk wat elk stukje code bijdraagt.  

De hoofdstukken die aan bod komen zijn:

1. Gedetailleerde ontwerpbeschrijving
2. Logisch relationeel schema
3. Tabellen beschrijvingen
4. Integrity rules
5. Ontwerp per functionaliteit
6. Indexen
7. Concurrency
8. Beveiliging  
  

# Gedetailleerde ontwerpbeschrijving

In dit hoofdstuk worden de ontwerpkeuzes en de beslissingen die zijn genomen beschreven.

## Deployment diagram

De overwogen ontwerpkeuzes voor het deployment diagram zijn beschreven in dit hoofdstuk.

![Figuur 1 Deployment diagram](./TO/Img/Deployment_diagram_ArtSupplies.png)

### Deployment diagram beschrijving

De applicatie draait op een server. Deze server kan zowel een microsoft server of een linux server zijn. Op deze server 
staat de ArtSupplies database, de Products database en RESTHeart. Wanneer er een website zou zijn gerealiseerd, communiceert 
de client machine direct met RESTHeart om de gegevens van de producten op te halen. De communicatie tussen deze twee artifacts
wordt niet in dit product gerealiseerd, dus ontbreekt de manier van communicatie tussen deze artifacts in het diagram. 

Het type van de server wordt niet uitgebreid gespecificeerd, omdat de server niet een onderdeel is van de oplevering.
Wel zou het informatiesysteem op elke server moeten kunnen draaien die de MSSQL database ondersteunt, en dockerimages kan
gebruiken. 

### Ontwerpkeuzes gerelateerd aan deployment

In dit hoofdstuk worden de ontwerpkeuzes van het deployment diagram beschreven. Voor elk keuze kijken we naar het
probleem en welke alternatieven er zijn. Ook beargumenteren we waarom we uiteindelijk voor de gemaakte keuze hebben
gekozen.

Installatiescript  

|                     |                                                                   |
|:--------------------|:------------------------------------------------------------------|
| Probleem/Issue      | Tijdens het ontwikkelen komt er vaker voor dat de versies van de databases van de programmeurs niet gelijk lopen. |  
| Gemaakte keuze      | Door een installatiescript die de hele database opnieuw opzet zorgen wij ervoor dat de database altijd volledig up to date is |  
| Alternatief         | Een aparte branch waar altijd de laatste versies van de scripts staan. Vanuit daar alle scripts handmatig uitvoeren. |  
| Argumenten          | Door het installatiescript hoeven niet alle bestanden handmatig uitgevoerd te worden, en dat scheelt heel veel tijd. Bovendien is het makkelijk een bestand te vergeten, waardoor niet de volledige database up to date is. |

MongoDB 4.4.5  
De opdrachtgever heeft aangegeven graag MongoDB te willen gebruiken. Versie 4.4.5 is de laatste versie op het moment van
schrijven.

# Logisch Relationeel Schema  
![LRS](./TO/Img/PDM.png)

In de bovenstaande afbeelding is het Logisch Relationeel Schema (LRS) te zien. Er zijn weinig verschillen vergeleken met
het ERM, die te vinden is in het Functioneel ontwerp.  
De verschillen die te zien zijn, hebben te maken met koppeltabellen. Deze ontstaan wanneer entiteiten een veel-op-veel
relatie hebben.  
Er is dan een extra tabel nodig om de juiste rijen met elkaar te koppelen, een koppeltabel dus.
Er zijn enkele tabellen bijgekomen, deze zijn te zien in de afwijkende kleur.

De records van de subtypen staan in dit datamodel bij zowel de parent als het subtype. De kolommen van de parent komen niet in de subtypes voor,
behalve de primary key. Dit maakt het makkelijker om relaties met elk product te leggen.

## Tabellen beschrijvingen

In dit hoofdstuk beschrijven we alle kolommen en tabellen die in het physical data model zitten.

### Product

| Product              |                                               |
|:---------------------|:----------------------------------------------|
| Productnr            | Het unieke nummer van een product.            |
| Product_Type         | Het type van het product.                     |
| Productomschrijving  | De beschrijving van het product.              |
| Product_Voorraad     | De voorraad van het product.                  |
| Product_Verkoopprijs | De prijs waarvoor het product wordt verkocht. |
| Merknaam | De naam van het merk |  

### Producttype

| Producttype  |                           |
|:-------------|:--------------------------|
| Product_Type | Het type van het product. |

### Merk

| Merk     |                      |
|:---------|:---------------------|
| Merknaam | De naam van het merk |  

### Product_In_Set

| Product_In_Set        |                                                   |
|:----------------------|:--------------------------------------------------|
| Productnr             | Het nummer van het product dat in de set nummer.  |
| Set_Productnr         | Het nummer van de set waar de producten inzitten. |
| Product_In_Set_Aantal | Hoe vaak een product in een set zit.              |  

### SetProduct

| SetProduct            |                                       |
|:----------------------|:--------------------------------------|
| Productnr             | Het unieke nummer van het setproduct. |
| SetProduct_Verpakking | De verpakking van set.                |    

### Materiaal

| Materiaal               |                            |
|:------------------------|:---------------------------|
| Materiaal_Materiaalnaam | De naam van het materiaal. |   

### Techniek

| Techniek              |                          |
|:----------------------|:-------------------------|
| Techniek_Technieknaam | De naam van de techniek. |

### KwastProduct

| KwastProduct            |                                                        |
|:------------------------|:-------------------------------------------------------|
| Productnr               | Het unieke nummer van het kwastproduct.                |
| Materiaal_Materiaalnaam | De naam van het materiaal waarmee de kwast is gemaakt. |
| PrijsgroepPrijsklasse   | De prijsklasse waar het kwastproduct zich in bevindt.  |
| Kwast_Maat              | De maat van de kwast.                                  |     
| Kwast_Vorm              | De vorm die de kwast heeft.                            |

### KwastProduct_Techniek

| KwastProduct_Techniek |                                                                   |
|:----------------------|:------------------------------------------------------------------|
| Techniek_Technieknaam | De naam van de techniek waarvoor het kwastproduct gebruikt wordt. |
| Productnr             | Het unieke nummer van het kwastproduct.                           |

### Prijsgroep

| Prijsgroep             |                               |
|:-----------------------|:------------------------------|
| Prijsgroep_Prijsklasse | Het nummer van de prijsgroep. | 
| Prijsgroep_Prijs       | De prijs van de prijsgroep.   |

### Lijst

| Lijst                |                                                   |
|:---------------------|:--------------------------------------------------|
| Lijst_Lijstnummer    | Het unieke nummer van een lijst.                  |   
| LijstType_Typenummer | Het unieke nummer van het LijstType van de lijst. |
| Lijst_Prijs          | De verkoopprijs van de lijst.                     |
| Lijst_Lijstnaam      | De naam van de lijst.                             |
| Merknaam | De naam van het merk |  

### LijstType

| LijstType            |                                       |
|:---------------------|:--------------------------------------|
| LijstType_TypeNummer | Het unieke nummer van een type lijst. |   
| LijstType_Soort      | De beschrijving van het soort lijst.  |

### VerfProduct

| VerfProduct               |                                                      |
|:--------------------------|:-----------------------------------------------------|  
| Productnr                 | Het unieke nummer van het verfproduct.               |
| Kleur_Kleurcode           | De code van de kleur van de verf.                    |
| Transparantie_Waarde      | De waarde van de transparantie van de verf.          |
| Prijsgroep_Prijsklasse    | De prijsklasse waar het verfproduct zich in bevindt. |
| Verfproduct_Inhoud        | De inhoud van de verf.                               |
| Verfproduct_Verdunner     | Het ingrediënt waarmee de verf kan worden verdunt.   |

### VerfProduct_Eigenschap

| VerfProduct_Eigenschap    |                                                      |
|:--------------------------|:-----------------------------------------------------|
| Eigenschap_Eigenschapcode | De code van de eigenschappen van de verf.            |
| Product_Productnr         | Het unieke nummer van het product.                   |

### Waarschuwingscode

| Waarschuwingscode                   |                                               |
|:------------------------------------|:----------------------------------------------|  
| Waarschuwingscode_Waarschuwingscode | De unieke code voor een waarschuwing.         |
| Pigmentnaam                         | De naam van het pigment van de verfkleur.     |
| Waarschuwingscode_Beschrijving      | Beschrijving van wat de waarschuwingscode is. |  

### VerfProduct_Waarschuwing

| VerfProduct_Waarschuwing            |                                     |
|:------------------------------------|:------------------------------------|
| Waarschuwingscode_Waarschuwingscode | De unieke code van de waarschuwing. |
| Productnr                           | De unieke code van het verfproduct. |

### Pigment

| Pigment     |                          |
|:------------|:-------------------------|  
| Pigmentnaam | De naam van een pigment. |

### Kleur

| Kleur                   |                                        |
|:------------------------|:---------------------------------------|  
| Kleur_Kleurcode         | De unieke code voor een kleur.         |
| Kleur_Kleuromschrijving | Beschrijving van een kleur.            |              
| Kleur_PGWaarde          | Dit geeft een nummer van de PG waarde. |  

### Kleur_Gemaakt_Van_Pigment

| Kleur_Gemaakt_Van_Pigment |                                |
|:--------------------------|:-------------------------------|
| Pigmentnaam               | De naam van een pigment.       |
| Kleur_Kleurcode           | De unieke code voor een kleur. |

### Eigenschap

| Eigenschap                |                                        |
|:--------------------------|:---------------------------------------|  
| Eigenschap_Eigenschapcode | Unieke code voor een eigenschapcode.   |
| Eigenschap_Beschrijving   | Beschrijving van wat de eigenschap is. |

### Transparantie

| Transparantie        |                                                       |
|:---------------------|:------------------------------------------------------|  
| Transparantie_Waarde | Unieke vorm/icon over hoe transparant het product is. |

### Verdunner

| Verdunner             |                                                       |
|:----------------------|:------------------------------------------------------|  
| Verfproduct_Verdunner | Het ingrediënt waarmee de verf kan worden verdunt.    |


### FramingOrder

| FramingOrder                   |                                                    |
|:-------------------------------|:---------------------------------------------------|  
| FramingOrder_Ordernummer       | Het unieke nummer van een framing order.           |
| Glas_Glassoort                 | Het soort glas.                                    |
| Framer_FramerId                | Het unieke nummer van een framer.                  |
| FramingOrder_Breedte_Kunstwerk | Hierin word de breedte van de lijst in opgeslagen. |
| FramingOrder_Hoogte_Kunstwerk  | Hierin word de hoogte van de lijst in opgeslagen.  |
| FramingOrder_Prijs             | Dit is de Prijs van de framing order.              |
| FramingOrder_Opleveringsdatum  | De datum van wanneer het word opgeleverd.          |
| FramingOrder_Aanvraagdatum     | De datum van wanneer het is aangevraagd.           |

### KunstwerkSoort

| KunstwerkSoort           |                                          |                                    
|:-------------------------|:-----------------------------------------|  
| KunstwerkSoort_Soortnaam | Naam van een kunstwerk soort.            | 

### FramingOrderMetPassePartout

| FramingOrderMetPassePartout     |                                                    |
|:--------------------------------|:---------------------------------------------------|  
| FramingOrder_Ordernummer        | Het unieke nummer van een framing order.           |
| PassePartout_PassePartoutNummer | Het unieke nummer van een passe partout.           |

### Glas

| Glas           |                 |
|:---------------|:----------------|  
| Glas_Glassoort | Het soort glas. |

### Framer

| Framer          |                                   |
|:----------------|:----------------------------------|  
| Framer_FramerId | Het unieke nummer van een framer. |
| Framer_Naam     | De naam van de framer.            |

### Papiersoort

| Papiersoort            |                               |
|:-----------------------|:------------------------------|  
| Papiersoort_Papiernaam | De naam van het soort papier. |

### PassePartout

| PassePartout                    |                                          |
|:--------------------------------|:-----------------------------------------|  
| PassePartout_PassePartoutNummer | Het unieke nummer van een passe partout. |
| Papiersoort_Papiernaam          | De naam van het soort papier.            |
| PassePartoutSoort_SoortNaam     | De naam van het soort passe partout.     |
| PassePartout_Kleur              | De kleur van de passe partout.           |
| PassePartout_Prijs              | De verkoopprijs van de passe partout.    |


### PassePartoutSoort
| PassePartoutSoort           |                                      |
|:----------------------------|:-------------------------------------|  
| PassePartoutSoort_SoortNaam | De naam van het soort passe partout. |


## Ontwerpkeuzes gerelateerd aan het pdm
### Koppeltabellen
|                     |                                                                   |
|:--------------------|:------------------------------------------------------------------|
| Probleem/Issue      | Bij veel op veel relaties ontstaat veel onnodig dubbele data. |  
| Gemaakte keuze      | Koppeltabellen die entiteiten koppelen aan elkaar door middel van primaire sleutels. |  
| Alternatief         | De gekoppelde entiteit als attribuut in de hoofd entiteit en opnemen in de primaire sleutel. |  
| Argumenten          | Als er wordt gekozen voor het alternatief zal er dus veel onnodige data in de database komen te staan. Door een koppeltabel te gebruiken worden de primaire sleutels van de entiteiten aan elkaar gekoppeld om dubbele data te voorkomen. |

## Integriteitsregels

In dit hoofdstuk worden de integriteitsregels besproken. Er wordt specifieker in gegaan op de constraints van het
systeem en de manier waarop deze worden geïmplementeerd.

I1 correspondeert met C1

- Specificatie: Producten van het Producttype Set kunnen geen relatie hebben met een ander product van het Producttype
  Set.
- Implementatie: Trigger TR_PRODUCTS_IN_SET op Product_In_Set tabel.

I2 correspondeert met C2

- Specificatie: Als product een kwast of verf is, is verkoopprijs leeg en prijsgroep bekend.
- Implementatie: Check-constraint op kolommen Producttype en verkoopprijs in Product tabel.

I3 correspondeert met C6

- Specificatie: Aanvraagdatum moet altijd eerder zijn dan opleverdatum.
- Implementatie: Check-constraint Check_Aanvraagdatum op Framingorder tabel.


### Toelichting constraints niet opgenomen als integriteitsregels

C3, C4 en C5 zijn niet opgenomen als integriteitsregels. Dit is omdat C3 met de domeinconstraints al wordt afgehandeld, en 
omdat C4 en C5 worden afgehandeld door hun betreffende attributen primaire sleutels te maken. Deze primaire sleutels zijn
automatisch gegenereerd uit het pdm.

# Foutmeldingen  
Om constraints goed te kunnen bewaken hebben wij onze eigen foutmeldingen aangemaakt. Hieronder is een overzicht te zien van alle error codes die zijn aangemaakt, en dus niet SQL standaard zijn.  

| Foutmeldingcode             | Foutmelding tekst                    |
|:----------------------------|:-------------------------------------|  
| 50001                       | A set cannot be part of another set|
| 50002                       | A set has to be of product type set|
| 50003                       | No changes were made in pigment for this color |
| 50004                       | Product must be of type Kwast |
| 50005                       | Product must be of type Verf|
| 50006                       | Product cannot be deleted as it is part of a set |
| 50007                       | No FRAMINGORDERMETPASSEPARTOUT exists with the given FRAMINGORDER_ORDERNUMMER |
| 50009                       | Framing order does not exist |  
| 50010                       | FRAMINGORDER with the given FRAMINGORDER_ORDERNUMMER already is of type FRAMINGORDERMETPASSEPARTOUT | 
| 50011                       | Connection failed with Restheart |  
| 50012                       | No FRAMINGORDER exists with the given FRAMINGORDER_ORDERNUMMER |
  

# Beschrijving functionaliteit

In dit hoofdstuk wordt de implementatie van de use cases beschreven. Ook wordt er toegelicht welke ontwerpkeuzes er zijn
gemaakt.

## Beheren producten (CRUD)
Het beheren van producten kan niet door iedereen worden gedaan. De beveiliging is beschreven in het [hoofdstuk beveiliging](#beveiliging).

### Aanmaken product  
Het aanmaken van een product betreft twee rollen: de systeembeheerder, die de domeintabellen vult, en de medewerker.  
De medewerker kan wanneer de domeintabellen gevuld zijn producten aanmaken.

De domeintabellen worden dus gevuld door de systeembeheerder. Dit is tijdens een overleg met de opdrachtgever besproken, op 18-05-2021.  
Het vullen van de domeintabellen wordt met gewone INSERT statements gedaan.  
Belangrijk is dat de domeintabellen worden gevuld voordat de beheerder van de winkel producten gaat aanmaken. Dit zal namelijk niet lukken i.v.m. foreign key constraints.  

Voor het toevoegen van producten zijn er 4 stored procedures gemaakt:
1. Stored procedure (SP) voor tabel Product
2. SP voor tabel Verfproduct
3. SP voor tabel Kwastproduct
4. SP voor tabel SetProduct


Dit wordt met stored procedures gedaan omdat applicaties die de database gaan gebruiken bij veranderingen niet opnieuw gecompileerd hoeven worden.  
Daarmee zijn die applicaties dus niet (kort) offline.

Omdat de tabellen verfproduct en kwastproduct een product zijn, zullen de stored procedures van deze tabellen de Product stored procedure aanroepen.

Dit wordt wel conditioneel gedaan. Als het product al bestaat in de tabel product, wordt het aanmaken van het product overgeslagen, 
en alleen het product in de betreffende subtype tabel aangemaakt.

#### Implementatie  
De 4 stored procedures zoals beschreven zijn als volgt geimplementeerd:  
  
- Sp_Createproduct
    + Parameters: Alle kolommen met dezelfde datatypes als tabel Product.
    + Betrokken tabellen: Product
- Sp_Createverfproduct
    + Parameters: Alle parameters van Sp_Createproduct, met alle aanvullende kolommen van de tabel Verfproduct.  
    + Betrokken tabellen: Product & Verfproduct
- Sp_Createkwastproduct
    + Parameters: Alle parameters van Sp_Createproduct, met alle aanvullende kolommen van de tabel Kwastproduct.
    + Betrokken tabellen: Product & Kwastproduct
- Sp_Createsetproduct
    + Parameters: Alle parameters van Sp_Createproduct, met alle aanvullende kolommen van de tabel Setproduct.
    + Betrokken tabellen: Product & Setproduct  
    
### Bijwerken product  
Het bijwerken van producten is nuttig voor de systeembeheerder en de medewerkers van de winkel.  
De gegevens van producten kunnen veranderen en daarom maken we het mogelijk om de producten te bewerken.  
  
Voor elke product soort is een stored procedure aangemaakt. Deze wordt aangeroepen op dezelfde manier als de stored procedures voor het aanmaken 
van de producten. Het enige verschil hierin is dat de waarden worden bewerkt in plaats van een nieuw product wordt aangemaakt.
De volgende tabellen hebben een stored procedure voor het bijwerken:
- Product
- Setproduct
- Kwastproduct
- Verfproduct

Ook is het mogelijk dat een product van type veranderd. Om deze reden zijn er voor elke productsoort 
stored procedures aangemaakt die een product met het gegeven productnummer veranderen naar het nieuwe type.
Deze drie betreffende stored procedures zijn:
- Sp_Updateproducttokwastproduct
- Sp_Updateproducttosetproduct
- Sp_Updateproducttoverfproduct

Deze worden met dezelfde attributen aangeroepen als de normale bijwerk stored procedures.

Ook hier is gekozen voor stored procedures omdat applicaties die de database gaan gebruiken bij veranderingen niet opnieuw gecompileerd hoeven te worden.  
Daarmee zijn die applicaties dus niet (kort) offline.
Bovendien weet de applicatie nu niet van de implementatie van de stored procedure, maar alleen van zijn resultaten.  
Dat betekent dus dat de stored procedure gemakkelijk uitbereid kan worden. Dit leunt sterk tegen het Open Closed principe van SOLID.

#### Sp_Updateproducttokwastproduct
Beschrijving: Update een record in product en maakt van dat product een kwastproduct aan. Verwijderd ook de gegevens uit een ander type product indien die bestaat.
Betrokken tabellen: Product, Kwastproduct (Setproduct, verfproduct).
Parameters: Elk attribuut van een kwastproduct.

#### Sp_Updateproducttosetproduct
Beschrijving: Update een record in product en maakt van dat product een setproduct aan. Verwijderd ook de gegevens uit een ander type product indien die bestaat.
Betrokken tabellen: Product, Setproduct (Kwastproduct, verfproduct).
Parameters: Elk attribuut van een setproduct.

#### Sp_Updateproducttoverfproduct
Beschrijving: Update een record in product en maakt van dat product een verfproduct aan. Verwijderd ook de gegevens uit een ander type product indien die bestaat.
Betrokken tabellen: Product, Verfproduct (Setproduct, kwastproduct).
Parameters: Elk attribuut van een verfproduct.


### Verwijderen product
Het verwijderen van een product betreft een persoon, de beheerder.
Zoals bij het aanmaken van een product gebeurt het verwijderen ervan met een stored procedure.
Voor het verwijderen hebben we echter maar een stored procedure nodig.
Deze stored procedure checked eerst of het product dat verwijderd gaat worden deel uit maakt van een set en als dit zo is geeft hij een error.
Als dit niet zo is worden alle records waar het productnummer in voorkomt verwijderd.

#### Sp_Deleteproduct

Beschrijving: Verwijdert een record uit tabel product en uit een subtype van product als dat bestaat met het opgegeven productnummer. 
Betrokken tabellen: PRODUCT, VERFPRODUCT, KWASTPRODUCT, SETPRODUCT, VERFPRODUCT_WAARSCHUWING, VERFPRODUCT_EIGENSCHAP, KWASTPRODUCT_TECHNIEK, PRODUCT_IN_SET.
Parameter: @Productnr: productnummer van het te verwijderen product.

## Beheren inlijstverzoeken  
Kunstwerk inlijsten is een crud use case, want het gaat alleen over het toevoegen, bijwerken en verwijderen van
records in de "FRAMINGORDER" en "FRAMINGORDERMETPASSEPARTOUT" tabellen. Wie recht hebben op het gebruik hiervan
worden in het [hoofdstuk beveiliging](#beveiliging) beschreven.

### Toevoegen inlijstverzoek

Om framing orders te kunnen toevoegen zijn stored procedures geschreven. De stored procedures 
voegen nieuwe verzoeken en word het gecontroleerd of het al bestaat of niet.


#### sp_Create_FramingOrder  

Beschrijving: Voegt een record in FRAMINGORDER met het aangegeven FRAMINGORDER_ORDERNUMMER.
Betrokken tabellen: FRAMINGORDER.
Parameters: Elk attribuut van de tabel FRAMINGORDER.
  
#### Sp_Create_FramingOrderMetPassepartout

Beschrijving: Voegt een record in FRAMINGORDER en FRAMINGORDERMETPASSEPARTOUT met het aangegeven FRAMINGORDER_ORDERNUMMER.
Betrokken tabellen: FRAMINGORDER, FRAMINGORDERMETPASSEPARTOUT.
Parameters: Elk attribuut van de tabel FRAMINGORDER, en het attributen van de passepartout.

### Bijwerken inlijstverzoek

Voor het updaten van een framing order moet de normale framing order moeten kunnen update, maar ook het subtype met een passepartout.
Hieronder is de implementatie beschreven van de stored procedures.

#### sp_update_framingOrder

Beschrijving: Update een record in FRAMINGORDER met het aangegeven FRAMINGORDER_ORDERNUMMER.
Betrokken tabellen: FRAMINGORDER.
Parameters: De parameters representeren elk attribuut van de tabel FRAMINGORDER.

#### sp_update_framingOrderMetPassePartout

Beschrijving: Update een record in FRAMINGORDER en FRAMINGORDERMETPASSEPARTOUT met het aangegeven FRAMINGORDER_ORDERNUMMER.
Betrokken tabellen: FRAMINGORDER, FRAMINGORDERMETPASSEPARTOUT.
Parameters: De parameters representeren elk attribuut van de tabel FRAMINGORDER, en het nummer van de passepartout.

#### sp_setFramingOrderToFramingOrderMetPassepartout

Ook willen we de mogelijkheid hebben om de subtype van de framing order te veranderen. Daar dient deze stored procedure voor.

Beschrijving: Voeg een record toe aan FRAMINGORDERMETPASSEPARTOUT met het opgegeven order nummer en passepartoutnummer.
Betrokken tabellen: FRAMINGORDER, FRAMINGORDERMETPASSEPARTOUT.
Parameters: @FRAMINGORDER_ORDERNUMMER FRAMINGORDERNUMMER: Het ordernummer van de te wijzigen FRAMINGORDER, @PASSEPARTOUT_PASSEPARTOUTNUMMER PASSEPARTOUTNUMMER: De nummer van de passepartout.

#### sp_setFramingOrderMetPassepartoutToFramingOrder

Ook willen we de mogelijkheid hebben om de subtype van de framing order te veranderen. Daar dient deze stored procedure voor.

Beschrijving: Verwijder een record uit FRAMINGORDERMETPASSEPARTOUT met het opgegeven order nummer.
Betrokken tabellen: FRAMINGORDER, FRAMINGORDERMETPASSEPARTOUT.
Parameters: @FRAMINGORDER_ORDERNUMMER FRAMINGORDERNUMMER: Het ordernummer van de te wijzigen FRAMINGORDER

### Verwijderen inlijstverzoek
Bij het verwijderen van een inlijstverzoek zijn de inlijster en beheerder betrokken.
Voor het verwijderen is een stored procedure geschreven waar het ordernummer aan mee wordt gegeven. 
In deze stored procedure wordt gecontroleerd of het inlijstverzoek een passepartout heeft. 
Zo wel worden de gegevens die de passepartout koppelen aan het verzoek eerst verwijderd en daarna de aanvraag zelf.  
  
Er is weer gekozen voor een stored procedure om dezelfde reden als bij het beheren van producten.

### Zoeken product

De zoekfunctionaliteit moet de mogelijkheid bieden om op elk attribuut van een product te kunnen zoeken. Deze attributen
zijn verspreid over meerdere tabellen in de MSSQL database. Zoals te zien in
het [logisch relationeel schema](#Logisch-relationeel-schema), heeft een product van subtype verf 4 tabellen tussen zijn
productinformatie en de waarschuwingen van zijn pigment. Dit betekent dat voor elke zoekopdracht de database naar een
groot gedeelte van de database moet kijken. Wanneer er veel requests vanuit de webshop binnenkomen, kan dit voor een
lagere performance zorgen. Omdat er op elk attribuut gezocht moet kunnen worden, moet de zoekfunctionaliteit bij de
implementatie op de MSSQL database op meerdere plekken worden aangepast.

Daarom is ervoor gekozen om gebruik te maken van een staging area om de productinformatie naar een makkelijker mee te
werken format te exporteren. De onderzochte functionaliteit in
het [architecturele prototype](../Prototype/Architectureel%20Prototype.md) is hiervoor hergebruikt en uitgebreid.
Een uitgebreide beschrijving van het effect op de
deployment is te zien in het [deployment diagram](#deployment-diagram).

De records die worden toegevoegd aan de MongoDB database bevatten alle eigenschappen van een product, in 1 entiteit. Dat betekent dus dat onze MongoDB database niet genormaliseerd is.  
Dit is niet erg bij het zoeken, want nu kan er makkelijk gezocht worden op elke eigenschap van een product zonder relaties af te gaan.  
Het zoeken is dus eenvoudiger geworden, want nu kan de API van restheart worden aangeroepen om te zoeken op elke eigenschap.

Om producten te zoeken hebben we GET requests aangemaakt waarmee we het bestand kunnen aanroepen en door filter te gebruiken op condities
kunnen gaan zoeken. We hebben hiervoor gekozen omdat het flexibel is. Als we een product met meerdere condities tegelijk willen tonen kunnen we dat
er bij toevoegen.

Om het zoeken op product te laten werken is er de API RESTHeart en de database MongoDB nodig. Voor onze doeleinden zijn
alleen de gratis versies hiervan voldoende. In 
het [architecturele prototype](../Prototype/Architectureel%20Prototype.md) wordt beschreven waarom er voor MongoDB en
RESTHeart is gekozen.

De datakwaliteit van de data in de MongoDB database wordt niet gewaarborgd in MongoDB zelf. Omdat alle data afkomstig is uit de SQL database.  
De datakwaliteitbewaking zit dus in de SQL database.

Voor het zoeken van producten hebben we twee bestanden aangemaakt genaamd "test.postman.collection.json" en
"Zoeken_Naar_Producten_GFilters.md".

Met het Json bestand "JsonProductenNoSQL" kan de gebruiker de MongoDB database via een get request naar RESTHeart opsturen.
Hierna kan de gebruiker gelijk alle tests doorlopen in "test.postman.collection.json" om te controleren of alle tests werken.
Daarnaast hebben we een bestand genaamd "Zoeken_Naar_Producten_GFilters.md" aangemaakt. Hierin zijn alle
Filter GET requests in beschreven. Dit document zorgt er dus voor dat als het later geïmplementeerd wordt in een interface,
de developers gemakkelijk met de requests het systeem verder kunnen uitbreiden. De reden dat het Json bestand niet genoeg is,
komt omdat er veel '/' en andere tekens in voorkomen tussen de requests. Dit zorgt er dus voor dat het structuur van de code
niet duidelijk is en het veel tijd gaat besparen om het in de volgende constructie fases te implementeren.

### Implementatie

In dit hoofdstuk worden de bestanden beschreven die zijn gebruikt voor de use case, en hun functionaliteit.

#### Sp_encodeBase64.sql

Deze stored procedure encrypt een string naar base 64 format. Ook worden de instellingen "ansi_nulls" en "
quoted_identifier" in de database op aan gezet.

Parameter: @input varchar(255) out

#### Sp_getRestheartURL.sql

Deze stored procedure returned een string die het url bevat voor waar RESTHeart draait.

Parameter: @url nvarchar(255) out

#### Sp_getDbUrl.sql

Deze stored procedure returned een string die het url van de collectie op de MongoDB database via RESTHeart bevat. De
waarde van de parameter wordt aan dit url vastgeplakt.

Parameter: @url nvarchar(255) out

#### Sp_getAuthorizationValue.sql

Deze stored procedure returned een string die de waarde bevat die nodig is voor de 'Authorization' header van de
requests naar RESTHeart. De parameter is als volgt: 'gebruikersnaam:wachtwoord'.

Parameter: @combined varchar(255) out

#### Sp_createDatabase.sql

Deze stored procedure verwijderd de collectie draaiende op de MongoDB instantie met de naam 'products'. Deze database
staat onder de database die wordt gedraaid op RESTHeart. De default database daarvan is database 'RESTHeart'. Daarna
maakt hij deze collectie opnieuw aan, waarna deze leeg is.

#### Sp_getETagIfExists.sql

Deze stored procedure haalt de 'E-Tag' op van de opgegeven url. Als er geen resource op dat url bestaat, wordt er geen
E-Tag teruggegeven.

Parameters: @path nvarchar(255), @etag nvarchar(4000) out

#### Sp_deleteResourceByETag.sql

Deze stored procedure verwijdert het object in de staging area die de opgegeven E-Tag heeft.

Parameters: @path nvarchar(255), @etag nvarchar(4000) out

#### Sp_generateKwastproductJsonByProductnr.sql

Deze stored procedure genereert de productinformatie van het kwastproduct met het gegeven productnummer voor de MongoDB
database. Deze json is de output op @jsonObject. Er is ervoor gekozen om als product_verkoopprijs de prijs van de prijsgroep te
gebruiken. Hierdoor hoeft de ontwikkelaar van de website niet te controleren of het product wel een prijs heeft. Normaal zou
de product_verkoopprijs leeg zijn als het product een prijsgroep heeft.

Parameters: @productnr int, @jsonObject varchar(max) out  
Betrokken tabellen: PRODUCT, KWASTPRODUCT, KWASTPRODUCT_TECHNIEK, PRIJSGROEP

#### Sp_generateProductJsonByProductnr.sql

Deze stored procedure genereert de productinformatie van het product met het gegeven productnummer voor de MongoDB
database. Deze json is de output op @jsonObject.

Parameters: @productnr int, @jsonObject varchar(max) out  
Betrokken tabellen: PRODUCT

#### Sp_generateVerfproductJsonByProductnr.sql

Deze stored procedure genereert de productinformatie van het verfproduct met het gegeven productnummer voor de MongoDB
database. Deze json is de output op @jsonObject. Er is ervoor gekozen om als product_verkoopprijs de prijs van de prijsgroep te
gebruiken. Hierdoor hoeft de ontwikkelaar van de website niet te controleren of het product wel een prijs heeft. Normaal zou
de product_verkoopprijs leeg zijn als het product een prijsgroep heeft.

Parameters: @productnr int, @jsonObject varchar(max) out  
Betrokken tabellen: PRODUCT, VERFPRODUCT, PRIJSGROEP, VERFPRODUCT_WAARSCHUWING, WAARSCHUWINGSCODE,
VERFPRODUCT_EIGENSCHAP, EIGENSCHAP, KLEUR, KLEUR_GEMAAKT_VAN_PIGMENT, WAARSCHUWING_VAN_PIGMENT

#### Sp_generateSetproductJsonByProductnr.sql

Deze stored procedure genereert de productinformatie van het setproduct met het gegeven productnummer voor de MongoDB
database. Deze json is de output op @jsonObject.

Parameters: @productnr int, @jsonObject varchar(max) out  
Betrokken tabellen: PRODUCT, SETPRODUCT, PRODUCT_IN_SET, VERFPRODRUCT, KWASTPRODUCT

#### Sp_directJsonGenerationAndPostDatabaseToStagingArea.sql

Deze stored procedure stuurt voor elk product in de database een request met zijn productinformatie naar RESTHeart, die
het in de MongoDB database zet. Ook voert hij hiervoor eerst de stored procedure "sp_createDatabase" uit.

Betrokken tabellen: PRODUCT, VERFPRODUCT, SETPRODUCT, KWASTPRODUCT

#### job.sql

Maakt een job en een schedule op de server aan, en zet de job op de schedule. Deze job wordt hierdoor elke dag om 06.00h
uitgevoerd. Deze job bestaat uit het uitvoeren van de stored procedure "Sp_directJsonGenerationAndPostDatabaseToStagingArea".

#### Staging-Area-Setup.sql

Veranderd de instellingen 'show advanced options' en 'OLE Automation Procedures' naar 1. Daarna voert hij de stored
procedure "sp_createDatabase" uit.

### Koppeltabellen
#### Sp_Productinsert
Deze stored procedure voegt een product toe aan een set. De attributen die de procedure verwacht zij  het setnr, productnr en het aantal.

Betrokken tabel: PRODUCT_IN_SET

#### Sp_Kwastproducttechniek
Deze stored procedure voegt een techniek en een productnummer toe aan een koppeltabel. 

Betrokken tabel: KWASTPRODUCT_TECHNIEK

#### Sp_Verfproductwaarschuwing
Deze procedure dient ervoor om waarcshuwingen van producten aan de producten te koppelen. 
Er wordt in een koppeltabel de waarde waarschuwingscode en productnummer toegevoegd.

Betrokken tabel: VERFPRODUCT_WAARSCHUWING

#### Sp_Verfproducteigenschap
Hier worden de eigenschappen van verfproducten gekoppeld aan de producten door middel van het toevoegen van waarden in de koppeltabel.

Betrokken tabel: VERFPRODUCT_EIGENSCHAP

### Ontwerpkeuzes

#### Hoe en wanneer verzenden we de producten naar de staging area?

De staging area zou eerst bij elke insert, update en delete event in de database moeten worden geupdate. Dit plan was
opgesteld toen de producten in de staging area alleen hun prijs en productnummers hadden. Nadat er was besloten om alle
informatie van de producten in de staging area te hebben, stuiten we op problemen. Elke tabel die verbonden staat met de
producten zou dan triggers moeten hebben om hun updates, inserts en deletes bij te houden. Er was al eerder overwogen om
i.p.v. een event-driven staging area te implementeren de hele database om de zoveel tijd in zijn geheel naar de staging
area te verzenden. Deze oplossing is makkelijker te implementeren, en het zorgt ervoor dat de database niet op bijna
alle tabellen een trigger moet hebben. Daarom is ervoor gekozen om zo de staging area te implementeren.

## Zoeken in geschiedenis

Het zoeken in de geschiedenis wordt mogelijk gemaakt doordat we voor elke tabel een geschiedenis tabel maken.  
Dit gebeurt met code generatie, omdat de code voor het maken van deze tabellen zoveel op elkaar lijkt. Alleen de tabelnamen hoeven namelijk verandert te worden.  
Deze geschiedenistabellen hebben de actie, timestamp + de primaire sleutel van de bijbehorende tabel als primaire sleutel.  
  
Voor elke tabel wordt ook een trigger gegenereerd, die de geschiedenistabel vult. Dit zorgt ervoor dat de geschiedenistabel altijd bijgewerkt wordt.  
Als de tabelstructuur verandert van een tabel, moeten de tabellen opnieuw gegenereerd worden. Dit kan eventueel geautomatiseerd worden door een DDL trigger te implementeren.  
Dat valt echter buiten de usecase.

Het zoeken op datageschiedenis kan met gebruikelijke SELECT statements gedaan worden. Echter hebben wij er voor gekozen onze queries op te slaan in stored procedures.
Dit is handig voor wanneer ons systeem geïntegreerd wordt in een ander systeem, of systemen.
Als alle applicaties de stored procedure aanroepen, zijn dit de voordelen:
- De query in de stored procedure kan aangepast worden, zonder alle afhankelijke systemen opnieuw te compileren en uit te rollen.  
- Het is altijd duidelijk welke tabellen geraakt worden met de query, want de query is vast en dus niet te beïnvloeden vanaf de applicatie. (Behalve mogelijke parameters)  
- Dezelfde select query staat op een centrale plaats, i.p.v. meerdere applicaties.  

### Implementatie

#### SpChanges_In_Price
Deze stored procedure haalt prijsveranderingen van een product op. dit werkt voor producten met en zonder een prijsgroep
Betrokken tabellen:
hist.product, hist.verfproduct, hist.kwastproduct en dbo.product.
Parameter(s) :
@Productnr PRODUCTNUMMER

#### SpChanges_In_Pigment
Deze stored procedure haalt de veranderingen van het gebruikte pigment van een verfproduct op.
Betrokken tabellen:
Hist.Kleur_Gemaakt_Van_Pigment en dbo.verfproduct.
Parameter(s) :
@Productnr PRODUCTNUMMER 
  

#### SpChanges_By_User
Deze stored procedure haalt de veranderingen op die door een gebruiker gedaan zijn op een table.
Betrokken tabellen:
Alle history tabellen kunnen betrokken zijn maar specifiek de tabel die meegegeven word door de variabele @Table.
Parameter(s):
@Username VARCHAR(255)  
@Table SYSNAME

# Indexen

Indexes in een database kunnen de performance van queries erg verbeteren. In onze database maken wij echter alleen gebruik
van queries die slechts de indexen die automatisch op de primaire sleutels worden gelegd, en op de vreemde sleutels.
Er worden dus wel niet-geclusterde indexen op alle vreemde sleutels gelegd. Deze indexen zijn automatisch gegenereerd uit het PDM.
Deze zijn er in gelaten zodat queries die tabellen joinen sneller zijn.
Zelfs bij de use case product zoeken hebben we geen extra indexen nodig, omdat
de data van de producten op de MongoDB database wordt gezocht. Daarom hebben wij geen aparte indexes nodig.

# Concurrency

In dit hoofdstuk wordt de concurrency en de afhandeling daarvan beschreven. Het MSSQL gedeelte van het informatiesysteem zal in de praktijk alleen door de beheerder
en de medewerkers die de framing orders afhandelen worden gebruikt. Er zullen maar 2 beheerders zijn, en het aantal medewerkers die de framing orders invullen zal onder
de 10 blijven. Omdat deze aantallen zo laag zijn, wordt er voor de use cases waarbij ze betrokken zijn alleen de standaard transactiemanagement toegepast.

De medewerkers en klanten maken gebruik van de zoekfunctionaliteit, en het totale aantal mensen kan hierdoor wel boven de 10.000 lopen. Hiervoor hoeven ze alleen de data van
de producten uit de MongoDB database te halen. Deze database wordt een keer per dag om 06:00 geüpdatet. Het enige concurrency probleem wat zich hier kan voorkomen is als de
gegevens voor 06:00 worden opgehaald en nog na 06:00 worden gebruikt.

# Beveiliging
Niet alle gebruikers van het informatiesysteem hebben dezelfde rechten. De actoren, en daarmee de rechten van de gebruiker, zijn per use case beschreven in het Functioneel Ontwerp.  
In dit hoofdstuk wordt beschreven hoe deze rechten gehandhaafd en getest worden.
Ook worden ook andere aspecten van beveiliging besproken.

## Rechten gebruikers
Voor elke actor in het systeem is een login en een gebruiker aangemaakt. Dit zijn voor nu de namen van de actoren: Framer en Employee.  
Echter is het beter dat dit de echte namen zijn van de framer en employee, omdat dan de geschiedenis beter kan registreren wie wat aanpast.  
Opvallend is dat er geen gebruiker is aangemaakt voor de klant in de winkel. Dat komt omdat de klant alleen zoekt in de database, en dat gebeurt niet in SQL.  
Zie het technisch ontwerp voor 'Zoeken product'

Een login heeft altijd 1, en hoogstens 1 user. Aan de login zit een wachtwoord gekoppeld, niet aan de gebruiker.

De gebruiker krijgt vervolgens rollen. Deze rollen zijn specifiek gemaakt voor elke actor. De aangemaakte rollen zijn:
- Employeerole
    - Deze rol wordt toegekend voor alle gebruikers die een employee moeten zijn.
- Framerrole
    - Deze rol wordt toegekend voor alle gebruikers die een framer moeten zijn.
- Adminrole
    - Deze rol wordt toegekend aan de gebruiker die bij de winkeleigenaar hoort.

Alle gebruikers hebben een bijbehorende rol. Deze rol wordt dus gedeeld over meerdere gebruikers. Dit betekent dat we de rechten van de gebruikers kunnen beperken door de rechten van de rol te beperken.  
Daardoor hoeven we dus niet de rechten voor meerdere gebruikers aan te passen, maar voor een enkele rol.  
De rollen beginnen standaard zonder enige rechten. Deze moeten dus worden toegevoegd.

De admin rol heeft de meeste rechten. Hij heeft select, insert, update, delete, execute op dbo, en select op hist. 

De framer rol heeft uitvoeringsrechten op alle stored procedures van de use case "Kunstwerk inlijsten". 
Ook kan hij alle data in de database selecteren behalve de datageschiedenis.

De employee rol heeft uitvoeringsrechten op alle stored procedures van de use cases die met beheren product te maken hebben.
Ook kan hij alle data in de database selecteren behalve de datageschiedenis.

## Command injection
Het installatiescript maakt gebruik van de commandline, waardoor elk willekeurig commando wat ingevuld wordt, wordt uitgevoerd op de commandline.  
Dit kan een risico zijn als het commando te beïnvloeden is van buitenaf, omdat er dan mogelijk onveilige commando's uitgevoerd kunnen worden.

Sonarcloud geeft dit ook als een veiligheidsrisico aan. Echter is wat er wordt uitgevoerd niet te beïnvloeden van buitenaf; er wordt altijd sqlcmd uitgevoert.  
Een mogelijke oplossing zou zijn om de commandline helemaal niet te gebruiken en het installatiescript verbinding met de database te geven.  
Daarmee is natuurlijk het veiligheidsrisico opgelost van de commandline.  
De moeilijkheid in deze oplossing zit in het uitvoeren van een SQL-bestand. Deze zouden we bijvoorbeeld in kunnen lezen als string, om vervolgens uit te voeren vanuit het script.  
Het probleem hiermee is dat in de SQL-bestanden gebruik wordt gemaakt van het woordje 'GO' om het script op te splitsen in verschillende batches.

Het woordje GO bestaat door de client van SQL, niet SQL-server zelf. Als je dit dus uitvoert met de databaseverbinding in het installatiescript, krijg je dus een syntax error want GO bestaat niet.  
Om dit op te lossen kan je in de ingelezen string zoeken naar het woordje GO, en die weghalen. Dit gaat goed totdat er een woord is die toevallig een G en een O na elkaar hebben staan, zoals FRAMIN**GO**RDER.

De oplossing om een risico op te lossen wat eigenlijk niet bestaat is dus heel erg complex. Daarom hebben we ervoor gekozen om dit in dit project niet op te lossen.

# Bronnenlijst

Solutions, M. (2018, 19 juni). A Comparison between MySQL vs. MS SQL Server - Mindfire Solutions. Geraadpleegd op 26 april 2021, van https://medium.com/@mindfiresolutions.usa/a-comparison-between-mysql-vs-ms-sql-server-58b537e474be#:%7E:text=The%20feature%20makes%20SQL%20Server%20score%20over%20MySQL.&text=Both%20enterprise%20database%20systems%20are,by%20other%20processes%20at%20runtime
