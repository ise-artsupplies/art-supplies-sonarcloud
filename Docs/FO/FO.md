---
title: |
        Functioneel Ontwerp\
        Art Supplies\
        ISE Project\
        Groep A4
date: 12-05-2021
---

# Inleiding
Dit document dient als functioneel ontwerp waarin we de functionaliteiten van het informatiesysteem beschrijven en het verduidelijken. 
De casus wordt geanalyseerd en er wordt een conceptueel datamodel opgesteld met behulp van de gevonden fact types.  
 
Het functioneel ontwerp bestaat uit de onderstaande onderdelen:
1. Use cases
2. BPMN modellen
3. Eisen(functionele en niet-functionele)
4. Domein
5. Entiteit relatie model
6. Business rules
7. Constraints
8. Interactie model
9. Gebruikersrechten

# Use cases
In dit hoofdstuk worden de use cases beschreven in zowel brief en fully dressed formaat.

## Brief descriptions
| Use case                        | Beschrijving                                                                    | 
|:--------------------------------|:--------------------------------------------------------------------------------|
| Beheren producten (CRUD)        | Producten kunnen worden beheerd. Onder het beheren valt het aanmaken, verwijderen, wijzigen en lezen. Dit is samen te vatten in een CRUD operatie. |
| Beheren inlijstverzoeken (CUD)  | De klant komt een kunstwerk brengen om ingelijst te worden. De aanvragen van het inlijsten worden beheert. Dit betekend dat ze worden aangemaakt, gewijzigd en verwijderd. |
| Zoeken product                  | Een klant of een medewerker zoekt naar een specifiek product door zoekcriteria op te geven. |
| Zoeken in geschiedenis          | Een beheerder zoekt de geschiedenis van de data op.                             |

## Toelichting use cases
In de onderstaande tabel tonen we welke acties elke gebruiker kan uitvoeren per use case.  
Dit laten we zien door middel van C(reate), R(ead), U(pdate) en D(elete).

| Gebruikersprocessen         | Klant | Medewerker | Inlijster | Beheerder |
|:----------------------------|:------|:-----------|:----------|:----------|
| Inlijst verzoek aanmaken    |       |            | C         | C         |
| Inlijst verzoek bewerken    |       |            | U         | U         |
| Inlijst verzoek verwijderen |       |            | D         | D         |
| Producten zoeken            | R     | R          |           | R         |
| Producten toevoegen         |       | C          |           | C         |
| Producten verwijderen       |       | D          |           | D         |
| Producten bewerken          |       | U          |           | U         |
| Zoeken in geschiedenis      |       |            |           | R         |  

## Use case diagram
![Use case diagram](./Img/usecasediagram.png) 

## Fully dressed use cases

### Beheren producten (CRUD)
#### Create
Primaire actor:  Medewerker

Stakeholders en belanghebbenden: 
- Art Supply Store

Preconditie(s):
-  Medewerkers hebben toegang tot het systeem.

Postconditie(s):
-  Product is toegevoegd aan het systeem.
   
Main succes scenario(Basic Flow):  
De volgende tabel beschrijft het proces wanneer de usecase succesvol uitgevoerd wordt.

| Actie                                                     | Systeemactie                                                  |
|:----------------------------------------------------------|:--------------------------------------------------------------|
| 1. Medewerker geeft aan een product toe te willen voegen, en geeft de gevens op nodig om een product toe te voegen. |                                                               |
|                                                           | 2. Systeem controleert de ingevulde gegevens.                 |
|                                                           | 3. Systeem geeft aan dat de ingevulde data goed is.           |
|                                                           | 4. Systeem slaat het nieuwe product op.                       |


Alternatieve flows:
De volgende tabel beschrijft afwijkingen die mogelijk zijn in het proces.

| Actie              | Systeemactie                                               |
|:-------------------|:-----------------------------------------------------------|
|                    | 3 A. Systeem geeft aan dat de ingevulde data niet goed is. |
|                    | 4 A. Systeem slaat het nieuwe product niet op.             |

#### Update
Primaire actor:  Medewerker
  
Preconditie(s):
-  Medewerkers hebben toegang tot het systeem.
   
Postconditie(s):
-  Data van een product is gewijzigd door de medewerker.

Main succes scenario(Basic Flow):  
De volgende tabel beschrijft het proces wanneer de usecase succesvol uitgevoerd wordt.

| Actie                                                                | Systeemactie                                         |
|:---------------------------------------------------------------------|:-----------------------------------------------------|
| 1. Medewerker geeft aan gegevens van een product te willen bewerken, en geeft een productcode op. |                         |
|                                                                      | 2. Systeem zoekt naar het desbetreffende product.    |
|                                                                      | 3. Systeem past gegevens aan van het desbetreffende product.|


Alternatieve flows:
De volgende tabel beschrijft afwijkingen die mogelijk zijn in het proces.

| Actie              | Systeemactie                                                                |
|:-------------------|:----------------------------------------------------------------------------|
|                    | 2 A. Systeem kan het product niet vinden.                                   |
|                    | 3 A. Systeem toont foutmelding dat het desbetreffende product niet bestaat. |
|                    |                                                                             |
|                    | 2 B. Systeem geeft aan dat de ingevulde data niet goed is.                  |
|                    | 3 B. Systeem slaat het nieuwe product niet op.                              |

#### Delete
Primaire actor:  Medewerker
  
Preconditie(s):
-  Medewerkers hebben toegang tot het systeem.
   
Postconditie(s):
-  Data van een product is verwijderd door de medewerker.

Main succes scenario(Basic Flow):  
De volgende tabel beschrijft het proces wanneer de usecase succesvol uitgevoerd wordt.

| Actie                                                      | Systeemactie                                         |
|:-----------------------------------------------------------|:-----------------------------------------------------|
| 1. Medewerker geeft aan een product te willen verwijderen, en geeft de productcode op. |                          |
|                                                            | 2. Systeem zoekt naar het desbetreffende product.    |
|                                                            | 3. Systeem verwijderd het product uit de database.   |


Alternatieve flows:
De volgende tabel beschrijft afwijkingen die mogelijk zijn in het proces.

| Actie              | Systeemactie                                                                |
|:-------------------|:----------------------------------------------------------------------------|
|                    | 2 A. Systeem kan het product niet vinden.                                   |
|                    | 3 A. Systeem toont foutmelding dat het desbetreffende product niet bestaat. |



### Beheren inlijstverzoeken
#### Create
Primaire actor: Inlijster

Stakeholders en belanghebbenden: 
- Klant

Preconditie(s):
-  Controle of de art supply store een framing service heeft.

Postconditie(s):
-  Een inlijstverzoek is toegevoegd.

Main succes scenario(Basic Flow): 
De volgende tabel beschrijft het proces wanneer de usecase succesvol uitgevoerd wordt.  
   
| Actie                                                               | Systeemactie                            |
|:--------------------------------------------------------------------|:----------------------------------------|
| 1. Inlijster vraagt inlijst service aan, met de benodigde gegevens om een framing order aan te maken.                           |                                         |
|                                                                     | 2.  Systeem voegt ingevoerde wensen in database. |   


#### Delete

Primaire actor: Inlijster

Stakeholders en belanghebbenden: 
- Klant

Preconditie(s):
-  Controle of de art supply store een framing service heeft.

Postconditie(s):
-  Een inlijstverzoek is verwijderd.

Main succes scenario(Basic Flow):  
De volgende tabel beschrijft het proces wanneer de usecase succesvol uitgevoerd wordt.  
   
| Actie                                                               | Systeemactie                            |
|:--------------------------------------------------------------------|:----------------------------------------|
| 1. Inlijster geeft aan een verzoek te willen verwijderen, en geeft het framing order nummer op.           |                                         |
|                                                                     | 2.  Systeem verwijderd de data van het verzoek. |   

Alternatieve flows
De volgende tabel beschrijft afwijkingen die mogelijk zijn in het proces.  

| Actie              | Systeemactie                                                     |
|:-------------------|:-----------------------------------------------------------------|
|                    | 2 A.[Inlijstverzoek met opgegeven order nummer bestaat niet] Systeem toont foutmelding met geen inlijstverzoek gevonden. |  

  
#### Update

Primaire actor: Inlijster
Stakeholders en belanghebbenden:
- Klant

Preconditie(s):

- De use case wordt als inlijster uitgevoerd.

Postconditie(s):

- Er is een framing order gewijzigd  

##### Main succes scenario

De volgende tabel beschrijft het proces wanneer de use case succesvol wordt uitgevoerd.  

| Actie                                                               | Systeemactie                            |
|:--------------------------------------------------------------------|:----------------------------------------|
| 1. Medewerker geeft aan een framing order, met of zonder passepartout, te willen updaten en geeft de nieuwe gegevens met het bijbehorende ordernummer op. ||
|                                                                     | 2. Systeem update de gegevens van de framing order.           |

##### Alternative flows:
1. Alternative flow 1 (Framing order met het opgegeven ordernummer bestaat niet): 

| Actie                                                               | Systeemactie                            |
|:--------------------------------------------------------------------|:----------------------------------------|
|                                                                     | 2a. [Framing order met opgegeven ordernummer bestaat niet] Systeem toont een foutmelding.           |

### Zoeken product  
Primaire actors: Klant en medewerker

Preconditie(s):
- Er zijn producten aanwezig in het systeem.

Postconditie(s):
- Systeem toont de producten die aan de zoekcriteria voldoen

Main succes scenario(Basic Flow):
De volgende tabel beschrijft het proces wanneer de usecase succesvol uitgevoerd wordt.  
  
| Actie                                                                               | Systeemactie                                                    |
|:------------------------------------------------------------------------------------|:----------------------------------------------------------------|
| 1. Actor geeft zoekcriteria op.                                                     |                                                                 |
|                                                                                     | 2. Systeem toont alle producten die aan de zoekcriteria voldoen.|

Alternatieve flows
De volgende tabel beschrijft afwijkingen die mogelijk zijn in het proces.  

| Actie              | Systeemactie                                                               |
|:-------------------|:---------------------------------------------------------------------------|
|                    | 4 A. [Geen producten gevonden die voldoen aan de zoekcriteria] Systeem toont dat er geen producten met de opgegeven zoekcriteria zijn gevonden. |

## BPMN Modellen
In dit hoofdstuk worden de bpmn modellen vinden van de use cases waarbij wij het nodig vonden voor elaboratie.  

### Kunstwerk inlijsten  

![BPMN inlijsting](./Img/bpmn_inlijsting.png) 

### Zoeken product  

![BPMN zoeken product](./Img/bpmn_zoeken.png)

# Eisen
## Functionele eisen  
In dit hoofdstuk worden functionele eisen beschreven die niet door een use-case worden afgedekt omdat er geen proces is.  
De eisen zijn geprioriteerd d.m.v. de MoSCoW methode.
M: Must have
S: Should have
C: Could have
W: Won't have.

| Functionele eis             | Beschrijving                                                                 | Prioriteit  |
|:----------------------------|:-----------------------------------------------------------------------------|:------------|
| FR1 Toegang beheerder       | Beheerder heeft volledig toegang tot alle functionaliteiten van het systeem. | Must have   |
| FR2 Zoeken naar producten   | Klanten en medewerkers moeten kunnen zoeken naar producten in het systeem.   | Must have   |
| FR3 Inlijstverzoek aanmaken | Medewerkers moeten inlijstverzoeken aanmaken die inlijsters kunnen inlezen.  | Should have |


### Overige 

| Eis                                       | Beschrijving                                                                                                                                    | Prioriteit  |
|:------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------|:------------|
| E1 Winkelmandje                           | Een systeem waarin klanten producten kunnen toevoegen in een winkelmandje om die tegelijk af te kunnen rekenen.                                 | Won't have  |
| E2 Puntenspaarsysteem                     | Een systeem waarin klanten door bijvoorbeeld aankopen punten kunnen sparen.                                                                     | Won't have  |
| E3 Kortingen                              | Tijdelijke kortingen om de prijs van een product te verlagen.                                                                                   | Could have  |  


## Niet-functionele eisen
Hier worden alle niet-functionele requirements beschreven waar de teamleden en het systeem zich aan moet houden.

### Prestatie

| Code               | Beschrijving                                                                               | Prioriteit  |
|:-------------------|:-------------------------------------------------------------------------------------------|:------------|
| NFR1               | Het informatiesysteem moet duizenden gebruikers op de website tegelijk kunnen onderhouden. | should have |

### Bruikbaarheid

| Code               | Beschrijving                                        | Prioriteit |
|:-------------------|:----------------------------------------------------|:-----------|
| NFR2               | De zoekfunctie wordt geïmplementeerd in mongodb.    | must have  |

### Kwaliteit

| Code               | Beschrijving                                                                                                  | Prioriteit  |
|:-------------------|:--------------------------------------------------------------------------------------------------------------|:------------|
| NFR3               | De mongodb database waar in wordt gezocht naar producten moet op elk moment overeenkomen met de SQL database. | should have |

### Onderhoudbaarheid

| Code               | Beschrijving                                                                     | Prioriteit  |
|:-------------------|:---------------------------------------------------------------------------------|:------------|
| NFR4               | Foutafhandeling zorgt ervoor dat er geen foutieve data in de database kan komen. | should have |

### Veiligheid

| Code               | Beschrijving                                                                        | Prioriteit  |
|:-------------------|:------------------------------------------------------------------------------------|:------------|
| NFR5               | Klanten kunnen geen producten toevoegen.                                            | must have   |
| NFR6               | Geschiedenis tabellen tonen aan wie welke aanpassingen heeft gedaan in de database. | should have |

#### Toelichting
KE2: Hier is gekozen voor 4 code smells. Dit is omdat code smells aangeven waar mogelijke onhandigheden in de code zitten. Omdat er zo min mogelijk onhandigheden in moeten zitten hebben wij gekozen voor een 
klein aantal getolereerde code smells namelijk 4.

# Domein  
Domeinen kunnen het gemakkelijker maken om de structuur van een database te begrijpen. Veranderingen door te geven gaat met gebruik van 
domeinen heel efficient wanneer een domein gewijzigd word.  

| Domein               | Datatype                                              | Domein value constraint |  
|:---------------------|:------------------------------------------------------|:------------------------|  
| Aantal               | Int                                                   | Waarde >= 0             |
| Beschrijving         | Varchar(256)                                          |                         |
| Datum                | DATE                                                  |                         |
| Eigenschapcode       | Varchar(256)                                          |                         |
| FramingOrderNummer   | Int                                                   | Waarde > 0              |
| Kleurcode            | Varchar(256)                                          |                         |
| LijstAfmeting        | Numeric(3,1)                                          | Waarde > 0              |
| Lijstnummer          | Int                                                   | Waarde > 0              |
| Naam                 | Varchar(256)                                          |                         |
| Nummer               | Int                                                   | Waarde > 0              |
| PassePartoutNummer   | Int                                                   | Waarde > 0              |
| Prijs                | Money                                                 | Waarde >= 0             |
| Productnummer        | Int                                                   | Waarde > 0              |
| Typenummer           | Int                                                   | Waarde > 0              |
| Waarschuwingscode    | Varchar(256)                                          |                         |

 
## Domeinconcepten glossary

In dit hoofdstuk wordt beschreven welke domeinconcepten er zijn, en welke betekenis ze hebben. Deze concepten komen zowel uit de opdrachtbeschrijving als vergaderingen met de opdrachtgever.

| Domeinconcept      | Beschrijving                                                                                               |
|:-------------------|:-----------------------------------------------------------------------------------------------------------|
| Klant              | De klant van de winkel. Er wordt geen data van klanten opgeslagen.                                         |
| Winkelmedewerker   | Een medewerker van de winkel. Er wordt geen data van winkelmedewerkers opgeslagen.                         |
| Framer / Inlijster | Een medewerker van de winkel die de kunstwerken verkregen bij de framing service inlijst. Van de framer wordt slechts de naam bijgehouden. |
| Framing service / Inlijsten    | Een klant kan aanvragen om zijn kunstwerk in te laten lijsten. De framer voert deze opdracht uit. Voor de framing service moet de volgende data zijn opgeslagen: Wat voor soort kunstwerk,  welke framer de opdracht uitvoert, de afmetingen, prijs, en de opleveringsdatum. Als het geen tekening is moet ook worden opgeslagen welke lijst het betreft. Als het wel een tekening is moet ook worden opgeslagen welke passe-partout en welk soort glas het betreft.             |
| Lijst              | Een lijst die wordt geselecteerd door de klant om een kunstwerk mee in te lijsten. Een lijst heeft een typenummer en een prijs. Types van lijsten hebben een typesoort, merk, omschrijving en prijs.                                                                                          |
| Passe-partout      | Een passe-partout wordt gebruikt wanneer het kunstwerk bij de framing service een tekening betreft. Een passe-partout heeft een kleur, soort, papiersoort en een prijs. |
| Product            | Een product dat wordt verkocht door de winkel. Elk product heeft een productnummer, productomschrijving, merk, verkoopprijs en een voorraad. |
| Merk               | De fabrikant van een product.                                                                              |
| Verf               | Een product waarvan ook de inhoud, de kleur, de PG-waarde, de verdunner, de transparantie, waarschuwingen en eigenschappen worden bijgehouden.   |
| Kleur              | De kleur van verf. Er wordt hiervan een kleurcode en een kleurbeschrijving bijgehouden.                    |
| Transparantie      | De mate van transparantie die verf heeft. Dit kan dekkend, half-dekkend, half-transparant, transparant of onbekend zijn. |
| Waarschuwingen     | De waarschuwingen die bij verf zitten. Hiervan wordt de code en de waarschuwingsbeschrijving bijgehouden.  |
| Eigenschappen      | De eigenschappen die verf kan hebben. Hiervan wordt de eigenschapscode en een eigenschapsbeschrijving bijgehouden. |
| Kwasten            | Een product waarvan ook de maat, vorm, materiaal en techniek wordt bijgehouden.                            |
| Set                | Een product dat bestaat uit een verzameling van andere producten. Er wordt ook bijgehouden wat de verpakking van de set is. |

## Entiteit Relatie Model
![ERM](./Img/ERM.png)  
In bovenstaand figuur is het ERM (Entiteit Relatie Model) te zien. In dit model is het domein schematisch weergegeven.  
Opvallend in dit model zijn de subtypen rond het Product. Deze subtypen hebben eigenschappen die alleen van toepassing zijn op deze producten. Zo heeft een set producten in een set, een kwast en verf niet.  
Mogelijk valt ook het subtype FramingOrderMetPassePartout op, die verder geen attributen heeft. De reden waarom deze opgenomen is als subtype, is omdat hij een relatie met PassePartout heeft.  
Een normaal framing order heeft dat dan niet.  

## Business Rules
In dit hoofdstuk worden de business rules beschreven die moeten worden bewaakt in dit project.  
 
BR1: Alleen producten die niet van het Producttype set zijn, kunnen onderdeel uit maken van een set.  
BR2: Als product een kwast of verf is, is verkoopprijs onbekend en prijsgroep bekend.  
BR3: Producten kunnen geen negatieve voorraad hebben.  
BR4: Producten moeten altijd een unieke Productnr hebben.  
BR5: FramingOrders moeten altijd een unieke FramingOrderNummer hebben.  
BR6: Aanvraagdatum moet altijd eerder zijn dan opleverdatum.


## Constraints  
Dit hoofdstuk sluit aan op het hoofdstuk Business Rules. De constraints die corresponderen met de business rules worden hier behandeld.

C1 correspondeert met BR1  
    - Betreft: ET Product_in_set, Attr: PRODUCT_PRODUCTNR, SET_PRODUCT_PRODUCTNR
               ET Product, Attr: Productnr, Product_Type
    - Specificatie: Producten van het Producttype Set kunnen geen relatie hebben met een ander product van het Producttype Set.

C2 correspondeert met BR2
    - Betreft: ET Product, Attr: Verkoopprijs. ET: Producttype Attr: Product_Type
    - Specificatie: Als product een kwast of verf is, is verkoopprijs onbekend en prijsgroep bekend.

C3 correspondeert met BR3  
    - Betreft: ET Product, Attr: Voorraad
    - Specificatie: Producten kunnen geen negatieve voorraad hebben.

C4 correspondeert met BR4  
    - Betreft: ET Product, Attr: Productnr
    - Specificatie: Producten moeten altijd een unieke Productnr hebben.

C5 correspondeert met BR5  
    - Betreft: ET FramingOrder, Attr: FramingOrderNummer
    - Specificatie: FramingOrders moeten altijd een unieke FramingOrderNummer hebben.

C6 correspondeert met BR6  
    - Betreft: ET FramingOrder, Attr: framingorder_Aanvraagdatum, framingorder_opleverdatum.
    - Specificatie: Aanvraagdatum moet altijd eerder zijn dan opleverdatum.


# Interactie model
## CRUD Operaties  
We hebben een CRUD matrix aangemaakt omdat dit een nuttige manier is om activiteiten binnen het systeem vast te leggen.
In de CRUD matrix beschrijven we per usecase welke entiteit betrokken is en op welke manier er gebruik van wordt gemaakt.
In de tabel zijn afkortingen te vinden. Dit zijn de volgende:  
- C: Create. Het aanmaken van de entiteit  
- U: Update. Het bijwerken van een entiteit  
- D: Delete. Het verwijderen van een entiteit  
- R: Read. Het lezen van een entiteit  

|                             | **Use case** | Product toevoegen         | Product verwijderen       | Product wijzigen          | Product zoeken       | Zoeken in datageschiedenis | Inlijstverzoek aanmaken | Inlijstverzoek wijzigen | Inlijstverzoek verwijderen |
|:----------------------------|:------------:|--------------------------:|--------------------------:|--------------------------:|---------------------:|---------------------------:|------------------------:|------------------------:|---------------------------:|
| **Entiteit**                | x            |  x                        |   x                       |  x                        |      x               |       x                    |      x                  |         x               |        x                   |
| Product                     | x            | Medewerker, Beheerder: CR | Medewerker, Beheerder: RD | Medewerker, Beheerder: RU | Medewerker, Klant: R | Beheerder: R               |                         |                         |                         |                            |
| Verfproduct                 | x            | Medewerker, Beheerder: CR | Medewerker, Beheerder: D  | Medewerker, Beheerder: RU | Medewerker, Klant: R | Beheerder: R               |                         |                         |                         |                            |
| Kwastproduct                | x            | Medewerker, Beheerder: CR | Medewerker, Beheerder: D  | Medewerker, Beheerder: RU | Medewerker, Klant: R | Beheerder: R               |                         |                         |                         |                            |
| Setproduct                  | x            | Medewerker, Beheerder: CR | Medewerker, Beheerder: D  | Medewerker, Beheerder: RU | Medewerker, Klant: R | Beheerder: R               |                         |                         |                         |                            |
| Product_In_Set              | x            | Medewerker, Beheerder: CR | Medewerker, Beheerder: RD | Medewerker, Beheerder: RU | Medewerker, Klant: R | Beheerder: R               |                         |                         |                         |                            |
| Kleur                       | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Pigment                     | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Techniek                    | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Transparantie               | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Waarschuwingscode           | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Merk                        | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Eigenschap                  | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Prijsgroep                  | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Producttype                 | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| Materiaal                   | x            |                           |                           |                           | Medewerker, Klant: R | Beheerder: R               |                         |                         |                            |
| PassePartout                | x            |                           |                           |                           |                      | Beheerder: R               |                         |                         |                            |
| FramingOrder                | x            |                           |                           |                           |                      | Beheerder: R               | Inlijster, Beheerder: C | Inlijster, Beheerder: U | Inlijster, Beheerder: D    |
| Framer                      | x            |                           |                           |                           |                      | Beheerder: R               |                         |                         |                            |
| Glas                        | x            |                           |                           |                           |                      | Beheerder: R               |                         |                         |                            |
| Kunstwerksoort              | x            |                           |                           |                           |                      | Beheerder: R               |                         |                         |                            |
| FramingOrderMetPassePartout | x            |                           |                           |                           |                      | Beheerder: R               | Inlijster, Beheerder: C | Inlijster, Beheerder: U | Inlijster, Beheerder: D    |
| PassePartoutSoort           | x            |                           |                           |                           |                      | Beheerder: R               |                         |                         |                            |
| Papiersoort                 | x            |                           |                           |                           |                      | Beheerder: R               |                         |                         |                            |

