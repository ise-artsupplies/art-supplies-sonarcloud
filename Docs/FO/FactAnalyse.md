# Facts
## Product
```
1. Product 24401112 heeft de productomschrijving CLASSIC Olieverf.
   "       20100343 "     "  "                   Aquarelverf.
   ________________          _____________________________________
   ET: Product               Attr: Productomschrijving
   ID: Productnr
   Product <Productnr> heeft de productomschrijving <Productomschrijving>.
   
   
2. Product 24401112 is van het merk Old Holland.
   "       20120666 "  "   "   "    Schmincke Akademie.
   ________________            _______________________
   ET: Product                 Attr: ET: Merk
   MATCH                             ID: Merknaam
   Product <Productnr> is van het merk <Merk>.
   RT: Product_Van_Merk tussen Product en Merk


3. Product 24401112 heeft de verkoopprijs 8,21.
   "       20100343  "   "        "       6,21.
   ________________          __________________
   ET: Product               Attr: Verkoopprijs
   MATCH
   Product <Productnr> heeft de verkoopprijs <Prijs>.
   

4. Product 24401112 heeft een voorraad van 3.
   "       20100343 "     "   "        "   2.
   ________________           _______________
   ET: Product                Attr: Voorraad
   MATCH
   Product <Productnr> heeft een voorraad van <Voorraad>.

5. Product 1234 is van type verf
   "       5678 "  "   "    set
   -----------------   ---------
   ET: Product    Attr: ET: Producttype
                         ID: Type
   RT: Type_Van_Product tussen Product en Producttype
   Product <Productnr> is van type <Type>
```
## Verf
```
1. Product 1234 is een verf product
   --------------------------------
   ET: Verfproduct
   ID: Productnr
   Product <Productnr> is een verf product
   
2. Verfproduct 24401112 heeft als inhoud Tube 18ml.
   "       20120666 "     "   "      1/2 napje.
   ________________           _________________
   ET: VerfProduct                Attr: Inhoud
   MATCH
   Verfproduct <Productnr> heeft als inhoud <Inhoud>.
  
 
3. Verfproduct 24401112 heeft de kleurcode 112.
   "           20100343 "     "  "         343.
   ________________          ______________
   ET: VerfProduct               Attr: ET: Kleur
   MATCH                           ID: Kleurcode
   Verfproduct <Productnr> heeft de kleurcode <Kleurcode>.
   RT: Kleur_Van_VerfProduct tussen Kleur en VerfProduct
   

4. Kleur 112 heeft de kleur Naples yellow_reddish extra.
   "     254 "     "  "     Permanent citroengeel.
   _________          __________________________________
   ET: Kleur          Attr: KleurOmschrijving
   MATCH
   Kleur <Kleurcode> heeft de kleur <KleurOmschrijving>.
   
5. Kleur 112 is gemaakt van pigment Dodekop rood
   "     5   "  "       "   "       Indisch geel
   -------------------- ------------------------
   ET: Kleur           +            ET: Pigment
   MATCH                            ID: Pigmentnaam
   Kleur <Kleurcode> is gemaakt van pigment <Pigmentnaam>
   RT: Kleur_Gemaakt_Van_Pigment tussen Kleur en Pigment
 
6. Pigment Dodekop rood heeft waarschuwingscode H411
   "       Indisch geel "     "                 EUH208_30
   -------------------------- ---------------------------
   ET: Pigment                                  ET: Waarschuwingscode
   MATCH                                            MATCH
   Pigment <Pigmentnaam> heeft waarschuwingscode <Waarschuwingscode>
   RT: Waarschuwing_Van_Pigment tussen Pigment en Waarschuwingscode 
   
7. Verfproduct 24401112 heeft  PG 2.
   "        20120666 "     "  3.
   _________________       _____
   ET: VerfProduct             Attr: PGWaarde
   MATCH
   Product <Productnr> heeft PG <PGWaarde>.
   
8. Verfproduct 24401112 is verdund met terpentine.
   "        123         "     "     "  terpentine.
   ________________    _______________________
   ET: VerfProduct         ET: Verdunner
   MATCH                   ID: Verfproduct_Verdunner
   Verfproduct <Productnr> is verdund met <Verfproduct_Verdunner>.
   RT: Verfproduct_Verdunner tussen Verfproduct en Verdunner
   

[comment]: <> (Volledig vierkant - dekkend)
[comment]: <> (Half vierkant - half-dekkend)
[comment]: <> (Leeg vierkant met diagonaal streepje - half-transparant)
[comment]: <> (Leeg vierkant - transparant)

9. Verfproduct 24401112 heeft de transparantie half-dekkend.
   "       20100343     "  "             " half-transparant. 
   ________________          ___________________________
   ET: VerfProduct               Attr: ET: Transparantie
   MATCH                           ID: TransparantieWaarde
   Verfproduct <Productnr> heeft de transparantie <TransparantieWaarde>.
   RT: VerfProduct_Transparantie tussen VerfProduct en Transparantie
   
   
10. Verfproduct 24401112 heeft de waarschuwing H411.
   "       20100343    "   "            "     EUH208_30
   ________________          ______________________
   ET: VerfProduct               Attr: ET: Waarschuwing 
   MATCH                           ID: Waarschuwingscode
   Verfproduct <Productnr> heeft de waarschuwing <Waarschuwingscode>.
   RT: VerfProduct_Waarschuwing tussen Waarschuwing en VerfProduct
   

11. Verfproduct 20100343 heeft als eigenschap ****.
   "       20100343     "   "          "     G.  
   ----------------           ----------------
   ET: VerfProduct                Attr: ET: Eigenschap
   MATCH                                ID: Eigenschapcode
   Verfproduct <Productnr> heeft als eigenschap <Eigenschapcode>.
   RT: VerfProduct_Eigenschap tussen Eigenschap en VerfProduct

12. Verfproduct 20100343 valt onder prijsgroep 1
    "           24401112 "     "    "          2
    -------------------------- -----------------
    ET: Verfproduct         +  ET: Prijsgroep
        MATCH                  ID: Prijsklasse
    RT: Prijsgroep_Van_Verfproduct tussen Verfproduct en Prijsgroep
    Verfproduct <Productnr> valt onder prijsgroep <Prijsklasse>
    
13. Waarschuwing  H411      geeft de waarschuwing niet in de buurt van kinderen bewaren. 
   "              EUH208_30  "     "            "  bewaar onder 24 graden. 
   -----------------------          ---------------------------------------------------
   ET: Waarschuwing                   Attr: Beschrijving
   MATCH
   Waarschuwing met waarschuwingscode <Waarschuwingscode> geeft de waarschuwing <Beschrijving>.


14. Eigenschap *** heeft de beschrijving lichtechtheid 3.
    "          ^   "     "  "            staining. 
    --------------          -----------------------------
    ET: Eigenschap           Attr: Beschrijving
    MATCH
    Eigenschap <Eigenschapcode> heeft de beschrijving <Beschrijving>.
```
## Kwasten
```
1. Product 82216 is een kwast
   --------------------------
   ET: KwastProduct
   ID: Productnr
   Product <Productnr> is een kwast
   
2. Kwastproduct 82216 heeft maat 3.
   "       72684 "     "    8
   -------------       -------
   ET: KwastProduct          Attr: Maat
   ID: Productnr
   Kwastproduct <Productnr> heeft maat <Maat>.
   

3. Kwastproduct 82216 heeft de vorm Rond.
   "       72684 "     "  "    Kattentong
   -------------          ---------------
   ET: KwastProduct             Attr: ET: Vorm
   MATCH                         ID: VormNaam
   Kwastproduct <Productnr> heeft de vorm <VormNaam>.
   RT: KwastProduct_Vorm tussen KwastProduct en Vorm.


4. Kwastproduct 82216 is gemaakt van Syntetisch haar.
   "            72684 "  "       "   Kolinsky roodmarter haar
   --------------------              ------------------------
   ET: KwastProduct                   Attr: ET: Materiaal
   MATCH                               ID: MateriaalNaam   
   Kwastproduct <Productnr> is gemaakt van <MateriaalNaam>.
   RT: KwastProduct_Materiaal tussen KwastProduct en Materiaal.
   

5. Kwastproduct 82216 heeft de techniek Aquarel.
   "            82216 "     "  "        Zijde
   -------------          -----------------
   ET: KwastProduct            Attr: ET: Techniek
   MATCH                        ID: TechniekNaam
   Kwastproduct <Productnr> heeft de techniek <TechniekNaam>.
   RT: KwastProduct_Techniek tussen KwastProduct en Techniek.

6.  Kwastproduct 82216 valt onder prijsgroep 1
    "            72684 "     "    "          2
    -------------------------- -----------------
    ET: Kwastproduct         +  ET: Prijsgroep
    MATCH                       MATCH
    RT: Prijsgroep_Van_Kwastproduct tussen Kwastproduct en Prijsgroep
    Kwastproduct <Productnr> valt onder prijsgroep <Prijsklasse>  
    
```
## Prijsgroep  
```
1. Prijsgroep 1 heeft als prijs 2.50 euro.  
   "          2 "     "   "     5.00 "  
   ------------           ----------------  
   ET: Prijsgroep         Attr: Prijs  
   MATCH  
   Prijsgroep <Prijsklasse> heeft als prijs <Prijs> euro.
  
```
## Sets
```
1. Product 69086 is een set
    -----------------------
    ET: SetProduct
    ID: Productnr
    Product <Productnr> is een set
   

2. Setproduct 69086    bevat product 72684  2 keer.
   "       05830190 "     "       20120666  1 "
   ---------------------------------------  -------
   ET: Product_In_Set                       Attr: Aantal
   ID: ET: SetProduct +      ET: Product
       MATCH                 MATCH  
   Setproduct <Productnr> bevat product <Productnr> <Aantal> keer.
   RT: Onderdeel_Zit_In_Set tussen Product_In_Set(dependent) en SetProduct
   RT: Product_In_Set_Onderdeel tussen Product_In_Set(dependent) en Product
   

3. Setproduct 05830190 heeft de verpakking Metalen doos voorzien van scharnierende deksels om als mengpaletten te gebruiken.
   "       29507    "     "  "          kunststof pocketbox met een rond penseel van synthetisch haar (nr. 6) en een mengschaal 
   ----------------          ---------------------------------------------------------------------------------------------------
   ET: SetProduct                Attr: Verpakking
   MATCH
   Setproduct <Productnr> heeft de verpakking <Verpakking>.
   
```
## Framing Service
```
1. Lijst 123 is van type 1.
   "     234 "  "   "    2. 
   ---------    ------------------------------
   ET: Lijst    Attr: ET: LijstType
   ID: LijstNummer    ID: Attr: TypeNummer   
   Lijst <LijstNummer> is van type <TypeNummer>.
   RT: Type_Van_Lijst tussen Lijst en Lijsttype

2. Lijst 123 kost 30,00 euro.
   "     234 "    40,00 "
   --------- ----------------
   ET: Lijst Attr: Prijs
   MATCH
   Lijst <LijstNummer> kost <Prijs> euro.


3. Lijst 123 heet klassieke lijst.
   "     234 "    post-moderne lijst.
   ---------      -------------------
   ET: Lijst       Attr: LijstNaam 
   MATCH
   Lijst <LijstNummer> heet <LijstNaam>.


4. Lijst 123 is gemaakt door merk ZEP
   "     5    "  "       "    "   ABC
   ------------              ---------
   ET: Lijst                 Attr: ET: Merk
   MATCH                           MATCH
   Lijst <LijstNummer> is gemaakt door merk <Merknaam>
   RT: Lijst_Door_Merk tussen Lijst en Merk
   
5. Lijst type 1 heeft de beschrijving Hout.
   "     "    2 "     "  "            Steen
   ------          ------------------
   ET: LijstType        Attr: TypeSoort
   MATCH
   Type <TypeNummer> heeft de beschrijving <TypeSoort>.


6. Framing order 687 heeft een kunstwerk van het soort schilderij.
   "       "     786 "     "   "         "   "   "     tekening.
   -----------------                             -----------------
   ET: FramingOrder                              Attr: ET: KunstwerkSoort
   ID: FramingOrderNummer                              ID: SoortNaam 
   Framing order <FramingOrderNummer> heeft een kunstwerk van het soort <SoortNaam>.
   RT: FramingOrder_Kunstwerksoort tussen FramingOrder en KunstwerkSoort

7. Framing order 687 heeft mat   glas.
   "       "     786 "     clear "
   -----------------       -----------
   ET: FramingOrder        Attr: ET: Glas
   MATCH                         ID: GlasSoort
   Framing order <FramingOrderNummer> heeft <GlasSoort> glas.
   RT: FramingOrder_GlasSoort tussen FramingOrder en Glas

8. Framing order 687 wordt uitgevoerd door framer 123      
   "             786 "     "            "   "     124.
   -----------------            ------------------
   ET: FramingOrder            Attr: ET Framer
   MATCH                             ID: FramerId
   RT: FramingOrder_Uitgevoerd_Door_Framer tussen FramingOrder en Framer.
   Framing order <FramingOrderNummer> wordt uitgevoerd door framer <FramerId>.  
   
9. Lijst 123 wordt gebruikt in framing order 687      
   "     368 "     "        "   "       "    124.
   -----------------            ------------------
   ET: FramingOrder            Attr: ET Lijst
   MATCH                             MATCH
   RT: Lijst_In_FramingOrder tussen FramingOrder en Lijst.
   Lijst <Lijstnr> wordt gebruikt in framing order <

10. Framer 123 heeft de naam Jan de Boer.
   "      124 "     "  "    Karel Appel
   ----------          -----------------
   ET: Framer          Attr: Naam
   MATCH
   Framer <FramingOrderNummer> heeft de naam <Naam>.
   

11. Framing order 687 heeft als hoogte 40 cm.
   "       "      786 "     "   "      20 "
   -----------------           -------------
   ET: FramingOrder            Attr: Hoogte
   MATCH
   Framing order <FramingOrderNummer> heeft als hoogte <Hoogte> cm.
   

12. Framing order 687 heeft als breedte 60 cm.
   -----------------           -------------
   ET: FramingOrder            Attr: Breedte
   MATCH
   Framing order <FramingOrderNummer> heeft als hoogte <Breedte> cm.

13. Framing order 687 is een order met passe-partout.
    ------------------
    ET: FramingOrderMetPassePartout
    ID: FramingOrderNummer
    Framing order <FramingOrderNummer> is een order met passe-partout.
    
14. Framing order 687 heeft passe-partout 1.
    "        "    786 "     "             2.
    ----------------        ---------------
    ET: FramingOrderMetPassePartout        Attr: ET: PassePartout
    MATCH                                  ID: PassePartoutNummer
    RT: PassePartout_Van_FramingOrderMetPassePartout tussen FramingOrderMetPassePartout en PassePartout.
    FramingOrderMetPassePartout <FramingOrderNummer> heeft passe-partout <PassePartoutNummer>

15. Framing order 687 kost 150,00 euro.
    "       "     234 "    125,00 "
    ----------------- ----------------
    ET: FramingOrder        Attr: Prijs
    MATCH
    Framing order <FramingOrderNummer> kost <Prijs> euro.
   
16. Framing order 687 heeft als opleveringsdatum 20-03-2020.
    "       "     786 "     "   "                16-12-2021
    -----------------           ----------------------------
    ET: FramingOrder            Attr: Opleveringsdatum
    MATCH
    Framing order <FramingOrderNummer> heeft als opleveringsdatum <Opleveringsdatum>.

17. Passe-partout 1 heeft de kleur blauw.
    "             2 "     "  "     rood
    ---------------          ------------
    ET: PassePartout         Attr: Kleur
    MATCH
    Passe-partout <PassePartoutNummer> heeft de kleur <Kleur>.


18. Passe-partout 1 is van soort bloemen.
    "             2 "  "   "     vlinders
    ---------------        ---------------
    ET: PassePartout       Attr: ET: PassePartoutSoort
    MATCH                        ID: SoortNaam
    RT: PassePartout_PassePartoutSoort tussen PassePartout en PassePartoutSoort
    Passe-partout <PassePartoutNummer> is van soort <SoortNaam>.

19. Passe-partout 1 kost 45,00 euro.
    "             50 "   30,00 "
    --------- ----------------------
    ET: PassePartout        Attr: Prijs
    MATCH
    Passe-partout <PassePartoutNummer> kost <Prijs> euro.
    
20. Passe-partout 1 heeft de papiersoort karton.
    "             2 "     "  "           drukpapier
    ---------------          ----------------------
    ET: PassePartout         Attr: ET: Papiersoort
    MATCH                          ID: PapierNaam
    Passe-partout <PassePartoutNummer> heeft de papiersoort <PapierNaam>.
    RT: PassePartout_Papiersoort tussen Papiersoort en PassePartout
```
# SDR-Regels
1. SDR1: x in VerfProduct als (x, 'verf') in Product(Productnr, Producttype)
   
2. SDR2: x in KwastProduct als (x, 'kwast') in Product(Productnr, Producttype)
   
3. SDR3: x in SetProduct als (x, 'set') in Product(Productnr, Producttype)
   
4. SDR4: x in FramingOrderMetPassePartout als (x, 'tekening') in FramingOrder(FramingOrderNummer, KunstwerkSoort)
