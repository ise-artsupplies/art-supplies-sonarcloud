---
title: |
        Testrapport\
        Art Supplies\
        ISE Project\
        Groep A4
date: 04-06-2021
---  

# Inleiding  
Het testrapport is een verzameling van alle soorten tests die zijn ontwikkeld en uitgevoerd gedurende het project.  
Het document is opgedeeld in twee onderdelen.  

- Beschrijving van de unittests
- Beschrijving van de integratietests.

Dit document is geschikt om na installatie te gebruiken.  
Als de installatie succesvol was, zouden de testresultaten moeten zijn zoals beschreven in dit document.  
Verder is dit document geschikt om verdere inzage te krijgen in onze teststrategie.  

# Unittests  

## Use case unittests  

Elke gemaakte functionaliteit heeft zijn eigen unit tests.  
Deze unit tests testen of de functionaliteit doet wat beschreven is in de use case beschrijving, zowel de main succes als de alternatieve flows.  
Op deze flows zijn de testcases ook gebaseerd.  

De implementatie van de integriteitsregels hebben ook unit tests. Deze testen zijn zoveel mogelijk geïsoleerd, door gebruik te maken van de FakeTable in tSQLt.  
Deze functie maakt een neppe tabel aan. Deze neppe tabel is leeg en heeft geen enkele integriteitsregel actief.  
Je kan vervolgens zelf de gewenste integriteitsregel weer aanzetten.  
Daardoor test je dus met zekerheid de specifieke integriteitsregel, en niet een ander.  
In de tests voor de integriteitsregels wordt getest of hij werkt zoals verwacht; hij weert foutieve data en staat correcte data toe.  


## Toegang unittests  
Voor elke use case zijn verschillende actoren betrokken. Voor deze actoren zijn gebruikers aangemaakt.  
Deze gebruikers hebben dus wel toegang tot de betreffende use case, andere gebruikers dus niet. Deze toegang is ook getest d.m.v. unittests.  

Om de use cases te implementeren zijn er veel stored procedures aangemaakt.  
Er moet worden getest of deze stored procedures toegankelijk zijn voor de gebruikers die toegang moeten hebben, en geen toegang voor de gebruikers die geen toegang horen te hebben.  
TsqlT biedt hier zeer beperkte functionaliteit voor, maar hier hebben we een oplossing voor gevonden.  

Een beperking die we hebben is dat we niet de inhoud van de stored procedure willen aanroepen. We testen nu immers op toegang, niet op functionaliteit.  
Bovendien wordt dan data in de echte database aangepast, tenzij je alle betrokken tabellen namaakt met tSqlT. Het nadeel daarvan is dat de test vaak moet worden aangepast als de implementatie verandert.  
Als er namelijk een extra tabel gebruikt wordt in de stored procedure, moet de test ook worden aangepast.  

Een andere oplossing zou een transactie kunnen zijn, die na de test alle wijzigingen in de data weer terugdraaid. Deze werken alleen niet goed in combinatie met tSqlT.  

De oplossing die we hebben gekozen is om de stored procedure aan te roepen zonder parameters, terwijl deze die wel hoort te krijgen.  
We testen dan of we de SQL errorcode 201 krijgen. Dit is de foutcode voor een syntaxfout. Deze foutmelding komt pas nadat de toegangscontrole is gekomen.  
Als je dus geen toegang hebt, krijg je een andere foutcode.  

Als je dus toegang moet hebben op een stored procedure controleren we op foutcode 201.  

## Resultaten  

De resultaten van alle unittests zijn de vinden in "UnittestsResults.txt" in de map Bijlagen  

# Integratietests  
De integratietests zijn ontwikkeld, zoals genoemd in het testplan, in de applicatie Postman.  
De collectie kan worden geïmporteerd met het meegeleverde json bestand.  
De collectie bevat voorbeeldrequests om te zoeken op een product, voor de usecase Zoeken product. Deze requests bevatten ook de integratietesten.  


Er zijn ongeveer 80 integratietests ontwikkeld die controleren of de data die wordt teruggegeven is zoals die wordt verwacht.  
De integratietesten vereist dat enkele voorwaarden worden voldaan voordat ze kunnen slagen:  

- De SQL-database moet worden gevuld met voorspelbare data, waardoor de MongoDB dezelfde voorspelbare data krijgt. Dit wordt automatisch geregeld in de SQL database.  
- Om de MongoDB database automatisch bij te kunnen werken vanuit SQL moet Restheart opgestart zijn.
- De integratietests maken gebruik van de standaard restheart gebruiker. Als deze gebruiker is verwijderd moeten de inloggegevens worden veranderd.  
  Er is niet getest met een andere gebruiker.  


## Schematisch overzicht
![Overzicht](./Testrapport/Img/IntegratietestenSchematisch.png)  

Hier is schematisch te zien hoe er wordt getest. Het testen begint met het uitvoeren van het installatiescript, de blauwe lijnen worden dan gevolgt.  

MSSQL zorgt ervoor dat de voorspelbare data met restheart wordt opgeslagen in MongoDB.  
Vervolgens kan in Postman die data worden opgehaald.  
Doordat Postman data opvraagt van Restheart, testen we dus de volledige keten van het aanmaken van data vanuit MSSQL, tot het ophalen van de data vanuit Postman.  

## Test inhoud  
Elke aanvraag heeft ongeveer dezelfde 'soort' tests. Niet alle 'soorten' zijn van toepassing op alle aanvragen die worden uitgevoerd.  
De tests die worden uitgevoerd staan hieronder beschreven.  

Voor elke aanvraag wordt getest of de HTTP code 200 ontvangen is. HTTP codes zijn standaard reacties die HTTP aanvragen terugsturen, die aangeven wat de status is van de aanvraag.  
Alle HTTP-codes zijn beschreven in de RFC 7231 specificatie. ([RFC 7231 specificatie](https://datatracker.ietf.org/doc/html/rfc7231#section-6.3.1))  
De HTTP code 200 staat voor OK, de aanvraag is dan dus succesvol geweest. Als de teruggegeven statuscode anders is, faalt de test.

Voor elke aanvraag wordt ook getest op het aantal items dat wordt teruggegeven.  
Er wordt altijd gezocht op producten die aan een voorwaarde voldoen. We kunnen vervolgens controleren of het aantal items wat we ontvangen ook is wat we verwachten.  
Het verwachtte aantal is per aanvraag natuurlijk anders.  
Als het verwachtte aantal anders is dan het ontvangen aantal, faalt de test.  

Er wordt ook gecontroleerd of de zoekopdracht wel de juiste gegevens terugstuurd. Dit is een uitbereiding op de vorige test.  
Een voorbeeld hiervan is bijvoorbeeld wanneer er wordt gezocht op een merknaam.  
Alle items die vervolgens worden opgehaald moeten dan de merknaam hebben waarop wordt gezocht, met exact dezelfde spelling. De controle is ook hoofdlettergevoelig.  

## Resultaten  
De resultaten van de tests zijn 82 geslaagd & 0 gefaald. De resultaten van de test zijn geëxporteerd, en kunnen worden ingeladen in Postman.  
Hier voor is het "TestsResults Art Supplies.postman_test_run.json" bestand voor nodig. Deze staat in de map "Code/Zoeken naar product/TestsResults Art Supplies.postman_test_run.json".

Hieronder is een foto te zien van de test resultaten.  

![Resultaten integratietests](./Testrapport/Img/IntegratietestsResultaten.png)
