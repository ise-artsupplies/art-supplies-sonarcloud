---
title: |
        Opleverdocument\
        Art Supplies\
        ISE Project\
        Groep A4
date: 31-05-2021
---
# Inleiding

Dit document beschrijft wat er wordt opgeleverd, en hoe de oplevering moet worden geïnstalleerd.

# Oplevering

In dit hoofdstuk wordt beschreven waar de oplevering precies uit bestaat.

## Documentatie

- Functioneel ontwerp
- Technisch ontwerp
- Testrapport
- Architectureel prototype

## Product

- Installatiescript;
- Opzet voor MongoDB database voor het zoeken naar producten met een set queries om te zoeken;
- Informatiesysteem met:
    - Automatische conversie productinformatie naar MongoDB database d.m.v. jobs;
    - Producten beheren;
    - Inlijst service beheren;
    - Zoeken in datageschiedenis.

# Installatiehandleiding

In dit hoofdstuk wordt beschreven hoe het informatiesysteem moet worden geïnstalleerd.

Voor een handleiding om een van de benodigde service RESTHeart en MongoDB te installeren, zie
[RESTHeart setup](https://restheart.org/docs/v5/setup/). Verder moet een versie van MSSQL zijn
geïnstalleerd van versie 18 of hoger. Ook moet de service SQL Server Agent aan het draaien zijn.

## Met installatiescript

Om gebruik te kunnen maken van het installatiescript, moet je op windows zitten. Ook moet het mogelijk
zijn om met je windows authenticatie in te kunnen loggen in de lokale versie van sql server. Verder moet 
de persoon die de database installeerd een sysadmin zijn.

Ook moet node.js zijn geïnstalleerd om het script uit te voeren.

Bij het installeren van de database wordt een al bestaande database onder de naam 'ArtSupplies' 
verwijderd. 

Om nu het informatiesysteem te installeren, moet je enkel het bestand "run-prod.bat" uit te voeren onder 
src/install. Er wordt geen data in de aangemaakte database gestopt.

## Zonder installatiescript

Hieronder is te lezen hoe het informatiesysteem kan worden geïnstalleerd zonder gebruik te maken van het
installatiescript. Er wordt geen data in de database gestopt. De volgende bestanden moeten op de aangegeven volgorde worden uitgevoerd:

| Stap | Bestand                          | Locatie                          |
|:-----|:---------------------------------|:--------------------------------|
| 1.   | DDL-no-analysis.sql              | src/Database_Generatie/DDL-no-analysis.sql
| 2.   | PrepareServer-no-analysis.sql    | src/tSQLt/PrepareServer-no-analysis.sql
| 3.   | tSQLt.class-no-analysis.sql      | src/tSQLt/tSQLt.class-no-analysis.sql
| 4.   | History-tables.sql               | src/Database_Generatie/History-tables.sql
| 5.   | Constraints.sql                  | src/Constraints/Constraints.sql
| 6.   | C1Tests.sql                      | src/Constraints/Tests/C1Tests.sql
| 7.   | C2Tests.sql                      | src/Constraints/Tests/C2Tests.sql
| 8.   | Users.sql                        | Users.sql
| 9.   | Users_Unittests.sql              | Users_Unittests.sql
| 10.  | Stored-Procedures.sql            | src/StoredProcedures/Search-in-history/Stored-Procedures.sql
| 11.  | Sp_Changes_In_PigmentTest.sql    | src/StoredProcedures/Search-in-history/Tests/Sp_Changes_In_PigmentTest.sql
| 12.  | Sp_Changes_by_userTests.sql      | src/StoredProcedures/Search-in-history/Tests/Sp_Changes_by_userTests.sql
| 13.  | Sp_Changes_In_PriceTests.sql     | src/StoredProcedures/Search-in-history/Tests/Sp_Changes_In_PriceTests.sql
| 14.  | Create_Product.sql               | src/StoredProcedures/CRUD_Producten/Create_Product.sql
| 15.  | Create_ProductTests.sql          | src/StoredProcedures/CRUD_Producten/Tests/Create_ProductTests.sql
| 16.  | Update_Product.sql             | src/StoredProcedures/CRUD_Producten/Update_Product.sql
| 17.  | Update_ProductTests.sql        | src/StoredProcedures/CRUD_Producten/Tests/Update_ProductTests.sql
| 18.  | Delete_Product.sql               | src/StoredProcedures/CRUD_Producten/Delete_Product.sql
| 19.  | Delete_ProductTests.sql          | src/StoredProcedures/CRUD_Producten/Tests/Delete_ProductTests.sql
| 20.  | Update_framingorder.sql          | src/StoredProcedures/CRUD_FramingOrder/Update_FramingOrder.sql
| 21.  | Delete_FramingOrder.sql          | src/StoredProcedures/CRUD_FramingOrder/Create_FramingOrder.sql
| 23.  | Update_framingorderTests.sql     | src/StoredProcedures/CRUD_FramingOrder/Tests/Update_FramingOrderTests.sql
| 24.  | Delete_FramingOrderTests.sql     | src/StoredProcedures/CRUD_FramingOrder/Tests/Delete_FramingOrderTests.sql
| 25.  | Create_FramingOrderTests.sql     | src/StoredProcedures/CRUD_FramingOrder/Tests/Create_FramingOrderTests.sql
| 26.  | Sp_encodeBase64.sql              | src/Staging-area/Requests/Security/Sp_encodeBase64.sql
| 27.  | Sp_getAuthorizationValue.sql     | src/Staging-area/Requests/Security/Sp_getAuthorizationValue.sql
| 28.  | Sp_getDbUrl.sql                  | src/Staging-area/Requests/Security/Sp_getDbUrl.sql
| 29.  | Sp_getRestheartUrl.sql           | src/Staging-area/Requests/Security/Sp_getRestheartUrl.sql
| 30.  | Sp_createDatabase.sql            | src/Staging-area/Requests/Sp_createDatabase.sql
| 31.  | Sp_deleteResourceByETag.sql      | src/Staging-area/Requests/Sp_deleteResourceByETag.sql
| 32.  | Sp_directJsonGenerationAndPostDatabaseToStagingArea.sql | src/Staging-area/Requests/Sp_directJsonGenerationAndPostDatabaseToStagingArea.sql
| 33.  | Sp_getETagIfExists.sql           | src/Staging-area/Requests/Sp_getETagIfExists.sql
| 34.  | sp_generateKwastProductJsonByProductnr.sql              | src/Staging-area/To-json-conversion/sp_generateKwastproductJsonByProductnr.sql
| 35.  | sp_generateProductJsonByProductnr.sql                   | src/Staging-area/To-json-conversion/sp_generateProductJsonByProductnr.sql
| 36.  | sp_generateSetproductJsonByProductnr.sql                | src/Staging-area/To-json-conversion/sp_generateSetproductJsonByProductnr.sql
| 37.  | sp_generateVerfproductJsonByProductnr.sql               | src/Staging-area/To-json-conversion/sp_generateVerfproductJsonByProductnr.sql
| 38.  | job.sql                          | src/Staging-area/job.sql
| 39.  | Staging-Area-Setup.sql           | src/Staging-area/Staging-Area-Setup.sql

# Bronnen

1. Setup. (z.d.). Geraadpleegd op 31 mei 2021, van https://restheart.org/docs/v5/setup/
