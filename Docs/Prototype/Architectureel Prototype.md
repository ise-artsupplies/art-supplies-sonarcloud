---
title: |
         Architectureel Prototype\
         Art Supplies\
         ISE Project\
         Groep A4
date: 07-06-2021
---
# Inleiding  
Dit document is het architectureel prototype van het te bouwen informatiesysteem.  
We gaan in op de architectuur van het systeem. Dit document is bedoeld voor de teamleden van het project. 

In dit document behandelen we de volgende hoofdstukken:  
- Architecturele constraints  
- Technische prioritisering use cases  
- Driver use case

# Architecturele constraints
- Gebruiken mongodb
    + De opdrachtgever wil graag mongodb gebruiken om de staging area op te zetten. Daarom zal ons systeem dus moeten integreren met mongodb.
- Linux en windows platform
    + Deployment vindt plaats op linux en development vind plaats op Windows. De software moet dus op beide platformen werken.  
    
# Technische prioritisering use cases

In dit hoofdstuk worden de use cases uit het functioneel ontwerp geanalyseerd op technische haalbaarheid en worden ze daar op geprioriteerd.

De use cases die zijn opgenomen in het functioneel ontwerp zijn de volgende:
1. Beheren Product (CRUD)
2. Kunstwerk inlijsten
3. Zoeken product
4. Zoeken in geschiedenis
5. Exporteren prijsdata

De use cases worden beoordeeld op de geschatte moeilijkheid tijdens implementatie.

| Use case               | Moeilijkheid | Toelichting |
|:-----------------------|:-------------|:------------|
| Beheren product (CRUD) | Laag         | In het maken van CRUD use cases heeft het projectteam al veel ervaring. Dit is namelijk al gedaan in vorige projecten. CRUD use cases lijken veelal op elkaar. Het gedeelte dat nieuw is voor het team, is om rekening te houden met de subtypes van producten. |
| Kunstwerk inlijsten    | Laag         | De use case kunstwerk inlijsten bestaat uit het toevoegen van een record aan een tabel. Verder moet er ook rekening worden gehouden met de subtype van framing order. |
| Zoeken product         | Hoog         | Bij de use case zoeken product moet er de mogelijkheid zijn om op verschillende eigenschappen naar producten te zoeken. Deze eigenschappen moeten ook kunnen worden gecombineerd. Het combineren van de zoekcriteria kan veel werk vergen. |
| Zoeken in geschiedenis | Middel       | Zoeken in geschiedenis is een use case die na de use case zoeken product wordt gemaakt. Het heeft namelijk een lagere prioriteit. Van de use case zoeken product kan er in deze use case veel functionaliteit worden hergebruikt. Wat hier wel bij komt is het generen van tabellen die de geschiedenis bijhouden, vandaar dat de moeilijkheid middel is. |
| Exporteren prijsdata   | Hoog         | De use case exporteren prijsdata heeft twee moeilijkheden. Allereerst is er de code generatie om in MSSQL records naar MongoDB format om te zetten. Ten tweede is er de moeilijkheid om de product data automatisch naar een MongoDB te sturen. Bij deze use case hoort namelijk ook de requirement FR1, waardoor er een staging area bij moet worden gemaakt. Deze usecase betrekt dus ook een functionele requirement.|

Uit deze tabel blijkt dat de moeilijkste use case 'Exporteren prijsdata' is, omdat er ook een functionele eis betrokken is, inclusief technisch lastige aspecten.  Daarom wordt hiervan een prototype gemaakt in dit document, zodat de grootste risico's van deze functionaliteit geanalyseerd en opgelost kunnen worden.  

# Driver use case
In dit hoofdstuk worden mogelijke oplossingen van de driver use case besproken, er wordt een oplossing uitgekozen. Voor deze uitwerking worden de benodigde API's, frameworks en programmeertalen besproken.  
Vervolgens wordt deze oplossing uitgewerkt.

## Mogelijke oplossingen  

### 1. Externe tijdsgebonden applicatie/script

Een externe tijdsgebonden applicatie of script is een externe applicatie die elke nacht de data van de MSSQL database ophaalt, en die data naar de MongoDB database stuurt. Deze applicatie kan of de hele tijd draaien, of door de database worden uitgevoerd.

#### Voordelen

1. De staging-area functionaliteit is afgezonderd van het informatiesysteem.
2. De MongoDB wordt een keer per dag volledig geüpdatet. Dit verbeterd de performance.

#### Nadelen

1. Het aanpassen van de functionaliteit duurt langer. Je moet dan mogelijk zowel het informatiesysteem als de applicatie aanpassen.
2. Het ontwikkelen van de externe applicatie kan veel tijd in beslag nemen. Er moet namelijk gebruik worden gemaakt van API's die met MSSQL en MongoDB verbinden worden gewerkt. Dit zou veel tijd kunnen kosten,
   omdat er veel onderzoek naar zou moeten worden gedaan.
3. Wanneer een product wordt aangepast, wordt er niet gelijk een aanpassing gedaan in de staging area.

### 2. Externe event-driven applicatie/script

Een extern event-driven script/applicatie is een extern script die na elke update, delete en insert wordt aangeroepen om de MongoDB database te updaten. Dit zou dan worden aangeroepen door triggers in MSSQL.

#### Voordelen

1. De staging-area functionaliteit is afgezonderd van het informatiesysteem.
2. De MongoDB is constant up-to-date.

#### Nadelen

1. Het ontwikkelen van dit script kan veel tijd in beslag nemen. Er moet namelijk gebruik worden gemaakt van API's die met MSSQL en MongoDB verbinden worden gewerkt. Dit zou veel tijd kunnen kosten,
   omdat er veel onderzoek naar zou moeten worden gedaan.
2. Na elke CRUD(create, update, delete) wordt de MongoDB database aangepast. Dit zorgt voor slechtere performance.

## Gekozen oplossing

De gekozen oplossing is oplossing 2. Dit is omdat de performance impact minimaal is. Er worden meestal niet veel producten tegelijkertijd toegevoegd. 

Er is gekozen om gebruik te maken van bestaande applicaties, zodat we meer ontwikkeltijd in het informatiesysteem kunnen investeren. We maken dus zelf geen applicatie/script die integratie met MongoDB verzorgd.

De volgende applicaties zijn onderzocht:
1. MongoDB Atlas
    - Beschrijving: MongoDB Atlas is een service die een MongoDB database in de cloud draait. Deze server ondersteunt het gebruik van HTTP-Requests om data te manipuleren met de standaard HTTP-Methods.
    - Voordelen:
        1. MongoDB Atlas heeft een uitgebreide documentatie.
    - Nadelen:
        1. Het REST-API staat in de cloud, waardoor het niet kan worden gedownload. Hierdoor kan doorgaande ondersteuning niet worden verzekerd.
2. RESTHeart
    - Beschrijving: RESTHeart is een open-source java applicatie wat met het gebruik van HTTP-Requests een MongoDB database manipuleert. 
    - Voordelen:
      1. RESTHeart is een java applicatie, waardoor het op bijna alle systemen kan draaien.
      2. RESTHeart heeft een uitgebreide documentatie.  

Uit deze mogelijkheden is RESTHeart gekozen, omdat het open source is en op ons eigen systeem kan draaien.

## Benodigde API's, frameworks en programmeertalen

De benodigde API's zijn RESTHeart, het tSQLt test framework. Het tSQLt test framework wordt gebruikt om testen te maken in de database.

## Uitwerking (Prototype)

De uitwerking van de use case, wat als het prototype functioneert, is te vinden in de map 'sql'. Om hem zelf te gebruiken, voer alle sql bestanden die met 'sp_' beginnen uit, en voer daarna het bestand 'db opzet.sql' uit. In dit bestand kan je testen of de verbinding met Restheart werkt.

# Bronnen

1. Get Started with Atlas — MongoDB Atlas. (z.d.). Geraadpleegd op 26 april 2021, van https://docs.atlas.mongodb.com/getting-started/
2. M. (2017, 2 maart). Ole Automation Procedures Server Configuration Option - SQL Server. Geraadpleegd op 26 april 2021, van https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/ole-automation-procedures-server-configuration-option?view=sql-server-ver15
3. Srl, S. (z.d.). RESTHeart. Geraadpleegd op 26 april 2021, van https://restheart.org/
