create or
alter proc sp_deleteDbOrColByETag @url varchar(max),
@etag nvarchar(4000)
as
begin
    declare @object as int;
    declare @status as int;
    declare @responsetext as varchar(8000);

    exec sp_oacreate 'msxml2.xmlhttp', @object out;
    exec sp_oamethod @object, 'open', null, 'delete',
         @url,
         'false'
    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', 'Basic YWRtaW46c2VjcmV0'
    exec sp_OAMethod @object, 'setrequestheader', null, 'If-Match', @etag
    exec sp_oamethod @object, 'send'
    exec sp_oamethod @object, 'responsetext', @responsetext output
    exec sp_oamethod @object, 'status', @status output
    select @responsetext, @status
    exec sp_oadestroy @object
end