create or
alter proc sp_stagingAreaSetup
as
begin
    declare @url nvarchar(max) = 'http://localhost:8080/products';
    declare @object as int;
    declare @status as int;
    declare @etag as nvarchar(4000);
    declare @responsetext as varchar(8000);

    exec sp_getETagDbOrCol @url = @url, @etag = @etag out

	-- Als etag bestaat, bestaat de database. Daarom gaan we hem verwijderen
    if (@etag is not null)
        begin
            exec sp_deleteDbOrColByETag @url = @url, @etag = @etag
        end

    exec sp_oacreate 'msxml2.xmlhttp', @object out;
    exec sp_oamethod @object, 'open', null, 'put',
         @url,
         'false'
    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', 'Basic YWRtaW46c2VjcmV0'
    exec sp_oamethod @object, 'send'
    exec sp_oamethod @object, 'responsetext', @responsetext output
    exec sp_oamethod @object, 'status', @status output
    select @responsetext, @status
    exec sp_oadestroy @object
end
go

exec sp_stagingAreaSetup