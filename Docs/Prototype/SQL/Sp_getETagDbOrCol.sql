create or alter proc sp_getETagDbOrCol @url varchar(max),
@etag nvarchar(4000) output
as
begin
    declare @object as int;
    exec sp_oacreate 'msxml2.xmlhttp', @object out;
    exec sp_oamethod @object, 'open', null, 'get',
         @url,
         'false'
    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', 'Basic YWRtaW46c2VjcmV0'
    exec sp_oamethod @object, 'send'
    exec sp_OAMethod @object, 'getResponseHeader', @etag out, 'ETag'
    exec sp_oadestroy @object
end
go