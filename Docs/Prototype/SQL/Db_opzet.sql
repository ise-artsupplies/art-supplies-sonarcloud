-- Omdat dit een voor een prototype is, wordt dit niet set-based gedaan. Dit is om tijd te besparen in de elaboration-fase.
-- Ook worden er geen http-responses afgehandeld.
-- In het uiteindelijke product moet dat overigens wel.
use master
drop database if exists testStagingArea
create database testStagingArea
use testStagingArea
create table product
(
    productnr int   not null primary key,
    prijs     money not null
)

exec sp_configure 'show advanced options', 1;
go
reconfigure;
go
sp_configure 'Ole Automation Procedures', 1;
go
reconfigure;
go

-- triggers

create or alter trigger productInsert
    on dbo.product
    after insert
    as
    declare
        @productnr int
    select @productnr = productnr
    from inserted
    declare
        @prijs money
    select @prijs = prijs
    from inserted
    exec sp_insertIntoDb @productnr, @prijs
go

create or alter trigger productUpdate
    on dbo.product
    after update
    as
    declare
        @productnr int
    select @productnr = productnr
    from inserted
    declare
        @prijs money
    select @prijs = prijs
    from inserted
    exec sp_updateInDb @productnr, @prijs
go

create or alter trigger productDelete
    on dbo.product
    after delete
    as
    declare
        @productnr int
    select @productnr = productnr
    from deleted
    exec sp_deleteFromDb @productnr
go

exec sp_stagingAreaSetup

insert into product
values (12345, 10.00)

update product
set prijs = 1200
where productnr = 12345

delete
from product
where productnr = 12345
