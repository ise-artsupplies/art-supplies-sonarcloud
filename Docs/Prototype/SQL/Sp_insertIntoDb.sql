create or
alter proc sp_insertIntoDb @productnr int,
                                           @prijs money
as
begin
    declare @url nvarchar(max) = 'http://localhost:8080/products/' + cast(@productnr as nvarchar) ;
    declare @object as int;
    declare @status as int;
    declare @responsetext as varchar(8000);
    declare @body as varchar(8000) =
        '{"productnr":'+cast(@productnr as nvarchar)+',"prijs": "'+cast(@prijs as nvarchar)+'"}'
    exec sp_oacreate 'msxml2.xmlhttp', @object out;
    exec sp_oamethod @object, 'open', null, 'put', @url,'false'
    exec sp_oamethod @object, 'setrequestheader', null, 'content-type', 'application/json'
    exec sp_OAMethod @object, 'setrequestheader', null, 'Authorization', 'Basic YWRtaW46c2VjcmV0'
    exec sp_oamethod @object, 'send', null, @body
    exec sp_oamethod @object, 'responsetext', @responsetext output
    exec sp_oamethod @object, 'status', @status output
    select @responsetext, @status
    exec sp_oadestroy @object
end
go