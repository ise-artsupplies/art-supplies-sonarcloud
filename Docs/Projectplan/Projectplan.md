---
title: |
        Projectplan\
        Art Supplies\
        ISE Project\
        Groep A4
date: 03-06-2021
---

 
# Inleiding
Dit document dient als overeenkomst tussen de opdrachtgever en projectgroep A4 op het project "Art Supplies".  
Zo zal onze opvatting van de probleemstelling en opdracht worden gedeeld, met afspraken over op te leveren producten en kwaliteitseisen.
De belanghebbenden worden weergegeven samen met de eisen waaraan dit project moet voldoen. De vision dient als een
algemeen beeld van het project. Vanuit dit beeld wordt uiteindelijk het eindproduct ontwikkeld.

Dit document is opgesteld voor het ISE-Project van de opleiding HBO-ICT op de HAN University of Applied Sciences.
De opdrachtgever van dit project is Matthijs de Jonge, de procesbegeleider is Jorg Janssen en de professional skills docent is Helen Visser. 

De opdracht voor groep A4 is om het informatiesysteem van een "Art Supply Store" te realiseren. Dit systeem bestaat uit een productcatalogus van verschillende producten die veel door kunstenaars worden gebruikt.  
Voor deze producten worden verschillende eigenschappen vastgehouden, zodat er op gezocht kan worden. Denk met deze eigenschappen bijvoorbeeld aan de vorm van een kwast, de dikte van een kwast, de kleur en transparantie van verf. 
Het informatiesysteem moet ook een framing service bevatten. Dit is een service waar amateur kunstenaars hun werk kunnen laten inlijsten door professionals.
De benodigde informatie hiervoor moet ook kunnen worden opgeslagen. Het zoeken naar de producten wordt door middel van een staging area gedaan. Hier worden de eigenschappen van de producten in opgeslagen.  

Het doel van het project is om het voor kunstenaars makkelijker te maken om makkelijker hun gespecialiseerde producten te vinden.  
Het zoeken op producten zal ook fijn zijn voor medewerkers, voor snelle dienstverlening.  

Het eerste gedeelte van dit document dient als plan van aanpak voor het project en bestaat uit te volgende onderdelen:  
1. Een beschrijving van de achtergrond van het project;  
2. De doelstelling, de opdracht en de op te leveren resultaten van het project;  
3. De projectgrenzen;  
4. De randvoorwaarden;  
5. De gebruikte ontwikkelmethode;  
6. Projectorganisatie en communicatie;  
7. De planning van het project;  
8. De risico's van het project;  

Het tweede gedeelte van dit document gaat over onze visie op het project. Dit bestaat uit een beschrijving van het doel van de opdracht, en onze opvatting van de probleemstelling. De belanghebbenden en de 
eisen waaraan het product moet voldoen worden ook beschreven. De eisen zijn niet verder uitgewerkt. Dit gedeelte van het document bestaat uit:
1. De positionering;  
2. De belanghebbenden;  
3. De eisen;

# Achtergrond van Project  
Dit project is toegewezen door HAN University Of Applied Sciences. De opdrachtgever van dit project is Matthijs de Jonge, wie een eigenaar van een art supply store is.
De uitkomst van dit project zal invloed hebben op de kunstenaars, doordat zij makkelijker kunnen zoeken naar gespecialiseerde producten.
Ook kunnen winkelmedewerkers makkelijker zoeken, waardoor de dienstverlening makkelijker zal zijn.  
Kunstenaars hebben een ruime keuze aan producten die zij kunnen gebruiken voor het maken van kunst. Voor het maken van een keuze tussen deze producten hebben
deze kunstenaars criteria waaraan het product wat zij zoeken moet voldoen. Het zoeken op deze criteria is moeilijk omdat veel webwinkels erg gelimiteerde zoekmachines 
hebben. Het zoeken naar het gewenste product kost dus erg veel tijd.  

De opdrachtgever wil een informatiesysteem laten ontwikkelen door de projectgroep, waarin de producten staan en er op alle criteria kan worden gezocht.
Dit moet ervoor zorgen dat kunstenaars eenvoudig naar hun gewenste product(en) kunnen zoeken.

# Doelstelling, opdracht en op te leveren resultaten  
Het probleem waar de art supply store mee te maken heeft, is dat er voor kunstenaars veel tijd verloren gaat in het zoeken naar producten.  
Ook is het bijhouden van informatie over de aanwezige framing service veel werk, omdat dit momenteel op papier gebeurt.

In dit project zijn dus twee doelen te onderscheiden:
1. Het zoeken op producten makkelijk maken waardoor kunstenaars minder tijd kwijt zijn met het zoeken;
2. Het bijhouden van informatie over de framing service.

Om beide doelen te halen wordt er een informatiesysteem ontwikkelt.
In dit informatiesysteem worden alle beschikbare producten bijgehouden, waarvan allerlei eigenschappen bekend zijn. Op deze manier kan er worden gezocht op deze specifieke eigenschappen. 
Ook moet er worden bijgehouden hoe veel de producten kosten.
  
In de Art Supply Store is een framing service aanwezig. Met deze framing service is het mogelijk voor kunstenaars om hun werk te laten framen door een professional.  
Hiervoor moeten o.a. het frame type, de dimensies van het kunstwerk, de prijs en de opleveringsdatum worden opgeslagen.  

Naast het informatiesysteem die voldoet aan de afgesproken requirements, moeten wij ook documentatie opleveren over ons project.  
Onder documentatie vallen de volgende onderdelen:
1. Projectplan
   - Het projectplan bestaat uit het plan van aanpak en de vision. Dit document bevat de voorbereidingen op het project. Onze visie van de opdracht, projectmethode en opleverdocumenten worden hier ook in behandeld. 
     Het dient ervoor om de opdrachtgever op de hoogte te stellen over de manier waarop wij de opdracht hebben begrepen en hoe wij als ontwikkelteam te werk gaan.  
     Dit document zal voornamelijk tijdens de inceptiefase worden ontwikkeld.
2. Functioneel ontwerp  
   - Het functioneel ontwerp bevat de prioritering van eisen. Deze eisen worden uitgewerkt in use cases, of als niet-functionele eis. 
     Dit document is te lezen zonder technische achtergrond. Dit document wordt ontwikkeld door (bijna) alle fasen door.
3. Technisch ontwerp  
   - Het technisch ontwerp bevat het technische ontwerp voor elke eis. Dit ontwerp wordt in de code geïmplementeerd.
     Om dit document te begrijpen is het aangeraden om een technische achtergrond te hebben. Dit document wordt ontwikkeld door (bijna) alle fasen door.
4. Architectureel prototype  
   - Dit document wordt ontwikkeld tijdens de elaboratiefase. Het dient als beschrijving van de architectuur, en het testen daarvan d.m.v. een prototype.  
     Als de test met het prototype slaagt, kan de architectuur worden overgenomen in de constructiefasen.
5. Tests  
   - De tests dienen ervoor om te laten zien dat de geschreven code werkt. Hierbij testen we de triggers en procedures die wij geschreven hebben. 
     Tests worden gedurende het hele project geschreven omdat we een iteratieve projectmethode hanteren (RUP).

Deze onderdelen zullen voor zowel de opdrachtgever als voor school nodig zijn.
Voor school komt er nog een extra onderdeel bij, namelijk het individuele projectverslag. In dit verslag word het project geëvalueerd
Al deze onderdelen samen vormen het project met de documentatie die wij moeten inleveren in onderwijsweek 8.

# Projectgrenzen  
In dit hoofdstuk worden de grenzen van het project beschreven. Dit geeft duidelijkheid van wat er wel en niet opgeleverd
gaat worden.  

- Na de eindoplevering op 10 juni zal er niet verder worden gewerkt aan het project.  
- Het team is niet verantwoordelijk voor het deployen van het informatiesysteem. Wel wordt er een functioneel en technisch ontwerp opgeleverd waarin
staat beschreven hoe het informatiesysteem gedeployed kan worden.  
- Visueel de data weergeven via een uitgebreide front end wordt niet door ons gedaan.  

# Randvoorwaarden
In dit hoofdstuk zijn de verschillende randvoorwaarden opgesteld die gelden voor dit project. 
Deze randvoorwaarden zijn opgesteld voor de opdrachtgever. 
Als de opdrachtgever niet voldoet aan een van deze randvoorwaarden is dit een risico voor de projectvoortgang.

## Randvoorwaarden project
1. De opdrachtgever is wekelijks minimaal 1 á 2 uur beschikbaar om in gesprek te gaan.
2. School stelt een project begeleider aan die beschikbaar is voor vragen en advies kan geven waar nodig.
3. Het team moet 5 van de 5 werkdagen toegang hebben tot de BitBucket omgeving die gereserveerd is voor het ISE-project.
4. Het team moet 5 van de 5 werkdagen toegang hebben tot de Jira omgeving die gereserveerd is voor het ISE-project.
5. De HAN dient tijdens de loop van het project de projectgroep toegang te geven tot bepaalde diensten (inclusief, maar niet beperkt tot: Jira, BitBucket, OnderwijsOnline)

## Randvoorwaarden externe middelen
1. In de tijd van de COVID-19 pandemie moet de omgeving binnen Microsoft Teams altijd beschikbaar zijn. Dit is omdat we Microsoft Teams gebruiken om te communiceren met de docenten.

# Op te leveren producten en kwaliteitseisen 
| Product | Productkwaliteitseisen (SMART) | Benodigde activiteiten om te komen tot het product | Proceskwaliteitseisen |
|:--------|:-------------------------------|:---------------------------------------------------|:----------------------|
| Plan van aanpak | Volgt de opbouw zoals te vinden op OnderwijsOnline | Gezamenlijke inhoudsbespreking gevolgd door een taakverdeling.  Voldoet aan de richtlijnen van Toelichting op PvA 3.1 | De gerealiseerde hoofdstukken zijn door minimaal 1 projectlid gereviewd. |
| Functioneel ontwerp | Alle Non-functional requirements zijn ingedeeld volgens FURPS.  Er zijn UML Syntactisch correcte diagrammen.  Na het lezen is het voor de lezer duidelijk welke functionaliteiten het te ontwikkelen systeem moet hebben.  Alle eisen zijn ingedeeld volgens MoSCoW.  Voldoet aan de ICA controlekaart.  Het document voldoet aan de Definition of Done voor documenten. | Non-functional Requirements opstellen.  Functional Requirements opstellen.  Use Case Diagram uitwerken.  Fully Dressed Use Cases uitschrijven.  Domeinmodel maken.  System Sequence Diagrams maken. | Product Owner raadplegen voor MoSCoW prioritering.  Feedback vragen aan projectbegeleider.  Iedere Use Case is door ten minste twee teamleden goedgekeurd. |
| Technisch ontwerp | Ontwerpen wijken niet af van de daadwerkelijke implementatie.  Er zijn UML Syntactisch correcte diagrammen. Na het lezen is het voor de lezer duidelijk hoe het systeem ontworpen is. Het document voldoet aan de Definition of Done van documenten. | Toelichten welke keuzes er binnen het ontwerp van de applicatie zijn gemaakt. | Beschrijvingen van diagrammen bevatten een duidelijke uitleg over keuzes of andere opvallende aspecten die toelichting nodig hebben.  Feedback vragen aan projectbegeleider. |
| Code/Applicatie | Code en commentaar word geschreven in het engels.  Code is traceable naar User Stories op Jira.  Code voldoet aan de Definition of Done zoals deze opgesteld is in het PvA. Code smells in sonarcloud zijn minder dan 4.| In het PvA richtlijnen opnemen waaraan de code moet voldoen.  Requirements opstellen waaraan de applicatie moet voldoen.  Designs bestuderen.  Code schrijven.  Code testen.  Code reviewen. | Code wordt door minimaal één projectlid gereviewd d.m.v. een pull request.  Projectleden reviewen code volgen de richtlijnen in het PvA.  Opgeleverde code bevat de benodigde testen.  Documentatie word up-to-date gehouden door project lid. |
| Testrapport | Elke unit test bevat een testcase in het test rapport.  Het document voldoet aan de Definition of Done voor documenten. | Het maken van testen.  Het maken van een basic en alternatieve flow.  Toelichten conclusies.  Verweken resultaten. | Elke geschreven test is terug te vinden in het testrapport en bevat daarnaast een omschrijving van de basic en alternatieve flow.  Elke nieuwe testcase in het testrapport is door minstens 1 teamlid gereviewd. |

## Definition of Done
Tijdens het ISE-Project moet er goed gekeken worden naar de kwaliteitseisen van de op te leveren producten.  
Om ervoor te zorgen dat het project met kwaliteit wordt afgerond moet er voor elke issue op Jira worden gecontroleerd of deze voldoet aan de 'Definition of Done' (D.O.D).

Voor de documenten is de volgende Definition of Done opgesteld:

1. Voldoet aan de eisen die het team vooraf afgesproken heeft met de opdrachtgever zoals geformuleerd het PvA en TO.
2. Minstens één groepsgenoot heeft de documentatie gereviewd op basis van de ICA controlekaart en acceptatiecriteria.
3. Er is overeenstemming met de procesbegeleider over het geleverde document.
4. Goedkeuring door in ieder geval de meerderheid van het team over het geleverde document.
5. Indien beschikbaar voldoen de documenten aan de geleverde templates uit de HAN.

Voor de geschreven code is de volgende Definition of done opgesteld:

1. Voldoet aan de eisen die het team vooraf afgesproken heeft met de opdrachtgever zoals geformuleerd het PvA en TO. (Zie: [Op te leveren producten en kwaliteitseisen](#op-te-leveren-producten-en-kwaliteitseisen))
2. De code is geschreven in de feature branch en gepushed naar BitBucket.
3. De nieuwe code is geïntegreerd met de bestaande code d.m.v. een pull-request.
4. Een pull-request wordt gereviewd door minstens één teamlid, die de code niet heeft geschreven.
5. Mogelijke paden in de functionaliteit zijn getest.
6. Feature bevat alle functionaliteiten beschreven in de bijbehorende Use Case beschrijving.
7. De code die zich bevindt in de feature branch voldoet aan de codeerstandaarden zoals gedefinieerd in het FO en PvA.
8. FO en TO zijn aangepast volgens de implementatie.  

# Ontwikkelmethoden  
Tijdens dit project gaan we te werk met de ontwikkelmethode RUP. In dit hoofdstuk vertellen we wat RUP precies inhoud en hoe we het gaan toepassen in het project.
Ook wordt beschreven hoe het ons verder gaat helpen. Naast RUP hebben we ervoor gekozen om het programma "Code with me" 
te gebruiken. Dit programma word gebruikt om documenten in realtime met elkaar te delen.
  
## Projectmethode: Rup
### Wat is RUP-methode?  
RUP is een agile ontwikkelmethode waarin de levenscyclus van een project in vier fasen verdeeld is. RUP-methode is iteratief, het is dus terugkerend. 
Het is agile en iteratief omdat de fasen van de cyclus herhaald worden tot dat het doel is behaald. Het proces is op "figuur1" visueel 
weergegeven.

![Figuur 1 RUP](./projectplan/Img/rup.png)


### RUP toepassen
Een RUP-project is opgebouwd uit verschillende fasen. Deze zal de projectgroep hanteren. Er zullen 3 constructiefasen plaatsvinden.  
RUP schrijft geen verplichte meetings voor. Daarom heeft de projectgroep er zelf voor gekozen om elke dag samen de ochtend te beginnen.  
Tijdens deze meeting vindt de taakverdeling plaats. Deze meeting is kort en iedereen komt aan bod. Er wordt door iedereen verteld wat ze vandaag gaan doen, en of daar eventuele problemen kunnen ontstaan.  
Dit zorgt voor transparantie, er kan dan worden geholpen bij moeilijke taken, of taken kunnen anders worden verdeeld als dat om welke reden dan ook handiger uitkomt.  
  
Ook wordt er elke week een vergadering gehouden met de opdrachtgever en procesbegeleider. Dit is ook te vinden in [Projectorganisatie en communicatie](#projectorganisatie-en-communicatie)  

### Rollen  
RUP schrijft wel een aantal rollen voor. De beschrijvingen van deze rollen zijn hieronder te vinden.

#### Teamleider  
De Teamleider is verantwoordelijk voor het plannen en managen van de projectopdracht. De teamleider definieert en bewaakt de procedures om kwaliteit van de producten hoog te houden.  

#### Softwarearchitect  
De Softwarearchitect beschrijft de technische keuzes in het project, die de architectuur voor de te bouwen applicatie bepalen. 
Ook is de Softwarearchitect verantwoordelijk voor het communiceren van de architectuur aan de ontwikkelaars en controleert of de implementatie voldoet aan de voorgestelde architectuur.

#### Informatieanalist  
De Informatieanalist is verantwoordelijk voor het helder krijgen van requirements. Ook neemt de Informatieanalist een groot deel aan 
het modelleren van Use cases, waardoor de functionaliteit en grenzen van het systeem bepaalt kunnen worden.  

#### Use case ontwerper  
De Use case ontwerper is verantwoordelijk voor het specificeren van Use cases en schetsen.  

#### Tester  
De Tester specificeert de test cases en vastleggen daarvan in een Testontwerp. Ook gaat de Tester vervolgens de testcases doorlopen
en het noteren.
  
#### Integrator
De Integrator is verantwoordelijk voor het integreren van software tot builds. De Integrator is eindverantwoordelijk voor de deployment en documentatie hiervan.

# Tooling
## Code with me
Code with me gebruiken we om samen als team tegelijk het informatiesysteem te realiseren. Met Code with me kan de host een 
project in een teksteditor delen met teamleden en er samen in realtime aan werken.  
Door dit programma te gebruiken hebben de teamleden een goed inzicht over wat de status van de opdrachten zijn. 
Ook kunnen we direct elkaars werk controleren en waar nodig feedback geven.

Een nadeel van Code With Me is dat alle veranderingen bij één iemand op de computer staan.  
Als deze persoon vervolgens dit op BitBucket zet, lijkt het dus alsof het team geen bijdrage heeft gehad, en die ene persoon heel veel.  
Daardoor worden rapportages die in bitbucket onbetrouwbaar, want de bijdrage van iedereen is dus minder goed zichtbaar.
Als oplossing hiervoor gaan we Jira taken aanmaken en specifiek hierop tijd loggen.  
Er kan dan dus met Jira worden bijgehouden wat de bijdrage van iedereen is, vervolgens kan daar een rapport uit worden gemaakt zoals in bitbucket ook gedaan kan worden.

## BitBucket  
Bitbucket wordt gebruikt om git repositories te beheren. Door gebruik te maken van Bitbucket zorgen we ervoor dat 
iedereen die aan het project werkt zijn werkzaamheden op één centraal punt kan opslaan. Ook zorgt git voor versiebeheer, zodat er altijd, als nodig, teruggegaan kan worden naar oudere code.

## Jira
In Jira gebruiken we een bord om agile met het team te kunnen werken. In dit bord zetten we de taken, die we vervolgens verdelen.  
Jira biedt transparantie voor al het werk van het team en maakt de status van iedere taak zichtbaar.  
Ook gebruiken we Jira om onze uren te loggen. Door het aantal uren wat verzet is te vergelijken met de schatting, weten we hoe goed de fase is verlopen. Dat geeft ons mogelijkheden om dingen anders te doen voor de volgende fase.  
  
## Sonarcloud
Sonarcloud is een tool om codekwaliteit door een computer te laten controleren. Deze tool gebruiken we, omdat de computer geen voorkeuren heeft en mensen wel.  
Er kijkt dus altijd een neutrale partij naar de code, die beslist of iets van goede of slechte kwaliteit is.  
Door gebruik te maken van deze tool kan de kwaliteit gewaarborgd worden, en ook aantoonbaar.  

# Projectorganisatie en communicatie
Dit project word georganiseerd door de HAN, en uitgevoerd door studenten van de HAN.  
Deze projectgroep wordt begeleid door docenten, waar regelmatig contact mee is. Verder in dit hoofdstuk zijn contactgegevens te vinden van alle betrokken personen.  

De projectgroep heeft minimaal eens per lesweek contact met de procesbegeleider. In deze vergadering kunnen eventuele vragen worden voorgelegd betreft het proces (vragen over RUP bijvoorbeeld).  
Ook heeft de projectgroep minimaal eens per week contact met de opdrachtgever. Er kan dan, als mogelijk, progressie getoond worden. Of er kunnen vragen gesteld worden over het domein, of functionaliteit.  
  
Door regelmatig af te spreken met zowel de procesbegeleider als opdrachtgever, kan op tijd gestuurd worden. Er kan bijvoorbeeld meer of minder werk worden ingeschat op basis van hoe goed het gaat.  

Communicatie zal in ieder geval tot aan de meivakantie volledig online plaatsvinden i.v.m. de coronacrisis. We gaan ervan uit dat na de vakantie dit niet veranderd is, maar de maatregelen veranderen regelmatig.
Communicatie met de opdrachtgever en school zal via het programma Microsoft Teams verlopen. De projectgroep zal veel gebruik maken van Discord, maar blijven bereikbaar op Teams. 
  
Online samenwerken vereist goede afspraken. Dit zijn een aantal van de gemaakte afspraken binnen de groep:
- Beginnen om 9 uur, stoppen om ± 5 uur.
- Pauze van ± 12 uur tot ± 1 uur.
- Elke ochtend een vergadering met de groep waar we de stand van zaken doorspreken.
- Aan het eind van elke dag een korte vergadering met de voortgang van die dag.   
- Documentatie en andere documenten worden geschreven in het Nederlands.
- Werken aan projectverslag op woensdagmiddag. (Of in eigen tijd)

De rollen zijn ook verdeeld binnen de groep. Alle rollen, behalve notulist, zijn rollen die bij de RUP methode horen. Deze zijn beschreven in hoofdstuk [Ontwikkelmethoden](#ontwikkelmethoden).  
Hoe de rollen zijn verdeeld, is te zien in de eerste tabel. In de tweede tabel zijn ook andere betrokkenen met contactgegevens te zien.

| Student       | Rol                            | E-Mail                       |
|:--------------|:-------------------------------|:-----------------------------|
|Romy de Weijer | Integrator & notulist          | rbc.deweijer@student.han.nl  |
|Enes Yildiz    | Tester & Informatie-analist    | e.yildiz2@student.han.nl     |
|Rens Harinck   | Software Architect             | r.harinck@student.han.nl     |
|Stein Jonker   | Teamleider                     | sa.jonker@student.han.nl     |
|Max Lodders    | Use case ontwerper, notulist   | m.lodders@student.han.nl     |

| Betrokkene      | Rol                            | E-Mail                       |
|:----------------|:-------------------------------|:-----------------------------|
|Matthijs de Jonge| Opdrachtgever                  | matthijs.dejonge@han.nl      |
|Jorg Janssen     | Procesbegeleider               | jorg.janssen@han.nl          |
|Helen Visser     | Professional Skills docent     | helen.visser@han.nl          |  

# Planning  
![Ruwe planning project](./projectplan/Img/planning.png)  

In de planning die hierboven te vinden is, is een globaal overzicht te zien van de planning van dit project.  
Donkergroene vakjes geven aan hoe lang een fase duurt. De inceptiefase duurt dus één week. Alle activiteiten tijdens deze fase zijn aangegeven met een lichtgroene kleur. Soms is een activiteit alleen van belang aan het begin of eind van een fase.  
Het grote rode vak is de meivakantie, dan zal er niet worden gewerkt. Deze vakantie duurt een week.  
De oranje vakken zijn bijzondere dagen die in die week plaatsvinden. Dat zijn tijdens dit project koningsdag en hemelvaart. Op beide dagen zal niet worden gewerkt.  
  
Belangrijk in deze planning zijn de weken die lichtblauw zijn gemarkeerd. In de week na de meivakantie gaat de elaboratie door tot en met dinsdag. Die woensdag begint dan de eerste constructiefase.  
Elke constructiefase duurt 1.5 week.  
  
Ook is er zichtbaar wanneer deadlines zijn, deze zijn onder 'Activiteiten' gezet.
  
In de transitiefase word het project afgerond.

Het volledige project duurt maximaal tien onderwijsweken. Er zal na deze tijd niet meer verder worden gewerkt aan dit project door deze projectgroep.  
  
## Opleveringen  
Hier worden de opleveringen per fase beschreven.  

Inceptiefase:  
        - Projectplan  
Elaboratiefase:  
        - Architectureel Prototype  
        - Functioneel ontwerp (Versie 1)  
        - Technisch ontwerp (versie 1)  
        - Opzet database 
Constructiefase 1, 2, 3:  
        - Functioneel ontwerp  
        - Technisch ontwerp  
        - Database systeem
Transitiefase:
        - Functioneel ontwerp (definitief)  
        - Technisch ontwerp (definitief)  
        - Database systeem (definitief)


# Risico's  
In dit hoofdstuk worden de risico's beschreven van dit project.  

## Technische risico's
Dit zijn de technische risico's van dit project.

| Risico | Kans (groot, middel, of klein) | Impact (groot, middel, of klein) | Uitwijkstrategie | Tegenmaatregel |
|:-------|:-------------------------------|:---------------------------------|:---------------|:-----------------|
| De ondersteuning van de gebruikte frameworks en/of API’s valt weg | Klein | Groot | Er wordt een soortgelijke API gekozen om de, nu deprecated, API te vervangen. Er wordt met de opdrachtgever overlegd hoeveel tijd er (verwacht) nodig is om alles te herschrijven. Tijdens het bespreken hiervan worden prioriteiten gelegd op taken. De minst belangrijke taken worden dan niet gerealiseerd in dien dat niet mogelijk is in verband met de tijd. | Bij het maken van de keuze van een framework wordt kritisch gekeken hoe groot de kans is dat het framework zijn ondersteuning verliest. |
| Gemaakt werk wordt overschreven door een ander bestand | Middel | Middel | Er wordt vergeleken met oudere versies van branches op de repository. Op basis daarvan wordt een oudere versie van het bestand teruggehaald. | Men kan niet zomaar commits pushen naar de development branche, dit moet altijd met een pull request vanuit een andere branch worden gedaan. De pull request moet door minstens twee teamleden zijn goedgekeurd voordat deze wordt doorgevoerd. |
| Ontwikkelaars programmeren in een database die niet de laatst geüpdatet versie heeft, waardoor sommige onderdelen hun werking kunnen verliezen. | Middel | Klein | De pull request wordt teruggedraaid en de branch waarop de ontwikkelaar werkt moet met de development branch mergen. De ontwikkelaar moet alle scripts voor het creëren en vullen van de database opnieuw uitvoeren. | Voordat een pull request wordt goedgekeurd, moet men eerst vaststellen of de branch die wordt gemerged wel in dezelfde lijn loopt als de development branch. Als dit niet zo is, wordt de pullrequest gemarkeerd als 'Needs Work'. Als gevolg hiervan moet de ontwikkelaar de development branch pullen en waar nodig de code updaten. |

## Proces risico's
Dit zijn de risico's in dit project die met het proces te maken hebben.

| Risico | Kans (groot, middel, of klein) | Impact (groot, middel, of klein) | Uitwijkstrategie | Tegenmaatregel   |
|:-------|:-------------------------------|:---------------------------------|:-----------------|:-----------------|
| Er wordt teveel functionaliteit beloofd, waardoor een tekort aan tijd ontstaat en niet alle beloofde functionaliteit gerealiseerd kan worden| Middel | Middel | Functionaliteit wordt in overleg met de opdrachtgever verder geprioriteerd, en er wordt alleen nog gewerkt aan de functionaliteit(en) met een hoge prioriteit| Voordat er dingen worden belooft wordt er duidelijk afgebakend wat er moet worden opgeleverd.|
| Er moet gebruik worden gemaakt van een technologie die onbekend is voor teamleden, waardoor tijd verloren gaat door het onderzoeken|Groot|Klein| Overleg met opdrachtgever over prioriteiten |Aantekeningen maken van de werking, zodat andere teamgenoten deze kunnen raadplegen als deze met de nieuwe technologie aan de slag gaan|
| Het domein is verkeerd begrepen door de ontwikkelaars, waardoor functionaliteit niet juist is geïmplementeerd | Middel | Middel | De functionaliteit wordt opnieuw geïmplementeerd in de volgende constructiefase. Als er geen fase meer volgt, overleg met opdrachtgever | Door regelmatige (wekelijkse) meetings, is er regelmatig controle op het verzette werk. Er kan dan als nodig worden gestuurd. Deze meeting is ook een goede gelegenheid om vragen te stellen over het domein. |  
  
# Positionering 
## Het huidige bedrijfsproces
Het bedrijf van de opdrachtgever verkoopt producten in een webshop voor kunstenaars. Deze producten zijn van verschillende soorten en hebben veel verschillende eigenschappen.
Ook worden deze producten in sets verkocht. Deze sets zijn op zichzelf ook een product. Het bedrijf verleent ook een framing service waarbij amateur kunstenaars hun werk in
kunnen laten lijsten.

## Probleemstelling
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Het probleem               | Kunstenaars (de klanten van de webshop) willen producten vinden die bepaalde eigenschappen hebben, maar er zijn geen webshops die de mogelijkheid bieden om op veel verschillende eigenschappen te zoeken. |  
| Dit beïnvloed:             | Direct: Kunstenaars en webshops (Deze kunnen hun gewenste product niet vinden) Indirect: Leveranciers en webshops (Deze verkopen mogelijk minder producten omdat de gewenste producten niet kunnen worden gevonden) | 
| De impact hiervan is:      | Groot. Veel aankopen kunnen worden misgelopen door de kunstenaars omdat zij producten niet kunnen vinden.| 
| Een succesvolle oplossing: | Er wordt een database ontwikkeld die de mogelijkheid bied om te zoeken op producten die bepaalde eigenschappen hebben. |

# Belanghebbenden	
## Overzicht belanghebbendenvertegenwoordiging
In dit project zijn er geen vertegenwoordigers van belanghebbenden. Het project is namelijk niet met een echte winkel. Wel zijn er belanghebbenden rollen die worden gebruikt om behoeftes vast te leggen.

| Belanghebbende     |
|:-------------------|
| Eigenaar           |
| Kunstenaars        |
| Framers            |
| Winkelmedewerkers  |

## Profiel van de belanghebbenden
### Eigenaar
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Beschrijving               | De eigenaar van de winkel.                                                             |
| Verantwoordelijkheden      | Zorgt ervoor dat het systeem draaiende is op zijn website.                                         | 
| Succescriteria             | Het systeem bevat op zijn minst alle must have requirements.                                 |

### Kunstenaars
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Beschrijving               | De klant(en) van de winkel.                                                                           |
| Verantwoordelijkheden      | 1. Maakt gebruik van de zoekfuncties van het systeem. 2. Maakt gebruik van de framing service van de webshop.           |
| Succescriteria             | 1. De zoekfunctionaliteiten werken. 2. Er kan een verzoek tot gebruik van de framing service worden ingediend.            |  

### Framers
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Beschrijving               | De personen die voor de framing service uitvoeren.                                          |
| Verantwoordelijkheden      | Haalt verzoeken voor de framing service op en verwerkt deze.                                |
| Succescriteria             | Kan verzoeken voor gebruik van de framing service ophalen, met daarbij de benodigde informatie voor de framing service. |

### Winkelmedewerkers
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Beschrijving               | De medewerkers van de winkel.                                                                         |
| Verantwoordelijkheden      | 1. Maakt gebruik van de zoekfuncties van het systeem. 2. Houdt de voorraad bij met behulp van het systeem. |
| Succescriteria             | 1. De zoekfunctionaliteiten werken. 2. De voorraad kan via het systeem bijgewerkt worden.              | 
| Opmerkingen                | Niet alle winkelmedewerkers houden de voorraad bij. Ze hebben wel allemaal het recht de voorraad in te zien. |   

## Behoeften van belanghebbenden	
### Zoeksysteem
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Belanghebbende             | Kunstenaars, winkelmedewerkers                                                                        |
| Prioriteit                 | Must                                                                                                  |
| Beschrijving               | Kunstenaars willen specifieke producten kopen op basis van veel verschillende eigenschappen die ze kunnen hebben. Winkelmedewerkers zoeken naar producten met eigenschappen die hun klanten beschrijven.    |
| Huidige situatie           | Webshops hebben geen goed filtersysteem om hun producten te laten zien, waardoor ze veel aankopen mislopen. Ook kunnen winkelmedewerkers de gewenste producten van klanten niet altijd vinden.              |
| Oplossing                  | Het informatiesysteem legt alle eigenschappen van de producten vast en bied de mogelijkheid om op producten met specifieke eigenschappen te zoeken.     |   

### Framing service
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Belanghebbende             | Kunstenaars, framers                                                                                  |
| Prioriteit                 | Must                                                                                                  |
| Beschrijving               | De data die nodig is om een kunstwerk volgens de wens van een klant in te lijsten is veel. Framers willen dat deze data overzichtelijk is en ergens wordt opgeslagen.  |
| Huidige situatie           | Kunstenaars geven aan gebruik te willen maken van de framing service, vullen de benodigde data in op een formulier, en leveren hun kunstwerk af.                       |
| Oplossing                  | Het informatiesysteem legt alle data van de framing service vast en bied de mogelijkheid om nieuwe toe te voegen.                                                     |  

### Bijhouden van voorraad
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Belanghebbende             | Eigenaar, winkelmedewerkers                                                                           |
| Prioriteit                 | Must                                                                                                  |
| Beschrijving               | De voorraad van producten moet worden geregistreerd zodat de eigenaar en de medewerkers weten wanneer ze producten bij moeten bestellen. |
| Huidige situatie           | Voorraad wordt handmatig bijgehouden.                                                                                                    |
| Oplossing                  | Elk product in het informatiesysteem krijgt een kolom 'voorraad'. Deze wordt bij transacties geüpdated.                                  |   


### Zoeken welke producten onderdeel uitmaken van een set
|                            |                                                                                                       |
|:---------------------------|:------------------------------------------------------------------------------------------------------|
| Belanghebbende             | Kunstenaars, winkelmedewerkers                                                                        |
| Prioriteit                 | Must                                                                                                  |
| Beschrijving               | Kunstenaars willen sets kopen die bepaalde producten bevatten.                                        |
| Huidige situatie           | Welke producten onderdeel uitmaken van een set wordt niet geregistreerd en er is geen mogelijkheid om naar sets te zoeken die een specifiek product hebben.    |
| Oplossing                  | Het informatiesysteem houd bij welke producten deel uit maken van welke sets en heeft een mogelijkheid om naar sets te zoeken die specifieke producten hebben. |

# Productperspectief	
Het product wat wordt opgeleverd is een informatiesysteem, die doorzocht kan worden met verschillende criteria.
Het zoeken van producten op deze criteria is het hoofdcomponent van dit product. Met behulp van het filteren kan er veel specifieker worden gezocht op producten in de webwinkel.  
De filtercriteria worden toegevoegd aan de zoekopdracht om zo andere producten terug te krijgen ten opzichten van 
de originele zoekopdracht.  
Het filtersysteem gaat klanten helpen om hun gewenste product(en) snel te vinden.  

Het informatiesysteem is een losstaand product die niet afhankelijk is van andere systemen.

# Eisen
## Functionele eisen  
In dit hoofdstuk worden functionele eisen beschreven die niet door een use-case worden afgedekt omdat er geen proces is.  
De eisen zijn geprioriteerd d.m.v. de MoSCoW methode.
M: Must have
S: Should have
C: Could have
W: Won't have.

| Functionele eis             | Beschrijving                                                                 | Prioriteit  |
|:----------------------------|:-----------------------------------------------------------------------------|:------------|
| FR1 Toegang beheerder       | Beheerder heeft volledig toegang tot alle functionaliteiten van het systeem. | Must have   |
| FR2 Zoeken naar producten   | Klanten en medewerkers moeten kunnen zoeken naar producten in het systeem.   | Must have   |
| FR3 Inlijstverzoek aanmaken | Medewerkers moeten inlijstverzoeken aanmaken die inlijsters kunnen inlezen.  | Should have |


### Overige 

| Eis                                       | Beschrijving                                                                                                                                    | Prioriteit  |
|:------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------|:------------|
| E1 Winkelmandje                           | Een systeem waarin klanten producten kunnen toevoegen in een winkelmandje om die tegelijk af te kunnen rekenen.                                 | Won't have  |
| E2 Puntenspaarsysteem                     | Een systeem waarin klanten door bijvoorbeeld aankopen punten kunnen sparen.                                                                     | Won't have  |
| E3 Kortingen                              | Tijdelijke kortingen om de prijs van een product te verlagen.                                                                                   | Could have  |  


## Niet-functionele eisen
Hier worden alle niet-functionele requirements beschreven waar de teamleden en het systeem zich aan moet houden.

### Prestatie

| Code               | Beschrijving                                                                               | Prioriteit  |
|:-------------------|:-------------------------------------------------------------------------------------------|:------------|
| NFR1               | Het informatiesysteem moet duizenden gebruikers op de website tegelijk kunnen onderhouden. | should have |

### Bruikbaarheid

| Code               | Beschrijving                                        | Prioriteit |
|:-------------------|:----------------------------------------------------|:-----------|
| NFR2               | De zoekfunctie wordt geïmplementeerd in mongodb.    | must have  |

### Kwaliteit

| Code               | Beschrijving                                                                                                  | Prioriteit  |
|:-------------------|:--------------------------------------------------------------------------------------------------------------|:------------|
| NFR3               | De mongodb database waar in wordt gezocht naar producten moet op elk moment overeenkomen met de SQL database. | should have |

### Onderhoudbaarheid

| Code               | Beschrijving                                                                     | Prioriteit  |
|:-------------------|:---------------------------------------------------------------------------------|:------------|
| NFR4               | Foutafhandeling zorgt ervoor dat er geen foutieve data in de database kan komen. | should have |

### Veiligheid

| Code               | Beschrijving                                                                        | Prioriteit  |
|:-------------------|:------------------------------------------------------------------------------------|:------------|
| NFR5               | Klanten kunnen geen producten toevoegen.                                            | must have   |
| NFR6               | Geschiedenis tabellen tonen aan wie welke aanpassingen heeft gedaan in de database. | should have |


## Interne kwaliteitseisen
| Code              | Beschrijving                                                                                               | Prioriteit |
|:------------------|:-----------------------------------------------------------------------------------------------------------|:-----------|
| KE1               | Alle documenten gebruiken dezelfde stijl(lettertype, voorblad, kopjes etc.)                                | should have |
| KE2               | In Sonar cloud bevinden zich minder dan 4 code smells.                                                     | should have |
| KE3               | Alle code wordt in camelcase geschreven.                                                                   | should have |
| KE4               | Het informatiesysteem moet de beschreven constraints geïmplementeerd hebben om foutieve data te voorkomen. | must have   |

#### Toelichting
KE2: Hier is gekozen voor 4 code smells. Dit is omdat code smells aangeven waar mogelijke onhandigheden in de code zitten. Omdat er zo min mogelijk onhandigheden in moeten zitten hebben wij gekozen voor een 
klein aantal getolereerde code smells namelijk 4.

## Documentatie requirements  

| Document              | Hoofdverantwoordelijke           |
|:----------------------|:---------------------------------|
| Plan van aanpak       | Teamleider                       |              
| Functioneel ontwerp   | Informatieanalist                |              
| Technisch ontwerp     | Softwarearchitect                |              
| Testrapport           | Tester                           |              

# Bronnenlijst
1. HAN. (2020). Controlekaart AIM documenten 2020 (1ste editie, Vol. 2020). Arnhem, Nederland: HAN.
2. RUP op Maat. (z.d.). Geraadpleegd op 13 april 2021, van https://rupopmaat.nl
