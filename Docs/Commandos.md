Alle commando's moeten vanaf de **Docs** map uitgevoerd worden op de terminal.  

#Projectplan genereren
```
pandoc Projectplan/Projectplan.md -o Projectplan/Projectplan_Groep_A4.docx --defaults defaults.yaml
```  


#FO Genereren
```
pandoc FO/FO.md -o FO/FO_Groep_A4.docx --defaults defaults.yaml
pandoc FO/FactAnalyse.md -o FO/FactAnalyse.docx --defaults defaults.yaml
```  


#TO Genereren
```
pandoc TO/TO.md -o TO/TO_Groep_A4.docx --defaults defaults.yaml
```   
  
# Iteratieplan
```
pandoc Iteratieplan/Iteratieplan.md -o Iteratieplan/Iteratieplan_Groep_A4.docx --defaults defaults.yaml
```

# Architectureel Prototype
```
pandoc "Prototype/Architectureel Prototype.md" -o "Prototype/Architectureel Prototype.docx" --defaults defaults.yaml
```

# Testplan
```
pandoc "Testplan/Testplan.md" -o "Testplan/Testplan_Groep_A4.docx" --defaults defaults.yaml
```

# Testrapport
```
pandoc "Testrapport/Testrapport.md" -o "Testrapport/Testrapport_Groep_A4.docx" --defaults defaults.yaml
```